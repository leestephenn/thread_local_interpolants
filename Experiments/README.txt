First, make sure that all your environment variables are set. The easiest way to do this is to make a file which you then ``source`` in. Mine looks something like the following: you will need to change it for your setup.


  export JAVA_HOME=/usr/lib/jvm/java-8-oracle/jre/lib/ ; location of rt.jar
  export LD_LIBRARY_PATH= <location of libz3>
  export CLASSPATH=<path to repository>/lib/soot-trunk.jar:<path to repository>/lib/jgrapht-ext-0.9.1-uber.jar:<path to libz3>/com.microsoft.z3.jar:$CLASSPATH

Then cd to the code_hypergraph/classes directory in the repository. Run
  java edu.gatech.threadlocalitps.InterprocMain ../../Experiments/<subdirectory> <method name>

For example, that last one might be
  java edu.gatech.threadlocalitps.InterprocMain -deadlock ../../Experiments/lockBasedFriendlyTreeMap/ remove

Before you do that, you will need to compile (with java) the experiment library you are using. Change the names of the files back from *.java$ to *.java. (There is a neat tool called mmv that can do this quickly and I have found useful for many things.) Then compile them, which should be as simple as
  javac -d . *.java
in each folder, but no promises. Then, rename all the .java files back to .java%$ so that they do not confuse Soot.
