# thread_local_interpolants

This repository holds the code that I wrote for my summer research position at Georgia Tech in the summer of 2016. 
It implements a model checker for concurrent Java programs.
I am posting it as a sample of a fairly large project that I did, but it will probably not work if you try to run it. In any 
case, installing the dependencies to build it is  non-trivial, even though they are in the lib/ directory.

Most of the code I wrote is in the subdirectory code_hypergraph/src/edu/gatech/threadlocalitps/ . Also, most of my code
commits are before the commit tagged 'last-summer-commit'.