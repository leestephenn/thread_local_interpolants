public class Absolute {
   public static int absolute(int x) {
       if (x < 0) {
	   x = -x;
       }
       return x;
   }
   public static int max(int x, int y) {
       int result = x;
       if (x < y) {
	   result = y;
       }
       return result;
   }
}  
