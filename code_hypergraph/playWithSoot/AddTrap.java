import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
//import soot.Scene;
import soot.SootMethod;
import soot.SootClass;
import soot.Unit;
import soot.Scene;
import soot.Type;
import soot.jimple.Stmt;
import soot.Trap;
import soot.Local;
import soot.Value;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.ReturnVoidStmt;
import soot.RefType;
import soot.jimple.Jimple;
import java.util.Map;

public class AddTrap extends BodyTransformer { 
    private AddTrap() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	Jimple j = Jimple.v();

	int numLocals = body.getLocalCount();
	//TODO: if I wanted to enable catching several exceptions, the way to 
	//do it would be to have each handler have an IdentityStmt succeeded by
	//a goto to the one error function call, which would would be followed 
	//by the return;
	RefType errorType = RefType.v("java.lang.ArithmeticException");
	Local arg = j.newLocal("ex" + (numLocals + 1), errorType);
	body.getLocals().add(arg);
	Unit id = j.newIdentityStmt(arg, j.newCaughtExceptionRef());

	SootMethod err = 
	    Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	InvokeExpr inv = j.newStaticInvokeExpr(err.makeRef());
	Unit errCall = j.newInvokeStmt(inv);

	Stmt ret = defaultReturn(body.getMethod().getReturnType());

	SootClass exc = errorType.getSootClass();
	Unit first = body.getUnits().getFirst();
	Unit last = body.getUnits().getLast();

	body.getUnits().add(id);
	body.getUnits().add(errCall);
	body.getUnits().add(ret);

	Trap t = j.newTrap(exc, first, last, id);
	body.getTraps().add(t);
    }

    public Stmt defaultReturn(Type t) {
	if(t instanceof soot.VoidType) {
	    return Jimple.v().newReturnVoidStmt();
	}
	Value ret = null;
	if(t instanceof soot.RefLikeType) {
	    ret = soot.jimple.NullConstant.v();
	}
	if(t instanceof soot.IntegerType) {
	    ret = soot.jimple.IntConstant.v(0);
	}
	if(t instanceof soot.LongType) {
	    ret = soot.jimple.LongConstant.v(0);
	}
	if(t instanceof soot.DoubleType) {
	    ret = soot.jimple.DoubleConstant.v(0.0);
	}
	if(t instanceof soot.FloatType) {
	    ret = soot.jimple.FloatConstant.v(0.0f);
	}
	return Jimple.v().newReturnStmt(ret);
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new AddTrap()));
	String[] sootArgs = {"-pp", "-ire", "-cp", ".", "Floats"};
	Scene.v().addBasicClass("ThreadItpsAnalysisError", SootClass.SIGNATURES);
	soot.Main.main(sootArgs);
    }
}
