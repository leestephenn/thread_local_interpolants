public class ArrayTest {

    public static int arrayTest(int s) {
	int[] ints = new int[5];
	ints[0] = s;
	ints[1] = s + 89;
	ints[2] = s + 17;
	ints[3] = ints[1] - 36;//s + 53
	ints[4] = s + 2;

	boolean[] bools = {(s < 41), (s == 96)};

	boolean[] bools2 = bools;

	String[] args = {"This", "is", "not", "really", "parsed"};

	int i = (bools2[0] && bools2[1]) ? 1 : 9;
	//System.out.println(bools2[0] + " " + bools2[1]);

	int result = ints.length + bools.length + args.length + i;
	if(result != 21) {
	   ThreadItpsAnalysisError.analysisError();
	}
	return result;
    }
}
