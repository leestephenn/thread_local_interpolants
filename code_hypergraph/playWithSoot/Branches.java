public class Branches {
    public static void branchToEnd(int x) {
	if(x < 0) {
	    System.out.println(x + 17);
	    return;
	} else {
	    System.out.println(x);
	    return;
	}
    }

    public int branchNotFirst(int i) {
	int k = i * 2;
	if(k > 3) {
	    k = k * 7;
	}
	k = k + 23;
	return k;
    }
}
