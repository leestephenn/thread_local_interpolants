public class BranchingInstanceMethodCalls {
    private static int myStatic;

    private int myFirst;
    private int mySecond = 4;

    public int foo(int j) {
	myFirst = 2 * j;
	mySecond = 4;
	int i = bar(j);
	if(i != 4 * j) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return i;
    }

    public int bar(int i) {
	i = i * 2;
	if(i > 0) {
	    i = baz(i) - mySecond;
	}
	//i = i + myFirst;
	i = i + i;
	return i; 
    }

    public int baz(int i) {
	return i + mySecond;
    }
}
