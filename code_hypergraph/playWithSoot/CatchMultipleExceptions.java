public class CatchMultipleExceptions {
    public static int multiple(String s, int x) {
	try {
	    int j = 5/x;
	    int result = j + s.length();
	    return result;
	} catch (ArithmeticException ex) {
	    System.out.println("Arithmetic");
	    return 0;
	} catch (NullPointerException ex) {
	    System.out.println("Null!Null!Null!Null!");
	    return -1;
	}
    }
}
