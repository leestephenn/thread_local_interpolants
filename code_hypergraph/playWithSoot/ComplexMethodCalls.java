public class ComplexMethodCalls {
    private static int field;

    public static int foo(int j) {
	int k = 2 * j;
	int i = bar(k, j);
	if(i != 5 * j) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return i;
    }

    public static int single(int j) {
	int k = j + 2;
	if ( k - j != 2) {
	    ThreadItpsAnalysisError.analysisError();
	}
        return k;
    }

    public static int bar(int i, int j) {
	i = i * 2;
	i = baz(i) - 3;
	i = i + j;
	return i; 
    }

    public static int baz(int i) {
	return i + 3;
    }

    public static int other(int i) {
	i = i * 3;
	other2(i);
	i = i + 17;
	return i;
    }

    public static void other2(int i ) {
	field = i;
    }
}
