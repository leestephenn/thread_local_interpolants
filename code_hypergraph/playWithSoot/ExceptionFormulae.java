public class ExceptionFormulae {
    private int f;

    public int foo(ExceptionFormulae other) {
	if(this.f > 3) {
	    return other.f;
	} else {
	    return this.f * this.f;
	}
    }
}
