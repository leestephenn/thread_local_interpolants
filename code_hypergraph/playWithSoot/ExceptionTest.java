public class ExceptionTest {
    public static int testException(int i) {
	try {
	    i = i + 1;
	    int re = 1/i;//method(i);
	    i = re + 1;
	} catch (ArithmeticException ex) {
	    System.out.println(ex.getMessage());
	    //return 0;
	}
	return i;
    }

    public static int method(int i) {
	return 1/i;
    }

    public static int testThrow(int i) {
	/* Yet another absolute value */
	try {
	    if(i < 0) {
		throw new IllegalArgumentException("Hello");
	    }
	} catch(IllegalArgumentException ex) {
	    i = -i;
	}
	return i;
    }

    public static int testExceptionReturn(int i) {
	try {
	    i = i + 1;
	    int re = 1/i;//method(i);
	    i = re + 1;
	    return i;
	} catch (ArithmeticException ex) {
	    System.out.println(ex.getMessage());
	    return 0;
	}
	//return i;
    }

    public static int testUncaughtException(int i) {
	int j = 4 /i;
	return j;
    }
}
