import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Scene;
import soot.SootMethod;
import soot.SootClass;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.ReachableMethods;
import soot.jimple.toolkits.callgraph.Edge;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.jimple.InvokeStmt;
import soot.Value;
import soot.ValueBox;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.HashSet;

public class FindErrorNode extends BodyTransformer { 
    private FindErrorNode() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("===========Transforming===========");
	//System.out.println("\n" + body.getMethod().getSignature());
	//System.out.println(body.getMethod().getSubSignature());
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	System.out.println(errorIndic);

	/* ReachableMethods rm = Scene.v().getReachableMethods();
	System.out.println("Reachable methods size: " + rm.size());
	Iterator<soot.MethodOrMethodContext> it = rm.listener();

	HashSet<SootClass> classes = new HashSet<SootClass>();
	while(it.hasNext()) {
	    classes.add(it.next().method().getDeclaringClass());
	}
	System.out.println("Reachable classes: " + classes.size());
	for (SootClass clazz : classes) {
	    System.out.println(clazz);
	} */
	

	/* CallGraph cg = Scene.v().getCallGraph();
	System.out.println("Call graph size: " + cg.size());
	/* for( Edge e : cg) {
	    if(!e.src().isJavaLibraryMethod()) {
		System.out.println("Edge : " + e);
	    }
	}  
	Iterator<Edge> calls = cg.edgesInto(errorIndic);
	System.out.println("calls empty? " + !calls.hasNext());
	while(calls.hasNext()) {
	    System.out.print("Call from: ");
	    System.out.println(calls.next().src().getName());
	} */


	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		System.out.println(u);
		System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		System.out.println("Method: " + m);
		System.out.println("Equals error: " + (errorIndic.equals(m)));
		
		//System.out.println("Invoke in call graph? " + cg.edgesOutOf(u).hasNext());
	    }
	}
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	return null;
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new FindErrorNode()));
	String[] sootArgs = {"-pp", "-ire", "-cp", ".", "NoDivideByZeroTest"};
	soot.Main.main(sootArgs);
    }
}
