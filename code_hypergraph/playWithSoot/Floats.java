public class Floats {
    public static int testFloats(double d, float f, long l) {
	int result = 0;
	if(d < 0.5) {
	    result = 2;
	}
	if (d >= 1.99) {
	    result = 3;
	} 
	if (d > 9.0) {
	    result = 5;
	}
	if (d <= -1.0) {
	    result = 7;
	}
	if(f < 0.5f) {
	    result = 11;
	}
	if (f >= 1.99f) {
	    result = 13;
	} 
	if (f > 9.0f) {
	    result = 17;
	}
	if (f <= -1.0f) {
	    result = 19;
	}
	if (d == 3.14159f) {
	    result = 23;
	}
	if (l > 5281L) {
	    result = 29;
	}
	if (l <= 65537L) {
	    result = 31;
	}
	return result;
    }

    public static double testFloatNonBoolean(double d, double e) {
	return 2.0 * d + e;
    }

    public static boolean testFloatNoIf(double d) {
	boolean b = d > 0.0;
	return b;
    }
}
