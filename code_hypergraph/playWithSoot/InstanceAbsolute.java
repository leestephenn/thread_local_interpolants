public class InstanceAbsolute {
    private int x;
    private int y;

    public int reciprocal() {
	if (x < 1) {
	    x = 1 - x;
	}
	if(x == 0) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return 1/x;
    }

    public int reciprocalInterproc() {
	rPlus();
	if(x == 0) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return 1/x;
    }

    public void rPlus() {
	if (x < 1) {
	    x = 1 - x;
	}
    } 

    public int reciprocalNoTest() {
	if (x < 1) {
	    x = 1 - x;
	}
	return 1/x;
    }

    public int reciprocalException() {
	try {
	    if (x < 1) {
		x = 1 - x;
	    }
	    return 1/x;
	} catch (ArithmeticException ex) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return 0;
    }

    public int max() {
	int result = x;
	if (x < y) {
	    result = y;
	}
	if (result < x || result < y) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return result;
    }
}  
