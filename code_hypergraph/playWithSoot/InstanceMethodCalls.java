public class InstanceMethodCalls {
    private static int myStatic;

    private int myFirst;
    private int mySecond = 4;

    public int foo(int j) {
	myFirst = 2 * j;
	int i = bar(j);
	if(i != 4 * j) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return i;
    }

    public int bar(int i) {
	i = i * 2;
	i = baz(i) - mySecond;
	i = i + myFirst;
	return i; 
    }

    public int baz(int i) {
	return i + mySecond;
    }

    public int other(int i) {
	i = i * 3;
	other2(i);
	mySecond = i + 17;
	return i;
    }

    public void other2(int i ) {
	myStatic = i;
    }
}
