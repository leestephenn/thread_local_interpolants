public class Looping {
    /* I am not actually sure this computes \n.3*n : it may compute \n. 3*(n-1) or something,
     * but I do not care. */
    public int triple(int x) {
	int result = 0;
	while(x > 0) {
	    result = result + 3;
	}
	return result;
    }

    public int switching(int x) {
	int result = 0;
	switch (x) {
	    case 2:
		result = 1;
		break;
	    case 3:
		result = 2;
		break;
	    case 5:
		result = 3;
		break;
	    case 7:
		result = 4;
		break;
	}
	return result;
    }
}
