public class LoopingToo {
    private int n = 0;

    public int foo(int x) {
	x = x * 2;
	while(x > 0) {
	    if(x < 10) {
		n = x;
	    }
	    x--;
	}
	return n;
    }
}
