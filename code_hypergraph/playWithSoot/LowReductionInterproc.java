public class LowReductionInterproc {
    private static int balance;
    private static int lock;
    private int i = 0;


    public static void foo() {
	lock = 2;
	lock = 3;
	balance = bar(balance);
	lock = 5;
	lock = 7;
    }
 
    public static int bar(int x) {
	return lock + x;
    }
}
