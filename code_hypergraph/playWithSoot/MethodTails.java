public class MethodTails {
    private static int foo;

    public static void testVoid(int i) {
	if(i <= -17) {
	    return;
	}
	foo = 3 * i + 31;
	if(i < -107) {
	    throw new IllegalArgumentException("this");
	}
	foo = foo + 5;
	return;
    }
}
