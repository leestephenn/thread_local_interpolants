public class MultipleDefs {
   public static void main(String[] args) {
       int x = args.length;
       System.out.println(x);
       x = args.length > 3 ? 6 : 2;
       System.out.println(x);
       x = x + 2;
       System.out.println(x);
   }
}
