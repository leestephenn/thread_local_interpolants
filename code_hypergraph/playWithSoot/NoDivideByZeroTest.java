public class NoDivideByZeroTest {
   public static int reciprocal(int x) {
       if (x < 1) {
	   x = 1 - x;
       }
       if(x == 0) {
	   ThreadItpsAnalysisError.analysisError();
       }
       return 1/x;
   }

   public static int reciprocalInterproc(int x) {
       x = rPlus(x);
       if(x == 0) {
	   ThreadItpsAnalysisError.analysisError();
       }
       return 1/x;
   }

   public static int rPlus(int x) {
       if (x < 1) {
	   x = 1 - x;
       }
       return x;
   } 

   public static int reciprocalNoTest(int x) {
       if (x < 1) {
	   x = 1 - x;
       }
       return 1/x;
   }

   public static int reciprocalException(int x) {
       try {
	   if (x < 1) {
	       x = 1 - x;
	   }
	   return 1/x;
       } catch (ArithmeticException ex) {
	   ThreadItpsAnalysisError.analysisError();
       }
       return 0;
   }

   public static int max(int x, int y) {
       int result = x;
       if (x < y) {
	   result = y;
       }
       if (result < x || result < y) {
	   ThreadItpsAnalysisError.analysisError();
       }
       return result;
   }
}  
