public class ObjectCreation {

    public ObjectCreation(int x) {
	System.out.println(x);
    }

    public static void test(int x) {
	ObjectCreation oc = new ObjectCreation(x);
    }

    public static int testArrays(int x) {
	ObjectCreation oc = new ObjectCreation(x);

	int[] xs = new int[x];
	int[][][] cells = new int[x][x + 2][];
	int res = xs[0] + cells[0][0][0];
	/* int[][] lit = { {2, 3}, {5, 7}};

	cells = new int[x][];
	res += (cells[0] == null) ? 5 : 7; */
	return res;
    }
}
