public class ObjectParameters {
    private int apple;
    //private int banana;

    public static int foo(ObjectParameters other) {
	int old = other.apple;
	other.apple = other.apple + 3;
	if(other.apple != old + 3) {
	    ThreadItpsAnalysisError.analysisError();
	    old++;
	}
	old++;
	return old;
    }

    public static int twoParams(ObjectParameters first, ObjectParameters second) {
	if(first != second) {
	    int old = first.apple;
	    second.apple = 17;
	    if(first.apple != old) {
		ThreadItpsAnalysisError.analysisError();
	    }
	}
	return first.apple;
    }
}
