public class Recursion {
    public int recursive(int i) {
	if(foo(i)) {
	    return recursive(i - 1);
	} else {
	    return 11;
	}
    }

    public int twice(int i) {
	if(i > 0) {
	    return 2 + twice(i - 1);
	} else {
	    return i;
	}
    }

    public boolean foo(int i) {
	return i >= 0;
    }
}
