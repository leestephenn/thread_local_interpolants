public class Scoping {
    public static void main(String[] args) {
	if (args.length > 3) {
	    boolean b = (args[0] == "");
	    System.out.println(b);
	} else {
	    boolean b = (args.length == 0);
	    System.out.println(b);
	}
    }
}
