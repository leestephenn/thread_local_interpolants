import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.TableSwitchStmt;
import soot.jimple.FieldRef;
import soot.jimple.CaughtExceptionRef;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class ShowBoxes extends BodyTransformer {
    private ShowBoxes() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	//LinkedList<Unit> queue = new LinkedList<Unit>(g.getHeads());
	for(Unit u : g) {
	    //Unit u = queue.removeFirst();
	    //queue.addAll(g.getSuccsOf(u));
	    //System.out.println(u.getClass());
	    //System.out.println(u);

	    /* if (u instanceof TableSwitchStmt) {
		TableSwitchStmt s = (TableSwitchStmt) u;
		int base = s.getLowIndex();
		for(int i = base; i <= s.getHighIndex(); i++) {
		    System.out.println(s.getTarget(i - base));
		}
	    } */

	    List<ValueBox> boxes = u.getUseAndDefBoxes();//u.getUseAndDefBoxes();
	    for(ValueBox box : boxes) {
		Value val = box.getValue();
		//System.out.println(box);
		//System.out.println(box.getValue().getClass() + "\t" + box.getValue() + "\t" +
			//box.getValue().getType());
		//if (val instanceof CaughtExceptionRef) {
		    //CaughtExceptionRef e = (CaughtExceptionRef) val;
		    //

		if(val instanceof soot.jimple.BinopExpr) {
		    System.out.println(val.getClass());
		    System.out.println(val);
		}
		/* if(val instanceof soot.jimple.InvokeExpr) {
		    soot.jimple.InvokeExpr inv = (soot.jimple.InvokeExpr) val;
		    soot.SootClass clss = inv.getMethod().getDeclaringClass();
		    System.out.println(inv.getMethod().getSignature());
		    System.out.println(inv.getMethod().getSubSignature());
		    System.out.println(clss.getName());
		    System.out.println(clss.getShortName());
		    System.out.println(clss.getShortJavaStyleName());
		} */
		//}
	    }
	    /* LinkedList<Value> values = new LinkedList<Value>();
	    for(ValueBox b : boxes) {
		values.add(b.getValue());
	    } */

	    //System.out.println();
	}
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new ShowBoxes()));
	String[] sootArgs = {"-pp", "-cp", ".", "Floats"};

	soot.Main.main(sootArgs);
    }
}
