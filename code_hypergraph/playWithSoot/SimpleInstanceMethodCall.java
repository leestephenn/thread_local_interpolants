public class SimpleInstanceMethodCall {
    private int myFirst;

    public int foo(int j) {
	myFirst = 2 * j;
	int i = bar(j);
	if(i != 3 * j) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return i;
    }

    public int bar(int i) {
	i = i + myFirst;
	return i;
    }

    public int single(int j) {
	myFirst = j + 2;
	if ( myFirst - j != 2) {
	    ThreadItpsAnalysisError.analysisError();
	}
        return myFirst;
    }

    public int other(int i) {
	i = i * 3;
	i = i + myFirst;
	return i;
    }

}
