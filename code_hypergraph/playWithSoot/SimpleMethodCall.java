public class SimpleMethodCall {

    public static int foo(int j) {
	int k = 2 * j;
	int i = bar(k, j);
	if(i != 3 * j) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return i;
    }

    public static int fooStraight(int j) {
	int k = 2 * j;
	int i = bar(k, j);
	return i;
    }

    public static int single(int j) {
	int k = j + 2;
	if ( k - j != 2) {
	    ThreadItpsAnalysisError.analysisError();
	}
        return k;
    }

    public static int bar(int i, int j) {
	//i = i * 2;
	i = i + j;
	return i; 
    }

    public static int other(int i) {
	i = i * 3;
	i = i + 17;
	return i;
    }

}
