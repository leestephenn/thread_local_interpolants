import java.util.concurrent.locks.ReentrantLock;

public class SimpleRaceCondition {
    private int field;
    private static ReentrantLock lock;


    public void erroneous() {
	field = field + 7;
    }

    public void safe() {
	ReentrantLock lk = lock;
	lk.lock();
	field = field + 5;
	lk.unlock();
    }
}
