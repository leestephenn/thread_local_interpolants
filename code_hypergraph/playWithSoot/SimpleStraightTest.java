public class SimpleStraightTest {
    private int x;

    public int foo(int i) {
	i = i * 2;
	x = i;
	return x;
    }

    public int test(int i) {
	x = 2 * i;
	x = x - i;
	if(x != i) {
	    ThreadItpsAnalysisError.analysisError();
	}
	return x;
    }
}
