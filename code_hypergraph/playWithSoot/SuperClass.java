class SuperClass {
    private int i;
    

    public int getI() {
	return i;
    }

    public int foo(int j) {
	return j + 2;
    }
    
    public int bar() {
	return this.getI() + 31;
    }

    public static void main(SuperClass a) {
	int i = a.foo(3);
	i = i + a.getI();
	SuperClass b = new SubclassB();
	i = b.bar();
	System.out.println(i);
    }
}

class SubclassA extends SuperClass {

    public int getI() {
	return super.getI() + 3;
    }
}

class SubclassB extends SuperClass {

    public int getI() {
	return 17;
    }
}
