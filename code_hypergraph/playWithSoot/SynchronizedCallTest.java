public class SynchronizedCallTest {
    public int foo(int i) {
        int ret = i * 2;
        ret = mySynch(ret);
        return ret + 17;
    }

    public synchronized int mySynch(int i) {
        return i * -5;
    }
}
