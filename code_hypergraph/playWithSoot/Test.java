import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Local;
import java.util.Map;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import soot.UnitBox;
import soot.ValueBox;
import soot.Value;
import soot.SootClass;

class MyTransformer extends BodyTransformer {

    protected void internalTransform(Body body, 
            String phaseName, Map<String, String> optionsMap) {

        //ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	for(Local l : body.getLocals()) {
	    if(l.getType() instanceof soot.RefType) {
		testSubclasses((soot.RefType) l.getType());
	    }
	}
    }

    private void testSuperclass(soot.RefType t) {
	System.out.println("Super of " + t);
	soot.SootClass c = t.getSootClass();
	java.util.ArrayDeque<SootClass> stack = new java.util.ArrayDeque<SootClass>();
	stack.push(c);
	java.util.HashSet<SootClass> visited = new HashSet<SootClass>();
	while (!stack.isEmpty()) {
	    c = stack.pop();
	    System.out.println(c);
	    if(c.hasSuperclass()) {
		SootClass s = c.getSuperclass();
		if(visited.add(s)) {
		    stack.push(s);
		}
	    } 
	    for(SootClass i : c.getInterfaces()) {
		if(visited.add(i)) {
		    stack.push(i);
		}
	    }
	}
    }

    public void testSubclasses(soot.RefType t) {
	System.out.println("Sub of " + t);
	soot.SootClass c = t.getSootClass();
	soot.Scene scene = soot.Scene.v();
	soot.FastHierarchy h = scene.getFastHierarchy();

	java.util.ArrayDeque<SootClass> stack = new java.util.ArrayDeque<SootClass>();
	stack.push(c);
	java.util.HashSet<SootClass> visited = new HashSet<SootClass>();
	visited.add(c);
	while(!stack.isEmpty()) {
	    c = stack.pop();
	    if(c.isConcrete()) {
		System.out.println(c);
		System.out.println(c.resolvingLevel());
	    }
	    if(c.isInterface()) {
		for(SootClass sub : h.getAllImplementersOfInterface(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
		for(SootClass sub : h.getAllSubinterfaces(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
	    } else {
		for(SootClass sub : h.getSubclassesOf(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
	    }
	}
    }

    private void testClone(Body b) {
	Body clone = (Body) b.clone();
	
	HashSet<Value> orig = new HashSet<Value>();
	HashSet<Value> newVals = new HashSet<Value>();
	HashSet<Value> check = new HashSet<Value>();

	
	for(ValueBox vb : b.getUseAndDefBoxes()) {
	    orig.add(vb.getValue());
	    check.add(vb.getValue());
	}
	
	for(ValueBox vb : clone.getUseAndDefBoxes()) {
	    boolean inNew, inCheck;
	    inNew = newVals.add(vb.getValue());
	    inCheck = check.add(vb.getValue());
	    if(inNew != inCheck) {
		System.out.println("Value " + vb.getValue() + " not cloned.");
	    }
	}

	System.out.println("orig, new, orig + new: " + orig.size() + ", " + 
		newVals.size() + ", " + (orig.size() + newVals.size()));
	System.out.println("check size: " + check.size());
    }

    private void graphBoxes(ExceptionalUnitGraph g) {
        for(Unit u : g) {
            List<UnitBox> boxes = u.getUnitBoxes();

            if(!boxes.isEmpty()) {
                LinkedList<Unit> units = new LinkedList<Unit>();
                for(UnitBox box : boxes) {
                    units.add(box.getUnit());
                }

                System.out.println("Contains? " + g.getSuccsOf(u).contains(units));
                System.out.println(u);
                System.out.println("Successors:");
                for(Unit s: g.getSuccsOf(u)) {
                    System.out.println(s);
                }
                System.out.println("Boxed Units:");
                for(Unit b : units) {
                    System.out.println(b);
                }

                System.out.println("\n");
            }
        }
    }

    private void backwardBFS(ExceptionalUnitGraph g) {
        LinkedList<Unit> queue = new LinkedList<Unit>(g.getTails());
        ArrayList<Unit> visited = new ArrayList<Unit>();
        while(!queue.isEmpty()) {
            Unit u = queue.removeFirst();
            System.out.println("Node: " + u);
            visited.add(u);

            List<Unit> preds = g.getUnexceptionalPredsOf(u);
            for(Unit pred : preds) {
                System.out.println(pred.getClass() + " " + pred);
                if(!visited.contains(pred)) {
                    queue.add(pred);
                }
            }
            //System.out.println(u.hashCode() % 26);
            System.out.println();
        }
    }
}

public class Test {

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        /* java.util.Collection<Pack> packs = PackManager.v().allPacks();
           for (Pack p : packs) {
           if(p.getDefaultOptions().contains("enabled:true")) {
           System.out.println(p.getPhaseName());
           }
           } */
        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new MyTransformer()));
        String[] sootArgs = {"-pp", "-cp", ".", "UsesSuperClasses"};
        soot.Main.main(sootArgs);
    }
} 
