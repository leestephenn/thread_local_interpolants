import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.IfStmt;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class TestBooleans extends BodyTransformer { 
    private TestBooleans() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	for (Unit u : g) {
	    System.out.println(u.getClass());
	    System.out.println(u);

	    //
	    if(u instanceof IfStmt) {
		List<ValueBox> boxes = u.getUseAndDefBoxes();
		for(ValueBox box : boxes) {
		    System.out.println(box);
		    System.out.println("Value:" + box.getValue().getClass() + "\t" + box.getValue());
		    System.out.println("Type: " + box.getValue().getType());
		}

	    }
	    System.out.println();
	}
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestBooleans()));
	String[] sootArgs = {"-pp", "-cp", ".", "BoolVarIf"};
	soot.Main.main(sootArgs);
    }
}
