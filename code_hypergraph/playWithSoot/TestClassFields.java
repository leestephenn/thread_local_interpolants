import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Scene;
import soot.SootClass;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.InvokeExpr;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class TestClassFields extends BodyTransformer { 
    private TestClassFields() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");
	
	Scene scene = Scene.v();
	soot.Type sc = scene.getType("int");
	//SootClass sc = scene.getSootClass("java.util.ArrayList");
	if(body.getMethod().getName().equals("hashCode")) {
	    System.out.println("Equals? " + sc.equals(body.getMethod().getReturnType()));
	}

	System.out.println();
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestClassFields()));
	String[] sootArgs = { /* "-w", */ "-ire", "-pp", "-cp", ".", "Super"};
	soot.Main.main(sootArgs);
    }
}
