import java.util.concurrent.locks.ReentrantLock;
/* import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock; */

public class TestDeadlock {
    private static int balance;
    private static ReentrantLock lockContext1;
    private static ReentrantLock lockContext2;
    private ReentrantLock lock = new ReentrantLock();
    private int i = 0;


    public static void transfer(ReentrantLock lock1, ReentrantLock lock2) {
	lock1.lock();
	lock2.lock();
	balance = balance - 13;
	lock2.unlock();
	lock1.unlock();
    }

    public static void noArgTransfer1() {
	ReentrantLock lock1 = lockContext1;
	ReentrantLock lock2 = lockContext2;
	lock1.lock();
	lock2.lock();
	balance = balance - 13;
	lock2.unlock();
	lock1.unlock();
    }

    public static void noArgTransfer2() {
	ReentrantLock lock1 = lockContext2;
	ReentrantLock lock2 = lockContext1;
	lock1.lock();
	lock2.lock();
	balance = balance - 13;
	lock2.unlock();
	lock1.unlock();
    }

    public static int more(ReentrantLock lock) {
	int i = 3;
	lock.lock();
	i = i + 1;
	//lock.unlock();
	return i;
    }
}
