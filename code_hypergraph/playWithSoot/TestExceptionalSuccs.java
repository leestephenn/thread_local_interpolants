import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.ExceptionalUnitGraph.ExceptionDest;
import soot.toolkits.exceptions.UnitThrowAnalysis;
import soot.Unit;
import java.util.Map;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import soot.UnitBox;

public class TestExceptionalSuccs extends BodyTransformer {
    private TestExceptionalSuccs() {
    }

    protected void internalTransform(Body body, 
            String phaseName, Map<String, String> optionsMap) {

	String methodName = body.getMethod().getName();
	System.out.println("====================Transforming " + methodName + 
		"====================");
        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body, UnitThrowAnalysis.interproc());
        if(body.getMethod().isStatic()) {
            System.out.println("Graph:");
            anyExceptionalSuccs(g);
	    //printDests(g);
        }
    }

    private void backwardBFS(ExceptionalUnitGraph g) {
        LinkedList<Unit> queue = new LinkedList<Unit>(g.getTails());
        ArrayList<Unit> visited = new ArrayList<Unit>();
        while(!queue.isEmpty()) {
            Unit u = queue.removeFirst();
            System.out.println("Node: " + u);
            visited.add(u);

            List<Unit> preds = g.getUnexceptionalPredsOf(u);
            for(Unit pred : preds) {
                System.out.println(pred.getClass() + " " + pred);
                if(!visited.contains(pred)) {
                    queue.add(pred);
                }
            }
            //System.out.println(u.hashCode() % 26);
            System.out.println();
        }
    }

    private void anyExceptionalSuccs(ExceptionalUnitGraph g) {
	for(Unit u : g) {
	    List<Unit> succs = g.getExceptionalPredsOf(u);
	    if(!succs.isEmpty()) {
		System.out.println("Unit: ");
		System.out.println(u);
		System.out.println("had " + g.getUnexceptionalPredsOf(u).size() + " preds and ");
		System.out.println("Had exceptional predecessors: ");
		for(Unit s : succs) {
		    System.out.println(s);
		}
	    }
	    Collection<ExceptionDest> dests = g.getExceptionDests(u);
	    if(!dests.isEmpty()) {
		System.out.println(u + " had " + dests.size() + " dests");
		for(ExceptionDest dest: dests) {
		    if(dest.getTrap() != null) {
			System.out.println(dest);
			//System.out.println("Trap: " + dest.getTrap());
			System.out.println();
		    }
		}
		System.out.println("-----");
	    }
	}
    }

    public void printDests(ExceptionalUnitGraph g) {
	for(Unit u : g) {
	    Collection<ExceptionDest> dests = g.getExceptionDests(u);
	    for(ExceptionDest d : dests) {
		System.out.println(d);
		System.out.println(d.getTrap());
		System.out.println();
	    }
	}
    }

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        /* java.util.Collection<Pack> packs = PackManager.v().allPacks();
           for (Pack p : packs) {
           if(p.getDefaultOptions().contains("enabled:true")) {
           System.out.println(p.getPhaseName());
           }
           } */
        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new TestExceptionalSuccs()));
        String[] sootArgs = {"-pp", "-cp", ".", "ExceptionTest"};
        soot.Main.main(sootArgs);
    }
} 
