import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.SootField;
import soot.jimple.FieldRef;
import soot.jimple.TableSwitchStmt;
import soot.jimple.FieldRef;
import soot.jimple.CaughtExceptionRef;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.HashSet;

public class TestFieldClone extends BodyTransformer { 
    private TestFieldClone() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	Body clone = (Body) body.clone();

	HashSet<SootField> fields = new HashSet<SootField>();
	//System.out.println("Graph:");

	//LinkedList<Unit> queue = new LinkedList<Unit>(g.getHeads());
	for(Unit u : body.getUnits()) {

	    List<ValueBox> boxes = u.getUseAndDefBoxes();//u.getUseAndDefBoxes();
	    for(ValueBox box : boxes) {
		Value val = box.getValue();

		if(val instanceof FieldRef) {
		    FieldRef f = (FieldRef) val;
		    fields.add(f.getField());
		}
	    }
	}

	System.out.println("First size: " + fields.size());

	for(Unit u : clone.getUnits()) {

	    List<ValueBox> boxes = u.getUseAndDefBoxes();//u.getUseAndDefBoxes();
	    for(ValueBox box : boxes) {
		Value val = box.getValue();

		if(val instanceof FieldRef) {
		    FieldRef f = (FieldRef) val;
		    fields.add(f.getField());
		}
	    }
	}
	System.out.println("New size: " + fields.size());
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestFieldClone()));
	String[] sootArgs = {"-pp", "-cp", ".", "Field"};

	soot.Main.main(sootArgs);
    }
}
