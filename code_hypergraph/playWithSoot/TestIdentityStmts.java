import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.IdentityStmt;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class TestIdentityStmts extends BodyTransformer {
    private TestIdentityStmts() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	//System.out.println("\n" + body.getMethod().getName());
	//System.out.println("Graph:");

	for (Unit u : body.getUnits()) {
	    if(u instanceof IdentityStmt) {
		IdentityStmt is = (IdentityStmt) u;
		Value left = is.getLeftOp();
		Value right = is.getRightOp();

		if(right.getType() instanceof soot.RefType) {
		    System.out.println("Reftype " + right);
		}

		if( !left.getType().equals(right.getType())) {
		    System.out.println(left.getType() + " != " + right.getType());
		}

	    }
	}
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestIdentityStmts()));
	String[] sootArgs = {"-pp", "-cp", ".", "--process-dir", "classes", "-d", "./sootOutput"};
	soot.Main.main(sootArgs);
    }
}
