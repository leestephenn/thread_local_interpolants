import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

public class TestLock {
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public int foo(int i) {
	i = i + 2;
	ReadLock rl = (ReadLock) lock.readLock();
	rl.lock();
	//ThreadLocal<String> tl = new ThreadLocal<String>();
	//String s = tl.get();
	i = i * 3;
	rl.unlock();
	i = i + 17;
	lock.writeLock().lock();
	i = i - 31;
	lock.writeLock().unlock();
	ThreadItpsAnalysisError.analysisError();
	return i;
    }

    public static void single(ReentrantLock lock) {
	//ReentrantLock lock = new ReentrantLock();
	lock.lock();
    }
}
