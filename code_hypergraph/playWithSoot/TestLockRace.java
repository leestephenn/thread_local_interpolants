import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

public class TestLockRace {
    private ReentrantLock lock = new ReentrantLock();
    private int i = 0;

    public int foo() {
	i = i + 2;
	lock.lock();
	i = i * 3;
	lock.unlock();
	i = i + 17;
	return i;
    }

    public int simple() {
	ReentrantLock lk = lock;
	lk.lock();
	int tmp = i;
	i = tmp + 2;
	lk.unlock();
	return tmp;
    }
}
