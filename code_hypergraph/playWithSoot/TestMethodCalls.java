import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.InvokeExpr;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class TestMethodCalls extends BodyTransformer { 
    private TestMethodCalls() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	for (Unit u : g) {
	    //System.out.println(u.getClass());
	    //System.out.println(u);

	    //
	    List<ValueBox> boxes = u.getUseAndDefBoxes();
	    for(ValueBox box : boxes) {
		Value val = box.getValue();
		if(box.getValue() instanceof InvokeExpr) {
		    InvokeExpr inv = (InvokeExpr) val;
		    System.out.println(inv);
		    System.out.println("Method " + inv.getMethod());
		    System.out.println("MethodRef " + inv.getMethodRef());
		    System.out.println("Has body? " + inv.getMethod().hasActiveBody());
		    System.out.println("Body " + inv.getMethod().getActiveBody());

		}
	    }

	    System.out.println();
	}
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestMethodCalls()));
	String[] sootArgs = { "-w",  "-ire", "-pp", "-cp", ".", "Super"};
	soot.Main.main(sootArgs);
    }
}
