import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.SootMethodRef;
import soot.SootMethod;
import soot.jimple.InvokeExpr;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TestMethodRef extends BodyTransformer { 
    HashMap<String, SootMethod> refs = new HashMap<String, SootMethod>();

    private TestMethodRef() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	SootMethod method = body.getMethod();
	put(method.getName(), method);
	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	for (Unit u : g) {
	    //System.out.println(u.getClass());
	    //System.out.println(u);

	    //
	    List<ValueBox> boxes = u.getUseAndDefBoxes();
	    for(ValueBox box : boxes) {
		Value val = box.getValue();
		if(box.getValue() instanceof InvokeExpr) {
		    InvokeExpr inv = (InvokeExpr) val;
		    System.out.println(inv);
		    SootMethod ref = inv.getMethod();
		    System.out.println("Method " + ref);
		    put(ref.getName(), ref);

		    //System.out.println("Method " + inv.getMethod());//this just calls resolve() internally
		    //System.out.println("Resolved " + inv.getMethodRef().resolve());
		    //System.out.println("Has body? " + inv.getMethod().hasActiveBody());
		    System.out.println();

		}
	    }

	}
    }

    public void put(String key, SootMethod ref) {
	SootMethod old = refs.put(key, ref);
	if(old != null && !old.equals(ref)) {
	    System.out.println("Two refs for same name were not equal.");
	    System.out.println(old + ", " + ref);
	    System.out.println();
	}

    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestMethodRef()));
	String[] sootArgs = {/* "-w", */ "-ire", "-pp", "-cp", "/home/leesn/Classes/WH/thread-local-itps/Experiments/lockBasedFriendlyTreeMap", "trees.lockbased.LockBasedFriendlyTreeMap"};
	soot.Main.main(sootArgs);
    }
}
