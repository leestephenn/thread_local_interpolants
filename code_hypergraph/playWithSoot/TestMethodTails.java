import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.IfStmt;
import soot.jimple.ThrowStmt;
import java.util.Map;
import java.util.LinkedList;
import java.util.List;

public class TestMethodTails extends BodyTransformer { 
    private TestMethodTails() {
    }

    protected void internalTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	if(g.getHeads().size() != 1) {
	    System.out.println(body.getMethod().getName() + " had more than 1 head: ");
	    System.out.println(g.getHeads());
	}
    }

    protected void tailTransform(Body body, 
	    String phaseName, Map<String, String> optionsMap) {

	System.out.println("\n" + body.getMethod().getName());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//System.out.println("Graph:");

	for (Unit u : g.getTails()) {
	    System.out.println(u.getClass());
	    System.out.println(u);

	    System.out.println();
	}
	for (Unit u : g) {
	    if(u instanceof ThrowStmt) {
		System.out.println("Throw successors: ");
		System.out.println(g.getSuccsOf(u));
	    }
	}
    }


    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestMethodTails()));
	String[] sootArgs = {"-pp", "-cp", ".", "-process-dir", 
	    "Absolute",
	    "ArrayTest",
	    "BoolVarIf",
	    "BothSides",
	    "Branches",
	    "CatchMultipleExceptions",
	    "ClassCast",
	    "ComplexMethodCalls",
	    "ExceptionTest",
	    "Field",
	    "Floats",
	    "HelloWorld",
	    "InstanceMethodCalls",
	    "InstanceMethod",
	    "Looping",
	    "MethodCalls",
	    "MultipleDefs",
	    "NoDivideByZeroTest",
	    "ObjectCreation",
	    "Scoping",
	    "SimpleArrayTest",
	    "SimpleMethodCall",
	    "ThreadItpsAnalysisError"
	};
	soot.Main.main(sootArgs);
    }
}
