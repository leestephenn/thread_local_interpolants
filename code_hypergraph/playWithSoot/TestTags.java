import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import java.util.Map;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import soot.UnitBox;
import soot.ValueBox;
import soot.Value;

public class TestTags extends BodyTransformer {

    private TestTags() {
    }

    protected void internalTransform(Body body, 
            String phaseName, Map<String, String> optionsMap) {

	testClone(body);
        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
        if(body.getMethod().getName().equals("switching")) {
            //System.out.println("Graph:");
            //backwardBFS(g);
        }
    }

    private void testClone(Body b) {
	HashSet<soot.tagkit.Tag> tags = new HashSet<soot.tagkit.Tag>();
	
	int c = 0;
	for(Unit u : b.getUnits()) {
	    u.addTag(new MyTag(c++));
	    tags.addAll(u.getTags());
	}
	System.out.println("Number of tags: " + tags.size());

	Body copy = (Body) b.clone();
	for(Unit u : copy.getUnits()) {
	    System.out.println(u.getTags());
	    tags.addAll(u.getTags());
	    System.out.println();
	}
	System.out.println("New Number of tags: " + tags.size());
    }

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        /* java.util.Collection<Pack> packs = PackManager.v().allPacks();
           for (Pack p : packs) {
           if(p.getDefaultOptions().contains("enabled:true")) {
           System.out.println(p.getPhaseName());
           }
           } */
        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new TestTags()));
        String[] sootArgs = {"-pp", "-cp", ".", "Field"};
        soot.Main.main(sootArgs);
    }
}

class MyTag implements soot.tagkit.Tag {
    private final int i;

    public MyTag(int i) {
	this.i = i;
    }

    public String getName() {
	return "Hello " + i;
    }

    public byte[] getValue() {
	return new byte[]{(byte) i};
    }

    public String toString() {
	return getName();
    }
}
