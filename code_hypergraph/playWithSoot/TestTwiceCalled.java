import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import java.util.Map;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import soot.UnitBox;
import soot.ValueBox;
import soot.Value;
import soot.jimple.Stmt;
import soot.jimple.InvokeExpr;
import soot.SootMethod;

public class TestTwiceCalled extends BodyTransformer {

    private TestTwiceCalled() {
    }

    protected void internalTransform(Body body, 
            String phaseName, Map<String, String> optionsMap) {

	testClone(body);
        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
        if(body.getMethod().getName().equals("switching")) {
            //System.out.println("Graph:");
            //backwardBFS(g);
        }
    }

    int ctr = 0;
    private void testClone(Body b) {
	boolean hasCall = false;
	
	for(Unit u : b.getUnits()) {
	    Stmt s = (Stmt) u;
	    if(s.containsInvokeExpr()) {
		InvokeExpr inv = s.getInvokeExpr();
		if(inv != null) {
		    hasCall = true;
		    SootMethod m = inv.getMethod();
		    if(m.hasActiveBody()) {
			Body c = m.getActiveBody();
			for(Unit v : c.getUnits()) {
			    v.addTag(new MyTag(ctr++));
			}
		    }
		}
	    }
	}

	if(!hasCall) {
	    for(Unit u : b.getUnits()) {
		System.out.println(u.getTags());
	    }
	}
    }

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        /* java.util.Collection<Pack> packs = PackManager.v().allPacks();
           for (Pack p : packs) {
           if(p.getDefaultOptions().contains("enabled:true")) {
           System.out.println(p.getPhaseName());
           }
           } */
        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new TestTwiceCalled()));
        String[] sootArgs = {"-pp", "-cp", ".", "TwiceCalled"};
        soot.Main.main(sootArgs);
    }
}
