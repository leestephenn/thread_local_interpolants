public class TwiceCalled {
    public int foo(int i) {
	i = i + 17;
	i = abs(i);
	return i;
    }

    public int bar(int i) {
	i = i + 19;
	i = baz(i);
	return i;
    }

    public int baz(int i) {
	return 2*i + 17;
    }

    public int abs(int i) {
	if(i < 0) {
	    i = -i;
	}
	return i;
    }
}
