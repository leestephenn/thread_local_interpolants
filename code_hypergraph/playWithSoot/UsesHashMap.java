import java.util.HashMap;
import java.util.Map;
public class UsesHashMap {
    public static int foo() {
	HashMap<String, Integer> map = new HashMap<String, Integer>();
	return map.size();
    }

    public static int foo(Map<String, Integer> map) {
	return map.size();
    }
}
