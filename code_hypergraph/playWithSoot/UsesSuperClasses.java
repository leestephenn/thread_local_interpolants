import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class UsesSuperClasses {
    public static int foo() {
	HashMap<String, Integer> map = new HashMap<String, Integer>();
	return map.size();
    }

    public static int foo(Map<String, Integer> map) {
	return map.size();
    }

    public static int others(Map<String, Integer> map, List<String> list) {
	Map<String, Integer> m = new HashMap<String, Integer>(map);
	List<String> l = new ArrayList<String>(list);
	m = new TreeMap<String, Integer>(map);
	l = new LinkedList<String>(list);
	return l.size() + m.size();
    }
}
