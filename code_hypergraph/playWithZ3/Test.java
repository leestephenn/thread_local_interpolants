import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.EnumSort;
import com.microsoft.z3.Symbol;
import com.microsoft.z3.Expr;
import com.microsoft.z3.enumerations.Z3_lbool;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.Model;
import com.microsoft.z3.Params;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Sort;
import com.microsoft.z3.DatatypeSort;
import com.microsoft.z3.Constructor;
import com.microsoft.z3.Tactic;
import com.microsoft.z3.Goal;
import com.microsoft.z3.ApplyResult;
import java.util.Arrays;

public class Test {
    private static InterpolationContext con = new InterpolationContext();


    
    public static void main(String[] args) {
	/* IntExpr a = con.mkIntConst("a");
	IntExpr a2 = con.mkIntConst("a");

	BoolExpr eq = con.mkEq(a, con.mkInt(3));
	BoolExpr eq2 = con.mkEq(a2, con.mkInt(3));

	System.out.println("Entails: " + entails(eq, eq2)); */
	//interpSubArgs();

	//optionTypeTest();
	//BoolExpr b = con.mkBoolConst("P");
	//System.out.println(b.getFuncDecl());
	//playWithModel();
	
        testSequenceInterpolant();
	/* BoolExpr b = con.mkBoolConst("heffalump");
	IntExpr i = con.mkIntConst("heffalump");
	BoolExpr eq = con.mkEq(i, con.mkInt(0));
	eq = con.mkOr(b, eq);
	System.out.println(eq); */
    }

    public static void testSequenceInterpolant() {
        IntExpr a = con.mkIntConst("a");
        IntExpr b = con.mkIntConst("b");
        IntExpr c = con.mkIntConst("c");
        IntExpr d = con.mkIntConst("d");
        IntExpr e = con.mkIntConst("e");

        BoolExpr twoEq = con.mkAnd(con.mkEq(a, b), con.mkEq(a, c));
        BoolExpr oneEq = con.mkEq(c, d);
        BoolExpr notImpl = con.mkAnd(con.mkEq(b, e), con.mkNot(con.mkEq(d, e)));

        BoolExpr unsat = con.MkInterpolant(con.mkTrue());
        unsat = con.MkInterpolant(con.mkAnd(unsat, twoEq));
        unsat = con.MkInterpolant(con.mkAnd(unsat, oneEq));
        unsat = con.mkAnd(unsat, notImpl);

	InterpolationContext.ComputeInterpolantResult o =
	    con.ComputeInterpolant(unsat, con.mkParams());
	System.out.println("result: " + o.status);
        System.out.println(Arrays.toString(o.interp));
    }

    public static void testFlatten() {
	BoolExpr conj = con.mkTrue();
	for(int i = 0; i < 5; i++) {
	    conj = con.mkAnd(conj, con.mkBoolConst("Prop" + i));
	}
	System.out.println(conj);
	System.out.println("Args size: " + conj.getArgs().length);

	Params p = con.mkParams();
	p.add("flat", true);
	conj = (BoolExpr) conj.simplify(p);

	System.out.println(conj);
	System.out.println("Args size: " + conj.getArgs().length);
	for(Expr arg: conj.getArgs()) {
	    System.out.println("Arg: " + arg);
	}
    }

    public static void testIntSorts() {
	Context ctx = con;
	Sort s = ctx.mkIntSort();
	System.out.println("Equal? 0 " + s.equals(ctx.getIntSort()));
	Expr norm = ctx.mkInt(13);
	Expr fab = ctx.mkNumeral(13, s);

	System.out.println("norm sort: " + norm.getSort());
	System.out.println("fab sort: " + fab.getSort());
	System.out.println("Equal? " + norm.getSort().equals(fab.getSort()));
	System.out.println(ctx.mkEq(norm, fab));
    }

    public static void enumTest() {
	Context ctx = con;
	System.out.println("EnumExample");

        Symbol name = ctx.mkSymbol("fruit");

        EnumSort fruit = ctx.mkEnumSort(name, ctx.mkSymbol("apple"),
                ctx.mkSymbol("banana"), ctx.mkSymbol("orange"));

        System.out.println((fruit.getConsts()[0]));
        System.out.println((fruit.getConsts()[1]));
        System.out.println((fruit.getConsts()[2]));

        System.out.println((fruit.getTesterDecls()[0]));
        System.out.println((fruit.getTesterDecls()[1]));
        System.out.println((fruit.getTesterDecls()[2]));

        Expr apple = fruit.getConsts()[0];
        Expr banana = fruit.getConsts()[1];
        Expr orange = fruit.getConsts()[2];

	BoolExpr dist = ctx.mkNot(ctx.mkDistinct(apple, banana, orange));
	Solver s = con.mkSolver();
	s.add(dist);
	System.out.println(s.check());
    }

    public static void optionTypeTest() {
	Context ctx = con;
        System.out.println("TreeExample");

        DatatypeSort option;
        FuncDecl nil_decl, is_nil_decl, some_decl, is_some_decl, get_decl;
        Expr nil, l1, l2, x, y, u, v;
        BoolExpr fml, fml1;
        String[] head_tail = new String[] { "get" };
        Sort[] sorts = new Sort[] { ctx.getIntSort() };
        Constructor nil_con, some_con;

        nil_con = ctx.mkConstructor("nil", "is_nil", null, null, null);
        some_con = ctx.mkConstructor("some", "is_some", head_tail, sorts,
                null);
        Constructor[] constructors = new Constructor[] { nil_con, some_con };

        option = ctx.mkDatatypeSort("option", constructors);

	System.out.println(Arrays.toString(option.getConstructors()));
	System.out.println(Arrays.toString(option.getRecognizers()));
	System.out.println(Arrays.deepToString(option.getAccessors()));

        nil_decl = nil_con.ConstructorDecl();
        is_nil_decl = nil_con.getTesterDecl();
        some_decl = some_con.ConstructorDecl();
        is_some_decl = some_con.getTesterDecl();
        FuncDecl[] some_accessor = some_con.getAccessorDecls();
        get_decl = some_accessor[0];

        l1 = ctx.mkApp(some_decl, ctx.mkInt(-1));
	System.out.println("L2: " + l1);
    }

    public static void datatypeTest() {
	Context ctx = con;
        System.out.println("objects");

	Sort pbIds = ctx.getIntSort();
	Sort acIds = ctx.getIntSort();

	Constructor nil = ctx.mkConstructor("null", "is_null", null, null, null);
	String[] predef = { "pre_id" };
	//TODO: find a better name than predef -- prealloc would be more descriptive
	//Also, predef has kind of name collision with Undef
	Constructor predef_cnstr = ctx.mkConstructor("Predef", "isPredef", predef, new Sort[] {pbIds}, null);
	DatatypeSort prebuilt = ctx.mkDatatypeSort("PrebuiltObj", new Constructor[] {nil, predef_cnstr});

	Constructor pre_cstr = ctx.mkConstructor("Pre", "isPre", new String[]{"pre_obj"}, new Sort[] {prebuilt}, null);
	Constructor ac_cstr = ctx.mkConstructor("Alloc", "isAlloc", new String[]{ "alloc_id" }, new Sort[] { acIds }, null);
	DatatypeSort referenceSort = ctx.mkDatatypeSort("Objs", new Constructor[] {pre_cstr, ac_cstr});

	Constructor undef = ctx.mkConstructor("Undef", "isUndef", null, null, null);
	Constructor actObj = ctx.mkConstructor("DefObj", "isDefObj", new String[]{ "def_obj" }, new Sort[] { referenceSort}, null);
	DatatypeSort SamDefObj = ctx.mkDatatypeSort("MaybeObj", new Constructor[]{ undef, actObj });

	FuncDecl mkNull = prebuilt.getConstructors()[0];
	Expr nullConst = ctx.mkConst(mkNull);
	FuncDecl mkPre = referenceSort.getConstructors()[0];
	System.out.println(mkPre.apply(nullConst));

	IntExpr id = ctx.mkInt(37);
	FuncDecl mkAlloc = referenceSort.getConstructors()[1];
	Expr newObj = mkAlloc.apply(id);
	System.out.println(newObj);
    }

    public static void treeExample(Context ctx) {
        System.out.println("TreeExample");

        DatatypeSort cell;
        FuncDecl nil_decl, is_nil_decl, cons_decl, is_cons_decl, car_decl, cdr_decl;
        Expr nil, l1, l2, x, y, u, v;
        BoolExpr fml, fml1;
        String[] head_tail = new String[] { "car", "cdr" };
        Sort[] sorts = new Sort[] { null, null };
        int[] sort_refs = new int[] { 0, 0 };
        Constructor nil_con, cons_con;

        nil_con = ctx.mkConstructor("nil", "is_nil", null, null, null);
        cons_con = ctx.mkConstructor("cons", "is_cons", head_tail, sorts,
                sort_refs);
        Constructor[] constructors = new Constructor[] { nil_con, cons_con };

        cell = ctx.mkDatatypeSort("cell", constructors);

	System.out.println(Arrays.toString(cell.getConstructors()));
	System.out.println(Arrays.toString(cell.getRecognizers()));
	System.out.println(Arrays.deepToString(cell.getAccessors()));

    }
    

    public static void testConstNames() {

	for(char c = '!'; c <= '~'; c++) {
	    if( !Character.isJavaIdentifierPart(c)) {
		IntExpr x = con.mkIntConst(c + "StartEnd");
		BoolExpr eq = con.mkEq(x, con.mkInt(0));
		String string = eq.toString();
		if( !string.contains("|")) {
		    System.out.println(c + " " + eq);
		}
	    }
	}
    }

    public static void interpSubArgs() {
	BoolExpr a = con.mkBoolConst("a");
	BoolExpr b = con.mkBoolConst("b");
	BoolExpr c = con.mkBoolConst("c");

	BoolExpr conj = con.mkAnd(a, b, c);
	BoolExpr interp = con.MkInterpolant(conj);
	for(Expr arg : interp.getArgs()) {
	    System.out.println(arg);
	}
    }

    public static boolean entails(BoolExpr prem, BoolExpr conc) {
	Solver s = con.mkSolver();
	s.add(con.mkNot(conc));
	s.add(prem);
	return s.check() == Status.UNSATISFIABLE;
    }

    public static void testSimplification() {
	IntExpr a = con.mkIntConst("a");
	IntExpr b = con.mkIntConst("b");
	IntExpr c = con.mkIntConst("c");

	BoolExpr d1 = con.mkGt(a, con.mkInt(0));
	BoolExpr d2 = con.mkGt(b, con.mkInt(3));
	d2 = con.mkAnd(d2, con.mkLt(b, con.mkInt(0)));

	Solver s = con.mkSolver();
	BoolExpr form = con.mkOr(d1, d2);
	System.out.println("Formula: " + form);
	System.out.println("Simplified: " + 
		form.substitute(b, con.mkInt(-1)).substitute(a, con.mkInt(1)).simplify());
	s.add(form);
	System.out.println(s.check());
	//System.out.println(s.getProof());
	Model model = s.getModel();
	System.out.println(model);
	System.out.println(model.evaluate(form, false));
    }

    /* What I really want is something that could get a path through the DAG.
     * This might actually be easier to do using the DAG itself. */
    public static Expr recur(Expr e) {
	Expr[] args = e.getArgs();
	for(int i = 0; i < args.length; i++) {
	    Expr arg = args[i];
	    if(!arg.simplify().isTrue()) {
		args[i] = recur(arg);
	    }
	}
	//TODO
	return null;
    }

    public static void playWithModel() {
	BoolExpr p = con.mkBoolConst("P");
	BoolExpr q = con.mkBoolConst("Q");
	BoolExpr form = con.mkAnd(p, q);

	Solver s = con.mkSolver();
	s.add(form);
	System.out.println(s.check());
	Model m = s.getModel();
	System.out.println(m.getConstInterp(p));
	System.out.println(Arrays.toString(m.getFuncDecls()));
	System.out.println();
	System.out.println(Arrays.toString(m.getDecls()));
	//System.out.println(m.getFuncInterp(p.getFuncDecl()));
    }

    public static void testTactics() {
	Goal g = con.mkGoal(false, false, false);
	BoolExpr a = con.mkBoolConst("a");
	BoolExpr b = con.mkBoolConst("b");
	BoolExpr c = con.mkBoolConst("c");
	BoolExpr d = con.mkBoolConst("d");

	BoolExpr dnf = con.mkOr(con.mkAnd(a, b), con.mkAnd(c, d));
	g.add(dnf);

	Tactic t = con.mkTactic("tseitin-cnf");
	ApplyResult res = t.apply(g);
	System.out.println(res);
	System.out.println(Arrays.toString(res.getSubgoals()[0].getFormulas()));
    }

    public static void interp() {
	InterpolationContext con = new InterpolationContext();

	IntExpr a = con.mkIntConst("a");
	IntExpr b = con.mkIntConst("b");
	IntExpr c = con.mkIntConst("c");
	IntExpr d = con.mkIntConst(con.toString() + "[2, 4]");
	//e = 

	BoolExpr ab = con.mkEq(a, b);
	BoolExpr ac = con.mkEq(a, c);

	BoolExpr bd = con.mkEq(b, d);
	BoolExpr cd = con.mkEq(c, d);

	BoolExpr iMinus = con.MkInterpolant(con.mkAnd(ab, ac));
	//BoolExpr iMinus = con.mkAnd(ab, ac);
	BoolExpr iPlus = con.mkAnd(bd, con.mkNot(cd));
	BoolExpr unsat = con.mkAnd(iMinus, iPlus);

	System.out.println("Expression: " + unsat);

	InterpolationContext.ComputeInterpolantResult o =
	    con.ComputeInterpolant(unsat, con.mkParams());
	
	System.out.println("result: " + o.status);
	System.out.println("reference equal: " + o.status.getClass().isEnum());
	System.out.println("Result length: " + o.interp.length);

	for (int i = 0; i < o.interp.length; i++) {
	    System.out.println("Interpolant: " + o.interp[i]);
	}
    }
}
 
