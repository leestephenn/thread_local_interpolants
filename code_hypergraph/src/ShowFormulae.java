/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Local;
import soot.ValueBox;
import soot.SootField;

import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

public class ShowFormulae extends BodyTransformer {
    private List<Unit> called;
    private List<Unit> caller;
    private List<Unit> other;

    private ShowFormulae() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("foo")) {
	    return;
	}
	Z3Converter.initFakeFields();

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit init = g.getHeads().get(0);
	Unit exit = g.getTails().get(0);
	UnwindingContext uc = new UnwindingContext(g, init, exit, 2);

	SubpathTuple whole = new SubpathTuple(uc.pathTuple(), uc);

	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, topoOrder);

	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	    if(st == whole) {
		//System.out.println("Pre: " + st.getPreStaticFields());
		//System.out.println("Post: " + st.getPostStaticFields());
	    }
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputShowFormulae.dot", MakeGraph.SUFFIX_LENGTH_NAMES);

	testFields(topoOrder);
    }

    public void showForwardContextFormulae(ArrayList<SubpathTuple> topoOrder) {
	SubpathTuple whole = topoOrder.get(topoOrder.size() - 1);

	BoolExpr initConsts = whole.checkFinal(false, true, false);

	SubpathTuple empty = topoOrder.get(0);
	System.out.println("Empty?: " + empty);
	empty.setAuxilliaryConstraints(initConsts);

	for(SubpathTuple st : topoOrder) {
	    /* for(UnitEdge e : st.getInEdges()) {
		System.out.println(e.getFormula());
	    } */
	    System.out.println(st.makeForwardContext(empty));
	    //System.out.println();
	}
    }

    public void testClassConstraints(ArrayList<SubpathTuple> topoOrder) {
	final Context con = Z3Converter.CONTEXT;
	SubpathTuple whole = topoOrder.get(topoOrder.size() - 1);

	BoolExpr initConsts = whole.checkFinal(false, false, true);

	for(SubpathTuple st : topoOrder) {
	    for(UnitEdge e : st.getInEdges()) {
		if (e instanceof UnitSingleEdge) {
		    //System.out.println("Head: " + st);
		    UnitSingleEdge se = (UnitSingleEdge) e;
		    if(usesObject(se.getStmt())) {
			System.out.println(se.getStmt());
			System.out.println(se.getFormula());
		    }
		}
	    }
	}
    }

    public void testFields(ArrayList<SubpathTuple> topoOrder) {
	final Context con = Z3Converter.CONTEXT;
	SubpathTuple whole = topoOrder.get(topoOrder.size() - 1);

	BoolExpr initConsts = whole.checkFinal(false, false, true);

	for(SubpathTuple st : topoOrder) {
	    if(st.isZero()) {
		st.setAuxilliaryConstraints(initConsts);
	    }
	    for(UnitEdge e : st.getInEdges()) {
		if (e instanceof UnitSingleEdge) {
		    //System.out.println("Head: " + st);
		    UnitSingleEdge se = (UnitSingleEdge) e;
		    if(usesObject(se.getStmt())) {
			System.out.println(se.getStmt());
			System.out.println(se.getFormula());
		    }
		}
	    }
	    //System.out.println(st.makeForwardContext(whole));
	}
    }

    public void testLocks(ArrayList<SubpathTuple> topoOrder) {
	final Context con = Z3Converter.CONTEXT;
	SubpathTuple whole = topoOrder.get(topoOrder.size() - 1);

	BoolExpr initConsts = whole.checkFinal(false, false, true);

	for(SubpathTuple st : topoOrder) {
	    if(st.isZero()) {
		st.setAuxilliaryConstraints(initConsts);
	    }
	    for(UnitEdge e : st.getInEdges()) {
		if(e instanceof UnitHyperEdge) {
		    System.out.println("Lock was not converted correctly: " +
			    "found hyperedge");

		    //System.out.println(e.getFormula());
		} else if (e instanceof UnitSingleEdge) {
		    //System.out.println("Head: " + st);
		    UnitSingleEdge se = (UnitSingleEdge) e;
		    if(hasFieldOrInvoke(se.getStmt())) {
			System.out.println(se.getStmt());
			System.out.println(se.getFormula());
		    }
		}
	    }
	    //System.out.println(st.makeForwardContext(whole));
	}
    }

    public Expr findLock(Expr formula) {
	for(Expr arg : formula.getArgs()) {
	    if(arg.getSort().equals(Z3Converter.refSort)) {
		return arg;
	    } else {
		Expr result = findLock(arg);
		if(result != null) {
		    return result;
		}
	    }
	}
	return null;
    }

    public boolean hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public boolean hasFieldOrInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseAndDefBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof soot.jimple.FieldRef) {
		return true;
	    }
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public boolean hasField(Unit u) {
	List<ValueBox> boxes = u.getUseAndDefBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof soot.jimple.FieldRef) {
		return true;
	    }
	}

	return false;
    }

    public boolean usesObject(Unit u) {
	for (ValueBox vb : u.getUseAndDefBoxes()) {
	    if(vb.getValue().getType() instanceof soot.RefType) {
		return true;
	    }
	}
	return false;
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new ShowFormulae()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "Fields" };
	soot.Main.main(sootArgs);
    }
}
