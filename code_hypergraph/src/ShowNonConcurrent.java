/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.Scene;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.ValueBox;

import soot.SootMethod;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;

import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;

public class ShowNonConcurrent extends BodyTransformer {
    private List<Unit> called;
    private List<Unit> caller;
    private List<Unit> other;

    private ShowNonConcurrent() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("bar")) {
	    called = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    caller = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	if(called != null && caller != null && other != null) {
	    testCallReturn();
	}
    }

    public void testCallReturn() {
	Context con = Z3Converter.CONTEXT;

	System.out.println("\n=====Transform Output=========");

	int exitSite = 0;
	for(; exitSite < caller.size(); exitSite++) {
	    if(hasInvoke(caller.get(exitSite))) {
		caller.addAll(exitSite + 1, called);
		break;
	    }
	}

	Path path1 = new Path(caller, "f");
	path1.addMatch(exitSite, exitSite + called.size() + 1);
	Path path2 = new Path(other, "th");

	if(!hasInvoke(path1.get(exitSite)) || !isReturn(path1.get(exitSite + called.size()))) {
	    System.out.print("Put the matching relation wrong: ");
	    System.out.print("points to ");
	    System.out.print(path1.get(exitSite));
	    System.out.print(" and ");
	    System.out.println(path1.get(exitSite + called.size()));
	}

	PathTuple paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
	SubpathTuple whole = new SubpathTuple(paths);
	System.out.println(whole);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(whole, topoOrder);

	System.out.println("Number of path pairs: " + topoOrder.size());

	//System.out.println("Before generating paths ");
	for(SubpathTuple st : topoOrder) {
	    //System.out.println("Generating formulae");
	    st.fillVariables();
	}

	MakeGraph m = new MakeGraph(whole);
	int counter = 0;
	for(SubpathTuple st : topoOrder) {
	    m.writeToFile("outputShowNonConcurrent" + counter + ".dot", st);
	    counter++;
	}

	//MakeGraph m = new MakeGraph(whole);
	//m.writeToFile("outputShowNonConcurrent.dot");


    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new ShowNonConcurrent()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "SimpleMethodCall",
	    /* "NoDivideByZeroTest"*/};
	soot.Main.main(sootArgs);
    }
}
