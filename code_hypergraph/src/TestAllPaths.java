/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.DefinitionStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestAllPaths extends BodyTransformer {
    private boolean verbose = false;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private PathTuple paths;

    private TestAllPaths() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("foo")) {
	    return;
	}

	HashSet<Unit> units = new HashSet<Unit>();
	units.addAll(body.getUnits());
	System.out.println("Old size: " + units.size());
	
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);

	for(Unit u : g) {
	    units.add(u);
	}
	System.out.println("New size: " + units.size());
	System.out.println(units);

	Unit head = g.getHeads().get(0);
	Unit tail = null;
	for(Unit u : g) {
	    if(hasInvoke(u)) {
		if (u instanceof DefinitionStmt) {
		    tail = u;
		    break;
		}
	    }
	}
	UnwindingContext.getCFGs().put(head, g);

	//for(Unit u : g) {
	    //System.out.println(u.hashCode());
	//}
	//System.out.println();

	java.util.Set<Unit> all = UnwindingContext.allPaths(head, tail, true);
	System.out.println("Set: " + all);
    }

    public boolean hasInvoke(Unit u) {
	if(verbose) {
	    System.out.println(u);
	}
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestAllPaths()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "SimpleMethodCall", };
	soot.Main.main(sootArgs);
    }
}
