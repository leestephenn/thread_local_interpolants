/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestCallsiteConvert extends BodyTransformer {
    private List<Unit> foo;
    private List<Unit> bar;
    private List<Unit> baz;
    private List<Unit> other;
    private List<Unit> other2;
    private boolean verbose = false;

    private PathTuple paths;

    private TestCallsiteConvert() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	//initSimple(body);
	initComplex(body);

	if(paths == null) {
	    return;
	}
	//System.out.println("Heffalump");

	SubpathTuple whole = new SubpathTuple(paths);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	try {
	    DFS(whole, topoOrder);
	} catch (IllegalStateException ex) {
	    MakeGraph m = new MakeGraph(whole);
	    m.writeToFile("outputCallsiteConvert.dot", MakeGraph.SUFFIX_LENGTH_NAMES);
	    throw ex;
	}

	System.out.println("Number of path pairs: " + topoOrder.size());

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputCallsiteConvert.dot", MakeGraph.SUFFIX_LENGTH_NAMES);

	for(SubpathTuple st : topoOrder) {
	    //System.out.println("Generating formulae");
	    //System.out.println("subpath tuple " + st.intervals());
	    st.fillVariables();

	}


	UnitHyperEdge he = null;
	for(SubpathTuple head : topoOrder) {
	    for(UnitEdge e : head.getInEdges()) {
		if (e instanceof UnitHyperEdge) {
		    System.out.println(head.intervals());
		    he = (UnitHyperEdge) e;
		    testHyperEdge(he);
		}
	    }
	}
    }

    private void initSimple(Body body) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("bar")) {
	    bar = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    foo = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	if(bar == null || foo == null || other == null) {
	    return;
	}

	System.out.println("\n=====Transform Output=========");

	int i = 0;
	for(; i < foo.size(); i++) {
	    if(hasInvoke(foo.get(i))) {
		foo.addAll(i + 1, bar);
		break;
	    }
	}

	Path path1 = new Path(foo, "f");
	path1.addMatch(i, i + bar.size() + 1);
	Path path2 = new Path(other, "th");

	if(!hasInvoke(path1.get(i)) || !isReturn(path1.get(i + bar.size()))) {
	    System.out.print("Put the matching relation wrong: ");
	    System.out.print("points to ");
	    System.out.print(path1.get(i));
	    System.out.print(" and ");
	    System.out.println(path1.get(i + bar.size()));
	}

	paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    private void initComplex(Body body) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	//System.out.println("Method name: " + body.getMethod().getName());
	if(body.getMethod().getName().equals("bar")) {
	    bar = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    foo = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other2")) {
	    other2 = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("baz")) {
	    baz = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	//System.out.println((bar == null) + ", " + (foo == null) + ", " + (baz == null) + ", " + (other == null) + ", " + (other2 == null));
	if(bar == null || foo == null || baz == null ||
		other == null || other2 == null) {
	    return;
	}

	//System.out.println("Woozle");

	System.out.println("\n=====Transform Output=========");

	int callBar = 0;
	for(; callBar < foo.size(); callBar++) {
	    if(hasInvoke(foo.get(callBar))) {
		foo.addAll(callBar + 1, bar);
		break;
	    }
	}

	int callBaz = callBar + 1;
	for(; callBaz < foo.size(); callBaz++) {
	    if(hasInvoke(foo.get(callBaz))) {
		foo.addAll(callBaz + 1, baz);
		break;
	    }
	}

	Path path1 = new Path(foo, "f");
	path1.addMatch(callBaz, callBaz + baz.size() + 1);
	path1.addMatch(callBar, callBar + bar.size()  + baz.size() + 1);
	//System.out.println((callBaz + baz.size() + 1) + " " + (callBar + bar.size() + 1));
	System.out.println("Path1: " + path1);

	int callO = 0;
	//verbose = true;
	for(; callO < other.size(); callO++) {
	    if(hasInvoke(other.get(callO))) {
		//System.out.println("Found invoke in other.");
		verbose = false;
		other.addAll(callO + 1, other2);
		break;
	    }
	}
	//verbose = false;
	Path path2 = new Path(other, "th");
	path2.addMatch(callO, callO + other2.size() + 1);
	System.out.println("Path2: " + path2);

	System.out.println("Path1 match: " + path1.matchRelation());
	System.out.println("Path2 match: " + path2.matchRelation());

	checkMatching(path1, bar, callBar, "bar");
	checkMatching(path1, baz, callBaz, "baz");
	checkMatching(path2, other2, callO, "other2");


	paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    public void checkMatching(Path path, List<Unit> sub, int callI, String name) {
	try {
	    if(!hasInvoke(path.get(callI)) || !isReturn(path.get(callI + sub.size()))) {
		System.out.print("Put the matching relation wrong for " + name + ": ");
		System.out.print("points to ");
		System.out.print(path.get(callI));
		System.out.print(" and ");
		System.out.println(path.get(callI + sub.size()));
	    }
	} catch (IndexOutOfBoundsException ex) {
	    System.out.println(name);
	    throw ex;
	}
    }

    private void testHyperEdge(UnitHyperEdge he) {

	BoolExpr rho = he.getFormula();
	//System.out.println("Formula:");
	//System.out.println(rho);

	SubpathTuple exitTail = he.getExitTail();
	SubpathTuple callTail = he.getCallTail();

	Map<Local, Expr> cvtrPostVars = he.cvtr().getNewThreadMappings();

	if(cvtrPostVars.size() > 1) {
	    System.out.println("A return statement defined " + cvtrPostVars.size() + " variables, more than 1");
	}

	List<HashMap<Local, Expr>> postVars = he.getPostVars();
	for(int t = 0; t < postVars.size(); t++) {
	    List<HashMap<Local, Expr>> callPost = callTail.getPostVars();
	    List<HashMap<Local, Expr>> exitPost = exitTail.getPostVars();

	    HashMap<Local, Expr> heff = callPost.get(t);
	    if(!contains(heff, postVars.get(t))) {
		System.out.println("The post variables for thread " + t +
			" did not contain those the call-pred's " + callTail.intervals());
	    }

	    if(t != he.thread && !contains(exitPost.get(t), postVars.get(t))) {
		System.out.println("The post variables for thread " + t +
			" did not contain those the exit-pred's " + exitTail.intervals());
	    }
	}

	ArrayList<HashMap<Local, Expr>> preVars = he.getPreVars();
	for(int t = 0; t < preVars.size(); t++) {
	    List<HashMap<Local, Expr>> callPre = callTail.getPreVars();
	    if (!contains(callPre.get(t), preVars.get(t))) {
		System.out.println("Thread " + t + " did not contain the foo pre variables: " + callPre.get(t) + " " + preVars.get(t));
	    }


	    List<HashMap<Local, Expr>> exitPre = exitTail.getPreVars();
	    if(t == he.thread) {
		if(!exitPre.get(t).isEmpty()) {
		    System.out.println("Exit tail had pre var for calling thread");
		}
	    } else for(Local l : exitPre.get(t).keySet()) {
		if(!preVars.get(t).containsKey(l) &&
			!isSubExpr(exitPre.get(t).get(l), rho)) {
		    List<HashMap<Local, Expr>> c = callTail.getPostVars();
		    List<HashMap<Local, Expr>> h =he.getHead().getPostVars();
		    if(!c.get(t).containsKey(l) && !h.get(t).containsKey(l)) {
			System.out.println("Did not correctly handle exit pre variable " + l + " on thread " + t);
		    }
		}
	    }
	}

	for(Expr par : he.getExitTail().getParams().get(he.thread).values()) {
	    if(! isSubExpr(par, rho)) {
		System.out.println("Parameter " + par +
			" was not in the constraint for hyperedge ");
		System.out.println("Thread " + he.thread);
		SubpathTuple pred = he.getExitTail();
		boolean cont = true;
		while(cont) {
		    System.out.println(pred.intervals());
		    System.out.println(pred.getParams());
		    cont = false;
		    for(UnitEdge e : pred.getInEdges()) {
			if(e instanceof UnitSingleEdge) {
			    pred = ((UnitSingleEdge) e).getTail();
			    cont = true;
			}
		    }
		}
		System.out.println("InvokeStmt? " + (he.getCallStmt() instanceof InvokeStmt));
		System.out.println(rho);
	    }
	}

	Map<SootField, ArrayExpr> preFields = he.getPreFields();
	for(SootField k : preFields.keySet()) {
	    if(k.isStatic()) {
		System.out.println("Instance fields included static field " + k);
	    }
	}
	Map<SootField, ArrayExpr> callPre = callTail.getPreFields();
	if (!contains(callPre, preFields)) {
	    System.out.println("Did not contain the call pre fields: " +
		    callPre + " " + preFields);
	}

	Map<SootField, ArrayExpr> exitPre = exitTail.getPreFields();
	for(SootField f : exitPre.keySet()) {
	    if(!preFields.containsKey(f) &&
		    !isSubExpr(exitPre.get(f), rho)) {
		Map<SootField, ArrayExpr> c = callTail.getPostFields();
		Map<SootField, ArrayExpr> h = he.getHead().getPreFields();

		//TODO: I am not exactly sure what this if does, 
		//so I am removing it for now.
		//if(exitTail.readsField(f)) {
		    System.out.println("On thread " + he.thread);
		    System.out.println("Did not correctly handle exit pre field " + f);
		    System.out.println("Its const: " + exitPre.get(f));
		    System.out.println("Call:\n" + callTail.subpathsString());
		    System.out.println("Exit:\n" + exitTail.subpathsString());
		    System.out.println("Head:\n" + he.getHead().subpathsString());
		    System.out.println("PreFields: " + preFields);
		    System.out.println("HeadFields: " + h);
		    System.out.println("CallPostFields: " + c);
		//}
	    }
	}

	Map<SootField, ArrayExpr> postFields = he.getPostFields();
	Map<SootField, ArrayExpr> callPost = callTail.getPostFields();
	Map<SootField, ArrayExpr> exitPost = exitTail.getPostFields();

	for(SootField k : postFields.keySet()) {
	    if(k.isStatic()) {
		System.out.println("Instance fields included static field " + k);
	    }
	}
	if(!contains(callPost, postFields)) {
		System.out.println("The post variables " +
			" did not contain those the call-pred's " + callTail.intervals());
	}
	if(!contains(exitPost, postFields)) {
	    System.out.println("The post variables " +
		    " did not contain those the exit-pred's " + exitTail.intervals());
	}
	for(SootField f : postFields.keySet()) {
	    if(postFields.get(f).equals(callPost.get(f)) &&
		    !postFields.get(f).equals(exitPost.get(f)) &&
		    exitPost.get(f) != null) {
		System.out.println("Did not put exit in post second.");
		System.out.println("Result: " + postFields.get(f));
		System.out.println("Call: " + callPost.get(f));
		System.out.println("Exit: " + exitPost.get(f));
	    }
	}

	//Static

	Map<SootField, Expr> preStaticFields = he.getPreStaticFields();
	for(SootField k : preStaticFields.keySet()) {
	    if(!k.isStatic()) {
		System.out.println("Instance fields included static field " + k);
	    }
	}
	Map<SootField, Expr> callStaticPre = callTail.getPreStaticFields();
	if (!contains(callStaticPre, preStaticFields)) {
	    System.out.println("Did not contain the call pre fields: " +
		    callStaticPre + " " + preStaticFields);
	}

	Map<SootField, Expr> exitStaticPre = exitTail.getPreStaticFields();
	for(SootField f : exitStaticPre.keySet()) {
	    if(!preStaticFields.containsKey(f) &&
		    !isSubExpr(exitStaticPre.get(f), rho)) {
		Map<SootField, Expr> c = callTail.getPostStaticFields();
		Map<SootField, Expr> h = he.getHead().getPreStaticFields();

		//TODO: I am not exactly sure what this if does, so I am
		//removing it for now
		//if(exitTail.readsField(f)) {
		    System.out.println("On thread " + he.thread);
		    System.out.println("Did not correctly handle exit pre field " + f);
		    System.out.println("Its const: " + exitStaticPre.get(f));
		    System.out.println("Call:\n" + callTail.subpathsString());
		    System.out.println("Exit:\n" + exitTail.subpathsString());
		    System.out.println("Head:\n" + he.getHead().subpathsString());
		    System.out.println("PreFields: " + preStaticFields);
		    System.out.println("HeadFields: " + h);
		    System.out.println("CallPostFields: " + c);
		//}
	    }
	}

	Map<SootField, Expr> postStaticFields = he.getPostStaticFields();
	Map<SootField, Expr> callStaticPost = callTail.getPostStaticFields();
	Map<SootField, Expr> exitStaticPost = exitTail.getPostStaticFields();

	for(SootField k : postStaticFields.keySet()) {
	    if(!k.isStatic()) {
		System.out.println("Instance fields included static field " + k);
	    }
	}
	if(!contains(callStaticPost, postStaticFields)) {
		System.out.println("The post variables " +
			" did not contain those the call-pred's " + callTail.intervals());
	}
	if(!contains(exitStaticPost, postStaticFields)) {
	    System.out.println("The post variables " +
		    " did not contain those the exit-pred's " + exitTail.intervals());
	}
	for(SootField f : postStaticFields.keySet()) {
	    if(postStaticFields.get(f).equals(callStaticPost.get(f)) &&
		    !postStaticFields.get(f).equals(exitStaticPost.get(f)) &&
		    exitStaticPost.get(f) != null) {
		System.out.println("Did not put exit in post second.");
		System.out.println("Result: " + postStaticFields.get(f));
		System.out.println("Call: " + callStaticPost.get(f));
		System.out.println("Exit: " + exitStaticPost.get(f));
	    }
	}

	System.out.println();
    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	if(verbose) {
	    System.out.println(u);
	}
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public boolean isSubExpr(Expr sub, Expr sup) {
	if(sub.equals(sup)) {
	    return true;
	} else {
	    for(Expr arg : sup.getArgs()) {
		if (isSubExpr(sub, arg)) {
		    return true;
		}
	    }
	    return false;
	}
    }

    public static <K, V> boolean contains(Map<K, V> sub, Map<K, V> sup) {
	for(K l: sub.keySet()) {
	    if(!sup.keySet().contains(l)) {
		System.out.println("Not contained: " + l);
		return false;
	    }
	}
	return true;
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestCallsiteConvert()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "InstanceMethodCalls", };
	soot.Main.main(sootArgs);
    }
}
