/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;
import soot.RefType;
import soot.Value;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestClassConstraint extends BodyTransformer {
    private boolean verbose = false;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private PathTuple paths;

    private TestClassConstraint() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	final Context ctx = Z3Converter.CONTEXT;

	Expr obj = ctx.mkConst("foo", Z3Converter.refSort);

	for(ValueBox vb : body.getUseAndDefBoxes()) {
	    Value v = vb.getValue();
	    if(v.getType() instanceof RefType) {
		RefType rt = (RefType) v.getType();
		System.out.println(rt);
		System.out.println(Z3Converter.classConstraint(obj, rt));
		System.out.println();
	    }
	}

    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestClassConstraint()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "UsesSuperClasses"};
	soot.Main.main(sootArgs);
    }
}
