/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import com.microsoft.z3.Context;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.IntExpr;

import java.util.Arrays;

public class TestEntails {
    public static void main(String[] args) {
	testNonConcurrent();
    }

    public static void actualTest() {
	IntExpr x, y, z;
	Context con = Z3Converter.CONTEXT;

	x = con.mkIntConst("x");
	y = con.mkIntConst("y");
	z = con.mkIntConst("z");

	IntExpr eleven = con.mkInt(11);
	IntExpr five = con.mkInt(5);
	
	BoolExpr e1 = con.mkLt(x, five);
	BoolExpr e2 = con.mkLt(x, eleven);
	
	if( !InterprocAnalysis.entails(e1, e2)) {
	    System.out.println("x < 5 did not entail x < 10");
	}

	BoolExpr conj1 = con.mkLt(con.mkMul(x, five), con.mkMul(y, five));
	BoolExpr conj2 = con.mkLt(y, z);
	e1 = con.mkAnd(conj1, conj2);
	e2 = con.mkLt(x, z);

	if( !InterprocAnalysis.entails(e1, e2)) {
	    System.out.println("x < y & y < z did not entail x < z");
	}

    }

    public static void testNonConcurrent() {
	IntExpr[] myStarts = new IntExpr[2];
	IntExpr[] oStarts = new IntExpr[2];

	IntExpr[] myEnds = new IntExpr[2];
	IntExpr[] oEnds = new IntExpr[2];
	Context con = Z3Converter.CONTEXT;
	
	String base = "v";
	int counter = 0;
	for(int i = 0; i < myStarts.length; i++) {
	    myStarts[i] = con.mkIntConst(base + (counter++));
	}
	for(int i = 0; i < oStarts.length; i++) {
	    oStarts[i] = con.mkIntConst(base + (counter++));
	}
	for(int i = 0; i < myEnds.length; i++) {
	    myEnds[i] = con.mkIntConst(base + (counter++));
	}
	for(int i = 0; i < oEnds.length; i++) {
	    oEnds[i] = con.mkIntConst(base + (counter++));
	}


	BoolExpr premise = concurrentCond(oStarts, oEnds, myStarts, myEnds);

	BoolExpr[] allEqConjs = new BoolExpr[myStarts.length];
	for(int i = 0; i < myStarts.length; i++) {
	    allEqConjs[i] = con.mkEq(myStarts[i], myEnds[i]);
	}
	BoolExpr allEq = con.mkAnd(allEqConjs);

	IntExpr[] t0Ends = Arrays.copyOf(myEnds, myEnds.length);
	t0Ends[0] = (IntExpr) con.mkSub(t0Ends[0], con.mkInt(1));

	BoolExpr t0Conc = concurrentCond(oStarts, oEnds, myStarts, t0Ends);

	IntExpr[] t1Ends = Arrays.copyOf(myEnds, myEnds.length);
	t1Ends[1] = (IntExpr) con.mkSub(t1Ends[1], con.mkInt(1));

	BoolExpr t1Conc = concurrentCond(oStarts, oEnds, myStarts, t1Ends);

	BoolExpr conclusion = con.mkOr(allEq, t0Conc, t1Conc);
	boolean entails = InterprocAnalysis.entails(premise, conclusion);
	System.out.println("Entails: " + entails);
    }

    public static BoolExpr concurrentCond(IntExpr[] oStarts, IntExpr[] oEnds, 
	    IntExpr[] myStarts, IntExpr[] myEnds) {
	Context con = Z3Converter.CONTEXT;

	BoolExpr[] premDisj = new BoolExpr[4];
	int pI = 0;

	for(int i = 0; i < 2; i++) {
	    for(int j = 0; j < 2; j++) {
		if (i != j) {
		    BoolExpr tmp = con.mkLt(myStarts[i], oStarts[i]);
		    tmp = con.mkAnd(tmp, con.mkLt(oStarts[j], myStarts[j]));
		    premDisj[pI++] = tmp;

		    tmp = con.mkLt(myEnds[i], oEnds[i]);
		    tmp = con.mkAnd(tmp, con.mkLt(oEnds[j], myEnds[j]));
		    premDisj[pI++] = tmp;
		}
	    }
	}

	return con.mkNot(con.mkOr(premDisj));
    }
}
