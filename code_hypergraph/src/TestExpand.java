/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.jimple.InvokeStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.Scene;
import soot.SootMethod;

public class TestExpand extends BodyTransformer {
    private TestExpand() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	if(!body.getMethod().getName().equals("foo")) {
	    return;
	}
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);

	UnwindingContext con = new UnwindingContext(g, 2);
	int invT = -1;
	while(invT < 0) {
	    List<UnwindingContext> result = con.expand();
	    con = result.get(0);
	    for(int i = 0; i < 2; i++) {
		if(null != UnwindingContext.hasInvoke(con.getExpandUnit(i))) {
		    invT = i;
		}
	    }
	}

	System.out.println("Expanding ");
	for(int i = 0; i < 2; i++) {
	     System.out.println(con.getExpandUnit(i));
	}
	System.out.println();

	List<UnwindingContext> result = con.expand();

	for(UnwindingContext oth : result) {
	    if(oth.getExpandUnit(invT) instanceof ReturnStmt ||
		oth.getExpandUnit(invT) instanceof ReturnVoidStmt) {

		if(!oth.getExpandUnit(1 - invT).
			equals(con.getExpandUnit(1 - invT))) {
		    System.out.println("Callee was not expanding at the same location.");
		}

		if(oth.getContext(1 - invT).get(oth.getExpandPoint(1 - invT) - 1).
			equals(con.getContext(1 - invT).get(
				con.getExpandPoint(1 - invT) - 1)))
		{
		    System.out.println("Callee top boundary was equal to old in other thread.");
		}
	    } else if ( ! oth.getContext(invT).equals(con.getContext(invT))) {

		if(oth.getExpandUnit(1 - invT)
			.equals(con.getExpandUnit(1 - invT))) {

		    System.out.println("Caller was expanding at the same location.");
		}

		if ( ! g.getSuccsOf(oth.getExpandUnit(invT)).
			contains(con.getExpandUnit(invT))) {
		    System.out.println("Did not go to previous statement in caller thread.");
		    System.out.println("New: " + oth.getExpandUnit(invT));
		    System.out.println("Old: " + con.getExpandUnit(invT));
		    System.out.println("Context: " + oth.getContext(invT));
		}

		if( oth.getContextSize(invT) != con.getContextSize(invT) + 1) {
		    System.out.print("Caller thread had more than one additional Unit: ");
		    System.out.println(oth.getContextSize(invT) -
			    con.getContextSize(invT)); 
		}
	    }

	    for(int i = 0; i < 2; i++) {
		System.out.println(oth.getExpandUnit(i));
	    }
	    System.out.println();
	}
    }

    public void simpleExpand(Body body) {
	if(!body.getMethod().getName().equals("main")) {
	    return;
	}
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);

	UnwindingContext con = new UnwindingContext(g, 2);
	List<UnwindingContext> result = con.expand();

	for(UnwindingContext uc : result) {
	    int diffSizes = 0;
	    int diffUnits = 0;

	    for(int t = 0; t < 2; t++) {
		if(uc.getContextSize(t) != con.getContextSize(t)) {
		    diffSizes++;
		}
		if(!uc.getExpandUnit(t).equals(con.getExpandUnit(t))) {
		    diffUnits++;
		}
	    }
	    if(diffSizes != 1) {
		System.out.println("New context " + uc + " had a different size on more than one thread.");
	    }
	    if(diffUnits != 1) {
		System.out.println("New context " + uc + " had  a new unit on more than one thread.");
	    }
	    //System.out.println(uc);
	}
	//System.out.println(result);
    }

    public static void main(String[] args) {

	Options.v().set_output_format(Options.output_format_jimple);
	Options.v().set_omit_excepting_unit_edges(false);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestExpand()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "SimpleMethodCall"};
	soot.Main.main(sootArgs);
    }
}
