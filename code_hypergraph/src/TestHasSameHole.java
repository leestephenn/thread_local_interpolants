/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.jimple.InvokeStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.Scene;
import soot.SootMethod;

public class TestHasSameHole extends BodyTransformer {
    private TestHasSameHole() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	if( !body.getMethod().getName().equals("foo")) {
	    return;
	}
	
	ExceptionalUnitGraph g1 = new ExceptionalUnitGraph(body);
	UnwindingContext c1 = new UnwindingContext(g1, g1.getHeads().get(0),
		g1.getTails().get(0), 2);

	HashSet<UnwindingContext> gContexts = new HashSet<UnwindingContext>();
	SubpathTuple whole = new SubpathTuple(c1.pathTuple(), c1);
	ArrayList<SubpathTuple> gen1 = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, gen1);
	for(SubpathTuple st : gen1) {
	    gContexts.add(st.getUnwindingContext());
	}
	System.out.println("Num contexts: " + gContexts.size());
	whole = new SubpathTuple(c1.pathTuple(), c1);
	ArrayList<SubpathTuple> gen2 = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, gen2);
	for(SubpathTuple st : gen2) {
	    gContexts.add(st.getUnwindingContext());
	}
	System.out.println("Num contexts: " + gContexts.size());

	for(UnwindingContext uCon1 : gContexts) {
	    for(UnwindingContext uCon2 : gContexts) {
		if( !uCon1.equals(uCon2)) {
		    boolean someNotEquals = false;
		    for(int k = 0; k < 2; k++) {
			if( !uCon1.getExpandUnit(k).equals(uCon2.getExpandUnit(k))) {
			    someNotEquals = true;
			}
		    }
		    if(someNotEquals && uCon1.hasSameHole(uCon2)) {
			System.out.println("Have same hole\n" + uCon1);
			System.out.println("\n" + uCon2);
			System.out.println();
		    }
		}
	    }
	}

	/* List<UnwindingContext> exp = c1.expand();
	for(int i = 0; i < 24 && i < exp.size(); i++) {
	    UnwindingContext uCon = exp.get(i);
	    exp.addAll(uCon.expand());
	}

	//System.out.println(exp);
	//
	
	for(int i = 0; i < exp.size(); i++) {
	    UnwindingContext uCon1 = exp.get(i);
	    for(int j = 0; j < exp.size(); j++) {
		UnwindingContext uCon2 = exp.get(j);
		if(i != j && uCon1.hasSameHole(uCon2)) {
		    boolean someNotEquals = false;
		    for(int k = 0; k < 2; k++) {
			if( !uCon1.getExpandUnit(k).equals(uCon2.getExpandUnit(k))) {
			    someNotEquals = true;
			}
		    }
		    
		    if(someNotEquals) {
			System.out.println("Have same hole\n" + uCon1);
			System.out.println("\n" + uCon2);
			System.out.println();
		    }
		}
	    }
	} */
    }

    public static void main(String[] args) {

	Options.v().set_output_format(Options.output_format_jimple);
	Options.v().set_omit_excepting_unit_edges(false);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestHasSameHole()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "TwiceCalled"};
	soot.Main.main(sootArgs);
    }
}
