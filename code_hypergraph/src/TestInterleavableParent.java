/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestInterleavableParent extends BodyTransformer {
    private boolean verbose = false;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private TestInterleavableParent() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("fooStraight")) {
	    return;
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	
	UnwindingContext uCon = new UnwindingContext(g, g.getHeads().get(0), g.getTails().get(0), 2);

	SubpathTuple whole = new SubpathTuple(uCon.pathTuple(), uCon);

	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, topoOrder);
	System.out.println("# of Subpath Tuples: " + topoOrder.size());

	/* for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	} */

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestInterleavableParent.dot", MakeGraph.SUFFIX_LENGTH_NAMES);
	
	for(int i = 0; i < topoOrder.size(); i++) {
	    SubpathTuple cur = topoOrder.get(i);

	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple other = topoOrder.get(j);
		if(cur.interleavable(other)) {
		    //System.out.println("Interleavable: " + other);

		    boolean someInterleavable = false;
		    for(UnitEdge in : other.getInEdges()) {
			boolean bothTails = true;
			for(SubpathTuple tail : in.getTails()) {
			    if(!cur.interleavable(tail)) {
				bothTails = false;
			    }
			}

			if(bothTails) {
			    someInterleavable = true;
			}
		    }

		    if(!someInterleavable && !other.isEmpty()) {
			System.out.println("SubpathTuple " + other + " was interleavable but had no interleavable predecessor with " + cur);
			m.writeToFile("outputTestInterleavable0.dot", cur);
			return;
		    }
		}
	    }
	}

    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestInterleavableParent()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "SimpleMethodCall"};
	soot.Main.main(sootArgs);
    }
}
