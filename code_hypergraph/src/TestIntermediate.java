/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.Transform;
import java.util.Map;
import java.util.LinkedList;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;
import java.util.List;
import java.util.Set;


public class TestIntermediate extends BodyTransformer {
    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit head = g.getHeads().get(0);
	for (Unit tail : g.getTails()) {
	    Set<Unit> test = UnwindingContext.intermediate(g, head, tail);
	    if(test.isEmpty()) {
		System.out.println("test was empty");
	    } else {
		System.out.println("test: " + test);
	    }
	    
	    LinkedList<Unit> queue = new LinkedList<Unit>();
	    queue.add(tail);
	    while(!queue.isEmpty()) {
		Unit suff = queue.remove();
		List<Unit> preds = g.getPredsOf(suff);
		for(Unit pred : preds) {
		    if(!test.contains(pred) && !pred.equals(head)) {
			System.out.println("Predecessor " + pred + " was not an intermediate node.");
			//System.out.println("For tail: " + tail);
		    }

		    //Not complete, but should save some time
		    if(!queue.contains(pred) && !queue.equals(head)) {
			queue.add(pred);
		    }
		}
	    }
	}
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestIntermediate()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "HelloWorld"};
	soot.Main.main(sootArgs);

    }

}
