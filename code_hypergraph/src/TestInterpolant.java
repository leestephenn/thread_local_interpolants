/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.Scene;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.ValueBox;

import soot.SootMethod;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;

import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

public class TestInterpolant extends BodyTransformer {
    private List<Unit> called;
    private List<Unit> caller;
    private ExceptionalUnitGraph callerGraph;
    private List<Unit> single;
    private List<Unit> other;

    private TestInterpolant() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("bar")) {
	    called = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    caller = g.getExtendedBasicBlockPathBetween(entrance, exit);
	    callerGraph = g;
	} else if (body.getMethod().getName().equals("single")){
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    single = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	if(called != null && caller != null && other != null && single != null) {
	    testCallReturn();
	    //testSingleMethod();
	}
    }

    public void testSingleMethod() {
	Context con = Z3Converter.CONTEXT;

	Path path1 = new Path(single, "s");
	Path path2 = new Path(other, "th");
	PathTuple paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
	SubpathTuple whole = new SubpathTuple(paths);
	System.out.println(whole);

	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(whole, topoOrder);

	for(SubpathTuple st : topoOrder) {
	    //System.out.println("Generating formulae");
	    st.fillVariables();
	}

	for(SubpathTuple st : topoOrder) {
	    for (UnitEdge e : st.getInEdges()) {
		if(e.getFormula() == null ) {
		    System.out.println("Null formula for " + e + " with head ");
		    System.out.println(st.intervals());
		    if(e instanceof UnitSingleEdge) {
			System.out.println("Unit: " + ((UnitSingleEdge) e).getStmt());
		    }
		}
	    }
	}

	for(int i = 0; i < topoOrder.size(); i++) {
	    ArrayList<BoolExpr> contexts = new ArrayList<BoolExpr>();
	    SubpathTuple cur = topoOrder.get(i);
	    //System.out.println("current: " + cur);
	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple other = topoOrder.get(j);
		if(cur.interleavable(other)) {
		    //System.out.print(other.intervals() + " ");
		    BoolExpr conjunct = other.makeForwardContext(cur);
		    //System.out.println(other.intervals());
		    //System.out.println(conjunct + "\n");//yes, two newlines
		    contexts.add(conjunct);
		}
	    }

	    //System.out.println("Number of interleavable nodes: " + contexts.size());

	    BoolExpr context = con.mkAnd(contexts.toArray(new BoolExpr[0]));
	    try {
		cur.findInterpolant(context);
	    } catch (ErrorReachableException ex) {
		System.out.println(cur.intervals());
		for(int j = i + 1; j < topoOrder.size(); j++) {
		    SubpathTuple other = topoOrder.get(j);
		    if(cur.interleavable(other)) {
			System.out.print(other.intervals() + " ");
		    }
		}
		MakeGraph m = new MakeGraph(whole);
		m.writeToFile("outputSingleTestInterpolant.dot", m.INTERPOLANT_NAMES);

		System.out.println();
		throw ex;
	    }
	}
	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestInterpolant.dot", m.INTERPOLANT_NAMES);
    }

    public void testCallReturn() {
	Context con = Z3Converter.CONTEXT;

	System.out.println("\n=====Transform Output=========");

	int exitSite = 0;
	for(; exitSite < caller.size(); exitSite++) {
	    if(hasInvoke(caller.get(exitSite))) {
		caller.addAll(exitSite + 1, called);
		break;
	    }
	}

	Path path1 = new Path(caller, "f");
	path1.addMatch(exitSite, exitSite + called.size() + 1);
	Path path2 = new Path(other, "th");

	if(!hasInvoke(path1.get(exitSite)) || !isReturn(path1.get(exitSite + called.size()))) {
	    System.out.print("Put the matching relation wrong: ");
	    System.out.print("points to ");
	    System.out.print(path1.get(exitSite));
	    System.out.print(" and ");
	    System.out.println(path1.get(exitSite + called.size()));
	}

	PathTuple paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
	UnwindingContext unwindCont = new UnwindingContext(callerGraph, 2);
	SubpathTuple whole = new SubpathTuple(paths, unwindCont);
	System.out.println(whole);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(whole, topoOrder);

	System.out.println("Number of path pairs: " + topoOrder.size());
	//System.out.println("Before generating paths ");
	for(SubpathTuple st : topoOrder) {
	    //System.out.println("Generating formulae");
	    st.fillVariables();
	}

	for(SubpathTuple st : topoOrder) {
	    for (UnitEdge e : st.getInEdges()) {
		if(e.getFormula() == null ) {
		    System.out.println("Null formula for " + e + " with head ");
		    System.out.println(st.intervals());
		    if(e instanceof UnitSingleEdge) {
			System.out.println("Unit: " + ((UnitSingleEdge) e).getStmt());
		    }
		}
	    }
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputPlainTestInterpolant.dot", m.SUFFIX_LENGTH_NAMES);



	for(int i = 0; i < topoOrder.size(); i++) {
	    ArrayList<BoolExpr> contexts = new ArrayList<BoolExpr>();
	    SubpathTuple cur = topoOrder.get(i);

	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple other = topoOrder.get(j);
		if(cur.interleavable(other)) {
		    contexts.add(other.makeForwardContext(cur));
		}
	    }

	    //System.out.println("Number of interleavable nodes: " + contexts.size());

	    BoolExpr context = con.mkAnd(contexts.toArray(new BoolExpr[0]));
	    try {
		cur.findInterpolant(context);
		/* if(cur.intervals().equals("[0, 2][0, 4]")) {
		    Solver slvr = con.mkSolver();
		    slvr.add(context);
		    Status stat = slvr.check();
		    if(stat != Status.UNSATISFIABLE) {
			System.out.println(cur.summary() + " interpolant for SAT context.");
		    }

		    Expr[] args = context.getArgs();
		    System.out.println("context size: " + args.length);
		    for(Expr arg : args) {
			slvr = con.mkSolver();
			slvr.add((BoolExpr) arg);
			System.out.println("Arg: " + slvr.check());
			System.out.println(arg);
			System.out.println();
		    }
		} */
	    } catch (ErrorReachableException ex) {
		System.out.println(cur.intervals());
		for(int j = i + 1; j < topoOrder.size(); j++) {
		    SubpathTuple other = topoOrder.get(j);
		    if(cur.interleavable(other)) {
			System.out.print(other.intervals() + " ");
		    }
		}
		m.writeToFile("outputTestInterpolant.dot", m.INTERPOLANT_NAMES);

		System.out.println();
		throw ex;
	    }
	}

	m.writeToFile("outputTestInterpolant.dot", m.INTERPOLANT_NAMES);
	//System.out.println("After generating paths");

    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestInterpolant()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "SimpleInstanceMethodCall",
	    /* "NoDivideByZeroTest"*/};
	soot.Main.main(sootArgs);
    }
}
