/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestMethodPairs extends BodyTransformer {
    ExceptionalUnitGraph foo = null;
    ExceptionalUnitGraph other = null;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private PathTuple paths;

    private TestMethodPairs() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( body.getMethod().getName().equals("foo")) {
	    foo = new ExceptionalUnitGraph(body);
	}

	if( body.getMethod().getName().equals("other")) {
	    other = new ExceptionalUnitGraph(body);
	}

	if(other == null || foo == null) {
	    return;
	}
	System.out.println("Here");
	ExceptionalUnitGraph[] cfgs = {foo, other};
	List<ExceptionalUnitGraph> cfgsList = Arrays.asList(cfgs);
	InterprocAnalysis ia = new InterprocAnalysis(cfgsList, AnalysesEnum.NONE);
	for(UnwindingContext uCon : ia.getInitContexts()) {
	    System.out.println(uCon);
	    System.out.println();
	}

	other = null;
	//int count = ia.getDisjoinInterpolantsCalled();
	//System.out.println("disjoinInterpolants called " + count + " times.");

	
	//DFS(uCon);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestMethodPairs()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "ReturnMethodCalls"};
	soot.Main.main(sootArgs);
    }
}
