/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestNewFindPath extends BodyTransformer {
    private boolean verbose = false;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private PathTuple paths;

    private TestNewFindPath() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("foo")) {
	    return;
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	testMaxUsage(g);

    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {
	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    public static void testMaxUsage(ExceptionalUnitGraph g) {
	Unit head = g.getHeads().get(0);
	Unit tail = g.getTails().get(0);
	tail = g.getSuccsOf(head).get(0);
	tail = g.getSuccsOf(tail).get(0);
	head = tail;
	tail = g.getSuccsOf(tail).get(0);
	System.out.println("Head: " + head);
	System.out.println("Tail: " + tail);

	HashSet<SootField> usd = new HashSet<SootField>();
	HashSet<SootField> written = new HashSet<SootField>();
	for(ValueBox vb : g.getBody().getDefBoxes()) {
	    soot.Value v = vb.getValue();
	    if(v instanceof soot.jimple.FieldRef) {
		SootField used = ((soot.jimple.FieldRef) v).getField();
		written.add(used);
		break;
	    }
	}


	Cons hc = UnwindingContext.findPathWithAccesses(head, tail, g, null, usd, written, usd);
	//System.out.println("Parents: " + reverse);
	System.out.println("Must write: " + written);

	List<Unit> path = new ArrayList<Unit>();
	for(Cons c = hc; c != null; c = c.cdr) {
	    path.add(c.car);
	}


	System.out.println(path);

	/* UnwindingContext uCon = new UnwindingContext(g, 2);

	visited.add(uCon);
	DFS(uCon); */
    }

    public void DFS(UnwindingContext uCon) {
	PathTuple wholePaths = uCon.pathTuple();
	SubpathTuple whole = new SubpathTuple(wholePaths);

	for(int i = 0; i < 2; i++) {
	    Path path = whole.getPaths().getPath(i);
	    checkMatching(path);
	}

	for(int t = 0; t < 2; t++) {
	    Path path = whole.getPaths().getPath(t);
	    for (int i = path.length() - 1; i > 0; i--) {
		if(path.hasPredecessor(i) < 0) {//is not a return site
		    Unit succ = path.get(i);
		    Unit pred = path.get(i - 1);
		    if(!hasInvoke(pred)) {//is not a call site
			ExceptionalUnitGraph subG = uCon.getCFGs().get(succ);

			if(! subG.getPredsOf(succ).contains(pred)) {
			    System.out.println("Predecessors of " + succ +
				    " at " + i + " did not contain " + pred);
			}
		    }
		}
	    }
	}

	for(UnwindingContext next : uCon.expand()) {
	    for(int t = 0; t < 2; t++) {
		List<Unit> context = next.getContext(t);
		for(int i = 0; i < context.size() - 1; i++) {
		    Unit pred = context.get(i);
		    Unit succ = context.get(i + 1);
		    ExceptionalUnitGraph g = UnwindingContext.getCFGs().get(pred);

		    if(g.getPredsOf(pred).contains(succ)) {
			System.out.println("Context had units in wrong order.");
			System.out.println("From " + uCon);
		    }
		}
	    }
	    if( visited.add(next) && visited.size() <= 41) {
		DFS(next);
	    }
	}
    }

    public void checkMatching(Path path) {
	for(int j = path.length() - 1; j >= 0; j--) {
	    int i = path.hasPredecessor(j);
	    if(i >= 0) {
		//Note this uses the hasInvoke here, not from UnwindingContext
		if(!hasInvoke(path.get(i)) || !isReturn(path.get(j - 1))) {
		    /* String name = UnwindingContext.hasInvoke(path.get(i))
			.getMethod()
			.getName(); */
		    System.out.print("Put the matching relation wrong for "
			    /* + name */ + ": ");
		    System.out.print("points to ");
		    System.out.print(path.get(i));
		    System.out.print(" and ");
		    System.out.println(path.get(j - 1));
		    System.out.println("Path: " + path);
		    System.out.println();
		}
	    }
	}
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	if(verbose) {
	    System.out.println(u);
	}
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestNewFindPath()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "LoopingToo", };
	soot.Main.main(sootArgs);
    }
}
