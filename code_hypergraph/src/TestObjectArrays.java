/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.Transform;
import java.util.Map;
import java.util.LinkedList;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;
import soot.SootMethod;
import soot.jimple.InvokeExpr;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;


public class TestObjectArrays extends BodyTransformer {
    private TestObjectArrays() {
    }
    
    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( !body.getMethod().getName().equals("main")) {
	    return;
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit head = g.getHeads().get(0);
	Unit tail = g.getTails().get(0);

	UnwindingContext uCon = new UnwindingContext(g, head, tail, 2);
	SubpathTuple whole = new SubpathTuple(uCon.pathTuple(), uCon);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, topoOrder);

	System.out.println("# of subpaths: " + topoOrder.size());
	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestObjectArrays.dot", MakeGraph.SUFFIX_LENGTH_NAMES);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestObjectArrays()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "ObjectArrays"};
	soot.Main.main(sootArgs);

    }

}
