/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;

import java.util.Map;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import com.microsoft.z3.Solver;

public class TestObjectConversion extends BodyTransformer {
    private TestObjectConversion() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	List<Unit> units = g.getExtendedBasicBlockPathBetween(entrance, exit);
	Path path = new Path(units, "p");
	PathTuple paths = new PathTuple(Arrays.asList(new Path[] {path}));

	System.out.println("\n=====Transform Output " + body.getMethod().getSubSignature() +"=========");

	SubpathTuple whole = new SubpathTuple(paths);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(whole, topoOrder);

	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();

	    for (UnitEdge e : st.getInEdges()) {
		System.out.println("Unit: " + ((UnitSingleEdge) e).getStmt());
		System.out.println("Formula: " + e.getFormula());
		if (e.getFormula() != null) {//this can happen, for statements we cannot handle
		    /* Solver s = Z3Converter.CONTEXT.mkSolver();
		    s.add(e.getFormula());
		    System.out.println(s.check()); */
		}
	    }
	}

	System.out.println("\n-----Transform Output---------");
    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {
	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestObjectConversion()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "ObjectCreation" };
	soot.Main.main(sootArgs);
    }
}
