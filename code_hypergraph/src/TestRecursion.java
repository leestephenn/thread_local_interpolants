/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestRecursion extends BodyTransformer {
    private boolean verbose = false;
    private HashSet<UnwindingContext> visited = new HashSet<UnwindingContext>();

    private PathTuple paths;

    private TestRecursion() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("twice")) {
	    return;
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit head = g.getHeads().get(0);

	Unit tail = null;
	for(Unit ret: g.getTails()) {
	    if(ret.getBoxesPointingToThis().isEmpty()) {
		tail = ret;
	    }
	}
	UnwindingContext uCon = new UnwindingContext(g, head, tail, 2);
	System.out.println("Context: " + uCon);
	PathTuple pt = uCon.pathTuple();
	System.out.println(pt);
	SubpathTuple whole = new SubpathTuple(pt, uCon);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, topoOrder);

	System.out.println("# of subpath tuples: " + topoOrder.size());
	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestRecursion.dot", m.new PrePostVarsProvider());


	/* InterprocAnalysis ia = new InterprocAnalysis(g);
	ia.unwind();

	//int count = ia.getDisjoinInterpolantsCalled();
	//System.out.println("disjoinInterpolants called " + count + " times.");

	Map<UnwindingContext, BoolExpr> summaries = ia.summaries();
	System.out.println("Summary size: " + summaries.size()); */

	
	//DFS(uCon);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestRecursion()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "Recursion"};
	soot.Main.main(sootArgs);
    }
}
