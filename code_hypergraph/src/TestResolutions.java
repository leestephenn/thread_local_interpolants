/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.Transform;
import java.util.Map;
import java.util.LinkedList;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;
import soot.SootMethod;
import soot.jimple.InvokeExpr;
import java.util.List;
import java.util.Set;


public class TestResolutions extends BodyTransformer {
    private TestResolutions() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	System.out.print(body.getMethod().getDeclaringClass() + ".");
	System.out.println(body.getMethod().getName());

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	for(Unit u : g) {
	    InvokeExpr call = UnwindingContext.hasInvoke(u);
	    if(call != null) {
		System.out.println("Call: " + call);
		List<ExceptionalUnitGraph> posses = UnwindingContext.
		    resolutions(call, false);
		for(ExceptionalUnitGraph poss : posses) {
		    System.out.println(poss.getBody().getMethod().getSignature());
		}
		System.out.println();
	    }
	}
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestResolutions()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "SubclassA", "SubclassB", "SuperClass"};
	soot.Main.main(sootArgs);

    }

}
