/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.Scene;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.ValueBox;

import soot.SootMethod;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;

import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

public class TestRestrictedSubpathGen extends BodyTransformer {
    private List<Unit> foo;
    private List<Unit> bar;
    private List<Unit> baz;
    private List<Unit> other;
    private List<Unit> other2;
    private PathTuple paths;

    private List<Unit> single;

    private TestRestrictedSubpathGen() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	System.out.println(body.getMethod().getName());

	initComplex(body);

	if(paths == null) {
	    return;
	}

	SubpathTuple whole = new SubpathTuple(paths);
	System.out.println("Whole:\n" + whole.subpathsString());
	System.out.println();

	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(whole, topoOrder);
	System.out.println("Number of subpaths: " + topoOrder.size());

	for(int i = 0; i < topoOrder.size(); i++) {
	    SubpathTuple st = topoOrder.get(i);
	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple lat = topoOrder.get(j);
		for(UnitEdge e : lat.getInEdges()) {
		    for(SubpathTuple p : e.getTails()) {
			if(p.equals(st) && p != st) {
			    System.out.print("Found != preds ");
			    System.out.println(st + " -> " + lat);
			}
		    }
		}
	    }
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestRestrictedSubpathGen.dot", m.SUFFIX_LENGTH_NAMES);

	boolean foundNonRestricted = false;
	for(SubpathTuple st : topoOrder) {
	    List<UnitEdge> inEdges = st.getInEdges();
	    if(inEdges.size() >= 1) {
		boolean twoSingle = false;
		for(UnitEdge e : inEdges) {
		   if(e instanceof UnitSingleEdge) {
		       if(twoSingle) {
			   foundNonRestricted = true;
		       }
		       twoSingle = true;
		   }
		}
	    }
	}

	//This is not necessarily an error, just not what I think I am testing.
	if (!foundNonRestricted) {
	    System.out.println("Found no edges with multiple single in-edges.");
	}

	for(SubpathTuple st : topoOrder) {
	    //System.out.println("Generating formulae");
	    //System.out.println("subpath tuple " + st.intervals());
	    try {
		st.fillVariables();
	    } catch (RuntimeException ex) {
		System.out.println("Head: " + st);
		throw ex;
	    }
	}
    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {
	List<SubpathTuple> preds = st.genSubpaths();
	if(preds.isEmpty() && ! st.isEmpty()) {
	    System.out.println("Non-empty subpath " + st + " had no predecessors.");
	}
	//System.out.println("Preds: " + preds);
	for (SubpathTuple p : preds) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    private void initSimple(Body body) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("bar")) {
	    bar = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    //System.out.println("Found foo.");
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    foo = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	//System.out.println((bar == null)  + " " + (foo == null) + " " +  (other == null));
	if(bar == null || foo == null || other == null) {
	    return;
	}

	System.out.println("\n=====Transform Output=========");

	int i = 0;
	for(; i < foo.size(); i++) {
	    if(hasInvoke(foo.get(i))) {
		foo.addAll(i + 1, bar);
		break;
	    }
	}

	Path path1 = new Path(foo, "f");
	path1.addMatch(i, i + bar.size() + 1);
	Path path2 = new Path(other, "th");

	if(!hasInvoke(path1.get(i)) || !isReturn(path1.get(i + bar.size()))) {
	    System.out.print("Put the matching relation wrong: ");
	    System.out.print("points to ");
	    System.out.print(path1.get(i));
	    System.out.print(" and ");
	    System.out.println(path1.get(i + bar.size()));
	}

	paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    private void initComplex(Body body) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	//System.out.println("Method name: " + body.getMethod().getName());
	if(body.getMethod().getName().equals("bar")) {
	    bar = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    Unit error = findErrorNode(g);
	    exit = g.getPredsOf(error).get(0);
	    foo = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other2")) {
	    other2 = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("baz")) {
	    baz = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	//System.out.println((bar == null) + ", " + (foo == null) + ", " + (baz == null) + ", " + (other == null) + ", " + (other2 == null));
	if(bar == null || foo == null || baz == null ||
		other == null || other2 == null) {
	    return;
	}

	//System.out.println("Woozle");

	System.out.println("\n=====Transform Output=========");

	int callBar = 0;
	for(; callBar < foo.size(); callBar++) {
	    if(hasInvoke(foo.get(callBar))) {
		foo.addAll(callBar + 1, bar);
		break;
	    }
	}

	int callBaz = callBar + 1;
	for(; callBaz < foo.size(); callBaz++) {
	    if(hasInvoke(foo.get(callBaz))) {
		foo.addAll(callBaz + 1, baz);
		break;
	    }
	}

	Path path1 = new Path(foo, "f");
	path1.addMatch(callBaz, callBaz + baz.size() + 1);
	path1.addMatch(callBar, callBar + bar.size()  + baz.size() + 1);
	//System.out.println((callBaz + baz.size() + 1) + " " + (callBar + bar.size() + 1));
	//System.out.println("Path1: " + path1);

	int callO = 0;
	//verbose = true;
	for(; callO < other.size(); callO++) {
	    if(hasInvoke(other.get(callO))) {
		//System.out.println("Found invoke in other.");
		//verbose = false;
		other.addAll(callO + 1, other2);
		break;
	    }
	}
	//verbose = false;
	Path path2 = new Path(other, "th");
	path2.addMatch(callO, callO + other2.size() + 1);
	System.out.println("Path2: " + path2);

	System.out.println("Path1 match: " + path1.matchRelation());
	System.out.println("Path2 match: " + path2.matchRelation());

	paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    public void initSingle(Body body) {
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("single")) {
	    single = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	if(single == null || other == null) {
	    return;
	}

	Path path1 = new Path(single, "s");
	Path path2 = new Path(other, "th");
	this.paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    public void buildMultiMethods() {
	int exitSite = 0;
	for(; exitSite < foo.size(); exitSite++) {
	    if(hasInvoke(foo.get(exitSite))) {
		foo.addAll(exitSite + 1, bar);
		break;
	    }
	}

	Path path1 = new Path(foo, "f");
	path1.addMatch(exitSite, exitSite + bar.size() + 1);
	Path path2 = new Path(other, "th");

	if(!hasInvoke(path1.get(exitSite)) || !isReturn(path1.get(exitSite + bar.size()))) {
	    System.out.print("Put the matching relation wrong: ");
	    System.out.print("points to ");
	    System.out.print(path1.get(exitSite));
	    System.out.print(" and ");
	    System.out.println(path1.get(exitSite + bar.size()));
	}

	paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestRestrictedSubpathGen()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "InstanceMethodCalls",
	    /* "NoDivideByZeroTest"*/};
	soot.Main.main(sootArgs);
    }
}
