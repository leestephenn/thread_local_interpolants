/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.Scene;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.ValueBox;

import soot.SootMethod;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;

import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

public class TestSubpathContext extends BodyTransformer {

    private TestSubpathContext() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().equals("foo")) {
	    return;
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	InterprocAnalysis interprocAnalysis = new InterprocAnalysis(g, AnalysesEnum.NONE);
	UnwindingContext full = new UnwindingContext(g, 2);

	PathTuple paths = full.pathTuple();
	SubpathTuple whole = new SubpathTuple(paths, full);
	System.out.println(whole.subpathsString());
	for(int i = 0; i < paths.numThreads; i++) {
	    System.out.println(paths.getPath(i).matchRelation());
	}

	/* ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	InterprocAnalysis.DFS(whole, topoOrder);
	for(int i = 0; i < topoOrder.size(); i++) {
	    SubpathTuple st = topoOrder.get(i);
	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple lat = topoOrder.get(j);
		for(UnitEdge e : lat.getInEdges()) {
		    for(SubpathTuple p : e.getTails()) {
			if(p.equals(st) && p != st) {
			    System.out.print("Found != preds ");
			    System.out.println(st + " -> " + lat);
			}
		    }
		}
	    }
	}

	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("outputTestSubpathContext.dot", MakeGraph.SUFFIX_LENGTH_NAMES);
	System.out.println("Size: " + topoOrder.size());

	for(SubpathTuple st : topoOrder) {
	    for(UnitEdge e : st.getInEdges()) {
		if(e instanceof UnitHyperEdge) {
		    UnitHyperEdge he = (UnitHyperEdge) e;

		    System.out.println(st);
		    System.out.println(st.getUnwindingContext());
		    System.out.println(he.getCallTail());
		    System.out.println(he.getCallTail().getUnwindingContext());
		    System.out.println(he.getExitTail());
		    System.out.println(he.getExitTail().getUnwindingContext());
		    System.out.println();
		}
	    }
	} */
    }

    //This does not test calls
    public static void testInternal(ArrayList<SubpathTuple> topoOrder) {
	for(SubpathTuple st : topoOrder) {
	    UnwindingContext uCon = st.getUnwindingContext();
	    //System.out.println(uCon);
	    
	    for(int t = 0; t < 2; t++) {
		int end = st.getStartEnd(t)[1];
		boolean atEnd = (end == st.getPaths().length(t));

		if(!atEnd &&  // this has to be first
			!uCon.getExpandUnit(t).equals(st.getNextUnit(t))) {
		    System.out.println("The expand point for a context " +
			    "was not equal to the last unit for " + st);
		    System.out.println("Expand: " + uCon.getExpandUnit(t));
		    System.out.println("Last: " + st.getNextUnit(t));
		    System.out.println();
		}
	    }
	}

	//m.writeToFile("outputTestSubpathContext.dot", m.INTERPOLANT_NAMES);
	//System.out.println("After generating paths");

    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestSubpathContext()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "InstanceMethodCalls" };
	soot.Main.main(sootArgs);
    }
}
