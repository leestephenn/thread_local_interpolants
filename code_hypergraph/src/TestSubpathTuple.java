/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import soot.Unit;

class StringUnit extends soot.AbstractUnit {
    private final String name;

    public StringUnit(String name) {
	this.name = name;
    }

    public String toString() {
	return name;
    }

    public void toString(soot.UnitPrinter up) {
	//return toString();
    }

    public boolean branches() {
	return false;
    }

    public boolean fallsThrough() {
	return true;
    }

    public Object clone() {
	return new StringUnit(new String(name));
    }
}


public class TestSubpathTuple {
    public static void main(String[] args) {
	//testExpandReturn();
	testDFS();
    }

    public static void testExpandReturn() {
	String[] pathNames = {"i2 = 0", "j2 = 1", "k2 = 2"};
	List<String> pathStrings = Arrays.asList(pathNames);
	ArrayList<Unit> path2 = new ArrayList<Unit>(pathStrings.size());
	for(String unit : pathStrings) {
	    path2.add(new StringUnit(unit));
	}

	pathNames = new String[]{"i1 = 0", "j1 = bar(7)", "s1 = 2", "return 7", "l1 = 17"};
	pathStrings = Arrays.asList(pathNames);
	ArrayList<Unit> path1 = new ArrayList<Unit>(pathStrings.size());
	for(String unit : pathStrings) {
	    path1.add(new StringUnit(unit));
	}

	ArrayList<Path> paths = new ArrayList<Path>();
	Path path1path = new Path(path1, "first");
	path1path.addMatch(1, path1path.length() - 1);
	paths.add(path1path);
	paths.add(new Path(path2, "second"));
	PathTuple pt = new PathTuple(paths);

	SubpathTuple whole = new SubpathTuple(pt);
	List<SubpathTuple> news = whole.genSubpaths();

	print(news);

	System.out.println("Return subpaths");
	SubpathTuple atRet = news.get(0);
	news = atRet.genSubpaths();

	print(news);
    }

    public static void print(List<SubpathTuple> list) {
	for(SubpathTuple st : list) {
	    System.out.println(st);
	    //System.out.println(st.hashCode());
	    System.out.println();
	}
    }

    public static void testDFS() {

	String[] pathNames = {"i2 = 0", "j2 = 1", "k2 = 2"};
	List<String> pathStrings = Arrays.asList(pathNames);
	ArrayList<Unit> path2 = new ArrayList<Unit>(pathStrings.size());
	for(String unit : pathStrings) {
	    path2.add(new StringUnit(unit));
	}

	pathNames = new String[]{"i1 = 0", "j1 = bar(7)", "s1 = 2", "return 7", "l1 = 17"};
	pathStrings = Arrays.asList(pathNames);
	ArrayList<Unit> path1 = new ArrayList<Unit>(pathStrings.size());
	for(String unit : pathStrings) {
	    path1.add(new StringUnit(unit));
	}

	ArrayList<Path> paths = new ArrayList<Path>();
	Path path1path = new Path(path1, "first");
	path1path.addMatch(1, path1path.length() - 1);
	paths.add(path1path);
	paths.add(new Path(path2, "second"));
	PathTuple pt = new PathTuple(paths);

	SubpathTuple whole = new SubpathTuple(pt);
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();
	InterprocAnalysis.DFS(whole, topoOrder);

	for(SubpathTuple st : topoOrder) {
	    for(UnitEdge e : st.getInEdges()) {
		//This just checks VarTail, not every tail. I can be more thorough if necessary.
		if (e instanceof UnitSingleEdge) {
		    SubpathTuple tail = ((UnitSingleEdge) e).getTail();
		    if(!topoOrder.contains(tail)) {
			System.out.println("Predecessor " + tail + " was not in the topological order.");
		    }
		}
		if (e instanceof UnitHyperEdge) {
		    UnitHyperEdge he = (UnitHyperEdge) e;

		    SubpathTuple tail = he.getExitTail();
		    if(!topoOrder.contains(tail)) {
			System.out.println("Predecessor " + tail  + " was not in the topological order.");
		    }
		    if (!in(tail, st)) {
			System.out.println(tail + " was not in " + st);
		    }

		    tail = he.getCallTail();
		    if(!topoOrder.contains(tail)) {
			System.out.println("Predecessor " + tail + " was not in the topological order.");
		    }
		    if (!in(tail, st)) {
			System.out.println(tail + " was not in " + st);
		    }
		}
	    }
	}

	for(int i = 0; i < topoOrder.size(); i++) {
	    SubpathTuple st = topoOrder.get(i);
	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple lat = topoOrder.get(j);
		for(UnitEdge e : lat.getInEdges()) {
		    for(SubpathTuple p : e.getTails()) {
			if(p.equals(st) && p != st) {
			    System.out.print("Found != preds ");
			    System.out.println(st + " -> " + lat);
			}
		    }
		}
	    }
	}


	/* for(SubpathTuple st : topoOrder) {
	    if(st.getOutEdges().size() > 1) {
		System.out.println("Found a tuple " + st.toString().replace("\n"," ")
			+ " with more than one out-edge. Good.");
	    }
	} */

	//print(topoOrder);

	System.out.println("# of Subpaths: " + topoOrder.size());
	MakeGraph m = new MakeGraph(whole);
	m.writeToFile("hyperOutputFirst.dot", MakeGraph.SUFFIX_LENGTH_NAMES);
    }

    private static boolean in(SubpathTuple sub, SubpathTuple sup) {
	for(int t = 0; t < sub.getNumThreads(); t++) {
	    int[] subInt = sub.getStartEnd(t);
	    int[] supInt = sup.getStartEnd(t);
	    if(!(subInt[0] >= supInt[0] && subInt[1] <= supInt[1])) {
		return false;

	    }
	}
	return true;
    }


    private static void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }
}
