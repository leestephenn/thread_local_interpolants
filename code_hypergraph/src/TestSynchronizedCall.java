/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestSynchronizedCall extends BodyTransformer {

    private TestSynchronizedCall() {
    }

    public static Cons buildConsList(ExceptionalUnitGraph g, Cons tail) {
        Unit u = g.getTails().get(0);
        Cons c = new Cons(u, tail);
        while(!g.getPredsOf(u).isEmpty()) {
            u = g.getPredsOf(u).get(0);
            c = new Cons(u, c);
        }
        return c;
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

        if(!body.getMethod().getName().equals("foo")) {
            return;
        }

        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
        Cons c = buildConsList(g, null);

        //print cons list
        ArrayList<Unit> toPrint = new ArrayList<Unit>(g.size());
        for(Cons h = c; h != null; h = h.cdr) {
            toPrint.add(h.car);
        }
        System.out.println(toPrint);

        Cons back = null;
        for(Cons h = c; h != null; h = h.cdr) {
            InvokeExpr inv = UnwindingContext.hasInvoke(h.car);
            if(inv != null) {
                ExceptionalUnitGraph newG =
                    UnwindingContext.resolutions(inv, false).get(0);
                Cons cc = buildConsList(newG, h.cdr);
                back = h.cdr;
                h.cdr = cc;
            }
        }
        System.out.println("Back: " + back.car);

        //print cons list
        toPrint = new ArrayList<Unit>(g.size());
        for(Cons h = c; h != null; h = h.cdr) {
            toPrint.add(h.car);
        }
        System.out.println(toPrint);

        for(Cons h = c; h != null; h = h.cdr) {
            InvokeExpr inv = UnwindingContext.hasInvoke(h.car);
            if(inv != null) {
                UnwindingContext.makeSynchStatements(inv, h, back);
            }
        }

        //print cons list
        toPrint = new ArrayList<Unit>(g.size());
        for(Cons h = c; h != null; h = h.cdr) {
            toPrint.add(h.car);
        }
        System.out.println(toPrint);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestSynchronizedCall()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/",
			     "SynchronizedCallTest", };
	soot.Main.main(sootArgs);
    }
}
