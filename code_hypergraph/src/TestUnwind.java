/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.SootMethod;
import soot.Scene;
import soot.Unit;
import soot.ValueBox;
import soot.Local;
import soot.SootField;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;
import java.util.HashSet;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;

public class TestUnwind extends BodyTransformer {
    private boolean verbose = false;
    private Body body1 = null;
    private Body body2 = null;

    private PathTuple paths;

    private TestUnwind() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	if( ! body.getMethod().getName().contains("foo")) {
	    return;
	}
	singleMethod(body);
    }

    public void twoMethods(Body body) {
	if(body.getMethod().getName().equals("noArgTransfer1")) {
	    body1 = body;
	} else if(body.getMethod().getName().equals("noArgTransfer2")) {
	    body2 = body;
	}

	if(body1 != null && body2 != null) {
	    ExceptionalUnitGraph g1 = new ExceptionalUnitGraph(body1);
	    ExceptionalUnitGraph g2 = new ExceptionalUnitGraph(body2);

	    InterprocAnalysis ia = new InterprocAnalysis(Arrays.asList(new ExceptionalUnitGraph[]{g1, g2}),
		    AnalysesEnum.DEADLOCK);
	    ia.unwind();
	}
    }

    public void singleMethod(Body body) {
	System.out.println("Analyzing " + body.getMethod().getSignature());
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	//soot.Scene.v().addBasicClass("ThreadItpsAnalysisError", soot.SootClass.SIGNATURES);
	InterprocAnalysis ia = new InterprocAnalysis(g, AnalysesEnum.DEADLOCK);
	ia.unwind();

	final com.microsoft.z3.Context ctx = Z3Converter.CONTEXT;

	Map<UnwindingContext, BoolExpr> summaries = ia.summaries();
	System.out.println("Summary size: " + summaries.size());

	/*
	SubpathTuple whole = ia.getDisjoinInterpolantsArg();
	//MakeGraph m = new MakeGraph(whole);
	//m.writeToFile("outputUnwindingIntervals.dot", MakeGraph.SUFFIX_LENGTH_NAMES);

	ArrayDeque<SubpathTuple> queue = new ArrayDeque<SubpathTuple>();
	HashSet<SubpathTuple> visited = new HashSet<SubpathTuple>();

	queue.add(whole);
	visited.add(whole);
	while ( !queue.isEmpty()) {
	    SubpathTuple cur = queue.remove();
	    BoolExpr curSumm = cur.summary();//summaries.get(cur.getUnwindingContext());
	    BoolExpr aux = cur.getAuxilliaryConstraints();
	    for(UnitEdge e : cur.getInEdges()) {
		BoolExpr edgeForm = e.getBackwardPath();
		BoolExpr newConsts = cur.makeNewConstEqs(e);

		BoolExpr prem = ctx.mkAnd(edgeForm, newConsts);
		if(aux != null) {
		    prem = ctx.mkAnd(prem, aux);
		}
		if( !InterprocAnalysis.entails(prem, curSumm)) {
		    System.out.println("Problem: " + e.getTails().get(0) + " " + e.ID + " " + cur);
		    System.out.println(prem + "\n" + curSumm);
		}
		if( visited.add(e.getTails().get(0))) {
		    queue.add(e.getTails().get(0));
		}
	    }
	} */
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestUnwind()));
	String[] sootArgs = {"-pp", "-cp", "playWithSoot/",
			     "LibraryCalls"};
	soot.Main.main(sootArgs);
    }
}
