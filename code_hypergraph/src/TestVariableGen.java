/** Copyright (c) Stephen N. Lee. */

import edu.gatech.threadlocalitps.*;

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.Local;
import soot.ValueBox;
import soot.SootField;

import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import soot.jimple.ReturnVoidStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeExpr;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Arrays;

public class TestVariableGen extends BodyTransformer {
    private List<Unit> called;
    private List<Unit> caller;
    private List<Unit> other;

    private TestVariableGen() {
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	Unit exit = g.getTails().get(0);
	Unit entrance = g.getHeads().get(0);

	if(body.getMethod().getName().equals("bar")) {
	    called = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("foo")){
	    caller = g.getExtendedBasicBlockPathBetween(entrance, exit);
	} else if (body.getMethod().getName().equals("other")) {
	    other = g.getExtendedBasicBlockPathBetween(entrance, exit);
	}

	if(called != null && caller != null && other != null) {

	    System.out.println("\n=====Transform Output=========");

	    int callSite = 0;
	    for(; callSite < caller.size(); callSite++) {
		if(hasInvoke(caller.get(callSite))) {
		    caller.addAll(callSite + 1, called);
		    break;
		}
	    }

	    Path path1 = new Path(caller, "f");
	    path1.addMatch(callSite, callSite + called.size() + 1);
	    Path path2 = new Path(other, "th");

	    if(!hasInvoke(path1.get(callSite)) ||
		    !isReturn(path1.get(callSite + called.size()))) {
		System.out.print("Put the matching relation wrong: ");
		System.out.print("points to ");
		System.out.print(path1.get(callSite));
		System.out.print(" and ");
		System.out.println(path1.get(callSite + called.size()));
	    }

	    PathTuple paths = new PathTuple(Arrays.asList(new Path[]{path1, path2}));
	    SubpathTuple whole = new SubpathTuple(paths);
	    ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	    DFS(whole, topoOrder);

	    System.out.println("Number of path pairs: " + topoOrder.size());

	    //System.out.println("Before generating paths ");
	    for(SubpathTuple st : topoOrder) {
		//System.out.println("Generating formulae");
		st.fillVariables();
	    }
	    //System.out.println("After generating paths");

	    UnitHyperEdge he = null;
	    for(SubpathTuple head : topoOrder) {
		for(UnitEdge ue : head.getInEdges()) {
		    if (ue instanceof UnitSingleEdge) {
			testSingleEdge(head, (UnitSingleEdge) ue);
		    } else if (ue instanceof UnitHyperEdge) {
			testHyperEdge(head, (UnitHyperEdge) ue);
		    }
		    testFormulae(ue);
		}
	    }

	}

    }

    public void testSingleEdge(SubpathTuple head, UnitSingleEdge e) {
	SubpathTuple tail = e.getTail();

	if(!contains(tail.getPreFields(),
		    head.getPreFields())) {
	    System.out.println("head did not contain tail pre shared vars.");
	}

	if(!contains(e.cvtr().getPreFields(),
		    head.getPreFields())) {
	    System.out.println("head did not contain cvtr pre shared vars.");
	}

	if(!contains(tail.getPostFields(),
		    head.getPostFields())) {
	    System.out.println("head did not contain tail post shared vars.");
	}

	if(!contains(e.cvtr().getPostFields(),
		    head.getPostFields())) {
	    System.out.println("head did not contain cvtr post shared vars.");
	}

	List<HashMap<Local, Expr>> pre = head.getPreVars();
	for (int i = 0; i < pre.size(); i++) {
	    if(i != e.thread) {
		if(!pre.get(i).equals(
			    tail.getPreVars().get(i))) {
		    System.out.println("Pre-thread vars for other thread " + i + " were not equal");
			    }
	    } else {
		if (!contains(tail.getPreVars().get(i),
			    pre.get(i))) {
		    System.out.println("Pre-thread vars for edge thread " + i + " did not contain");
		}
	    }
	}
	List<HashMap<Integer, Expr>> params = head.getParams();
	for (int i = 0; i < params.size(); i++) {
	    if(i != e.thread) {
		if(!params.get(i).equals(
			    tail.getParams().get(i))) {
		    System.out.println("Params for other thread " + i + " were not equal");
			    }
	    } else {
		if (!containsParam(tail.getParams().get(i),
			    params.get(i))) {
		    System.out.println("Params for edge thread " + i + " did not contain");
		}
		//if (!containsParams(cvt
	    }
	}
	List<HashMap<Local, Expr>> post = head.getPostVars();
	for (int i = 0; i < post.size(); i++) {
	    if(i != e.thread) {
		if(!post.get(i).equals(
			    tail.getPostVars().get(i))) {
		    System.out.println("Post-thread vars for other thread " + i + " were not equal");
			    }
	    } else {
		if (!contains(tail.getPostVars().get(i),
			    post.get(i))) {
		    System.out.println("Post-thread vars for edge thread " + i + " did not contain");
		}
	    }
	}
    }

    public void testHyperEdge(SubpathTuple head, UnitHyperEdge e) {
	SubpathTuple call = e.getCallTail();
	SubpathTuple exit = e.getExitTail();

	if(!contains(call.getPreFields(),
		    head.getPreFields())) {
	    System.out.println("head did not contain call pre shared vars.");
	}

	if(!contains(e.cvtr().getPreFields(),
		    head.getPreFields())) {
	    System.out.println("head did not contain cvtr pre shared vars.");
	}

	if(!contains(call.getPostFields(),
		    head.getPostFields())) {
	    System.out.println("head did not contain call post shared vars.");
	}

	if(!contains(exit.getPostFields(),
		    head.getPostFields())) {
	    System.out.println("head did not contain exit post shared vars.");
	}

	if(!contains(e.cvtr().getPostFields(),
		    head.getPostFields())) {
	    System.out.println("head did not contain cvtr post shared vars.");
	}

	List<HashMap<Local, Expr>> pre = head.getPreVars();
	for (int i = 0; i < pre.size(); i++) {
	    if(i != e.thread) {
		if(!pre.get(i).equals(
			    exit.getPreVars().get(i))) {
		    System.out.println("Pre-thread vars for other thread " + i + " were not equal");
			    }
	    } else {
		if (!contains(pre.get(i),
			    exit.getPreVars().get(i))) {
		    System.out.println("Pre-thread vars for edge thread " + i + " did not contain");
		}
	    }
	}
	List<HashMap<Integer, Expr>> params = head.getParams();
	for (int i = 0; i < params.size(); i++) {
	    if(i != e.thread) {
		if(!containsParam(call.getParams().get(i), params.get(i))) {
		    System.out.println("Params for call other thread " + i + " were not contained");
		}
		if(!containsParam(exit.getParams().get(i), params.get(i))) {
		    System.out.println("Params for exit other thread " + i + " were not contained");
		}
	    } else {
		if (!containsParam(call.getParams().get(i),
			    params.get(i))) {
		    System.out.println("Params for edge thread " + i + " did not contain");
		}
	    }
	}
	List<HashMap<Local, Expr>> post = head.getPostVars();
	for (int i = 0; i < post.size(); i++) {
	    if(i != e.thread) {
		if(!contains(post.get(i), call.getPostVars().get(i))) {
		    System.out.println("Post-thread vars for call other thread " + i + " were not contained");
		}
		if(!contains(post.get(i), exit.getPostVars().get(i))) {
		    System.out.println("Post-thread vars for exit other thread " + i + " were not contained");
		}
	    } else {
		if (!contains(post.get(i), call.getPostVars().get(i))) {
		    System.out.println("Post-thread vars for call edge thread " + i + " did not contain");
		}
	    }
	}
    }

    public void testFormulae(UnitEdge e) {
	final Context con = Z3Converter.CONTEXT;
	final SubpathTuple head = e.getHead();

	for(Map.Entry<SootField, ArrayExpr> ent : head.getPostFields().entrySet()) {
	    /* if (e.getPostFields().equals(head.getPostFields())) {
		continue;
	    }  else {
		System.out.println(e.getPostFields() + " " + head.getPostFields());
	    } */

	    //else
	    BoolExpr eq = con.mkEq(ent.getValue(),
		    e.getPostFields().get(ent.getKey()));
	    eq = con.mkNot(eq);
	    BoolExpr edge = head.makeNewConstEqs(e);
	    eq = con.mkAnd(edge, eq);
	    Solver s = con.mkSolver();
	    s.add(eq);
	    Status stat = s.check();
	    if(!stat.equals(Status.UNSATISFIABLE)) {
		System.out.println("Edge equations: " + edge);
		System.out.println("Equation: " + eq);
		System.out.println("new const equations not right for local " +
			ent.getKey());
	    }
	}

	List<HashMap<Local, Expr>> eThreads = head.getPostVars();
	for (int t = 0; t < eThreads.size(); t++) {
	    for(Map.Entry<Local, Expr> ent : eThreads.get(t).entrySet()) {
		BoolExpr eq = con.mkEq(ent.getValue(),
			e.getPostVars().get(t).get(ent.getKey()));
		eq = con.mkNot(eq);
		BoolExpr edge = head.makeNewConstEqs(e);
		eq = con.mkAnd(edge, eq);
		Solver s = con.mkSolver();
		s.add(eq);
		Status stat = s.check();
		if(!stat.equals(Status.UNSATISFIABLE)) {
		    System.out.println("Equation: " + eq);
		    System.out.println("new const equations not right for thread  local " +
			    ent.getKey() + " for thread " + t);
		}
	    }
	}

	if(e instanceof UnitHyperEdge) {
	    for(Map.Entry<SootField, ArrayExpr> ent : head.getPreFields().entrySet()) {

		//else
		BoolExpr eq = con.mkEq(ent.getValue(),
			e.getPreFields().get(ent.getKey()));
		eq = con.mkNot(eq);
		BoolExpr edge = head.makeNewConstEqs(e);
		eq = con.mkAnd(edge, eq);
		Solver s = con.mkSolver();
		s.add(eq);
		Status stat = s.check();
		if(!stat.equals(Status.UNSATISFIABLE)) {
		    System.out.println("Edge equations: " + edge);
		    System.out.println("Equation: " + eq);
		    System.out.println("new const equations not right for local " +
			    ent.getKey());
		}
	    }
	    List<HashMap<Local, Expr>> hThreads = head.getPreVars();
	    for (int t = 0; t < hThreads.size(); t++) {
		for(Map.Entry<Local, Expr> ent : hThreads.get(t).entrySet()) {
		    BoolExpr eq = con.mkEq(ent.getValue(),
			    e.getPreVars().get(t).get(ent.getKey()));
		    eq = con.mkNot(eq);
		    BoolExpr edge = head.makeNewConstEqs(e);
		    eq = con.mkAnd(edge, eq);
		    Solver s = con.mkSolver();
		    s.add(eq);
		    Status stat = s.check();
		    if(!stat.equals(Status.UNSATISFIABLE)) {
			System.out.println("Equation: " + eq);
			System.out.println("new const equations not right for thread  local " +
				ent.getKey() + " for thread " + t);
		    }
		}
	    }
	    List<HashMap<Integer, Expr>> pThreads = head.getParams();
	    for (int t = 0; t < pThreads.size(); t++) {
		for(Map.Entry<Integer, Expr> ent : pThreads.get(t).entrySet()) {
		    if(ent.getValue() == null) {
			System.out.println("Null value for key " + ent.getKey());
		    }
		    BoolExpr eq = con.mkEq(ent.getValue(),
			    e.getParams().get(t).get(ent.getKey()));
		    eq = con.mkNot(eq);
		    BoolExpr edge = head.makeNewConstEqs(e);
		    eq = con.mkAnd(edge, eq);
		    Solver s = con.mkSolver();
		    s.add(eq);
		    Status stat = s.check();
		    if(!stat.equals(Status.UNSATISFIABLE)) {
			System.out.println("Equation: " + eq);
			System.out.println("new const equations not right for thread  parameter " +
				ent.getKey() + " for thread " + t);
		    }
		}
	    }
	}
    }

    public <K, V> boolean contains(Map<K, V> sub, Map<K, V> sup) {
	for(K l: sub.keySet()) {
	    if(!sup.keySet().contains(l)) {
		System.out.println("Not contained: " + l);
		return false;
	    }
	}
	return true;
    }

    public boolean containsParam(Map<Integer, Expr> sub, Map<Integer, Expr> sup) {
	for(Integer i : sub.keySet()) {
	    if(!sup.keySet().contains(i)) {
		System.out.println("Parameter " + i);
		System.out.println("sub: " + sub + " sup: " + sup);
		return false;
	    }
	}
	return true;
    }

    private void DFS(SubpathTuple st, final List<SubpathTuple> out) {

	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    private boolean isReturn(Unit u) {
	return (u instanceof ReturnVoidStmt) || (u instanceof ReturnStmt);
    }

    public boolean hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		return true;
	    }
	}

	return false;
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestVariableGen()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "SimpleMethodCall",
	    /* "NoDivideByZeroTest"*/};
	soot.Main.main(sootArgs);
    }
}
