/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
public enum AnalysesEnum {
    NONE, EXCEPTIONS, DEADLOCK, RACE_CONDITION
}
