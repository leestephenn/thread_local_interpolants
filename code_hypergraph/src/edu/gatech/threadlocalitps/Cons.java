/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.Unit;
public class Cons {
    public Unit car;
    public Cons cdr;

    public Cons(Unit car, Cons cdr) {
	this.car = car;
	this.cdr = cdr;
    }
}
