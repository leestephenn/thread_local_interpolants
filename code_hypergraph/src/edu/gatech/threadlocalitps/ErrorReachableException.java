/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
/* This maybe should NOT be a runtime exception */
public class ErrorReachableException extends RuntimeException {
    public final SubpathTuple location;

    public ErrorReachableException() {
	super();
	location = null;
    }

    public ErrorReachableException(String message) {
	super(message);
	this.location = null;
    }

    public ErrorReachableException(String message, SubpathTuple location) {
	super(message);
	this.location = location;
    }
}
