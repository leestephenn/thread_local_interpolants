/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
public class FakeSootField extends soot.SootField {

    public FakeSootField(String name, soot.Type type) {
	super(name, type);
    }

    public boolean isDeclared() {
	return false;
    }

    public soot.SootClass getDeclaringClass() {
	return null;
    }

    public String toString() {
	return "<FakeField: " + getName() + ">";
    }
}
