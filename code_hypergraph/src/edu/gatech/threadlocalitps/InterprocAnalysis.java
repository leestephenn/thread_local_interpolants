/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Collections;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.enumerations.Z3_lbool;

import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Local;
import soot.SootField;
import soot.SootMethod;
import soot.Unit;
import soot.Scene;
import soot.jimple.IfStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.InvokeStmt;

public class InterprocAnalysis {
    public static final String[] SPECIAL_SIGS = {
	"<ThreadItpsAnalysisError: void analysisError()>",
	"<java.util.concurrent.locks.ReentrantLock: void lock()>",
	"<java.util.concurrent.locks.ReentrantLock: void unlock()>",
	"<java.lang.ThreadLocal: java.lang.Object get()>",
	"<java.util.concurrent.locks.ReadWriteLock: java.util.concurrent.locks.Lock writeLock()>",
	"<java.util.concurrent.locks.ReadWriteLock: java.util.concurrent.locks.Lock readLock()>",
	"java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock: void lock()",
	"<java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock: void unlock()>",
	"<java.util.concurrent.locks.Lock: void lock()>",
	"<java.util.concurrent.locks.Lock: void unlock()>"
    };

    private final List<UnwindingContext> initContexts =
	new ArrayList<UnwindingContext>();

    private final AnalysesEnum analysis;

    private HashMap<UnwindingContext, BoolExpr> contextSummaries = new HashMap<UnwindingContext, BoolExpr>();

    //Do the powerset representation
    private HashMap<UnwindingContext, ArrayList<UnwindingContext>> covers =
	new HashMap<UnwindingContext, ArrayList<UnwindingContext>>();

    public static boolean doBaseline = false;

    //private final ExceptionalUnitGraph cfg;

    public static SootMethod errorIndic = null;
    /* public static SootMethod lockIndic = null;
    public static SootMethod unlockIndic = null; */

    //this is primarily for testing
    //private int disjoinInterpolantsCalled = 0;

    public InterprocAnalysis(ExceptionalUnitGraph cfg, AnalysesEnum anlss) {

	//this.cfg = cfg;
	analysis = anlss;
	try {
	    errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	} catch (RuntimeException ex) {
	    System.err.print("InterprocAnalysis Could not find method ");
	    System.err.println(" Error " + errorIndic);
	    //throw ex;
	}
	Unit init = cfg.getHeads().get(0);//In all examples, only one head
	boolean foundErrorNode = false;
	if(errorIndic != null) {
	    Unit error = findErrorNode(cfg);
	    if(error != null) {
		foundErrorNode = true;
		UnwindingContext wholeCon = new UnwindingContext(cfg, init, error, 2);
		initContexts.add(wholeCon);
	    }
	}
	if( !foundErrorNode) {
	    for(Unit error : cfg.getTails()) {
		if(error instanceof ReturnStmt ||
			error instanceof ReturnVoidStmt) {
		    UnwindingContext wholeCon = new UnwindingContext(cfg, init, error, 2);
		    initContexts.add(wholeCon);
		}
	    }
	}
    }

    public InterprocAnalysis(List<ExceptionalUnitGraph> cfgs, AnalysesEnum ae) {
	analysis = ae;
	try {
	    errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	} catch (RuntimeException ex) {
	    System.err.print("InterprocAnalysis Could not find method ");
	    System.err.println(" Error " + errorIndic);
	    //throw ex;
	}

	List<Unit> inits = new ArrayList<Unit>();
	List<List<Unit>> exits = new ArrayList<List<Unit>>();
	for(int i = 0; i < cfgs.size(); i++) {
	    Unit init = cfgs.get(i).getHeads().get(0);//In all files, only one head
	    inits.add(init);

	    if(errorIndic != null) {
		Unit error = findErrorNode(cfgs.get(i));
		if(error != null) {
		    exits.add(Collections.singletonList(error));
		    break;
		}
	    }

	    //else (because of the break)
	    List<Unit> tExits = new ArrayList<Unit>();
	    for(Unit exit : cfgs.get(i).getTails()) {
		if(exit instanceof ReturnStmt ||
			exit instanceof ReturnVoidStmt) {
		    tExits.add(exit);
			}
	    }
	    exits.add(tExits);
	}

	List<Unit> oneInits = inits.subList(0, 2);
	for(Unit exit0 : exits.get(0)) {
	    for(Unit exit1 : exits.get(1)) {
		List<Unit> oneExits = Arrays.asList(new Unit[]{ exit0, exit1});
		UnwindingContext wholeCon =
		    new UnwindingContext(cfgs, oneInits, oneExits, 2);
		initContexts.add(wholeCon);
	    }
	}
    }

    public void addEntail(UnwindingContext prem, UnwindingContext conc) {
	ArrayList<UnwindingContext> concs = covers.get(prem);
	if(concs == null) {
	    concs = new ArrayList<UnwindingContext>();
	    covers.put(prem, concs);
	}

	concs.add(conc);
    }

    /* This is the method that should be called after creating a new instance. */
    public void unwind() {
	boolean more = true;

	int count = 0;

	HashMap<UnwindingContext, UnwindingContext> par
	    = new HashMap<UnwindingContext, UnwindingContext>();
	while(more) {
            //System.out.println("thread count: " + java.lang.Thread.activeCount());
	    Deque<UnwindingContext> stack =
		new ArrayDeque<UnwindingContext>();

            //This is essentially our visited set for the search. Items are
            //never removed from it once found, while they are popped from
            // stack
	    HashSet<UnwindingContext> enqueued =
		new HashSet<UnwindingContext>();

	    stack.addAll(initContexts);
	    enqueued.addAll(initContexts);

	    //System.out.println("looping\n");
	    HashSet<UnwindingContext> discharged =
		new HashSet<UnwindingContext>();

            //This is a representation of the context for the whole program
	    UnwindingContext wholeCon = null;
	    boolean foundNewContext = false;
	    while(!stack.isEmpty() && !foundNewContext) {
		//System.out.println("looping");
		UnwindingContext front = stack.pop();

                //determine whether this is the context for the whole program
		boolean isStart = true;
		for(int t = 0; t < 2; t++) {
		    if(front.getContextSize(t) != 2) {
			isStart = false;
		    }
		}
		if(isStart) wholeCon = front;

		//System.out.println("Front\n" + front);
		//System.out.println("Must write: " + front.getMustWrite());
		//System.out.println("Must use: " + front.getMustUse());

		//To do this properly, we really should check locations
		//(the context in contextSummaries may be cloned)
                //TODO: I think the above is no longer true
		if(! contextSummaries.containsKey(front)) {

		    Z3Converter.initFakeFields();
		    PathTuple paths = front.pathTuple();
		    //If no path could be formed with the variable uses required
		    if(paths == null) {
			//System.out.println("No paths:\n" + front);
			System.out.println("Null paths formed");
			continue;
		    }
		    //System.out.println("Whole context:\n" + wholeCon);
		    SubpathTuple pt = new SubpathTuple(paths, wholeCon);
		    //System.out.println("Subpaths: " + pt.subpathsString());

		    List<SubpathTuple> topoOrder;//This is only used for debugging
		    try {
                        //TODO: I think for the baseline this is where I replace with
                        //baselineDisjoinInterpolants

                        if(doBaseline) {
                            topoOrder = baselineDisjoinInterpolants(pt);
                        } else {
                            topoOrder = disjoinInterpolants(pt);
                        }
		    } catch(ErrorReachableException ex) {
			MakeGraph m = new MakeGraph(pt);
			m.writeToFile("outputSatisfiable" + count + ".dot",
				MakeGraph.INTERPOLANT_NAMES);
			throw ex;
		    }

		    //System.out.println("Summaries:" + contextSummaries.values());
		    //System.out.println("Summary map size: " + contextSummaries.size());
		    System.out.println("Falsified one path tuple.");
		    System.out.println("Context:\n" + front);
		    //System.out.println(pt.subpathsString());
		    MakeGraph m = new MakeGraph(pt);
		    m.writeToFile("outputUnwinding" + count + ".dot",
			    MakeGraph.INTERPOLANT_NAMES);
		    count++;

		    foundNewContext = true;

                    //This entire branch really should not exist.
                    // It exists because the path-finding algorith in front.pathTuple()
                    // is non-trivial withthe partial order reduction
		    if( !contextSummaries.containsKey(front)) {
			System.out.println("Output count: " + (count - 1));
			System.out.println("Context");
			System.out.println(front);
			//System.out.println("Paths");
			//System.out.println(pt.subpathsString());
			System.out.println("Must read: " + front.getMustRead());
			System.out.println("Must write: " + front.getMustWrite());
			System.out.println("Must use: " + front.getMustUse());

			int pathCtr = 0;
			for(UnwindingContext path = par.get(front); path != null; path = par.get(path)) {
			    if(contextSummaries.containsKey(path)) {
				pathCtr++;
				boolean brk = false;
				for(SubpathTuple subT : topoOrder) {
				    if(subT.getUnwindingContext().equals(path)) {
					System.out.println("Path ctr: " + pathCtr);
					System.out.println("context: " + path);
					System.out.println("Paths");
					System.out.println(paths.getPath(0).matchRelation());
					System.out.println(subT.subpathsString());
					System.out.println("Full path:\n" +
						pt.subpathsString());
					System.out.println("# In edges: " + subT.getInEdges().size());

					brk = true;
					break;
				    }
				}
				//System.out.println("Supercontext\n" + path);
				if (brk) break;
			    }
			}

			throw new IllegalStateException("Time to implement restricted preds for contexts.");
		    }


		} else {

		    ArrayList<UnwindingContext> concs = covers.get(front);
		    if(concs == null) {
			concs = new ArrayList<UnwindingContext>();
			covers.put(front, concs);
		    }

		    boolean covered = false;
		    for(UnwindingContext conc : concs) {
			//System.out.println("Possible cover " + conc);
			if(discharged.contains(conc)) {
			    System.out.println("Covered");
			    System.out.println(contextSummaries.get(front) +
				    " => " + contextSummaries.get(conc));
			    covered = true;
			}
		    }

		    if( !covered ) {
			List<UnwindingContext> preds = front.expand();
			for(UnwindingContext pred : preds) {
			    par.put(pred, front);
			    if(!enqueued.contains(pred)) {
				//System.out.println("Pushing context");
				stack.push(pred);
				enqueued.add(pred);
			    }
			}
		    }

		    discharged.add(front);
		}
	    } //end of bfs loop

	    more = foundNewContext || !stack.isEmpty();
	}
    }

    public void baselineDFS(SubpathTuple pt) {
        boolean atSource = pt.isEmpty();

        if(!atSource) {
            for (UnitEdge e : pt.getInEdges()) {

                //Right now this code won't work for interproc
                UnitSingleEdge se = (UnitSingleEdge) e;
                se.inInterleaving = true;
                baselineDFS(se.getTail());
                se.inInterleaving = false;
            }
        } else {
            ArrayList<SubpathTuple> itlv = new ArrayList<SubpathTuple>();

            SubpathTuple cur = pt;
            final InterpolationContext ctx = Z3Converter.CONTEXT;
            //The MkInterpolant means we have an interpolant for every subpath tuple
            BoolExpr sequence = ctx.MkInterpolant(ctx.mkTrue());

            boolean atSink = false;
            //Build the list of the interleaving and the formula
            while(!atSink) {
                itlv.add(cur);

                atSink = true;
                for(UnitEdge e : cur.getOutEdges()) {
                    atSink = false;
                    UnitSingleEdge se = (UnitSingleEdge) e;
                    if(se.inInterleaving) {
                        cur = se.getHead();
                        BoolExpr conj = ctx.mkAnd(se.getFormula(),
                                               cur.makeNewConstEqs(e));
                        sequence = ctx.MkInterpolant(ctx.mkAnd(sequence, conj));
                    }
                }
            }

            InterpolationContext.ComputeInterpolantResult o =
                ctx.ComputeInterpolant(sequence, ctx.mkParams());

            if (o.status == Z3_lbool.Z3_L_FALSE) {
                if(o.interp == null || o.interp.length != (itlv.size() - 1)) {
                    throw new UnsupportedOperationException(
                            "No interpolant was formed from the refutation " +
                            "or too many: " + o.interp);
                }

                for(int i = 0; i < o.interp.length; i++) {
                    disjoinSingleInterpolant(o.interp[i], itlv.get(i));
                }
            } else {
		throw new ErrorReachableException("Interleaving " + itlv + " was satisfiable.");
            }
        }
    }

    /* This method disjoins its first argument into the summary for the context of its
       second argument. It handles the variable translation for this.
       It does the cover check. */
    /* Currently, disjoinInterpolants has a copy of this (I copied this from there)
       TODO: replace the copy in disjoinInterpolants with a call to this method. */
    public void disjoinSingleInterpolant(BoolExpr newSum, SubpathTuple cur) {
        Context con = Z3Converter.CONTEXT;
        UnwindingContext uCon = cur.getUnwindingContext();

        ArrayList<Expr> oldConsts = new ArrayList<Expr>();
        ArrayList<Expr> newConsts = new ArrayList<Expr>();

        for(int t = 0; t < cur.getPreVars().size(); t++) {
            HashMap<Local, Expr> tMap = cur.getPreVars().get(t);
            for(Map.Entry<Local, Expr> ent : tMap.entrySet()) {
                oldConsts.add(ent.getValue());

                String newName = Z3Converter.getThreadString(
                        ent.getKey().getName(), t);
                Expr n=con.mkConst(newName, ent.getValue().getSort());
                newConsts.add(n);
            }
        }

        for(int t = 0; t < cur.getPostVars().size(); t++) {
            HashMap<Local, Expr> tMap = cur.getPostVars().get(t);
            for(Map.Entry<Local, Expr> ent : tMap.entrySet()) {
                oldConsts.add(ent.getValue());

                String newName = Z3Converter.getThreadString(
                        ent.getKey().getName(), t);
                newName = newName + "+";
                Expr n=con.mkConst(newName, ent.getValue().getSort());
                newConsts.add(n);
            }
        }

        for(int t = 0; t < cur.getParams().size(); t++) {
            HashMap<Integer, Expr> tMap = cur.getParams().get(t);
            for(Map.Entry<Integer, Expr> ent : tMap.entrySet()) {
                oldConsts.add(ent.getValue());

                String newName = Z3Converter.getThreadString("Param", t);
                newName = newName;
                Expr n=con.mkConst(newName, ent.getValue().getSort());
                newConsts.add(n);
            }
        }

        normalizeFields(cur.getPreFields(), oldConsts, newConsts, false);
        normalizeFields(cur.getPostFields(), oldConsts, newConsts, true);

        normalizeFields(cur.getPreStaticFields(), oldConsts, newConsts, false);
        normalizeFields(cur.getPostStaticFields(), oldConsts, newConsts, true);

        Expr[] dummy = new Expr[0];
        newSum = (BoolExpr) newSum.substitute(oldConsts.toArray(dummy),
                newConsts.toArray(dummy));

        BoolExpr curSummary = contextSummaries.get(uCon);
        if(curSummary != null) {
            newSum = con.mkOr(curSummary, newSum);
        }

        contextSummaries.put(uCon, newSum);

        //This is actually the cover check, but it can go here
        for(UnwindingContext oth : contextSummaries.keySet()) {
            if(uCon.hasSameHole(oth)) {
                //System.out.println("Had same hole");
                //System.out.println(newSum + " =>? " +
                //contextSummaries.get(oth));
                if(entails(newSum, contextSummaries.get(oth))) {
                    addEntail(uCon, oth);
                }
            }
        }
    }

    public List<SubpathTuple> baselineDisjoinInterpolants(SubpathTuple pt) {
	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(pt, topoOrder);

	System.out.println("Num SubpathTuples: " + topoOrder.size());

	boolean exceptions, deadlock, races;
	exceptions = deadlock = races = false;
	if(analysis == AnalysesEnum.EXCEPTIONS) {
	    exceptions = true;
	}
	if(analysis == AnalysesEnum.DEADLOCK) {
	    deadlock = true;
	}
	if(analysis == AnalysesEnum.RACE_CONDITION) {
	    races = true;
	}
	Z3Converter.findExceptions = exceptions;
	Z3Converter.findDeadlock = deadlock;
	Z3Converter.findRaceCondition = races;

	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	}

	BoolExpr initConds = pt.checkFinal(exceptions, deadlock, races);

        for(SubpathTuple possZero : topoOrder) {
	    if(possZero.isZero()) {
		possZero.setAuxilliaryConstraints(initConds);
                break;
	    }
        }

        baselineDFS(pt);

        return topoOrder;
    }

    public List<SubpathTuple> disjoinInterpolants(SubpathTuple pt) {
	final Context con = Z3Converter.CONTEXT;

	ArrayList<SubpathTuple> topoOrder = new ArrayList<SubpathTuple>();

	DFS(pt, topoOrder);

	System.out.println("Num SubpathTuples: " + topoOrder.size());

	boolean exceptions, deadlock, races;
	exceptions = deadlock = races = false;
	if(analysis == AnalysesEnum.EXCEPTIONS) {
	    exceptions = true;
	}
	if(analysis == AnalysesEnum.DEADLOCK) {
	    deadlock = true;
	}
	if(analysis == AnalysesEnum.RACE_CONDITION) {
	    races = true;
	}
	Z3Converter.findExceptions = exceptions;
	Z3Converter.findDeadlock = deadlock;
	Z3Converter.findRaceCondition = races;

	for(SubpathTuple st : topoOrder) {
	    st.fillVariables();
	}
        /*for(SubpathTuple st : topoOrder) {
            if(st.getPreStaticFields().containsKey(Z3Converter.deadlockField)) {
                System.out.println(st + " has pre deadlock field " + st.getPreStaticFields().get(Z3Converter.deadlockField));
            }
        } */

	BoolExpr initConds = pt.checkFinal(exceptions, deadlock, races);
        System.out.println("Init conds: " + initConds);
        //System.out.println("For deadlock, races: " + deadlock + " " + races + " Initial conditions: " + initConds);
	//System.out.println("Full forward: " + pt.makeForwardContext(pt));

	//System.out.println("Final constraints: " + pt.makeForwardContext(pt));

	for(int i = 0; i < topoOrder.size(); i++) {
	    SubpathTuple cur = topoOrder.get(i);

	    if(cur.isZero()) {
		cur.setAuxilliaryConstraints(initConds);
	    }

	    List<BoolExpr> contexts = new ArrayList<BoolExpr>();
	    //System.out.println("Interleavable: " + cur);
	    /*
	    for(int j = i + 1; j < topoOrder.size(); j++) {
		SubpathTuple other = topoOrder.get(j);
		if(cur.interleavable(other) && !other.subOf(cur)) {
		    BoolExpr forward = other.makeForwardContext(cur);
		    contexts.add(forward);
		}
	    } */
	    //System.out.println();

	    //System.out.println("Number of interleavable nodes: " + contexts.size());

	    //BoolExpr context = con.mkAnd(contexts.toArray(new BoolExpr[0]));
	    BoolExpr context = pt.makeForwardContextRec(cur);
	    try {
		cur.findInterpolant(context);
		//System.out.println("Finding interpolant");
		UnwindingContext uCon = cur.getUnwindingContext();
		BoolExpr newSum = cur.summary();

		ArrayList<Expr> oldConsts = new ArrayList<Expr>();
		ArrayList<Expr> newConsts = new ArrayList<Expr>();

		for(int t = 0; t < cur.getPreVars().size(); t++) {
		    HashMap<Local, Expr> tMap = cur.getPreVars().get(t);
		    for(Map.Entry<Local, Expr> ent : tMap.entrySet()) {
			oldConsts.add(ent.getValue());

                        String newName = Z3Converter.getThreadString(
				ent.getKey().getName(), t);
			Expr n=con.mkConst(newName, ent.getValue().getSort());
			newConsts.add(n);
		    }
		}
		for(int t = 0; t < cur.getPostVars().size(); t++) {
		    HashMap<Local, Expr> tMap = cur.getPostVars().get(t);
		    for(Map.Entry<Local, Expr> ent : tMap.entrySet()) {
			oldConsts.add(ent.getValue());

                        String newName = Z3Converter.getThreadString(
				ent.getKey().getName(), t);
			newName = newName + "+";
			Expr n=con.mkConst(newName, ent.getValue().getSort());
			newConsts.add(n);
		    }
		}

		for(int t = 0; t < cur.getParams().size(); t++) {
		    HashMap<Integer, Expr> tMap = cur.getParams().get(t);
		    for(Map.Entry<Integer, Expr> ent : tMap.entrySet()) {
			oldConsts.add(ent.getValue());

                        String newName = Z3Converter.getThreadString("Param", t);
			newName = newName;
			Expr n=con.mkConst(newName, ent.getValue().getSort());
			newConsts.add(n);
		    }
		}

		normalizeFields(cur.getPreFields(), oldConsts, newConsts, false);
		normalizeFields(cur.getPostFields(), oldConsts, newConsts, true);

		normalizeFields(cur.getPreStaticFields(), oldConsts, newConsts, false);
		normalizeFields(cur.getPostStaticFields(), oldConsts, newConsts, true);

		Expr[] dummy = new Expr[0];
		newSum = (BoolExpr) newSum.substitute(oldConsts.toArray(dummy),
			newConsts.toArray(dummy));

		BoolExpr curSummary = contextSummaries.get(uCon);
		if(curSummary != null) {
		    newSum = con.mkOr(curSummary, newSum);
		}

		for(UnwindingContext oth : contextSummaries.keySet()) {
		    if(uCon.hasSameHole(oth)) {
			//System.out.println("Had same hole");
			//System.out.println(newSum + " =>? " +
				//contextSummaries.get(oth));
			if(entails(newSum, contextSummaries.get(oth))) {
			    addEntail(uCon, oth);
			}
		    }
		}

		contextSummaries.put(uCon, newSum);

	    } catch (ErrorReachableException ex) {
		MakeGraph m = new MakeGraph(pt);
		m.writeToFile("outputSatisfiableIntervals.dot",
			MakeGraph.SUFFIX_LENGTH_NAMES);

		//System.out.println();
		throw ex;
	    }
	}
	return topoOrder;
    }

    public static void DFS(SubpathTuple st, final List<SubpathTuple> out) {
	for (SubpathTuple p : st.genSubpaths()) {
	    //This is safe because it is a DAG
	    if(!out.contains(p)) {
		DFS(p, out);
	    }
	}

	out.add(st);
    }

    public <E extends Expr> void normalizeFields(Map<SootField, E> map,
	    List<Expr> oldConsts, List<Expr> newConsts, boolean post) {
	final Context con = Z3Converter.CONTEXT;
	for(Map.Entry<SootField, E> ent : map.entrySet()) {
	    oldConsts.add(ent.getValue());

	    String newName = Z3Converter.getFieldString(ent.getKey());
	    newName = post ? newName + "+" : newName;
	    Expr n = con.mkConst(newName, ent.getValue().getSort());

	    newConsts.add(n);
	}
    }

    public static boolean entails(BoolExpr prem, BoolExpr conc) {
	final Context con = Z3Converter.CONTEXT;
	Solver s = con.mkSolver();
	s.add(con.mkNot(conc));
	s.add(prem);
	return s.check() == Status.UNSATISFIABLE;
    }

    /*
    public int getDisjoinInterpolantsCalled() {
	return disjoinInterpolantsCalled;
    } */

    public Map<UnwindingContext, BoolExpr> summaries() {
	return Collections.unmodifiableMap(contextSummaries);
    }

    public static Unit findErrorNode(ExceptionalUnitGraph g) {
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		if (errorIndic.equals(m)) {
		    return invoke;
		}
	    }
	}
	return null;
    }

    public List<UnwindingContext> getInitContexts() {
	return initContexts;
    }
}
