/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Transform;
import soot.Body;
import soot.Scene;
import soot.SootClass;
import java.util.Map;
import java.util.Arrays;
import soot.toolkits.graph.ExceptionalUnitGraph;

public class InterprocMain extends BodyTransformer {
    private final String[] methodsToAnalyze;
    private AnalysesEnum analysis = AnalysesEnum.NONE;
    private boolean doBaseline = false;

    private InterprocMain(String[] methods, AnalysesEnum a) {
	methodsToAnalyze = methods;
	if(a != null) {
	    analysis = a;
	}
    }

    private InterprocMain(String[] methods, AnalysesEnum a, boolean baseline) {
        this(methods, a);
        doBaseline = baseline;
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);

	boolean shouldAnalyze = false;
	for(String name : methodsToAnalyze) {
	    if(body.getMethod().getName().equals(name)) {
                //if(body.getMethod().getDeclaringClass().getShortName().equals("supplier"))
		shouldAnalyze = true;
	    }
	}

	if(!shouldAnalyze) {
	    return;
	}

	System.out.println("Processing " + body.getMethod());
	System.out.println("For " + analysis);
	Scene.v().addBasicClass("java.util.concurrent.locks.ReentrantLock", SootClass.SIGNATURES);
	//Scene.v().addBasicClass("java.util.concurrent.locks.ReentrantLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.ReadWriteLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.lang.ThreadLocal", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.Lock", SootClass.SIGNATURES);

        InterprocAnalysis.doBaseline = doBaseline;
	InterprocAnalysis ia = new InterprocAnalysis(g, analysis);
	ia.unwind();
    }

    public static void main(String[] args) {

	int i = 0;

	AnalysesEnum analysis = null;
        boolean baseline = false;
        //If the user gives multiple analysis flags (which they shouldn't),
        //this loop uses the last one
        //Need to put the baseline flag before the analysis flag
	for(; i < args.length && analysis == null; i++) {
	    String e = args[i];
	    if(e.startsWith("-")) {
		switch (e) {
                    //This one may not actually do much
		    case "-exceptions":
			analysis = AnalysesEnum.EXCEPTIONS;
			break;
		    case "-deadlock":
			analysis = AnalysesEnum.DEADLOCK;
			break;
		    case "-races":
			analysis = AnalysesEnum.RACE_CONDITION;
			break;
                    case "-baseline":
                        baseline = true;
                        break;
		}
	    }
	}

	//If no flag was found, set i back to zero
	if(analysis == null) {
	    i = 0;
	}

	String[] methods = Arrays.copyOfRange(args, i + 1, args.length);

	Options.v().set_output_format(Options.output_format_jimple);
	Options.v().set_omit_excepting_unit_edges(false);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new InterprocMain(methods, analysis, baseline)));
	String[] sootArgs = {"-pp", "-cp", args[i], "--process-dir", args[i]};
	soot.Main.main(sootArgs);
    }
}
