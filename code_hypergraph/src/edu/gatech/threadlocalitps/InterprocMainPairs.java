/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Transform;
import soot.Body;
import soot.Scene;
import soot.SootClass;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import soot.toolkits.graph.ExceptionalUnitGraph;

public class InterprocMainPairs extends BodyTransformer {
    private final ArrayList<String> methodsToAnalyze;
    private final ArrayList<ExceptionalUnitGraph> graphs = 
	new ArrayList<ExceptionalUnitGraph>();
    private AnalysesEnum analysis = AnalysesEnum.NONE;
    //private ExceptionalUnitGraph first;
    //private ExceptionalUnitGraph second;

    private InterprocMainPairs(String[] methods, AnalysesEnum a) {
	methodsToAnalyze = new ArrayList<String>(Arrays.asList(methods));
	if (a != null) {
	    analysis = a;
	}
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {
	//ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);

	boolean shouldAnalyze = false;
	String analyzedName = null;
	for(String name : methodsToAnalyze) {
	    if(body.getMethod().getName().equals(name)) {
                //if(!body.getMethod().getName().equals("run") || body.getMethod().getDeclaringClass().getShortName().equals("supplier")) {
		shouldAnalyze = true;
		analyzedName = name;
                //}
	    }
	}

	if(!shouldAnalyze) {
	    return;
	} 

	methodsToAnalyze.remove(analyzedName);
	graphs.add(new ExceptionalUnitGraph(body));
	if(graphs.size() != 2) {
	    return;
	}

	System.out.print("Processing ");
	System.out.println("For " + analysis);
        /* if (graphs.get(0).getBody().getMethod().getName().equals("size")) {
            java.util.Collections.reverse(graphs);
        } */
	for(ExceptionalUnitGraph graph : graphs) {
	    System.out.print(graph.getBody().getMethod() + " ");
	}
	System.out.println();

	Scene.v().addBasicClass("java.util.concurrent.locks.ReentrantLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.lang.ThreadLocal", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.ReadWriteLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock", SootClass.SIGNATURES);
	Scene.v().addBasicClass("java.util.concurrent.locks.Lock", SootClass.SIGNATURES);

	InterprocAnalysis ia = new InterprocAnalysis(graphs, analysis);
	ia.unwind();
    }

    public static void main(String[] args) {
	int i = 0;

	AnalysesEnum analysis = null;
	for(; i < args.length && analysis == null; i++) {
	    String e = args[i];
            System.out.println("Arg: " + e);
	    if(e.startsWith("-")) {
		switch (e) {
		    case "-exceptions":
			analysis = AnalysesEnum.EXCEPTIONS;
			break;
		    case "-deadlock":
			analysis = AnalysesEnum.DEADLOCK;
			break;
		    case "-races":
			analysis = AnalysesEnum.RACE_CONDITION;
			break;
		}
	    }
	}

	String[] methods = Arrays.copyOfRange(args, i + 1, args.length);

	Options.v().set_output_format(Options.output_format_jimple);
	Options.v().set_omit_excepting_unit_edges(false);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new InterprocMainPairs(methods,
			analysis)));
	String[] sootArgs = {"-pp", "-cp", args[i], "--process-dir", args[i]};
	soot.Main.main(sootArgs);
    }
}
