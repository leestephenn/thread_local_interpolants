/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
//This class is now unused -- it was unnecessary to have this equality check
//for Units
public class LocationTag implements soot.tagkit.Tag {
    private static int count = 0;
    private final int i;

    public LocationTag() {
	this.i = count;
	count++;
    }

    public String getName() {
	return "edu.gatech.hypergraph.LocationTag";
    }

    public byte[] getValue() {
	return java.nio.ByteBuffer.
	    allocate(4).putInt(i).array();
    }

    public String toString() {
	return "Tag" + i;
    }

    //TODO: Add an equals method?
}
