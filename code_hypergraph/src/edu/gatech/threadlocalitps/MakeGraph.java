/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import org.jgraph.graph.DefaultEdge;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.ext.VertexNameProvider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;

import com.microsoft.z3.Context;
import com.microsoft.z3.BoolExpr;

public class MakeGraph {
    public static final VertexNameProvider<SubpathTuple> SUFFIX_LENGTH_NAMES =
	new StartEndProvider();
    public static final VertexNameProvider<SubpathTuple> INTERPOLANT_NAMES =
	new SummaryProvider();


    private SimpleDirectedGraph<SubpathTuple, HyperEdge> graph =
	new SimpleDirectedGraph<SubpathTuple, HyperEdge>(HyperEdge.class);

    public MakeGraph(SubpathTuple whole) {
	graph.addVertex(whole);
	addDAG(whole);

	/* ArrayList<SubpathTuple> toRemove = new ArrayList<SubpathTuple>();
	for(SubpathTuple st : graph.vertexSet()) {
	    boolean remove = true;
	    for(HyperEdge e : graph.outgoingEdgesOf(st)) {
		remove = remove && (e.color != 0);
	    }
	    if(remove) {
		toRemove.add(st);
	    }
	}

	for(SubpathTuple st : toRemove) {
	    graph.removeVertex(st);
	} */

	//System.out.println("Graph size: " + graph.vertexSet().size());

	/*System.out.println("Graph:");//\n" + graph.vertexSet());
	for(SubpathTuple t : graph.vertexSet()) {
	    System.out.println(t);
	    System.out.println();
	} */
    }

    private static int numCalls = 0;
    public void addDAG(SubpathTuple start) {
	//System.out.println("Added vertex time " + numCalls++ + " " + start);
	//
	int count = 1;
	for (UnitEdge edge : start.getInEdges()) {
	    if (edge instanceof UnitSingleEdge) {
		UnitSingleEdge e = (UnitSingleEdge) edge;
		SubpathTuple tail = e.getTail();
		boolean unDcycle = graph.containsVertex(tail);
		if(!unDcycle) {
		    graph.addVertex(tail);
		}
		graph.addEdge(tail, start, new HyperEdge(e.getFormula(), 0));
		if(!unDcycle) {
		    addDAG(tail);
		}
	    } else if (edge instanceof UnitHyperEdge) {
		UnitHyperEdge e = (UnitHyperEdge) edge;
		SubpathTuple caller = e.getCallTail();
		SubpathTuple exit = e.getExitTail();

		boolean unDcycle = graph.containsVertex(caller);
		if(!unDcycle) {
		    graph.addVertex(caller);
		}
		graph.addEdge(caller, start, new HyperEdge(e.getFormula(), count));
		if(!unDcycle) {
		    addDAG(caller);
		}

		unDcycle = graph.containsVertex(exit);
		if(!unDcycle) {
		    graph.addVertex(exit);
		}
		graph.addEdge(exit, start, new HyperEdge(e.getFormula(), count));
		if(!unDcycle) {
		    addDAG(exit);
		}
		count++;
	    }
	}
    }

    public void writeToFile(String filename, VertexNameProvider<SubpathTuple> namer) {
	File out = new File(filename);
	DOTExporter<SubpathTuple, HyperEdge> outputDot =
	    new DOTExporter<SubpathTuple,HyperEdge>(
		new IntegerNameProvider<SubpathTuple>(),
		namer,
		new UnitStringProvider(),
		null, new HyperAttrProvider());
	try {
	    FileWriter writer = new FileWriter(out);
	    outputDot.export(writer, graph);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public void writeToFile(String filename, SubpathTuple cur) {
	File out = new File(filename);
	DOTExporter<SubpathTuple, HyperEdge> outputDot =
	    new DOTExporter<SubpathTuple,HyperEdge>(
		new IntegerNameProvider<SubpathTuple>(),
		new StartEndProvider(),
		new UnitStringProvider(),
		new InterleavableProvider(cur), new HyperAttrProvider());
	try {
	    FileWriter writer = new FileWriter(out);
	    outputDot.export(writer, graph);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public class HyperEdge extends DefaultEdge {
	public final String label;
	public final int color;//if this is zero, it is a normal edge

	public HyperEdge(soot.Unit u, int c) {
	    label = u.toString();
	    color = c;
	}

	public HyperEdge(BoolExpr form, int c) {
	    label = form.toString();
	    color = c;
	}
    }

    public static class StringProvider
	    implements VertexNameProvider<SubpathTuple> {

	public String getVertexName(SubpathTuple st) {
	    String result = String.valueOf(st);
	    result = result.replace("Subpaths", "").replace("\n","\\n");
	    return result;
	}
    }

    public static class SummaryProvider
	    implements VertexNameProvider<SubpathTuple> {

	public String getVertexName(SubpathTuple st) {
	    BoolExpr summ = st.summary();
	    String result = (summ == null) ? intervals(st) : summ.toString();
	    result = result.replace("Subpaths", "").replace("\n","\\n");
	    return result;
	}

	public String intervals(SubpathTuple st) {
	    String result = "";
	    for(int i = 0; i < st.getNumThreads(); i++) {
		if(!result.equals("")) {
		    result = result + "\n";
		}
		int[] se = st.getStartEnd(i);
		result = result + Arrays.toString(se);
	    }
	    result = result.replace("\n","\\n");
	    return result;
	}
    }

    public static class StartEndProvider
	    implements VertexNameProvider<SubpathTuple> {

	public String getVertexName(SubpathTuple st) {
	    String result = "";
	    for(int i = 0; i < st.getNumThreads(); i++) {
		if(!result.equals("")) {
		    result = result + "\n";
		}
		int[] se = st.getStartEnd(i);
		result = result + Arrays.toString(se);
	    }
	    result = result.replace("\n","\\n");
	    //System.out.println("Result: " + result);
	    return result;
	}
    }

    public class PrePostVarsProvider implements VertexNameProvider<SubpathTuple> {

	public String getVertexName(SubpathTuple st) {
	    String result = st.getPreVars() + "\n" +
		st.getPostVars();
	    result = st.intervals() + "\n" + result;
	    //result = result.replace(", i", "\ni");
	    return result.replace("\n", "\\n");
	    //return "";
	}
    }

    public class UnitStringProvider implements EdgeNameProvider<HyperEdge> {
	public String getEdgeName(HyperEdge e) {
	    String result = e.label;
	    if(result.contains("invoke")) {
		int ind = result.indexOf(':');
		ind = ind < 0 ? 0 : ind + 2;
		result = result.substring(ind);
	    }
	    return result.replace("\n", "\\n").replace("\"", "\\\"");
	}
    }

    public class HyperAttrProvider
	    implements ComponentAttributeProvider<HyperEdge> {
	String[] colorNames = {"black", "green", "blue", "red", "orange",
	                       "purple", "fuchsia", "gold", "violet", "lime",
			       "darkred", "olivedrab", "navy", "teal", "cyan"};

	public Map<String, String> getComponentAttributes(HyperEdge e) {
	    Map<String, String> result = new HashMap<String, String>();
	    result.put("color", colorNames[e.color]);
	    result.put("fontcolor", colorNames[e.color]);
	    return result;
	}
    }

    public class InterleavableProvider
	    implements ComponentAttributeProvider<SubpathTuple> {
	    private final SubpathTuple cur;

	public InterleavableProvider(SubpathTuple cur) {
	    this.cur = cur;
	}

	public Map<String, String> getComponentAttributes(SubpathTuple other) {
	    Map<String, String> result = new HashMap<String, String>();
	    if(!other.interleavable(cur)) {
		result.put("fillcolor", "lightblue");
		result.put("style", "filled");
	    } else if (other.equals(cur)) {
		result.put("fillcolor", "yellow");
		result.put("style", "filled");
	    }
	    return result;
	}
    }
}
