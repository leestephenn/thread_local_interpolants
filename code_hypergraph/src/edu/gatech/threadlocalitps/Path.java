/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import soot.Unit;
import soot.RefType;

public class Path {
    private List<Unit> path;
    //matchRel could actually be an array, with -1 at non-dest points
    // Note that this will go BACKWARDS: matchRel.get(j) = i means i is  j's call-predecessor
    private HashMap<Integer, Integer> matchRel = new HashMap<Integer, Integer>();
    private boolean[] takesBranch;
    private HashMap<Unit,RefType> throwsException = new HashMap<Unit,RefType>();
    public final String functionName;

    public Path(List<Unit> aPath, String name) {
	path = new ArrayList<Unit>(aPath);
	functionName = name;
	takesBranch = new boolean[path.size() + 1];
    }

    //TODO remove -- for testing ???
    public void addMatch(int i, int j) {
	matchRel.put(j, i);
    }

    /* Returns a negative number if no predecessor */
    /* This means that matchRel could be an array, where -1 means no pred */
    public int hasPredecessor(int j) {
	//System.out.println("Matching relation: " + matchRel);
	Integer pred = matchRel.get(j);
	return (pred == null) ? -1 : pred.intValue();
    }

    public void addBranch(int i) {
	takesBranch[i] = true;
    }

    public boolean takesBranch(int i) {
	return takesBranch[i];
    }

    public RefType throwsException(Unit u) {
	return throwsException.get(u);
    }

    public Unit get(int i) {
	return path.get(i);
    }

    public int size() {
	return path.size();
    }

    public int length() {
	return path.size();
    }

    List<Unit> subpath(int start, int end) {
	return path.subList(start, end);
    }

    public Map<Integer, Integer> matchRelation() {
	return Collections.unmodifiableMap(matchRel);
    }

    //Least I can do.
    public String toString() {
	return path.toString();
    }

}
