/** Copyright (c) Stephen N. Lee. */

package edu.gatech.threadlocalitps;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import soot.Unit;

public class PathTuple {
    private List<Path> threadPaths = new ArrayList<Path>();

    public final int numThreads;

    public PathTuple(int numThreads) {
	this.numThreads = numThreads;
    }

    public PathTuple(List<Path> paths) {
	this.numThreads = paths.size();
	threadPaths = new ArrayList<Path>(paths);
    }

    public Unit get(int thread, int i) {
	return threadPaths.get(thread).get(i);
    }

    public int hasPredecessor(int thread, int j) {
	return threadPaths.get(thread).hasPredecessor(j);
    }

    public Path getPath(int thread) {
	return threadPaths.get(thread);
    }

    public int length(int thread) {
	return threadPaths.get(thread).size();
    }

    /* This is intentionally package-private -- it is called from SubpathTuple
     * for printing */
    List<Unit> subpath(int thread, int start, int end) {
	return threadPaths.get(thread).subpath(start, end);
    }

    public String toString() {
	String result = "(";
	for(int i = 0; i < numThreads; i++) {
	    result = result + threadPaths.get(i);
	    if(i != numThreads - 1) {
		result = result + ",\n ";
	    }
	}
	result = result + ")";
	return result;
    }
}
