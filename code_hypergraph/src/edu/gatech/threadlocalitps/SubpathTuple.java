/** Copyright (c) Stephen N. Lee. */

package edu.gatech.threadlocalitps;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayDeque;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.Model;
import com.microsoft.z3.enumerations.Z3_lbool;
//import com.microsoft.z3.Tactic;
//import com.microsoft.z3.ApplyResult;
//import com.microsoft.z3.Goal;

import soot.Unit;
import soot.Local;
import soot.Value;
//import soot.ValueBox;
//import soot.SootMethod;
import soot.SootField;
import soot.jimple.InvokeExpr;
import soot.jimple.FieldRef;

public class SubpathTuple {
    private static int numNestedCalls = 0;
    //This stores the "correct" memory location for each equivalence class of
    // subpath tuples under equals(). Note that it is static.
    private static HashMap<SubpathTuple, SubpathTuple> found;

    private final PathTuple paths;
    //As far as I can tell, the below is used with funcContexts
    private final ArrayList<String> contextNames;

    private ArrayList<Integer> starts;
    private ArrayList<Integer> ends;
    private ArrayList<UnitEdge> outEdges = new ArrayList<UnitEdge>();
    private ArrayList<UnitEdge> inEdges = new ArrayList<UnitEdge>();

    private BoolExpr interpolant = null;
    private UnwindingContext uContext;

    private ArrayList<HashMap<Local, Expr>> preVars; //=
	//new ArrayList<HashMap<Local, Expr>>();
    private ArrayList<HashMap<Integer, Expr>> params;// =
	//new ArrayList<HashMap<Integer, Expr>>();
    private ArrayList<HashMap<Local, Expr>> postVars =
	new ArrayList<HashMap<Local, Expr>>();

    private HashMap<SootField, ArrayExpr> preFields;
    private HashMap<SootField, ArrayExpr> postFields =
	new HashMap<SootField, ArrayExpr>();

    private HashMap<SootField, Expr> preStaticFields;
    private HashMap<SootField, Expr> postStaticFields = new HashMap<SootField, Expr>();

    private ArrayExpr preArrayCells = null;
    private ArrayExpr postArrayCells = null;
    //private ArrayExpr locks = null;


    private boolean newPreArrayCells = false;
    private boolean newPostArrayCells = false;
    //private boolean newLocks = false;
    private ArrayList<ArrayList<Local>> newPreConsts =
	new ArrayList<ArrayList<Local>>();
    private ArrayList<ArrayList<Local>> newPostConsts =
	new ArrayList<ArrayList<Local>>();
    private ArrayList<ArrayList<Integer>> newParamConsts =
	new ArrayList<ArrayList<Integer>>();
    private ArrayList<SootField> newPreFieldConsts = new ArrayList<SootField>();
    private ArrayList<SootField> newPreStaticFieldConsts =
	new ArrayList<SootField>();
    private ArrayList<SootField> newPostFieldConsts = new ArrayList<SootField>();
    private ArrayList<SootField> newPostStaticFieldConsts =
	new ArrayList<SootField>();

    private BoolExpr auxilliaryConstraints = null;

    //As far as I can tell, this is currently only used for giving names
    // to variables
    private HashMap<Local, String> funcContext;

    //tells whether to reduce the number of subpaths generated
    private boolean reduceSubp = true;
    private SubpathTuple notConstrainedBy = null;

    //private final UnwindingHyperDAG dag;


    private SubpathTuple(PathTuple aPath, boolean dummy) {
	// this assignment (rather than copy) is necessary for the equality
	this.paths = aPath;
	starts = new ArrayList<Integer>(paths.numThreads);
	ends = new ArrayList<Integer>(paths.numThreads);
	contextNames = new ArrayList<String>();

	postVars = new ArrayList<HashMap<Local, Expr>>(
		paths.numThreads);
	for (int i = 0; i < paths.numThreads; i++) {
	    postVars.add(new HashMap<Local, Expr>());
	    newPostConsts.add(new ArrayList<Local>());
	}
    }

    public SubpathTuple(PathTuple aPath, UnwindingContext uCon) {
	this(aPath);
	uContext = uCon;
    }


    //this could now be private
    public SubpathTuple(PathTuple aPath) {
	this(aPath, false);
	found = new HashMap<SubpathTuple, SubpathTuple>();
	for(int i = 0; i < paths.numThreads; i++) {
	    starts.add(0);
	    ends.add(paths.length(i));
	}

	for(int i = 0; i < paths.numThreads; i++) {
	    contextNames.add(paths.getPath(i).functionName);
	}
    }


    /* public void addSuccessor(UnitEdge e) {
	outEdges.add(e);
    } */

    public List<UnitEdge> getInEdges() {
	return Collections.unmodifiableList(inEdges);
    }

    public List<UnitEdge> getOutEdges() {
	return Collections.unmodifiableList(outEdges);
    }

    public Unit getHeadUnit(int thread) {
	return paths.get(thread, starts.get(thread));
    }

    public boolean takesBranch(int thread) {
	return paths.getPath(thread).takesBranch(ends.get(thread));
    }

    //This is strictly for testing
    public Unit getNextUnit(int thread) {
	return paths.get(thread, ends.get(thread));
    }

    public int[] getStartEnd(int thread) {
	return new int[]{starts.get(thread), ends.get(thread)};
    }

    public UnwindingContext getUnwindingContext() {
	return uContext;
    }

    //For testing, primarily
    public PathTuple getPaths() {
	return paths;
    }

    public String intervals() {
	String result = "";
	for(int i = 0; i < paths.numThreads; i++) {
	    result = result + Arrays.toString(getStartEnd(i));
	}
	return result;
    }

    public int getNumThreads() {
	return paths.numThreads;
    }

    //public boolean subpathsCalled = false;

    public List<SubpathTuple> genSubpaths() {
        List<SubpathTuple> result = new ArrayList<SubpathTuple>(paths.numThreads);
	//subpathsCalled = true;
	List<SubpathTuple> brandNew = new ArrayList<SubpathTuple>();

	for (int i = 0; i < paths.numThreads; i++) {
            if(starts.get(i).equals(ends.get(i))) {
                continue;
            }
	    int end = ends.get(i);
	    int pred = paths.hasPredecessor(i, end);

	    //System.out.println("Call predecessor: " + pred);
	    if(pred >= 0) {
		ArrayList<UnitEdge> callRes = new ArrayList<UnitEdge>();

		//this should never be necessary, but I was getting a bug
		if(isEmpty()) {
		    break;
		}

		//System.out.println(pred + " ~> " + end);
		//Assert: paths.get(i, end - 1) instanceof Return(Void)Stmt
		callRes = allSplits(starts.get(i), pred, pred + 1, end - 1, i,
			       brandNew);

		if(reduceSubp && callRes.size() == 1) {
		    inEdges = callRes;
		    return callRes.get(0).getTails();
		} else {
		    inEdges.addAll(callRes);
		    for(UnitEdge e : callRes) {
			result.addAll(e.getTails());
		    }
		}
		//System.out.println("Subpaths: " + result);
	    } else {
		SubpathTuple next = new SubpathTuple(paths, false);
		for(int t = 0; t < paths.numThreads; t++) {
		    next.starts.add(starts.get(t));
		    next.ends.add(ends.get(t));
		}
		next.ends.set(i, ends.get(i) - 1);
		next.contextNames.addAll(contextNames);

		for(int t = 0; t < next.starts.size(); t++) {
		    if(next.starts.get(t) > next.ends.get(t)) {
			throw new IllegalStateException("start was greater than end: "
				+ next.intervals() +
				" with successor\n" + this);
		    }
		}

		SubpathTuple prior = found.get(next);
		if(prior != null) {
		    next = prior;
		} else {
		    found.put(next, next);
		    brandNew.add(next);
		}

		Unit last = paths.get(i, next.ends.get(i));
		next.uContext = new UnwindingContext(i, last, this.uContext);

		soot.RefType ete = paths.getPath(i).throwsException(last);
		UnitSingleEdge e = new UnitSingleEdge(next, last, this, i, ete);
		next.outEdges.add(e);

		//Z3Converter.inThread = i;
		boolean noShared = !involvesAnyField(last);

                //"read" is an adjective
		boolean readNotWritten = !interferes(last, i, next, false);
                boolean writtenNotTouched = !interferes(last, i, next, true);

		boolean onlyEdge = noShared ||
				(readNotWritten && writtenNotTouched);
		if(reduceSubp && onlyEdge) {
		    for(SubpathTuple oth : brandNew) {
			if (oth != next) {// Should be safe to use !=
			    found.remove(oth);
			}
		    }
		    this.inEdges.clear();
		    result.clear();
		}

		result.add(next);
		this.inEdges.add(e);

		if(onlyEdge) {
		    return result;
		}
	    }
	}

        return result;
    }

    /* numbering the first four parameters 1-4
     *        1 - 2
     *      /      \
     * 3 - 4 .......end -
     * where 4-1 has an InvokeExpr and 2-end has a Return(Void)Stmt */
    public ArrayList<UnitEdge> allSplits(int callerStart, int callerEnd,
	    int calleeStart, int calleeEnd,
	    int cThread, List<SubpathTuple> brandNew) {

	ArrayList<UnitEdge> result = new ArrayList<UnitEdge>();

	int[] maxes = new int[paths.numThreads - 1];
	for(int i = 0; i < paths.numThreads; i++ ) {
	    if(i != cThread) {
		final int pI = (i > cThread) ? i - 1 : i;

		maxes[pI] = ends.get(i) - starts.get(i);
	    }
	}

	int[] product = new int[paths.numThreads - 1];
	for(int i = 0; i < product.length; i++) {
	    product[i] = maxes[i];
	}
	//boolean atStart = true;

	while (true) {

	    SubpathTuple sub = new SubpathTuple(paths, false);
	    SubpathTuple caller = new SubpathTuple(paths, false);

	    //whether we should add the predecessor found below
	    boolean shouldAdd = true;
	    //sUnits for subUnits
	    List<Unit> sUnits = paths.subpath(cThread, calleeStart, calleeEnd);
	    //System.out.println("Product: " + Arrays.toString(product));

	    Unit firstU = null;// this should always be assigned if continuing

	    for(int j = 0; j < paths.numThreads; j++) {
		if( j != cThread) {
		    int otherS = starts.get(j);
		    int otherE = ends.get(j);

		    final int pI = (j > cThread) ? j - 1 : j;
		    final int s = product[pI];
		    if(s < 0) {
			throw new IllegalStateException("split " + s + " less than 0");
		    }
		    caller.starts.add(otherS);
		    caller.ends.add(otherS + s);

		    sub.starts.add(otherS + s);
		    sub.ends.add(otherE);

		    if(otherS + s != otherE) {
			firstU = paths.get(j, otherS + s);
			List<Unit> first = Collections.singletonList(firstU);

			//Z3Converter.inThread = cThread;
			List<SootField> read = fieldsUsed(sUnits, false);
			boolean readAndWritten = usesFields(first, read, false);
			List<SootField> written = fieldsUsed(sUnits, true);
			boolean writtenAndUsed=usesFields(first, written, true);

			//this may not work for numThreads > 2
			if(reduceSubp && !readAndWritten && !writtenAndUsed) {
			    shouldAdd = false;
			    break;
			}
		    }
		}
	    }

	    //Used just below (if continuing) and at bottom (if not).
	    boolean shouldExit = manageProduct(product, maxes, cThread);

	    if(!shouldAdd) {
		if(shouldExit) {
		    break;
		} else {
		    continue;
		}
	    }

	    caller.starts.add(cThread, callerStart);
	    caller.ends.add(cThread, callerEnd);

	    sub.starts.add(cThread, calleeStart);
	    sub.ends.add(cThread, calleeEnd);

	    Unit call = paths.get(cThread, callerEnd);
	    Unit ret = paths.get(cThread, calleeEnd);
	    Unit entry = paths.get(cThread, calleeStart);

	    caller.contextNames.addAll(contextNames);
	    sub.contextNames.addAll(contextNames);
	    String callName = Z3Converter.calledMethodName(call);
	    sub.contextNames.set(cThread, callName + (numNestedCalls++));

	    caller.uContext = new UnwindingContext(cThread, call, firstU,
		    uContext);
	    sub.uContext = new UnwindingContext(cThread, call, entry, ret,
		    firstU, uContext);

	    for(int i = 0; i < sub.starts.size(); i++) {
		if(sub.starts.get(i) > sub.ends.get(i)) {
		    throw new IllegalStateException("start was greater than end: "
			    + sub.intervals() +
			    " with successor\n" + this);
		}
	    }

	    for(int i = 0; i < caller.starts.size(); i++) {
		if(caller.starts.get(i) > caller.ends.get(i)) {
		    System.out.println("cThread: " + cThread);
		    System.out.println("matching: " +
			    paths.getPath(cThread).matchRelation());
		    //System.out.println(starts + "\n" + ends);
		    System.out.println("Subpaths:\n" + this.subpathsString());
		    throw new IllegalStateException("On thread " + cThread +
			    ", start was greater than end: "
			    + caller.intervals() +
			    " with successor\n" + this);
		}
	    }

	    SubpathTuple prior = found.get(caller);
	    if(prior == null) {
		found.put(caller, caller);
		brandNew.add(caller);
	    } else {
		caller = prior;
	    }

	    prior = found.get(sub);
	    if(prior == null) {
		found.put(sub, sub);
		brandNew.add(sub);
	    } else {
		sub = prior;
	    }

	    soot.RefType ete = paths.getPath(cThread).throwsException(call);
	    UnitHyperEdge e = new UnitHyperEdge(caller, call, sub, ret,
		    this, cThread, ete);
	    //Using the same one may be dangerous, but I'll risk it.
	    caller.outEdges.add(e);
	    sub.outEdges.add(e);

	    result.add(e);

	    //result.add(caller);
	    //result.add(sub);

	    //System.out.println("should exit: " + shouldExit);
	    if(shouldExit) {
		break;
	    }
	}
	return result;
    }

    //Returns whether finished
    public boolean manageProduct(int[] product, int[] maxes, int cThread) {
	int i = 0;
	for(; i < maxes.length; i++) {

	    final int threadI = (i >= cThread) ? i + 1 : i;
	    final int base = starts.get(threadI);
	    int predI = paths.hasPredecessor(threadI, base + product[i]);
	    product[i] = (predI >= 0) ? predI - base : product[i] - 1;

	    if(product[i] >= 0) {
		break;
	    }
	}

	if(i == maxes.length) {
	    return true;
	}

	for(int j = 0; j < i; j++) {
	    product[j] = maxes[j];
	}
	return false;
    }

    public BoolExpr backwardInterpolant() {
	final Context con = Z3Converter.CONTEXT;
	if (interpolant != null) {
	    BoolExpr result = interpolant;
	    if(auxilliaryConstraints != null) {
		result = con.mkAnd(result, auxilliaryConstraints);
	    }

	    /* if(this.isZero()) {
		System.out.println("Initial constraints: " + result);
	    } */
	    return result;
	}

	BoolExpr[] eTransition = new BoolExpr[inEdges.size()];
	for(int i = 0; i < eTransition.length; i++) {
	    final UnitEdge e = inEdges.get(i);
	    eTransition[i] = e.getBackwardPath();

	    eTransition[i] = con.mkAnd(eTransition[i], makeNewConstEqs(e));
	}
	BoolExpr result = con.mkOr(eTransition);
	if(auxilliaryConstraints != null) {
	    result = con.mkAnd(result, auxilliaryConstraints);
	}

	return result;
    }

    //This assumes that it is interleavable
    public BoolExpr makeForwardContext(SubpathTuple other) {
	final Context con = Z3Converter.CONTEXT;
	ArrayList<BoolExpr> results = new ArrayList<BoolExpr>();
	for(UnitEdge e : inEdges) {
	    BoolExpr eForm = e.contextFormula(other);
	    if(eForm != null) {
		BoolExpr tmp = con.mkAnd(eForm, makeNewConstEqs(e));
		results.add(tmp);
	    }
	}

	if(!results.isEmpty()) {
	    notConstrainedBy = null;

	    BoolExpr actuallyHappened = getActuallyHappened();
	    //Order of disjuncts matters not and this is slower, but more readable
	    results.add(0, con.mkNot(actuallyHappened));
	    BoolExpr result = con.mkOr(results.toArray(new BoolExpr[0]));
	    if(auxilliaryConstraints != null) {
		result = con.mkAnd(result, auxilliaryConstraints);
	    }
	    return result;
	} else {

	    notConstrainedBy = this.isEmpty() ? null : other;

	    return con.mkTrue();
	}
    }

    //This assumes that it is interleavable
    public BoolExpr makeForwardContextRec(SubpathTuple other) {
	final Context con = Z3Converter.CONTEXT;
	if(interpolant != null) {
	    return interpolant;
	}
	if(this == other) {// == should be safe
	    return con.mkTrue();
	}
	if (isEmpty()) {
	    return con.mkTrue();
	}

	ArrayList<BoolExpr> results = new ArrayList<BoolExpr>();
	for(UnitEdge e : inEdges) {
	    BoolExpr eForm = e.contextFormulaRec(other);
	    if(eForm != null) {
		BoolExpr tmp = con.mkAnd(eForm, makeNewConstEqs(e));
		results.add(tmp);
	    }
	}

	if(!results.isEmpty()) {
	    notConstrainedBy = null;

	    BoolExpr result = con.mkOr(results.toArray(new BoolExpr[0]));
	    if(auxilliaryConstraints != null) {
		result = con.mkAnd(result, auxilliaryConstraints);
	    }
	    //Cannot actually memoize this
	    return result;
	} else {

	    notConstrainedBy = this.isEmpty() ? null : other;

	    return con.mkTrue();
	}
    }

    public boolean isEmpty() {
	return starts.equals(ends);
    }

    public boolean isZero() {
	for(int e : ends) {
	    if(e != 0) {
		return false;
	    }
	}

	//This may be unnecessary -- starts should always be (elementwise) <= to
	//the ends, and neither starts nor ends should ever be negative
	return starts.equals(ends);
    }

    public void findInterpolant(BoolExpr context) {
	InterpolationContext con = Z3Converter.CONTEXT;
	if (isEmpty()) {
	    interpolant = con.mkTrue();
	} else if (interpolant == null) {
	    BoolExpr itpMinus = backwardInterpolant();
	    //System.out.println("Minus: " + itpMinus);
	    itpMinus = con.MkInterpolant(itpMinus);
	    BoolExpr itpPlus = context;
	    BoolExpr unsat = con.mkAnd(itpMinus, itpPlus);

            /* Maybe have the result of mkParams be a constant somewhere */
	    InterpolationContext.ComputeInterpolantResult o =
		con.ComputeInterpolant(unsat, con.mkParams());

	    if (o.status == Z3_lbool.Z3_L_FALSE) {
		/* The following if may not be necessary -- it is possible
		 * (but undocumented) that if the formula is FALSE, and with one
                 * interpolant in the formula, there will always be an interp
                 * array with 1 element. */
		if(o.interp != null && o.interp.length == 1) {
		    //System.out.println("Found interpolant for " + this);
		    interpolant = o.interp[0];

		    /*if(interpolant.isFalse()) {
			System.out.println("Unsat " + this + ":\n" + unsat);
		    } */

		    //unsatCore(context);
		} else {
                    throw new UnsupportedOperationException(
                            "No interpolant was formed from the refutation " +
                            "or too many: " + o.interp);
                }
	    } else {
		//System.out.println(unsat);
		/*MakeGraph graph = new MakeGraph(this);
		graph.writeToFile("outputSatisfiable.dot",
			MakeGraph.INTERPOLANT_NAMES);*/
		//System.out.println(forwardInterpolantPair);

		Solver s = con.mkSolver();
		s.add((BoolExpr) itpMinus.getArgs()[0]);
		s.add(itpPlus);
		System.out.println(s.check());
		Model model = s.getModel();
                //System.out.println(model);

		//System.out.println("Back:\n" + itpMinus);
		//System.out.println(itpPlus);

		/*
		 * This approach walks through the formula, trying to find the
		 * disjuncts that are satisfiable */
		ArrayDeque<Expr> stack = new ArrayDeque<Expr>();
		stack.push(itpPlus);
		while ( !stack.isEmpty()) {
		    Expr top = stack.pop();
		    if(top.isAnd()) {
			for(Expr arg : top.getArgs()) {
			    stack.push(arg);
			}
		    } else if(top.isOr()) {
			for(Expr arg : top.getArgs()) {
			    //System.out.println("Evaluated: " + model.evaluate(arg, false));
			    if( !model.evaluate(arg, false).isFalse()) {
				System.out.println();
				stack.push(arg);
			    } else {
				System.out.println(model.evaluate(arg, false));
			    }
			}
		    } else {
			if( !top.isTrue()) {
			    System.out.println(top);
			}
		    }
		} // */

		//System.out.println(itpPlus);
		//System.out.println(itpPlus.getFuncDecl());

		/*
		 * This approach walks over the subpath tuples, trying to find the
		 * out-edges which have non-false formulae
		Set<SubpathTuple> visited = new java.util.HashSet<SubpathTuple>();
		ArrayDeque<SubpathTuple> stack = new ArrayDeque<SubpathTuple>();
		stack.push(this);
		visited.add(this);

		Set<Expr> consts = new java.util.HashSet<Expr>();
		while( !stack.isEmpty()) {
		    SubpathTuple top = stack.pop();
		    System.out.print("Reached: ");
		    System.out.println(top);
		    if(top.outEdges.isEmpty()) {
			break;
		    }

		    for(UnitEdge e : top.outEdges) {
			BoolExpr form = e.getFormula();
			SubpathTuple arg = e.getHead();
			form = con.mkAnd(form, arg.makeNewConstEqs(e));

			if( model.evaluate(form, false).isTrue()) {
			    System.out.println(form);
			    if(visited.add(arg)) {
				ArrayDeque<Expr> treeQueue =
				    new ArrayDeque<Expr>();
				treeQueue.add(form);
				while( !treeQueue.isEmpty()) {
				    Expr node = treeQueue.remove();
				    if(node.isConst()) {
					consts.add(node);
				    }
				    for(Expr subArg : node.getArgs()) {
					treeQueue.add(subArg);
				    }
				}

				stack.push(arg);
			    }
			} else {
			    System.out.print("Not true: ");
			    System.out.println(model.evaluate(form, false));
			}
		    }
		}
		System.out.println();
		for(Expr c : consts) {
		    try {
			Expr interp = model.getConstInterp(c);
			System.out.print(c + "\t");
			System.out.println(interp);
		    } catch (RuntimeException ex) {
		    }
		} */
		//System.out.println(model);
		//System.out.println("Heffalump");
		//System.out.println(model);

		throw new ErrorReachableException("Interpolant pair was "
			+ o.status + " at vertex " + this, this);

	    }
	}
    }

    public void unsatCore(BoolExpr context) throws RuntimeException {
	final Context con = Z3Converter.CONTEXT;
	BoolExpr unsat = con.mkAnd(backwardInterpolant(), context);
	System.out.println("Full unsat:\n" + unsat);
	com.microsoft.z3.Params p = con.mkParams();
	p.add("flat", true);
	unsat = (BoolExpr) unsat.simplify(p);
	//System.out.println("Simplified is and: " + unsat.isAnd());
	System.out.println("Simplified # args: " + unsat.getArgs().length);
	Solver s = con.mkSolver();
	s.add(unsat);
	Status stat = s.check();
	if(stat == Status.UNSATISFIABLE) {
	    BoolExpr sat = unsat;
	    //System.out.println("Is and: " + sat.isAnd());
	    s = con.mkSolver();
	    HashMap<BoolExpr, BoolExpr> names = new HashMap<BoolExpr, BoolExpr>();
	    int c = 0;
	    for(Expr arg : sat.getArgs()) {
		BoolExpr name = con.mkBoolConst("BoolConst:" + c);
		names.put(name, (BoolExpr) arg);
		s.assertAndTrack((BoolExpr) arg, name);
		c++;
	    }
	    System.out.println("Assertions size: " + s.getAssertions().length);
	    System.out.println("Z3: " + s.check());
	    BoolExpr[] core = s.getUnsatCore();
	    System.out.println("core size: " + core.length);
	    System.out.println("Z3 core: " );
	    BoolExpr[] coreConjs = new BoolExpr[core.length];
	    int i = 0;
	    for(BoolExpr coreConj : core) {
		System.out.println(names.get(coreConj));
		coreConjs[i++] = names.get(coreConj);
	    }

	    throw new RuntimeException("Halt at " + this);
	}
    }

    public BoolExpr summary() {
	return interpolant;
    }

    public String toString() {
	return intervals();
    }

    public String subpathsString() {
	String result = "(";
	for(int i = 0; i < paths.numThreads; i++) {
	    result = result + starts.get(i) + "" + paths.subpath(i, starts.get(i), ends.get(i)) + ends.get(i);
	    if(i != paths.numThreads - 1) {
		result = result + ",\n ";
	    }
	}
	result = result + ")";
	return result;
    }

    public boolean interleavable(SubpathTuple other) {

	for(int i = 0; i < paths.numThreads; i++) {
	    for( int j = 0; j < paths.numThreads; j++) {
		if( i != j) {
		    boolean s = (this.starts.get(i) < other.starts.get(i)) &&
			(other.starts.get(j) < this.starts.get(j));
		    boolean e = (this.ends.get(i) < other.ends.get(i)) &&
			(other.ends.get(j) < this.ends.get(j));

		    if (s || e) {
			return false;
		    }
		}
	    }
	}

	return true;
    }

    public boolean constrainedBy(SubpathTuple other) {

	// == (rather than .equals()) should be safe here
	return other != notConstrainedBy;
    }

    public boolean subOf(SubpathTuple other) {
	for(int i = 0; i < ends.size(); i++) {
	    if(this.ends.get(i) > other.ends.get(i)) {
		return false;
	    }
	}

	for(int i = 0; i < starts.size(); i++) {
	    if(this.starts.get(i) < other.starts.get(i)) {
		return false;
	    }
	}

	return true;

    }

    public ArrayList<HashMap<Integer, Expr>> getParams() {
	return params;
    }

    public Map<Integer, Expr> getParams(int thread) {
	return params.get(thread);
    }

    private boolean calledFillVariables = false;

    //This should only be called on the whole-paths subpathtuple
    //Right now you can only check this
    //This returns the initial conditions to check
    public BoolExpr checkFinal(boolean exceptions, boolean deadlock, boolean races)
    {
	//System.out.println("Check called on " + this);
	//System.out.println("Exceptions?, Deadlock? " + exceptions + " " + deadlock);
	final Context con = Z3Converter.CONTEXT;
	SootField et = Z3Converter.exceptionField;
	SootField dl = Z3Converter.deadlockField;

	BoolExpr errorConditions = con.mkTrue();

	BoolExpr postET = (BoolExpr) getStaticField(et);
	//The second should never be true
	if(postET != null && errorConditions != null) {
	    if(!exceptions) {
		postET = con.mkNot(postET);
	    }
	    //System.out.println(postET);
	    errorConditions = con.mkAnd(errorConditions, postET);
	}

	BoolExpr postDL = (BoolExpr) getStaticField(dl);
	if(postDL != null && errorConditions != null) {
	    if(!deadlock) {
		postDL = con.mkNot(postDL);
	    }
	    errorConditions = con.mkAnd(errorConditions, postDL);
	}
        if(deadlock && postDL == null && errorConditions != null) {
            postDL = con.mkBoolConst(Z3Converter.getFieldString(dl));
	    errorConditions = con.mkAnd(errorConditions, postDL);
        }

	SootField rc = Z3Converter.raceFoundField;
	BoolExpr postRC = (BoolExpr) getStaticField(rc);
	if(postRC != null && errorConditions != null) {
	    if(!races) {
		postRC = con.mkNot(postRC);
	    }
	    errorConditions = con.mkAnd(errorConditions, postRC);
	}

        if(races && postRC == null && errorConditions != null) {
            postRC = con.mkBoolConst(Z3Converter.getFieldString(dl));
	    errorConditions = con.mkAnd(errorConditions, postRC);
        }
	//System.out.println("Error conditions: " + errorConditions);
	//errorConditions = con.mkAnd(errorConditions, initialConditions);


	//BoolExpr actuallyHappened = getActuallyHappened();
	//errorConditions = con.mkAnd(errorConditions, actuallyHappened);
	auxilliaryConstraints = errorConditions;

	return checkInitial(exceptions, deadlock, races);
    }

    private BoolExpr checkInitial(boolean exceptions, boolean deadlock,
	    boolean races) {
	final Context con = Z3Converter.CONTEXT;
	BoolExpr initialConditions = con.mkTrue();

	int paramI = 0;
        //This makes initializes object parameters to be environment objects
	for(HashMap<Integer, Expr> param : params) {
	    ArrayList<BoolExpr> entryParams = new ArrayList<BoolExpr>();
	    for(Expr par : param.values()) {
		if(par.getSort().equals(Z3Converter.refSort)) {
		    Expr notNull = Z3Converter.refSort.getRecognizers()[0].
			apply(par);
		    Expr isEnv = Z3Converter.refSort.getAccessors()[1][0].
			apply(par);
		    isEnv = Z3Converter.isEnv.apply(isEnv);

		    //Well-typednes is handled already
		    entryParams.add(con.mkImplies((BoolExpr) notNull,
				(BoolExpr) isEnv));
		}
	    }
	    BoolExpr parInitEqs = con.mkAnd(entryParams.toArray(new BoolExpr[0]));
	    initialConditions = con.mkAnd(initialConditions, parInitEqs);
	}

	SootField et = Z3Converter.exceptionField;
	BoolExpr preET = (BoolExpr) preStaticFields.get(et);
	if(preET != null) {
	    initialConditions = con.mkAnd(initialConditions, con.mkNot(preET));;
	}
	//Everything above this really needs to be done to every run
	SootField dl = Z3Converter.deadlockField;
	BoolExpr preDL = (BoolExpr) preStaticFields.get(dl);
        //System.out.println("PreDL: " + preDL);
	if(preDL != null) {
	    initialConditions = con.mkAnd(initialConditions, con.mkNot(preDL));;
	}
        if(deadlock && preDL == null) {
            preDL = con.mkBoolConst(Z3Converter.getFieldString(dl));
	    initialConditions = con.mkAnd(initialConditions, con.mkNot(preDL));
        }
	SootField lock = Z3Converter.lockField;
	ArrayExpr preLocks = (ArrayExpr) preFields.get(lock);
	if(preLocks != null) {
	    BoolExpr unlocked = con.mkEq(preLocks,
		    con.mkConstArray(Z3Converter.refSort, con.mkInt(-1)));
	    initialConditions = con.mkAnd(initialConditions, unlocked);
	} else {
            //System.out.println("Null prelocks");
        }
	BoolExpr[] waiting = new BoolExpr[Z3Converter.waitingFields.size()];
	for(int i = 0; i < waiting.length; i++) {
	    Expr wait = preStaticFields.get(Z3Converter.waitingFields.get(i));
	    if(wait != null) {
		waiting[i] = con.mkNot((BoolExpr) wait);
	    } else {
		waiting[i] = con.mkTrue();
	    }
	}
	initialConditions = con.mkAnd(initialConditions, con.mkAnd(waiting));

	BoolExpr[] enbld = new BoolExpr[Z3Converter.enabledFields.size()];
	for(int i = 0; i < enbld.length; i++) {
	    Expr c = preStaticFields.get(Z3Converter.enabledFields.get(i));
	    enbld[i] = (BoolExpr) c;
	}

	try {
	    if(Z3Converter.findDeadlock || Z3Converter.findRaceCondition) {
		initialConditions = con.mkAnd(initialConditions, con.mkAnd(enbld));
	    }
	} catch (NullPointerException ex) {
	    System.err.println(Arrays.toString(enbld));
	    System.err.println(this.subpathsString());
	    throw ex;
	}


	SootField rc = Z3Converter.raceFoundField;
	BoolExpr preRC = (BoolExpr) preStaticFields.get(rc);
	if(preRC != null) {
	    initialConditions = con.mkAnd(initialConditions, con.mkNot(preRC));;
	}
        if(races && preRC == null) {
            preRC = con.mkBoolConst(Z3Converter.getFieldString(dl));
	    initialConditions = con.mkAnd(initialConditions, con.mkNot(preRC));
        }

	for(SootField f : Z3Converter.releaseClocks) {
	    ArrayExpr pre = (ArrayExpr) preFields.get(f);
	    if(pre != null) {
		BoolExpr atZero = con.mkEq(pre,
			con.mkConstArray(Z3Converter.refSort, con.mkInt(0)));
		initialConditions = con.mkAnd(initialConditions, atZero);
	    }
	}

	for(SootField f : Z3Converter.writeClocks) {
	    ArrayExpr pre = (ArrayExpr) preFields.get(f);
	    if(pre != null) {
		BoolExpr atZero = con.mkEq(pre,
			con.mkConstArray(Z3Converter.refSort, con.mkInt(0)));
		initialConditions = con.mkAnd(initialConditions, atZero);
	    }
	}

	for(SootField f : Z3Converter.readClocks) {
	    ArrayExpr pre = (ArrayExpr) preFields.get(f);
	    if(pre != null) {
		BoolExpr atZero = con.mkEq(pre,
			con.mkConstArray(Z3Converter.refSort, con.mkInt(0)));
		initialConditions = con.mkAnd(initialConditions, atZero);
	    }
	}

	for(int i = 0; i < Z3Converter.threadClocks.size(); i++) {
	    List<SootField> threadClock = Z3Converter.threadClocks.get(i);
	    for(int j = 0; j < threadClock.size(); j++) {
		SootField f = threadClock.get(j);
		Expr pre = preStaticFields.get(f);
		if (pre != null) {
		    BoolExpr initVal;
		    if(i == j) {
			initVal = con.mkEq(pre, con.mkInt(1));
		    } else {
			initVal = con.mkEq(pre, con.mkInt(0));
		    }
		    initialConditions = con.mkAnd(initialConditions, initVal);
		}
	    }
	}
	return initialConditions;
    }

    //This calls generateFormula() on each in-edge
    public void fillVariables() {
	calledFillVariables = true;
	//System.out.println(this.intervals());
        Context con = Z3Converter.CONTEXT;

	//Determine whether this node is a source or has a hyperedge in
	boolean isSource = true;
	boolean hasHyperEdge = false;
	for(UnitEdge p : inEdges) {
	    isSource = false;
	    if (p instanceof UnitHyperEdge) {
		hasHyperEdge = true;
		break;
	    }
	}

	//The code below this block assumes that, if this node has no hyperedge,
	// then the maps for all its in-edge tails are the same.
	// This can be false if they descend from a hyperedge, so I test
	// for this and set hasHyperEdge appropriately.
	if ( !hasHyperEdge) {
	    ArrayList<HashMap<Local,Expr>> pv = null;
	    ArrayList<HashMap<Integer,Expr>> par = null;
	    HashMap<SootField,Expr> pf = null;
	    HashMap<SootField,Expr> psf = null;

	    for(UnitEdge ae : inEdges) {
		UnitSingleEdge e = (UnitSingleEdge) ae;
		ArrayList<HashMap<Local,Expr>> epv = e.getTail().preVars;
		ArrayList<HashMap<Integer,Expr>> ePar = e.getTail().params;
		HashMap<SootField, Expr> eFields = e.getTail().preStaticFields;
		HashMap<SootField, Expr> eSFields = e.getTail().preStaticFields;

		if(pv == null) pv = epv;
		if(par == null) par = ePar;
		if(pf == null) pf = eFields;
		if(psf == null) psf = eSFields;

		if(epv != pv || ePar != par ||
                       pf != eFields || psf != eSFields) {
		    hasHyperEdge = true;
		}
	    }
	}

	if(isSource || hasHyperEdge) {
	    preVars = new ArrayList<HashMap<Local, Expr>>();
	    params = new ArrayList<HashMap<Integer, Expr>>();
	    funcContext = new HashMap<Local, String>();

	    preFields = new HashMap<SootField, ArrayExpr>();
	    preStaticFields = new HashMap<SootField, Expr>();

	    for (int i = 0; i < paths.numThreads; i++) {
		params.add(new HashMap<Integer, Expr>());
		preVars.add(new HashMap<Local, Expr>());
		newPreConsts.add(new ArrayList<Local>());
		newParamConsts.add(new ArrayList<Integer>());
	    }
	}

	if(!isSource && !hasHyperEdge) {
	    UnitSingleEdge e = (UnitSingleEdge) inEdges.get(0);
	    preVars = e.getTail().preVars;
	    params = e.getTail().params;
	    preFields = e.getTail().preFields;
	    preStaticFields = e.getTail().preStaticFields;
	    preArrayCells = e.getTail().preArrayCells;

	    funcContext = e.getTail().funcContext;
	}

	// This requires the filling with empty maps in the constructor
	for(UnitEdge p : inEdges) {
	    //System.out.println("Before actually generating formula");
	    p.generateFormula();
	    //System.out.println("After actually generating formula");

	    if(hasHyperEdge) {//being in the loop implies not isSource

		for (int t = 0; t < preVars.size(); t++) {
		    HashMap<Local, Expr> lMap = preVars.get(t);
		    ArrayList<Local> nConsts = newPreConsts.get(t);

		    Map<Local, Expr> etMap = p.getPreVars().get(t);
		    mergeVars(t, lMap, etMap, nConsts, p);
		}

		for (int t = 0; t < params.size(); t++) {
		    HashMap<Integer, Expr> lMap = params.get(t);
		    ArrayList<Integer> nConsts = newParamConsts.get(t);

		    Map<Integer, Expr> etMap = p.getParams().get(t);
		    mergeParams(lMap, etMap, nConsts, t);
		}

		Map<SootField, ArrayExpr> fMap = p.getPreFields();
		mergeFields(preFields, fMap, newPreFieldConsts);

		Map<SootField, Expr> fsMap = p.getPreStaticFields();
		mergeFields(preStaticFields, fsMap, newPreStaticFieldConsts);
	    } else {
		preVars.get(p.thread).putAll(
			p.cvtr().getNewThreadPreMappings());
		//params.get(p.thread).putAll(p.cvtr().getParams());
		for(Map.Entry<Integer, Expr> ey : p.cvtr().getParams().entrySet()) {
		    Expr prior = params.get(p.thread).put(ey.getKey(), ey.getValue());
		    if(prior != null) {
			System.out.println(params);
			throw new UnsupportedOperationException(
				"Tried to replace a value of parameter "
				+ ey.getKey() + " on an edge from"
			       	+ ((UnitSingleEdge) p).getTail().intervals() +
				" to " + this.intervals() + " thread " + p.thread);
		    }
		}

		preFields.putAll(p.cvtr().getPreFields());
		preStaticFields.putAll(p.cvtr().getPreStaticFields());

	    }

	    ArrayList<HashMap<Local, Expr>> n = p.getPostVars();
	    for (int t = 0; t < postVars.size(); t++) {
		HashMap<Local, Expr> lMap = postVars.get(t);
		ArrayList<Local> nConsts = newPostConsts.get(t);
		Map<Local, Expr> etMap = n.get(t);

		mergeVars(t, lMap, etMap, nConsts, p);
	    }

	    Map<SootField, ArrayExpr> newFields = p.getPostFields();
	    mergeFields(postFields, newFields, newPostFieldConsts);

	    Map<SootField, Expr> newStaticFields = p.getPostStaticFields();
	    mergeFields(postStaticFields, newStaticFields,
		    newPostStaticFieldConsts);

	    if(p.getPreArrayCells() != null)  {
		if(preArrayCells == null) {
		    preArrayCells = p.getPreArrayCells();
		} else if (!p.getPreArrayCells().equals(preArrayCells)) {
		    newPreArrayCells = true;
		    String newName = "ArrayCells" + toString();
		    ArrayExpr na = (ArrayExpr)
			   con.mkConst(newName, preArrayCells.getSort());
		    preArrayCells = na;
		}
	    }

	    if(p.getPostArrayCells() != null)  {
		if(postArrayCells == null) {
		    postArrayCells = p.getPostArrayCells();
		} else if (!p.getPostArrayCells().equals(postArrayCells)) {
		    newPostArrayCells = true;
		    String newName = "ArrayCellsDef" + toString();
		    ArrayExpr na = (ArrayExpr)
			con.mkConst(newName, postArrayCells.getSort());
		    postArrayCells = na;
		}
	    }

	    /*
	    ArrayExpr edgeLocks = p.getLocks();
	    if(edgeLocks != null) {
		if(locks == null) {
		    locks = edgeLocks;
		} else if ( !edgeLocks.equals(locks)) {
		    newLocks = true;
		    String newName = "Locks" + toString();
		    ArrayExpr nl = (ArrayExpr) con.mkConst(newName, locks.getSort());
		    locks = nl;
		}
	    } */
	}
    }

    public void mergeVars(int t, Map<Local, Expr> cur, Map<Local, Expr> eMap,
	    ArrayList<Local> nConsts, UnitEdge p) {
        Context con = Z3Converter.CONTEXT;

	for(Entry<Local, Expr> ey : eMap.entrySet()) {
	    Expr curConst = cur.get(ey.getKey());
	    if(curConst == null ) {
		cur.put(ey.getKey(), ey.getValue());
		String otherC = funcContext.get(ey.getKey());
		//This is true the very first time the variable is introduced
		if(otherC == null) {
		    otherC = contextNames.get(p.thread);
		}
		String f = funcContext.put(ey.getKey(), otherC);

	    } else if(!ey.getValue().equals(curConst)) {
		if (!nConsts.contains(ey.getKey())) {
		    nConsts.add(ey.getKey());

		    String fc = funcContext.get(ey.getKey());
		    String th = Z3Converter.THREAD_PRE + t;
		    String newName = ey.getKey().getName() + th + fc + intervals();
		    Expr newC = con.mkConst(newName, ey.getValue().getSort());
		    cur.put(ey.getKey(), newC);
		}
	    }
	}
    }

    public void mergeParams(Map<Integer, Expr> cur, Map<Integer, Expr> eMap,
	    ArrayList<Integer> nConsts, int t) {
        Context con = Z3Converter.CONTEXT;

	for(Entry<Integer, Expr> ey : eMap.entrySet()) {
	    Expr curConst = cur.get(ey.getKey());
	    if(curConst == null ) {
		cur.put(ey.getKey(), ey.getValue());

	    } else if(!ey.getValue().equals(curConst)) {
		if (!nConsts.contains(ey.getKey())) {
		    nConsts.add(ey.getKey());

		    String fc = contextNames.get(t);
		    String newName = Z3Converter.PARAMETER_PRE;
		    newName = newName + ey.getKey() + fc + intervals();
		    Expr newC = con.mkConst(newName, ey.getValue().getSort());
		    cur.put(ey.getKey(), newC);
		}
	    }
	}
    }

    public <E extends Expr> void mergeFields(Map<SootField, E> cur,
	    Map<SootField, E> eMap, ArrayList<SootField> nConsts) {
	Context con = Z3Converter.CONTEXT;

	for (Entry<SootField, E> ey : eMap.entrySet()) {
	    E newVal = null;

	    Expr curConst = cur.get(ey.getKey());
	    if(curConst == null) {
		newVal = ey.getValue();
	    } else if (!ey.getValue().equals(curConst)) {
		if(!nConsts.contains(ey.getKey())) {
		    nConsts.add(ey.getKey());
		    String newName = Z3Converter.getFieldString(ey.getKey());
		    newName = newName + intervals();
		    // If E is ArrayExpr, con.mkConst will be, too.
		    E newC = (E) con.mkConst(newName, ey.getValue().getSort());
		    newVal = newC;
		}
	    }
	    if (newVal != null) {
		cur.put(ey.getKey(), newVal);
	    }
	}
    }

    public ArrayExpr getPreArrayCells() {
	return preArrayCells;
    }

    public ArrayExpr getPostArrayCells() {
	return postArrayCells;
    }

/*
    public ArrayExpr getLocks() {
	return locks;
    } */

    //Returns null if both are null
    public ArrayExpr getArrayCells() {
	ArrayExpr result = postArrayCells;
	if(result == null) {
	    result = preArrayCells;
	}
	return result;
    }

    public Expr getVariable(Local l, int thread) {
	Expr result = null;
	result = postVars.get(thread).get(l);
	if (result == null) {
	    HashMap<Local, Expr> map = preVars.get(thread);
	    result = map.get(l);
	}
	return result;
    }

    public ArrayExpr getField(SootField f) {
	ArrayExpr res = postFields.get(f);
	if(res == null) {
	    res = preFields.get(f);
	}
	return res;
    }

    public Expr getStaticField(SootField f) {
	Expr res = postStaticFields.get(f);
	if(res == null) {
	    res = preStaticFields.get(f);
	}
	return res;
    }

    public Expr getLastException(int thread) {
	for(UnitEdge e : inEdges) {
	    if (e.thread == thread) {
		return e.cvtr().getLastException();
	    }
	}
	return null;
    }

    //In other methods I make the return unmodifiable.
    //I would need to do two levels of that here, so I omit it.
    public List<HashMap<Local, Expr>> getPreVars() {
        return preVars;
    }

    //In other methods I make the return unmodifiable.
    //I would need to do two levels of that here, so I omit it.
    public List<HashMap<Local, Expr>> getPostVars() {
        return postVars;
    }

    public Map<SootField, ArrayExpr> getPreFields() {
	return Collections.unmodifiableMap(preFields);
    }

    public Map<SootField, ArrayExpr> getPostFields() {
	return Collections.unmodifiableMap(postFields);
    }

    public Map<SootField, Expr> getPreStaticFields() {
	return Collections.unmodifiableMap(preStaticFields);
    }

    public Map<SootField, Expr> getPostStaticFields() {
	return Collections.unmodifiableMap(postStaticFields);
    }

    public BoolExpr makeNewConstEqs(UnitEdge e) {
	final Context con = Z3Converter.CONTEXT;

	BoolExpr result = con.mkTrue();
	if(!newPreFieldConsts.isEmpty()) {
	    BoolExpr tmp = makeNewFieldConstEqs(preFields, e.getPreFields(),
		    newPreFieldConsts);
	    result = con.mkAnd(result, tmp);
	}

	if(!newPreStaticFieldConsts.isEmpty()) {
	    BoolExpr tmp = makeNewFieldConstEqs(preStaticFields,
		    e.getPreStaticFields(), newPreStaticFieldConsts);
	    result = con.mkAnd(result, tmp);
	}

	if(!newPostFieldConsts.isEmpty()) {
	    BoolExpr tmp = makeNewFieldConstEqs(postFields, e.getPostFields(),
		    newPostFieldConsts);
	    result = con.mkAnd(result, tmp);
	}

	if(!newPostStaticFieldConsts.isEmpty()) {
	    BoolExpr tmp = makeNewFieldConstEqs(postStaticFields,
		    e.getPostStaticFields(), newPostStaticFieldConsts);
	    result = con.mkAnd(result, tmp);
	}

	Map<Local, Expr> vMap;
	Map<Local, Expr> eMap;
	for(int t = 0; t < newPreConsts.size(); t++) {
	    ArrayList<Local> nConsts = newPreConsts.get(t);
	    if(!nConsts.isEmpty()) {
		vMap = preVars.get(t);
		eMap = e.getPreVars().get(t);

		BoolExpr[] threadEq = new BoolExpr[nConsts.size()];
		for(int i = 0; i < threadEq.length; i++) {
		    Local l = nConsts.get(i);
		    threadEq[i] = con.mkEq(vMap.get(l), eMap.get(l));
		}
		BoolExpr junct = con.mkAnd(threadEq);
		result = con.mkAnd(result, junct);
	    }
	}

	for(int t = 0; t < newParamConsts.size(); t++) {
	    ArrayList<Integer> nConsts = newParamConsts.get(t);
	    if(!nConsts.isEmpty()) {
		Map<Integer, Expr> vpMap = params.get(t);
		Map<Integer, Expr> epMap = e.getParams().get(t);

		BoolExpr[] threadEq = new BoolExpr[nConsts.size()];
		for(int i = 0; i < threadEq.length; i++) {
		    Integer l = nConsts.get(i);
		    threadEq[i] = con.mkEq(vpMap.get(l), epMap.get(l));
		}
		BoolExpr junct = con.mkAnd(threadEq);
		result = con.mkAnd(result, junct);
	    }
	}

	for(int t = 0; t < newPostConsts.size(); t++) {
	    ArrayList<Local> nConsts = newPostConsts.get(t);
	    if(!nConsts.isEmpty()) {
		vMap = postVars.get(t);
		eMap = e.getPostVars().get(t);

		BoolExpr[] threadEq = new BoolExpr[nConsts.size()];
		for(int i = 0; i < threadEq.length; i++) {
		    Local l = nConsts.get(i);
		    threadEq[i] = con.mkEq(vMap.get(l), eMap.get(l));
		}
		BoolExpr junct = con.mkAnd(threadEq);
		result = con.mkAnd(result, junct);
	    }
	}

	if(newPreArrayCells) {
	    ArrayExpr otherArray = e.getPreArrayCells();
	    BoolExpr eq = con.mkEq(otherArray, preArrayCells);
	    result = con.mkAnd(result, eq);
	}

	if(newPostArrayCells) {
	    ArrayExpr otherArray = e.getPostArrayCells();
	    BoolExpr eq = con.mkEq(otherArray, postArrayCells);
	    result = con.mkAnd(result, eq);
	}

	return result;
    }

    public BoolExpr makeNewFieldConstEqs(Map<SootField, ? extends Expr> fMap,
	    Map<SootField, ? extends Expr> efMap,
	    ArrayList<SootField> nConsts) {
	final Context con = Z3Converter.CONTEXT;
	BoolExpr[] result = new BoolExpr[nConsts.size()];
	for(int j = 0; j < result.length; j++) {
	    SootField f = nConsts.get(j);
            result[j] = con.mkEq(fMap.get(f), efMap.get(f));
	}
	return con.mkAnd(result);
    }

    public boolean equals(Object other) {
	if(! (other instanceof SubpathTuple)) {
	    return false;
	} else {
	    SubpathTuple o = (SubpathTuple) other;
	    boolean result = true;
	    result = result && (this.paths == o.paths);
	    result = result && this.starts.equals(o.starts);
	    result = result && this.ends.equals(o.ends);

	    return result;
	}
    }

    public int hashCode() {
	return 53 * paths.hashCode() + 107 * starts.hashCode() +
	    71 * ends.hashCode();
    }

    public static boolean involvesAnyField(Unit u) {
	if(Z3Converter.hasInvoke(u) != null) {
	    return true;
	}
	List<soot.ValueBox> boxes = u.getUseAndDefBoxes();
	for(soot.ValueBox vb : boxes) {
	    Value v = vb.getValue();
	    if (v instanceof FieldRef) {
		return true;
	    }
	}
	return false;
    }

    public static List<SootField> fieldsUsed(Unit u, boolean inDef) {
	InvokeExpr inv = Z3Converter.hasInvoke(u);
	if(inv != null) {
	    return Z3Converter.fakeFieldsUsed(inv, inDef);
	}
	ArrayList<SootField> result = new ArrayList<SootField>();
	List<soot.ValueBox> boxes = inDef ? u.getDefBoxes() : u.getUseBoxes();
	for(soot.ValueBox vb : boxes) {
	    Value v = vb.getValue();
	    if(v instanceof FieldRef) {
		result.add(((FieldRef) v).getField());
	    }
	}
	return result;
    }

    public static List<SootField> fieldsUsed(Collection<Unit> units,
	    boolean inDef) {

	ArrayList<SootField> result = new ArrayList<SootField>();
	for(Unit u : units) {
	    result.addAll(fieldsUsed(u, inDef));
	}
	return result;
    }

    public static List<SootField> fieldsTouched(Unit u, boolean inBoth) {
	InvokeExpr inv = Z3Converter.hasInvoke(u);
	if(inv != null) {
	    return Z3Converter.fakeFieldsTouched(inv, inBoth);
	}
	ArrayList<SootField> result = new ArrayList<SootField>();
	List<soot.ValueBox> boxes = inBoth ? u.getUseAndDefBoxes()
					       : u.getDefBoxes();
	for(soot.ValueBox vb : boxes) {
	    Value v = vb.getValue();
	    if(v instanceof FieldRef) {
		result.add(((FieldRef) v).getField());
	    }
	}
	return result;
    }

    public static List<SootField> fieldsTouched(Collection<Unit> uuu,
	    boolean inBoth) {
	List<SootField> result = new ArrayList<SootField>();
	for(Unit u : uuu) {
	    result.addAll(fieldsTouched(u, inBoth));
	}
	return result;
    }


    public static boolean usesFields(Collection<Unit> units,
	    List<SootField> fields, boolean inBoth) {
	if(fields.isEmpty()) {
	    return false;
	}
	for(SootField f : fields) {
	    if (f instanceof FakeSootField) {
		for(Unit u : units) {
		    InvokeExpr inv = Z3Converter.hasInvoke(u);
		    if(inv != null) {
			List<SootField> used =
			    Z3Converter.fakeFieldsTouched(inv, inBoth);
			if( used.contains(f)) {
			    return true;
			}
		    }
		}
	    }
	}
	for(Unit u : units) {
	    List<soot.ValueBox> boxes = inBoth ? u.getUseAndDefBoxes()
					       : u.getDefBoxes();
	    for(soot.ValueBox vb : boxes) {
		Value v = vb.getValue();
		if(v instanceof FieldRef) {
		    SootField used = ((FieldRef) v).getField();
		    for(SootField f : fields) {
			if(used.equals(f)) {
			    return true;
			}
		    }
		}
	    }
	}
	return false;
    }

    public static boolean readsField(Unit u, SootField field) {
	if(field instanceof FakeSootField) {
	    InvokeExpr inv = Z3Converter.hasInvoke(u);
	    if(inv == null) {
		return false;
	    }
	    return Z3Converter.fakeFieldsUsed(inv, false).contains(field);
	}
	List<soot.ValueBox> boxes = u.getUseBoxes();
	for(soot.ValueBox vb : boxes) {
	    Value v = vb.getValue();
	    if(v instanceof FieldRef) {
		SootField used = ((FieldRef) v).getField();
		if(used.equals(field)) {
		    return true;
		}
	    }
	}
	return false;
    }

    public static boolean usesField(Unit u, SootField field, boolean inBoth) {
	if(field instanceof FakeSootField) {
	    InvokeExpr inv = Z3Converter.hasInvoke(u);
	    if(inv == null) {
		return false;
	    }
	    return Z3Converter.fakeFieldsTouched(inv, inBoth).contains(field);
	}
	List<soot.ValueBox> boxes = inBoth ? u.getUseAndDefBoxes()
                                           : u.getDefBoxes();
	for(soot.ValueBox vb : boxes) {
	    Value v = vb.getValue();
	    if(v instanceof FieldRef) {
		SootField used = ((FieldRef) v).getField();
		if(used.equals(field)) {
		    return true;
		}
	    }
	}
	return false;
    }

    public static boolean interferes(Unit last, int thread,
	    SubpathTuple next, boolean bool) {

	List<Unit> units = Collections.singletonList(last);

        //The following gets all the fields that were read or written by
        // the unit last, depending on the boolean argument (parameter inDef).
        List<SootField> fieldsUsed = fieldsUsed(units, bool);
        if(fieldsUsed.isEmpty()) {//saves time
            return false;
        }
        for(int t = 0; t < next.paths.numThreads; t++) {
            if (t != thread) {
                List<Unit> subpath = next.paths.subpath(t, next.starts.get(t),
                        next.ends.get(t));
                //Here, bool means "inBoth" (either just written or either read
                // or written)
                if (usesFields(subpath, fieldsUsed, bool)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setAuxilliaryConstraints(BoolExpr consts) {
	if(consts != null && this.isZero()) {
	    auxilliaryConstraints = consts;
	}
    }

    public BoolExpr getAuxilliaryConstraints() {
	return auxilliaryConstraints;
    }

    public BoolExpr getActuallyHappened() {
	//This method should never actually be called
	if (4 > 2) {
	    throw new RuntimeException("Called getActuallyHappened");
	}
	final Context con = Z3Converter.CONTEXT;
	return con.mkBoolConst("?ActuallyHappened" + this);
    }
}
