/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.Local;
import soot.SootField;
import soot.RefType;
import com.microsoft.z3.Expr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.ArrayExpr;

import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public abstract class UnitEdge {
    private final SubpathTuple head;

    //Right now this is a problem: when this is run, getTail is still null,
    //so lastVertex is always null
    private final Z3Converter cvtr = new Z3Converter(this);
    private final RefType excThrown;

    public final int ID;
    public final int thread;

    private HashMap<Local, Expr> preVars;
    private HashMap<Integer, Expr> params;

    private HashMap<Local, Expr> postVars;
    private ArrayExpr arrayCells = null;

    public boolean inInterleaving;

    private static int count = 0;

    public UnitEdge(SubpathTuple head, int thread, RefType excThrown) {
	this.head = head;
	ID = count++;
	this.thread = thread;
	this.excThrown = excThrown;

        preVars = new HashMap<Local, Expr>();

	params = new HashMap<Integer, Expr>();

        postVars = new HashMap<Local, Expr>();
    }

    public SubpathTuple getHead() {
	return head;
    }

    public abstract List<SubpathTuple> getTails();

    public abstract BoolExpr getBackwardPath();

    public abstract void generateFormula();

    public abstract BoolExpr getFormula();

    public abstract SubpathTuple getVarTail();

    //public abstract boolean tailsInterleavable(SubpathTuple other);

    /* Returns null if it actually cannot be run concurrently. */
    public abstract BoolExpr contextFormula(SubpathTuple other);

    /* Returns null if it actually cannot be run concurrently. */
    public abstract BoolExpr contextFormulaRec(SubpathTuple other);

    public Z3Converter cvtr() {
	return cvtr;
    }


    public ArrayList<HashMap<Local, Expr>> getPreVars() {
	ArrayList<HashMap<Local, Expr>> result = getTailPreVars();
	result.get(thread).putAll(cvtr.getNewThreadPreMappings());
	return result;
    }

    public List<HashMap<Integer, Expr>> getParams() {
	ArrayList<HashMap<Integer, Expr>> result = getTailParams();
	result.get(thread).putAll(cvtr.getParams());
	//
	return Collections.unmodifiableList(result);
    }

    public ArrayList<HashMap<Local, Expr>> getPostVars() {
	ArrayList<HashMap<Local, Expr>> result = getTailPostVars();
	result.get(thread).putAll(cvtr.getNewThreadMappings());
	return result;
    }

    public Map<SootField, ArrayExpr> getPreFields() {
	Map<SootField, ArrayExpr> fields = getTailPreFields();
	fields.putAll(cvtr().getPreFields());
	return fields;
    }

    public Map<SootField, ArrayExpr> getPostFields() {
	Map<SootField, ArrayExpr> fields = getTailPostFields();
	fields.putAll(cvtr().getPostFields());
	return fields;
    }

    public Map<SootField, Expr> getPreStaticFields() {
	Map<SootField, Expr> fields = getTailPreStaticFields();
	fields.putAll(cvtr().getPreStaticFields());
	return fields;
    }

    public Map<SootField, Expr> getPostStaticFields() {
	Map<SootField, Expr> fields = getTailPostStaticFields();
	fields.putAll(cvtr().getPostStaticFields());
	return fields;
    }

    public com.microsoft.z3.ArrayExpr getPreArrayCells() {
	arrayCells = getTailPreArrayCells();
	com.microsoft.z3.ArrayExpr cvtrCells = cvtr().getNewPreArrayCells();
	if(cvtrCells != null) {
	    arrayCells = cvtrCells;
	}
	return arrayCells;
    }

    public com.microsoft.z3.ArrayExpr getPostArrayCells() {
	arrayCells = getTailPostArrayCells();
	com.microsoft.z3.ArrayExpr cvtrCells = cvtr().getNewPostArrayCells();
	if(cvtrCells != null) {
	    arrayCells = cvtrCells;
	}
	return arrayCells;
    }

    /* public com.microsoft.z3.ArrayExpr getLocks() {
	com.microsoft.z3.ArrayExpr res = getTailLocks();
	com.microsoft.z3.ArrayExpr cvtrLocks = cvtr().getNewLocks();
	if(cvtrLocks != null) {
	    res = cvtrLocks;
	}
	return res;
    } */


    public abstract ArrayList<HashMap<Local, Expr>> getTailPreVars();

    public abstract ArrayList<HashMap<Integer, Expr>> getTailParams();

    public abstract ArrayList<HashMap<Local, Expr>> getTailPostVars();

    public abstract Map<SootField, ArrayExpr> getTailPreFields();

    public abstract Map<SootField, ArrayExpr> getTailPostFields();

    public abstract Map<SootField, Expr> getTailPreStaticFields();

    public abstract Map<SootField, Expr> getTailPostStaticFields();

    public abstract com.microsoft.z3.ArrayExpr getTailPreArrayCells();

    public abstract com.microsoft.z3.ArrayExpr getTailPostArrayCells();

    //public abstract com.microsoft.z3.ArrayExpr getTailLocks();

    public RefType getExcThrown() {
	return excThrown;
    }

}
