/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.Unit;
import soot.Local;
import soot.SootField;
import soot.RefType;
import soot.jimple.ReturnStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.InvokeExpr;
import soot.jimple.DefinitionStmt;
import soot.jimple.ThrowStmt;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;

public class UnitHyperEdge extends UnitEdge {
    private final SubpathTuple callTail;
    private final SubpathTuple exitTail;

    private final Unit callStmt;
    private final Unit exitStmt;

    private BoolExpr formula;

    private ArrayList<HashMap<Local, Expr>> newPreVars =
	new ArrayList<HashMap<Local, Expr>>();

    private Map<SootField, ArrayExpr> newPreFields = null;
    private Map<SootField, Expr> newPreStaticFields = null;
    private ArrayExpr newPreArrayCells = null;

    //I do not need to worry about this because params will always be created
    //on a single edge -- they come from the IdentityStmts at the beginning of
    //each method
    //private ArrayList<IdentityRef> params = null;

    public UnitHyperEdge(SubpathTuple callTail, Unit call,
	    SubpathTuple exitTail, Unit ret,
	    SubpathTuple head, int thread, RefType excThrown) {
	super(head, thread, excThrown);
	callStmt = call;
	exitStmt = ret;
	this.callTail = callTail;
	this.exitTail = exitTail;
    }

    public SubpathTuple getCallTail() {
	return callTail;
    }

    public SubpathTuple getExitTail() {
	return exitTail;
    }

    public List<SubpathTuple> getTails() {
	return Arrays.asList(new SubpathTuple[]{callTail, exitTail});
    }


    public Unit getCallStmt() {
	return callStmt;
    }

    public Unit getExitStmt() {
	return exitStmt;
    }

    public BoolExpr getBackwardPath() {
	final Context con = Z3Converter.CONTEXT;
	return con.mkAnd(formula, callTail.backwardInterpolant(),
		exitTail.backwardInterpolant());
    }

    public void generateFormula() {
	Context con = Z3Converter.CONTEXT;

	RefType eThrown = getExcThrown();
	if(eThrown == null) {
	    if(callStmt instanceof InvokeStmt) {
		formula = cvtr().ofInvokeStmt((InvokeStmt) callStmt, exitStmt,
			callTail, exitTail);
	    } else if (callStmt instanceof DefinitionStmt) {
		formula = cvtr().ofCall((DefinitionStmt) callStmt, (ReturnStmt) exitStmt,
			callTail, exitTail);
	    }
	} else {
	    soot.jimple.InvokeExpr inv = null;
	    if (callStmt instanceof InvokeStmt) {
		inv = ((InvokeStmt) callStmt).getInvokeExpr();
	    } else if (callStmt instanceof DefinitionStmt) {
		inv = (InvokeExpr) ((DefinitionStmt) callStmt).getRightOp();
	    }
	    //TODO : I actually do not want to cast to throw stmt, it could
	    //be an implicit exception
	    formula = cvtr().ofCallThrow(inv, (ThrowStmt) exitStmt,
		    callTail, exitTail);
	}

	ArrayList<BoolExpr> prepost = new ArrayList<BoolExpr>();

	List<HashMap<Local, Expr>> callPost = callTail.getPostVars();
	List<HashMap<Local, Expr>> exitPre = exitTail.getPreVars();
	for(int t = 0; t < exitPre.size(); t++) {
	    HashMap<Local, Expr> post = callPost.get(t);
	    //this should be empty for my thread
	    HashMap<Local, Expr> pre = new HashMap<Local, Expr>
		(exitPre.get(t));

	    Iterator<Local> itr = pre.keySet().iterator();
	    while (itr.hasNext()) {
		Local l = itr.next();
		Expr e = post.get(l);
		if(e != null) {
		    BoolExpr eq = con.mkEq(pre.get(l), e);
		    prepost.add(eq);
		    itr.remove();
		}
	    }
	    newPreVars.add(pre);
	}

	Map<SootField, ArrayExpr> cPreFields = callTail.getPreFields();
	Map<SootField, ArrayExpr> postFields = callTail.getPostFields();
	Map<SootField, ArrayExpr> preFields = exitTail.getPreFields();
	preFields = new HashMap<SootField, ArrayExpr>(preFields);;
	Iterator<SootField> itr = preFields.keySet().iterator();
	while (itr.hasNext()) {
	    SootField f = itr.next();
	    Expr e = postFields.get(f);
	    if(e != null) {
		BoolExpr eq = con.mkEq(preFields.get(f), e);
		prepost.add(eq);
		itr.remove();
	    }
	}
	itr = preFields.keySet().iterator();
	while (itr.hasNext()) {
	    SootField f = itr.next();
            Expr e = cPreFields.get(f);
	    if(e != null) {
		BoolExpr eq = con.mkEq(preFields.get(f), e);
		prepost.add(eq);
                //System.out.println("Prepost: " + eq);
		itr.remove();
	    }
        }
	newPreFields = new HashMap<SootField, ArrayExpr>(preFields);

	Map<SootField, Expr> cPreStatics = callTail.getPreStaticFields();
	Map<SootField, Expr> postStatics = callTail.getPostStaticFields();
	Map<SootField, Expr> preStatics = exitTail.getPreStaticFields();
	preStatics = new HashMap<SootField, Expr>(preStatics);
	itr = preStatics.keySet().iterator();
	while (itr.hasNext()) {
	    SootField f = itr.next();
	    Expr e = postStatics.get(f);
	    if(e != null) {
		BoolExpr eq = con.mkEq(preStatics.get(f), e);
		prepost.add(eq);
		itr.remove();
	    }
	}
	itr = preStatics.keySet().iterator();
	while (itr.hasNext()) {
	    SootField f = itr.next();
            Expr e = cPreStatics.get(f);
	    if(e != null) {
		BoolExpr eq = con.mkEq(preStatics.get(f), e);
		prepost.add(eq);
                //System.out.println("Prepost: " + eq);
		itr.remove();
	    }
        }
	newPreStaticFields = new HashMap<SootField, Expr>(preStatics);

	ArrayExpr preArrays = exitTail.getPreArrayCells();
	newPreArrayCells = preArrays;
	if(preArrays != null) {
	    ArrayExpr postArrays = callTail.getPostArrayCells();
	    if(postArrays != null) {
		BoolExpr eq = con.mkEq(preArrays, postArrays);
		newPreArrayCells = null;
		prepost.add(eq);
	    }
	}

	BoolExpr[] eqs = prepost.toArray(new BoolExpr[0]);
	BoolExpr eqsF = con.mkAnd(eqs);
	formula = con.mkAnd(formula, eqsF);

    }

    public ArrayList<HashMap<Local, Expr>> getTailPreVars() {
	for(int t = 0; t < newPreVars.size(); t++) {
	    newPreVars.get(t).putAll(callTail.getPreVars().get(t));
	}
	return newPreVars;
    }

    public ArrayList<HashMap<Integer, Expr>> getTailParams() {
	ArrayList<HashMap<Integer, Expr>> res = new ArrayList<HashMap<Integer, Expr>> ();
	List<HashMap<Integer, Expr>> other = callTail.getParams();
	for(int t = 0; t < other.size(); t++) {
	    res.add(new HashMap<Integer, Expr>(other.get(t)));
	}
	other = exitTail.getParams();
	for(int t = 0; t < other.size(); t++) {
	    if (t != thread) {
		int old_size = res.get(t).size();
		String old_string = res.get(t).toString();
		int other_size = other.get(t).size();
		//There actually can be conflicts here, since a subpath may have
		//a param that is never actually used.
		//Putting the exit in second handles this correctly.
		res.get(t).putAll(other.get(t));
	    }
	}
	return res;
    }

    public Map<SootField, ArrayExpr> getTailPreFields() {
	newPreFields.putAll(callTail.getPreFields());
	return newPreFields;
    }

    public Map<SootField, ArrayExpr> getTailPostFields() {
	HashMap<SootField, ArrayExpr> res = new HashMap<SootField, ArrayExpr>
	    (callTail.getPostFields());
	res.putAll(exitTail.getPostFields());
	return res;
    }

    public Map<SootField, Expr> getTailPreStaticFields() {
	newPreStaticFields.putAll(callTail.getPreStaticFields());
	return newPreStaticFields;
    }

    public Map<SootField, Expr> getTailPostStaticFields() {
	HashMap<SootField, Expr> res = new HashMap<SootField, Expr>
	    (callTail.getPostStaticFields());
	res.putAll(exitTail.getPostStaticFields());
	return res;
    }

    public com.microsoft.z3.ArrayExpr getTailPreArrayCells() {
	com.microsoft.z3.ArrayExpr res = newPreArrayCells;
	if(res == null) {
	    res = callTail.getPreArrayCells();
	}
	return res;
    }

    public com.microsoft.z3.ArrayExpr getTailPostArrayCells() {
	com.microsoft.z3.ArrayExpr res = exitTail.getPostArrayCells();
	if(res == null) {
	    res = callTail.getPostArrayCells();
	}
	return res;
    }

    /* public com.microsoft.z3.ArrayExpr getTailLocks() {
	com.microsoft.z3.ArrayExpr res = exitTail.getLocks();
	if(res == null) {
	    res = callTail.getLocks();
	}
	return res;
    } */

    public ArrayList<HashMap<Local, Expr>> getTailPostVars() {
	ArrayList<HashMap<Local, Expr>> res = new ArrayList<HashMap<Local, Expr>> ();
	List<HashMap<Local, Expr>> other = callTail.getPostVars();
	for(int t = 0; t < other.size(); t++) {
	    res.add(new HashMap<Local, Expr>(other.get(t)));
	}
	other = exitTail.getPostVars();
	for(int t = 0; t < other.size(); t++) {
	    //the locals for this thread have just gone out of scope
	    if(t != thread) {
		res.get(t).putAll(other.get(t));
	    }
	}
	return res;
    }

    public BoolExpr getFormula() {
	return formula;
    }

    public SubpathTuple getVarTail() {
	return callTail;
    }

    public boolean tailsInterleavable(SubpathTuple other) {
	return other.interleavable(callTail) &&
	    other.interleavable(exitTail);
    }

    public BoolExpr contextFormula(SubpathTuple other) {
	if(callTail.interleavable(other) &&
		exitTail.interleavable(other) &&

		callTail.constrainedBy(other) &&
		exitTail.constrainedBy(other)) {

	    BoolExpr res = getFormula();
	    final Context con = Z3Converter.CONTEXT;
	    res = con.mkAnd(res, callTail.getActuallyHappened(),
		    exitTail.getActuallyHappened());
	    if(callTail.summary() != null) {
		res = con.mkAnd(res, callTail.summary());
	    }
	    if(exitTail.summary() != null) {
		res = con.mkAnd(res, exitTail.summary());
	    }
	    return res;
	} //else
	return null;
    }

    public BoolExpr contextFormulaRec(SubpathTuple other) {
	if(callTail.interleavable(other) && exitTail.interleavable(other)) {

	    //The calls to makeForwardContextRec and the constrainedBy checks must
	    //be in this order since the calls set the check condition
	    BoolExpr callContext = callTail.makeForwardContextRec(other);
	    BoolExpr exitContext = exitTail.makeForwardContextRec(other);

	    if(callTail.constrainedBy(other) && exitTail.constrainedBy(other)) {

		BoolExpr res = getFormula();
		final Context con = Z3Converter.CONTEXT;
		res = con.mkAnd(res, callContext, exitContext);
		return res;
	    }
	} //else
	return null;
    }

    /* public SubpathTuple getOtherThreadVarTail() {
	return exitTail;
    } */
}
