/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.Unit;
import soot.Local;
import soot.SootField;
import soot.RefType;
import soot.jimple.InvokeStmt;

import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class UnitSingleEdge extends UnitEdge {
    private final Unit stmt;
    private final SubpathTuple tail;

    private BoolExpr formula;

    private Map<Integer, Expr> params = null;

    public UnitSingleEdge(SubpathTuple tail, Unit stmt,
	    SubpathTuple head, int thread, RefType excThrown) {
	super(head, thread, excThrown);
	this.stmt = stmt;
	this.tail = tail;

    }

    public BoolExpr getFormula() {
	return formula;
    }

    public BoolExpr getBackwardPath() {
	final Context con = Z3Converter.CONTEXT;
	return con.mkAnd(formula, tail.backwardInterpolant());
    }

    public SubpathTuple getTail() {
	return tail;
    }

    public List<SubpathTuple> getTails() {
	return Arrays.asList(new SubpathTuple[]{ tail });
    }

    public SubpathTuple getVarTail() {
	return tail;
    }

    public boolean tailsInterleavable(SubpathTuple other) {
	return tail.interleavable(other);
    }

    public BoolExpr contextFormula(SubpathTuple other) {
	if(tail.interleavable(other)
		&& tail.constrainedBy(other)) {

	    final Context con = Z3Converter.CONTEXT;

	    BoolExpr res = getFormula();
	    res = con.mkAnd(res, tail.getActuallyHappened());
	    if(tail.summary() != null) {
		res = con.mkAnd(res, tail.summary());
	    }
	    return res;
	}
	return null;
    }

    public BoolExpr contextFormulaRec(SubpathTuple other) {
	if(tail.interleavable(other)) {
	    //The call to makeForwardContextRec and the constrainedBy check must
	    //be in this order since the call sets the check condition
	    BoolExpr tailContext = tail.makeForwardContextRec(other);
	    if(tail.constrainedBy(other)) {
		final Context con = Z3Converter.CONTEXT;
		BoolExpr res = getFormula();
		res = con.mkAnd(res, tailContext);
		return res;
	    }
	}
	return null;
    }

    public Unit getStmt() {
	return stmt;
    }

    public void generateFormula() {
	if( !(stmt instanceof InvokeStmt)) {
            //try {
                formula = cvtr().ofUnit(stmt);
            /* } catch (com.microsoft.z3.Z3Exception ex) {
                System.out.println("Error unit: " + stmt);
                throw ex;
            } */
	} else {//This is the only way we should have an invoke statement
	    //calls to get are invokeExpr
	    formula = cvtr().ofLock(((InvokeStmt) stmt).getInvokeExpr());
	}
    }

    public ArrayList<HashMap<Local, Expr>> getTailPreVars() {
	ArrayList<HashMap<Local, Expr>> res = new ArrayList<HashMap<Local, Expr>> ();
	List<HashMap<Local, Expr>> other = tail.getPreVars();
	for(int t = 0; t < other.size(); t++) {
	    res.add(new HashMap<Local, Expr>(other.get(t)));
	}
	return res;
    }

    public ArrayList<HashMap<Integer, Expr>> getTailParams() {
	ArrayList<HashMap<Integer, Expr>> res = new ArrayList<HashMap<Integer, Expr>> ();
	List<HashMap<Integer, Expr>> other = tail.getParams();
	for(int t = 0; t < other.size(); t++) {
	    res.add(new HashMap<Integer, Expr>(other.get(t)));
	}
	return res;
    }

    public ArrayList<HashMap<Local, Expr>> getTailPostVars() {
	ArrayList<HashMap<Local, Expr>> res = new ArrayList<HashMap<Local, Expr>> ();
	List<HashMap<Local, Expr>> other = tail.getPostVars();
	for(int t = 0; t < other.size(); t++) {
	    res.add(new HashMap<Local, Expr>(other.get(t)));
	}
	return res;
    }

    public Map<SootField, ArrayExpr> getTailPreFields() {
	return new HashMap<SootField, ArrayExpr>(tail.getPreFields());
    }

    public Map<SootField, ArrayExpr> getTailPostFields() {
	return new HashMap<SootField, ArrayExpr>(tail.getPostFields());
    }

    public Map<SootField, Expr> getTailPreStaticFields() {
	return new HashMap<SootField, Expr>(tail.getPreStaticFields());
    }

    public Map<SootField, Expr> getTailPostStaticFields() {
	return new HashMap<SootField, Expr>(tail.getPostStaticFields());
    }

    public com.microsoft.z3.ArrayExpr getTailPreArrayCells() {
	return tail.getPreArrayCells();
    }

    public com.microsoft.z3.ArrayExpr getTailPostArrayCells() {
	return tail.getPostArrayCells();
    }

    /* public com.microsoft.z3.ArrayExpr getTailLocks() {
	return tail.getLocks();
    } */
}
