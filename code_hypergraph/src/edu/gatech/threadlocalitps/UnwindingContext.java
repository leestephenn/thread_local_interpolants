/** Copyright (c) 2016 Stephen N. Lee.  */

package edu.gatech.threadlocalitps;
import soot.Unit;
import soot.ValueBox;
import soot.RefType;
import soot.SootMethodRef;
import soot.SootClass;
import soot.SootMethod;
import soot.RefType;
import soot.Body;
import soot.SootField;
import soot.jimple.InvokeExpr;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.ThrowStmt;
import soot.jimple.Jimple;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.SimpleDominatorsFinder;
import soot.toolkits.graph.ExceptionalUnitGraph.ExceptionDest;
import soot.tagkit.Tag;
import soot.Scene;
import soot.FastHierarchy;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Iterator;


public class UnwindingContext {
    //This could be static, but then it might not load right.
    public static SootMethod errorIndic = null;
    //public static SootMethod lockIndic = null;
    //public static SootMethod unlockIndic = null;
    //public static SootMethod tLocalGetIndic = null;
    private static Set<SootMethod> libMethods = new HashSet<SootMethod>();

    /* private static Map<SootMethod, Map<InvokeExpr, ExceptionalUnitGraph>> methodBodies =
	new HashMap<SootMethod, Map<InvokeExpr, ExceptionalUnitGraph>>(); */
    private static Map<SootMethod, ExceptionalUnitGraph> methodsCalled =
	new HashMap<SootMethod, ExceptionalUnitGraph>();
    private static Map<Unit, ExceptionalUnitGraph> cfgs =
	new HashMap<Unit, ExceptionalUnitGraph>();

    //private static int locationCtr = 0;

    private List<ArrayList<Unit>> contexts;
    private List<Integer> expandPoints;
    private List<Set<SootField>> mustRead = new ArrayList<Set<SootField>>();
    private List<Set<SootField>> mustWrite = new ArrayList<Set<SootField>>();
    private List<Set<SootField>> mustUse = new ArrayList<Set<SootField>>();


    //this is really legacy
    public UnwindingContext(ExceptionalUnitGraph cfg, int numThreads) {
	fillLibMethods();
	/* //condition is necessary since this method could be called multiple times
	if(!methodsCalled.containsValue(cfg)) {
	    for(Unit u : cfg) {
		u.addTag(new LocationTag());
	    }
	} */
	Unit init = cfg.getHeads().get(0);//In all files, only one head
	Unit error = findErrorNode(cfg);

	Body thisBody = cfg.getBody();
	SootMethod top = thisBody.getMethod();
	methodsCalled.put(top, cfg);
	/* HashMap<InvokeExpr, ExceptionalUnitGraph> topMap =
	    new HashMap<InvokeExpr, ExceptionalUnitGraph>();
	topMap.put(null, cfg);
	methodBodies.put(top, topMap); */

	List<Unit> start = Arrays.asList(new Unit[]{init, error});

	contexts = new ArrayList<ArrayList<Unit>>(numThreads);
	expandPoints = new ArrayList<Integer>();
	//cfgs = new HashMap<Unit, ExceptionalUnitGraph>();
	for (int i = 0; i < numThreads; i++) {
	    contexts.add(new ArrayList<Unit>(start));
	    expandPoints.add(1);
	    cfgs.put(error, cfg);
	    cfgs.put(init, cfg);
	}

	for(int i = 0; i < numThreads; i++) {
	    mustRead.add(new HashSet<SootField>());
	    mustWrite.add(new HashSet<SootField>());
	    mustUse.add(new HashSet<SootField>());
	}
    }

    public UnwindingContext(ExceptionalUnitGraph cfg, Unit init, Unit error,
	    int numThreads) {
	fillLibMethods();
	/* //if it has been added, tags have already been added for its units
	if(!methodsCalled.containsValue(cfg)) {
	    for(Unit u : cfg) {
		u.addTag(new LocationTag());

		int s = u.getTags().size();
		if(s != 1) {
		    //System.err.println("When creating, " + s + " tags, not 1");
		    throw new IllegalStateException("When creating, " + s + " tags, not 1");
		}
	    }
	} */
	Body thisBody = cfg.getBody();
	SootMethod top = thisBody.getMethod();
	methodsCalled.put(top, cfg);
	/* HashMap<InvokeExpr, ExceptionalUnitGraph> topMap =
	    new HashMap<InvokeExpr, ExceptionalUnitGraph>();
	topMap.put(null, cfg);
	methodBodies.put(top, topMap); */

	contexts = new ArrayList<ArrayList<Unit>>(numThreads);
	expandPoints = new ArrayList<Integer>();

	List<Unit> start = Arrays.asList(new Unit[]{init, error});

	//cfgs = new HashMap<Unit, ExceptionalUnitGraph>();
	for (int i = 0; i < numThreads; i++) {
	    contexts.add(new ArrayList<Unit>(start));
	    expandPoints.add(1);
	    cfgs.put(error, cfg);
	    cfgs.put(init, cfg);
	}

	for(int i = 0; i < numThreads; i++) {
	    mustRead.add(new HashSet<SootField>());
	    mustWrite.add(new HashSet<SootField>());
	    mustUse.add(new HashSet<SootField>());
	}
    }

    public UnwindingContext(List<ExceptionalUnitGraph> CFGS, List<Unit> inits,
	    List<Unit> errors, int numThreads) {
	if( !(numThreads == CFGS.size() && CFGS.size() == inits.size() &&
		    inits.size() == errors.size())) {
	    throw new IllegalArgumentException("All the arguments of the UnwindingContext constructor must have the same length.");
	}
	fillLibMethods();

	for(ExceptionalUnitGraph cfg : CFGS) {
	    /* if(!methodsCalled.containsValue(cfg)) {
		for(Unit u : cfg) {
		    u.addTag(new LocationTag());
		}
	    } */
	}

	contexts = new ArrayList<ArrayList<Unit>>(numThreads);
	expandPoints = new ArrayList<Integer>(numThreads);
	for(int i = 0; i < CFGS.size(); i++) {

	    Body thisBody = CFGS.get(i).getBody();
	    SootMethod top = thisBody.getMethod();
	    methodsCalled.put(top, CFGS.get(i));
	    /* HashMap<InvokeExpr, ExceptionalUnitGraph> topMap =
		new HashMap<InvokeExpr, ExceptionalUnitGraph>();
	    topMap.put(null, CFGS.get(i));
	    methodBodies.put(top, topMap); */

	    List<Unit> start = Arrays.asList(new Unit[]{inits.get(i),
		errors.get(i)});

	//cfgs = new HashMap<Unit, ExceptionalUnitGraph>();
	    contexts.add(new ArrayList<Unit>(start));
	    expandPoints.add(1);
	    cfgs.put(errors.get(i), CFGS.get(i));
	    cfgs.put(inits.get(i), CFGS.get(i));
	}

	for(int i = 0; i < numThreads; i++) {
	    mustRead.add(new HashSet<SootField>());
	    mustWrite.add(new HashSet<SootField>());
	    mustUse.add(new HashSet<SootField>());
	}
    }

    //For the following constructors, I assume that the units are already tagged
    // because they come from later
    private UnwindingContext(List<ArrayList<Unit>> contexts,
	    List<Integer> expandPoints) {
	this.contexts = new ArrayList<ArrayList<Unit>>(contexts.size());
	for (ArrayList<Unit> stmts : contexts) {
	    this.contexts.add(new ArrayList<Unit>(stmts));//copy them
	}
	this.expandPoints = new ArrayList<Integer>(expandPoints);
	//this.cfgs = new HashMap<Unit, ExceptionalUnitGraph>(cfgs);

	for(int i = 0; i < contexts.size(); i++) {
	    mustRead.add(new HashSet<SootField>());
	    mustWrite.add(new HashSet<SootField>());
	    mustUse.add(new HashSet<SootField>());
	}

    }

    public UnwindingContext(int thread, Unit newU, UnwindingContext old) {
	this(old.contexts, old.expandPoints);
	int expandPoint = expandPoints.get(thread);
	//Actually has the same expandPoint, with the rest shifted
	contexts.get(thread).add(expandPoint, newU);

	for(int i = 0; i < mustWrite.size(); i++) {
	    mustRead.get(i).addAll(old.mustRead.get(i));
	    mustWrite.get(i).addAll(old.mustWrite.get(i));
	    mustUse.get(i).addAll(old.mustUse.get(i));
	}

	//Z3Converter.inThread = thread;
	mustRead.get(thread).removeAll(SubpathTuple.fieldsUsed(newU, false));
	mustWrite.get(thread).removeAll(SubpathTuple.fieldsTouched(newU, false));
	mustUse.get(thread).removeAll(SubpathTuple.fieldsTouched(newU, true));
    }

    //call is in thread, mid is in the other
    public UnwindingContext(int thread, Unit call, Unit mid, UnwindingContext old) {
	this(old.contexts, old.expandPoints);
	int expandPoint = expandPoints.get(thread);
	contexts.get(thread).add(expandPoint, call);

	expandPoint = expandPoints.get(1 - thread);
	if(mid == null) {
	    //this should be the error node
	    mid = contexts.get(1 - thread).get(expandPoint);
	}
	contexts.get(1 - thread).add(expandPoint, mid);

	//Add empties because this is only called from SubpathTuple
    }

    public UnwindingContext(int thread, Unit last, Unit entry, Unit exit, Unit mid,
	    UnwindingContext old) {
	this(old.contexts, old.expandPoints);
	int expandPoint = expandPoints.get(thread);
	List<Unit> n = Arrays.asList(new Unit[]{last, entry, exit});
	contexts.get(thread).addAll(expandPoint, n);
	expandPoints.set(thread, expandPoint + 2);

	expandPoint = expandPoints.get(1 - thread);
	if(mid == null) {
	    //this should be the error node
	    mid = contexts.get(1 - thread).get(expandPoint);
	}
	contexts.get(1 - thread).add(expandPoint, mid);
	expandPoints.set(1 - thread, expandPoint + 1);


	//Add nothing to mustWrite etc because this is only called from SubpathTuple
    }

    private UnwindingContext myClone() {
	UnwindingContext clone =
	    new UnwindingContext(this.contexts, this.expandPoints);
	for(int i = 0; i < contexts.size(); i++) {
	    clone.mustRead.get(i).addAll(this.mustRead.get(i));
	    clone.mustWrite.get(i).addAll(this.mustWrite.get(i));
	    clone.mustUse.get(i).addAll(this.mustUse.get(i));
	}
	return clone;
    }

    private void fillLibMethods() {
	if(libMethods.isEmpty()) {
	    for(String sig : InterprocAnalysis.SPECIAL_SIGS) {
		try {
		    SootMethod meth = Scene.v().getMethod(sig);
		    libMethods.add(meth);
		} catch (RuntimeException ex) {
		    //TODO
		    //System.out.println("Could not find method for " + sig);
		}
	    }
	}
	errorIndic = InterprocAnalysis.errorIndic;
    }

    public List<UnwindingContext> expand() {
	return restrictedExpand();
    }

    public List<UnwindingContext> restrictedExpand() {
	List<UnwindingContext> result = new ArrayList<UnwindingContext>();
	List<List<Unit>> allPreds = new ArrayList<List<Unit>>();

	int emptyPreds = -1;
	for(int t = 0; t < contexts.size(); t++) {
	    ArrayList<Unit> context = contexts.get(t);

	    Unit toExpand = context.get(expandPoints.get(t));
	    if(toExpand.equals(context.get(expandPoints.get(t) - 1))) {
		allPreds.add(new ArrayList<Unit>());
		emptyPreds = t;
	    } else {

		ExceptionalUnitGraph cfg = cfgs.get(toExpand);
		List<Unit> preds = cfg.getPredsOf(toExpand);
		for(Unit p : preds) {
		    cfgs.put(p, cfg);
		}
		allPreds.add(preds);
	    }
	}

	if(emptyPreds >= 0) {
	    int t = 1 - emptyPreds;
	    for(Unit u : allPreds.get(t)) {
		InvokeExpr inv = hasInvoke(u);

		UnwindingContext next = new UnwindingContext(t, u, this);
		if(inv == null) {
		    result.add(next);
		} else {
		    List<List<SootField>> dummy = new ArrayList<List<SootField>>();
		    result.addAll(expandCall(t, inv, next, dummy));
		}
	    }
	} //there is an implicit else here -- the loop will do nothing if the
	//condition above is true

	for(Unit u0: allPreds.get(0)) {
	    for(Unit u1 : allPreds.get(1)) {
		Set<Unit> paths0 = threadPaths(0, u0);
		Set<Unit> paths1 = threadPaths(1, u1);

		InvokeExpr inv0 = hasInvoke(u0);
		InvokeExpr inv1 = hasInvoke(u1);
		boolean isCall0 = (inv0 != null);
		boolean isCall1 = (inv1 != null);

		UnwindingContext next0 = new UnwindingContext(0, u0, this);
		UnwindingContext next1 = new UnwindingContext(1, u1, this);

		//These will both be set
		List<UnwindingContext> add0 = null;//new ArrayList<UnwindingContext>();
		List<UnwindingContext> add1 = null;//new ArrayList<UnwindingContext>();

		List<SootField> read0, written0, read1, written1;
		read0 = written0 = read1 = written1 = null;

		boolean read0AndWr = true;
		boolean wr0AndUsed = true;

		if( !isCall0) {

		    //Z3Converter.inThread = 0;
		    read0 = SubpathTuple.fieldsUsed(u0, false);
		    //Z3Converter.inThread = 1;
		    read0AndWr = SubpathTuple.usesFields(paths1, read0, false);

		    //Z3Converter.inThread = 0;
		    written0 = SubpathTuple.fieldsUsed(u0, true);
		    //Z3Converter.inThread = 1;
		    wr0AndUsed = SubpathTuple.usesFields(paths1, written0, true);
		    add0 = Collections.singletonList(next0);

		} else {
		    List<List<SootField>> touched =
			new ArrayList<List<SootField>>();
		    add0 = expandCall(0, inv0, next0, touched );

		    /* if(touched.isEmpty()) {
			System.err.println(inv0);
		    } */

		    read0 = touched.get(0);
		    written0 = touched.get(1);
		    if(add0.size() == 2 ||
			    (add0.size() == 1 && add0.get(0).equals(next0))) {
			read0AndWr = false;
			wr0AndUsed = false;
		    }
		}

		boolean read1AndWr = true;
		boolean wr1AndUsed = true;
		if( !isCall1) {
		    //Z3Converter.inThread = 1;
		    read1 = SubpathTuple.fieldsUsed(u1, false);
		    //Z3Converter.inThread = 0;
		    read1AndWr = SubpathTuple.usesFields(paths0, read1, false);

		    //Z3Converter.inThread = 1;
		    written1 = SubpathTuple.fieldsUsed(u1, true);
		    //Z3Converter.inThread = 0;
		    wr1AndUsed = SubpathTuple.usesFields(paths0, written1, true);
		    add1 = Collections.singletonList(next1);
		} //else handled later

		if(!read0AndWr && !wr0AndUsed) {
		    result.addAll(add0);
		} else {

		    if(read0AndWr) {
			next1.mustWrite.get(1).addAll(read0);
			next1.mustRead.get(0).addAll(read0);
		    }
		    if(wr0AndUsed) {
			next1.mustUse.get(1).addAll(written0);
			next1.mustWrite.get(0).addAll(written0);
		    }

		    if(isCall1) {
			List<List<SootField>> touched =
			    new ArrayList<List<SootField>>();
			add1 = expandCall(1, inv1, next1, touched);

			read1 = touched.get(0);
			written1 = touched.get(1);

			if(add1.size() == 2 ||
			    (add1.size() == 1 && add1.get(0).equals(next1))) {
			    read1AndWr = false;
			    wr1AndUsed = false;
			}
		    }

		    if(!read1AndWr && !wr1AndUsed) {
			result.addAll(add1);
		    } else {
			if(read1AndWr) {
			    next0.mustWrite.get(0).addAll(read1);
			    next0.mustRead.get(1).addAll(read1);
			}
			if(wr1AndUsed) {
			    next0.mustUse.get(0).addAll(written1);
			    next0.mustWrite.get(1).addAll(written1);
			}

			//hopefully this will happen rarely enough to not be a
			//problem
			if(isCall0) {
			    List<List<SootField>> touched =
				new ArrayList<List<SootField>>();
			    add0 = expandCall(0, inv0, next0, touched);
			}

			result.addAll(add0);
			result.addAll(add1);
		    }
		}
	    }
	}

	return result;
    }

    //The third argument is only MOST of the caller subcontext
    public List<UnwindingContext> expandCall(int i, InvokeExpr call,
	    UnwindingContext callerSubC, List<List<SootField>> touched) {

	List<ExceptionalUnitGraph> graphs = resolutions(call, false);
        //The second disjunct here is because baseline cannot handle interprocedural
	if(graphs.isEmpty() || InterprocAnalysis.doBaseline) {
	    //System.err.println("Could not find a body for method call: "
		    //+ call);
	    for(int c = 0; c < 2; c++) {
		touched.add(new ArrayList<SootField>());
	    }
	    return Collections.singletonList(callerSubC);
	}
        //else

	List<UnwindingContext> result = new ArrayList<UnwindingContext>();
	Unit otherEnd = contexts.get(1 - i).get(expandPoints.get(1 - i));
	Unit otherStart = contexts.get(1 - i)
	    .get( -1 + expandPoints.get(1 - i));
	Set<Unit> itm = intermediate(cfgs.get(otherEnd), otherStart,
		otherEnd);

	for (ExceptionalUnitGraph graph : graphs ) {
	    //Every graph I looked at had only one head
	    Unit head = graph.getHeads().get(0);
	    cfgs.put(head, graph);

	    for(Unit tail : graph.getTails()) {
		if(! (tail instanceof ReturnStmt || tail instanceof ReturnVoidStmt)) {
		    continue;
		}
		cfgs.put(tail, graph);

		//sUnits for subUnits
		Set<Unit> sUnits = allPaths(head, tail, true);

		//Z3Converter.inThread = i;
		List<SootField> read =
		    SubpathTuple.fieldsUsed(sUnits, false);
		List<SootField> written =
		    SubpathTuple.fieldsUsed(sUnits, true);
		List<SootField> used =
		    SubpathTuple.fieldsTouched(sUnits, true);

		touched.add(read);
		touched.add(written);

		for(Unit mid : itm) {
		    Set<Unit> midL = allPaths(mid, mid, true);
		    boolean readAndWritten =
			SubpathTuple.usesFields(midL, read, false);
		    boolean writtenAndUsed =
			SubpathTuple.usesFields(midL, written, true);
		    UnwindingContext cp = callerSubC.myClone();

		    boolean isLast = mid.equals(otherEnd);
		    if(isLast || readAndWritten || writtenAndUsed) {
			List<UnwindingContext> newC;
			newC = callTwoPreds(cp, i, head, tail, mid, read,
				written);
			result.addAll(newC);
		    }
		}
	    }
	}
	return result;
    }

    //head and tail are in the called method, mid in the other thread
    public List<UnwindingContext> callTwoPreds(UnwindingContext caller, int i,
	    Unit head, Unit tail, Unit mid,
	    List<SootField> read, List<SootField> written) {

	ArrayList<Unit> context = caller.contexts.get(i);
	int expandPoint = caller.expandPoints.get(i);
	Unit callStmt = context.get(expandPoint);

	UnwindingContext callee = new UnwindingContext(i, callStmt, head, tail, mid, this);

	callee.mustRead.get(i).addAll(read);
	callee.mustWrite.get(i).addAll(written);

	//other thread
	expandPoint = expandPoints.get(1 - i);

	caller.contexts.get(1 - i).add(expandPoint, mid);
	//caller.mustRead.get(i).removeAll(read);
	//caller.mustWrite.get(i).removeAll(written);

	/*
	System.out.println("\nCall site:\n" + this);
	System.out.println("Caller:\n" + caller);
	System.out.println("Callee:\n" + callee); */

	return Arrays.asList(new UnwindingContext[] {caller, callee});
    }

    public static InvokeExpr hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		InvokeExpr inv = (InvokeExpr) box.getValue();
		SootMethod meth = inv.getMethod();
		if( !libMethods.contains(meth)) {
		    return inv;
		}
	    }
	}

	return null;
    }

    //Because of the first line, this actually now never clones graphs:
    //it seems that my code can correctly handle recursive locals.
    //I am leaving the second parameter for now until more is discovered
    public static List<ExceptionalUnitGraph> resolutions(InvokeExpr inv,
	    boolean clone) {
	clone = false;

	List<ExceptionalUnitGraph> result = new ArrayList<ExceptionalUnitGraph>();
	SootMethod m = inv.getMethod();
	Set<SootMethod> possMethods;
	if(m.isStatic() || (inv instanceof SpecialInvokeExpr)) {
	    possMethods = Collections.singleton(m);
	} else {
	    InstanceInvokeExpr ii = (InstanceInvokeExpr) inv;
	    RefType r = (RefType) ii.getBase().getType();
	    FastHierarchy h = Scene.v().getFastHierarchy();
	    try {
		possMethods = h.resolveAbstractDispatch(
		    r.getSootClass(), m);
	    } catch (RuntimeException ex) {
		return result;
	    }
	}

	//Note the exceptional return value above
	for(SootMethod poss : possMethods) {
	    ExceptionalUnitGraph res = methodsCalled.get(poss);
	    if(res != null) {
		if(clone) {
		    Body copy = (Body) res.getBody().clone();
		    res = new ExceptionalUnitGraph(copy);
		}
		result.add(res);
	    } else {
		//System.out.println("In else, method " + poss);
		if(m.hasActiveBody()) {
		    Body b = poss.getActiveBody();
		    /* for(Unit u : b.getUnits()) {
			u.addTag(new LocationTag());
		    } */

		    res = new ExceptionalUnitGraph(b);
		    methodsCalled.put(poss, res);
		    result.add(res);
		}
	    }
	}

	return result;
    }

    public static Set<Unit> allPaths(Unit head, Unit tail, boolean takeCalls) {
	ExceptionalUnitGraph graph = cfgs.get(head);
	//System.out.println(graph.getBody().hashCode());
	HashSet<Unit> visited = new HashSet<Unit>();
	HashMap<Unit, Set<Unit>> subProcs = new HashMap<Unit, Set<Unit>>();
	HashSet<Unit> result = new HashSet<Unit>();
	result.add(head);
	result.add(tail);

	HashMap<Unit, Unit> searchPar = new HashMap<Unit, Unit>();
	//TODO: Should I use a queue here for exceptions like in findPath?
	Deque<Unit> stack = new ArrayDeque<Unit>();

	stack.push(head);
	while(!stack.isEmpty()) {
	    Unit top = stack.pop();

	    InvokeExpr inv = hasInvoke(top);

	    if(takeCalls && inv != null) {
		Set<Unit> subs = new HashSet<Unit>();
		List<ExceptionalUnitGraph> graphs = resolutions(inv, false);
		if(graphs.isEmpty()) {
		    //System.err.println("Could not find a body for method call: "
			    //+ inv);
		}
		for(ExceptionalUnitGraph subG : graphs) {
		    /* if(subG == null) {
			throw new NullPointerException("Null subgraph");
		    } */
		    if ( !subG.getBody().getMethod().equals(
				graph.getBody().getMethod())) {
			Unit newHead = subG.getHeads().get(0);
			cfgs.put(newHead, subG);

			for(Unit newTail : subG.getTails()) {
			    Set<Unit> sub = allPaths(newHead, newTail, true);
			    subs.addAll(sub);
			}
		    }
		}
		subProcs.put(top, subs);
	    }

	    if(takeCalls && result.contains(top)) {
		Set<Unit> subs = subProcs.get(top);
		if(subs != null) {
		    result.addAll(subs);
		}
	    }

	    //System.out.println("Top: " + top);
	    if(top.equals(tail)) {
		continue;
	    }

	    for(Unit u : graph.getSuccsOf(top)) {
		if(result.contains(u)) {
		    Unit par = top;
		    while(result.add(par)) {
			if(takeCalls) {
			    Set<Unit> subs = subProcs.get(par);
			    if(subs != null) {
				result.addAll(subs);
			    }
			}
			par = searchPar.get(par);
		    }
		}
                if(!visited.contains(u)) {
		    searchPar.put(u, top);
		    visited.add(u);
		    stack.push(u);
		}
	    }
	}

	return result;
    }

    //This is now a copy of threadPaths
    public static Set<Unit> intermediate(ExceptionalUnitGraph graph, Unit head, Unit tail) {
	HashSet<Unit> visited = new HashSet<Unit>();
	HashSet<Unit> result = new HashSet<Unit>();
	result.add(head);
	result.add(tail);

	HashMap<Unit, Unit> searchPar = new HashMap<Unit, Unit>();
	//TODO: Should I use a queue here for exceptions like in findPath?
	Deque<Unit> stack = new ArrayDeque<Unit>();
	stack.push(head);
	while(!stack.isEmpty()) {
	    Unit top = stack.pop();
	    for(Unit u : graph.getSuccsOf(top)) {
		//System.out.println("Successor: " + u);
		if(result.contains(u)) {
		    //System.out.println("Found tail: ");
		    Unit par = top;
		    while(result.add(par)) {
			par = searchPar.get(par);
		    }
		} else if(!visited.contains(u)) {
		    searchPar.put(u, top);
		    visited.add(u);
		    stack.push(u);
		}
	    }
	}

	return result;
    }

    //TODO: Avoid dominators by keeping track of the predecessors of EDGES,
    //i.e., pairs of units. Each should only be in the path at most once
    // I think you can use depth-first search for this
    public static Cons findPathWithAccesses(Unit head, Unit tail,
	    ExceptionalUnitGraph g, Cons tailCons,
	    Set<SootField> mustR, Set<SootField> mustWr, Set<SootField> mustU) {

	//Each unit holds negated number of accesses UP TO the unit
	HashMap<Unit, Integer> accesses = new HashMap<Unit, Integer>();


	for(Unit u : g) {
	    accesses.put(u, 1);
	}
	accesses.put(head, 0);

	boolean someChanged = true;
	ArrayList<Unit> onCycle = new ArrayList<Unit>();
	//Unit onCycle = null;

	Map<Unit, Unit> par = new HashMap<Unit, Unit>();
	//System.out.println("accesses size: " + accesses.size());
	for(int c = 1; someChanged && c <= accesses.size(); c++) {
	    someChanged = false;
	    HashSet<Unit> visited = new HashSet<Unit>();
	    Deque<Unit> queue = new ArrayDeque<Unit>();

	    queue.add(head);
	    while( !queue.isEmpty()) {
		Unit top = queue.remove();
		//System.out.println("Top: " + top);
		cfgs.put(top, g);

		int weight = 0;
		for(SootField f : mustR) {
		    if(SubpathTuple.readsField(top, f)) {
			weight = weight - 1;
		    }
		}
		for(SootField f : mustWr) {
		    if(SubpathTuple.usesField(top, f, false)) {
			weight = weight - 1;
		    }
		}

		for(SootField f : mustU) {
		    if(SubpathTuple.usesField(top, f, true)) {
			weight = weight - 1;
		    }
		}

		//if(weight < 0) {
		//System.out.println(top);
		//}

		int tops = accesses.get(top);

		//the statement for top is viewed as the edge to the vertex for v
		for(Unit v : g.getSuccsOf(top)) {

		    int cur = accesses.get(v);
		    //System.out.println("Cur: " + cur);

		    //System.out.println("Sizes: " + nnn.size() + " " + cur.size() );
		    //System.out.println(v + "; " + top);
		    if(cur > tops + weight) {
			someChanged = true;
			accesses.put(v, tops + weight);
			par.put(v, top);
			//System.out.println("Put parent " + top + " for " + v);
			if(c == accesses.size()) {
			    onCycle.add(top);
			}
		    }

		    if( !visited.contains(v)) {
			visited.add(v);
			queue.add(v);
		    }
		}
	    }

	    if(someChanged) {
		Unit tailPar = par.get(tail);
		if(tailPar != null && tailPar.equals(head)) {
		    someChanged = false;
		}
	    }
	}

	ArrayList<Unit> loopHeads = new ArrayList<Unit>();
	ArrayList<Unit> loopEscapes = new ArrayList<Unit>();

	if( ! onCycle.isEmpty()) {
	    SimpleDominatorsFinder<Unit> df = new SimpleDominatorsFinder<Unit>(g);
	    Unit loopEscape = onCycle.get(0);
	    Unit loopHead = null;

	    while(onCycle.contains(loopEscape)) {
		loopHead = loopEscape;
		while(true) {
		    Unit parent = par.get(loopHead);
		    if(df.isDominatedBy(parent, loopHead)) {
			break;
		    }
		    loopHead = parent;
		}
		//TODO: this will not work in the (hopefully rare) case
		//that the loop head is also the target of an if
		loopEscape = df.getImmediateDominator(loopHead);
		loopHeads.add(loopHead);
		loopEscapes.add(loopEscape);
	    }
	}

	//System.out.println("Body: " + g.getBody().getMethod().getName());
	//System.out.println("On cycle: " + onCycle);
	//HashSet<Unit> cycle = new HashSet<Unit>();

	//System.out.println("Final On cycle: " + onCycle);

	//System.out.println("On cycle: " + onCycle);
	//System.out.println("Cycle: " + cycle);

	Cons curCons = tailCons;
	Unit parent = tail;
	//boolean seenLoopHead = false;
	boolean[] seenLoopHead = new boolean[loopHeads.size()];
	//System.out.println("Parent, tail: " + parent + " " + tail);
	while( !parent.equals(head)) {
	    //System.out.println("Parent: " + parent);
	    //System.out.println("Here");
	    curCons = new Cons(parent, curCons);

	    mustR.removeAll(SubpathTuple.fieldsUsed(parent, false));
	    mustWr.removeAll(SubpathTuple.fieldsUsed(parent, true));
	    mustU.removeAll(SubpathTuple.fieldsTouched(parent, true));

	    boolean normalParent = true;
	    for(int i = 0; i < seenLoopHead.length; i++) {
		if(parent.equals(loopHeads.get(i))) {
		    if(seenLoopHead[i] ) {
			parent = loopEscapes.get(i);
			normalParent = false;
		    } else {
			seenLoopHead[i] = true;
		    }
		    break;
		}
	    }

	    if(normalParent) {
		parent = par.get(parent);
	    }
	}
	//System.out.println("Exiting");
	return new Cons(head, curCons);
    }

    //This is similar to, but different from, intermediate( )
    //Returns null if no path found with the required usages
    public PathTuple pathTuple() {
	List<Path> paths = new ArrayList<Path>(contexts.size());
	for(int t = 0; t < contexts.size(); t++) {
	    //System.out.println("looping");

	    //Z3Converter.inThread = t;
	    ArrayList<Unit> ctxt = contexts.get(t);
	    ArrayList<Unit> pathList = new ArrayList<Unit>();
	    ArrayList<Integer> matches = new ArrayList<Integer>();
	    LinkedList<Integer> callMatches = new LinkedList<Integer>();
	    ArrayList<Integer> branches = new ArrayList<Integer>();
	    HashMap<Unit, RefType> excThrown = new HashMap<Unit, RefType>();

	    //HashMap<Unit, Unit> parent = new HashMap<Unit, Unit>();

	    /* Set<SootField> mustR = Collections.emptySet();
	    Set<SootField> mustWr = Collections.emptySet();
	    Set<SootField> mustU = Collections.emptySet(); */

	    int holeStart = expandPoints.get(t) - 1;

	    //boolean foundSubpath = false;

	    for(int i = 0; i < ctxt.size() - 1; i++) {
		//System.out.println("looping2");
		Unit start = ctxt.get(i);
		Unit end = ctxt.get(i + 1);

		Set<SootField> mustR;
		Set<SootField> mustWr;
		Set<SootField> mustU;
		if(i == holeStart) {
		    mustR = new HashSet<SootField>(this.mustRead.get(t));
		    mustWr = new HashSet<SootField>(this.mustWrite.get(t));
		    mustU = new HashSet<SootField>(this.mustUse.get(t));
		} else {
		    mustR = Collections.emptySet();
		    mustWr = Collections.emptySet();
		    mustU = Collections.emptySet();
		}

		Cons headCons;
		if( !cfgs.get(start).equals(cfgs.get(end))) {
		    //System.out.println("Not equal");
		    //System.out.println(start + "; " + end);
		    //This is correct -- it was LAST round's end
		    int match = pathList.size();
		    if((start instanceof ReturnVoidStmt) ||
			    (start instanceof ReturnStmt)) {
			match = match + 1;
		    }
		    //matches.add(match);

		    //pathList.add(start);
		    headCons = new Cons(start, null);

		} else {

		    headCons = findPathWithAccesses(start, end, cfgs.get(start),
			    null, mustR, mustWr, mustU);
		}
		//System.out.println(parent);

		//Add returns to this so we do not take the same call twice
		Set<Unit> returns = new HashSet<Unit>();

		for(Cons c = headCons; c != null && !c.car.equals(end); c = c.cdr) {
		    //System.out.println("Cons looping");
		    Unit par = c.car;
		    pathList.add(par);

		    if (par instanceof IfStmt) {
			Unit next = c.cdr.car;
			//System.out.println("If, next: " + par + "; " + next);
			IfStmt ifS = (IfStmt) par;
			if(ifS.getTarget().equals(next)) {
			    branches.add(pathList.size());
			}
		    }

		    if((par instanceof ReturnVoidStmt) ||
			    (par instanceof ReturnStmt)) {
			    int cI = callMatches.pop();

			    matches.add(cI);
			    matches.add(pathList.size());
		    }

		    InvokeExpr inv = hasInvoke(par);
		    //System.out.println("Here 0"  );
		    if(inv != null) {
			//System.out.println("Here 1 " + inv  );

			boolean isStart = (c.cdr == null);
			// if the next unit in the context is in the call
			if(isStart) {
			    callMatches.push(pathList.size() - 1);
			}

			//I could avoid doing this, but not without increasing
			//indentation
			List<ExceptionalUnitGraph> graphs = resolutions(inv, true);
			/* if(graphs.isEmpty()) {
			//System.err.println("Could not find a body for method call: " + inv);
			} */

			if( !isStart && !graphs.isEmpty()) {
			    //System.out.println("Resolutions for " + inv + " not empty: " + graphs.size());
			    callMatches.push(pathList.size() - 1);
			    //System.out.print(callMatches.peek() + " ");
			    //System.out.println("Call:" + par);

			    ExceptionalUnitGraph newGraph = graphs.get(0);

			    Unit newStart = newGraph.getHeads().get(0);
			    cfgs.put(newStart, newGraph);

			    for(Unit newEnd : newGraph.getTails()) {
				if( !(newEnd instanceof ReturnStmt ||
					    newEnd instanceof ReturnVoidStmt)) {
					continue;
				}

				if(!returns.add(newEnd)) {
				    continue;
				}

				//System.out.println("New end: " + newEnd);
				cfgs.put(newEnd, newGraph);

				Cons back = c.cdr;

				Cons h = findPathWithAccesses(newStart, newEnd, newGraph,
					back, mustR, mustWr, mustU);
				if(mustU.isEmpty() && mustWr.isEmpty()) {
				    //System.out.println("Setting cdr");
				    c.cdr = h;
                                    makeSynchStatements(inv, c, back);
				    //foundSubpath = true;
				    break;
				}
			    }
			}
		    }
		}

		//Could not find a path with these accesses
		if(!mustR.isEmpty() || !mustU.isEmpty() || !mustWr.isEmpty()) {
		    //System.out.println(mustR);
		    //System.out.println(mustWr);
		    //System.out.println(mustU);
		    return null;
		}

		//System.out.println("Callsites: " + callMatches);
	    }


	    /* if(!foundSubpath) {
		System.err.println("Continuing without finding subpath.");
	    } */

	    //pathList.add(ctxt.get(ctxt.size() - 1));

	    //System.out.println("Making matches: " + matches);
	    if(matches.size() % 2 != 0) {
		System.out.println(pathList);
		for(int i : matches) {
		    System.out.println(pathList.get(i - 1) + "; " + pathList.get(i));
		}
                throw new IllegalArgumentException("Context was not well-matched.");
	    }

	    String name = cfgs.get(ctxt.get(0)).getBody().getMethod().getName();
	    Path path = new Path(pathList, name);

	    int numMatches = matches.size();
	    for(int i = 0; i < numMatches; i = i + 2) {
		path.addMatch(matches.get(i), matches.get(i + 1));
	    }

	    //System.out.println("branches: " + branches);
	    for(int i : branches) {
		path.addBranch(i);
	    }

	    Unit lastUnit = path.get(path.length() - 1);
	    if(lastUnit instanceof IfStmt) {
		IfStmt lastIf = (IfStmt) lastUnit;
		Unit error = ctxt.get(ctxt.size() - 1);
		if(lastIf.getTarget().equals(error)) {
		    path.addBranch(path.length());
		}
	    }

	    paths.add(path);
	}
	return new PathTuple(paths);
    }

    public static void makeSynchStatements(InvokeExpr inv, Cons c, Cons back) {
        Cons penult = c;
        while(penult.cdr.cdr != back) {
            penult = penult.cdr;
        }

        SootMethod m = inv.getMethod();
        if (m.isSynchronized()) {
            if(m.isStatic()) {
                throw new IllegalArgumentException("Cannot handle static synchronized");
            }
            InstanceInvokeExpr invE = (InstanceInvokeExpr) inv;
            soot.Value v = invE.getBase();
            Unit enter = Jimple.v().newEnterMonitorStmt(v);
            Unit exit = Jimple.v().newExitMonitorStmt(v);

            Cons enterC = new Cons(enter, c.cdr);
            Cons exitC = new Cons(exit, penult.cdr);
            c.cdr = enterC;
            penult.cdr = exitC;
        }
    }

    public Set<Unit> threadPaths(int t, Unit tail) {
	Unit head = contexts.get(t).get(expandPoints.get(t) - 1);

	return allPaths(head, tail, true);
    }

    public List<Unit> findThrows(ExceptionalUnitGraph g, RefType excType) {
	List<Unit> result = new ArrayList<Unit>();
	for(Unit u : g) {
	    Collection<ExceptionDest> dests = g.getExceptionDests(u);
	    for(ExceptionDest d : dests) {
		if(d.getThrowables().catchableAs(excType)) {
		    if(d.getTrap() == null) {
			result.add(u);
		    }
		}
	    }
	}
	return result;
    }

    public String toString() {
	String result = "";
	for(int t = 0; t < contexts.size(); t++) {
	    ArrayList<Unit> ctxt = contexts.get(t);
	    ArrayList<String> strings = new ArrayList<String>();

	    for(int i = 0; i < ctxt.size(); i++) {
		Unit u = ctxt.get(i);
		String newString = u.toString();
		if(i == expandPoints.get(t)) {
		    newString = "^^" + newString;
		}
		strings.add(newString);
	    }

	    if( !result.equals("")) {
		result = result + "\n";
	    }
	    result = result + strings.toString();
	}
	return result;
    }

    public boolean equals(Object other) {
	if( ! (other instanceof UnwindingContext)) {
	    return false;
	}
	UnwindingContext o = (UnwindingContext) other;
	boolean res = this.contexts.equals(o.contexts);
	res = res && this.expandPoints.equals(o.expandPoints);
	return res;
    }

    public int hashCode() {
	return 47 * contexts.hashCode() + 37 * expandPoints.hashCode();
    }

    public boolean hasSameHole(UnwindingContext other) {
	if(this.contexts.size() != other.contexts.size()) {
	    return false;
	}

	for(int t = 0; t < contexts.size(); t++) {
	    final List<Unit> myCtx = this.contexts.get(t);
	    final List<Unit> oCtx = other.contexts.get(t);
	    final int myE = this.expandPoints.get(t);
	    final int oE = other.expandPoints.get(t);

	    Unit mine = myCtx.get(myE);
	    Unit its = oCtx.get(oE);
	    /* List<Tag> myTags = myCtx.get(myE).getTags();
	    List<Tag> oTags = oCtx.get(oE).getTags();
	    //The != (rather than >) is correct: every unit should have 1 tag.
	    if(myTags.size() != 1 || oTags.size() != 1) {
		System.err.println("Context: " + this);
		System.err.println("Tags: " + myTags);
		System.err.println("Context: " + other);
		System.err.println("Other tags: " + oTags);
		throw new IllegalStateException("Each Unit should have only one tag.");
	    } */
	    boolean cond = mine.equals(its);

	    mine = myCtx.get(myE - 1);
	    its = oCtx.get(oE - 1);
	    /* if(myTags.size() != 1 || oTags.size() != 1) {
		throw new IllegalStateException("Each Unit should have only one tag.");
	    } */

	    cond = cond && mine.equals(its);
	    if( !cond) {
		return false;
	    }
	}
	return true;
    }

    public Unit getExpandUnit(int thread) {
	return contexts.get(thread).get(expandPoints.get(thread));
    }

    public int getExpandPoint(int thread) {
	return expandPoints.get(thread);
    }

    public int getContextSize(int thread) {
	return contexts.get(thread).size();
    }

    public List<Unit> getContext(int thread) {
	return Collections.unmodifiableList(contexts.get(thread));
    }

    public static Map<Unit, ExceptionalUnitGraph> getCFGs() {
	return cfgs;
	//return java.util.Collections.unmodifiableMap(cfgs);
    }

    public List<Set<SootField>> getMustRead() {
	return mustRead;
    }

    public List<Set<SootField>> getMustWrite() {
	return mustWrite;
    }

    public List<Set<SootField>> getMustUse() {
	return mustUse;
    }

    //This is mostly legacy
    public static Unit findErrorNode(ExceptionalUnitGraph g) {
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		if (errorIndic.equals(m)) {
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    public static Set<SootMethod> getLibMethods() {
	return libMethods;
    }
}
