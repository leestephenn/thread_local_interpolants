/** Copyright (c) 2016 Stephen N. Lee. Some sections derived from code by Qi Zhou */

package edu.gatech.threadlocalitps;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Sort;
import com.microsoft.z3.ArraySort;
import com.microsoft.z3.TupleSort;
import com.microsoft.z3.DatatypeSort;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Constructor;
import com.microsoft.z3.Symbol;

//import soot.jimple.*;
import soot.jimple.Constant;
import soot.jimple.IntConstant;
import soot.jimple.LongConstant;
import soot.jimple.DoubleConstant;
import soot.jimple.FloatConstant;
import soot.jimple.AddExpr;
import soot.jimple.AndExpr;
import soot.jimple.AnyNewExpr;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.BreakpointStmt;
import soot.jimple.CastExpr;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.CmpExpr;
import soot.jimple.CmpgExpr;
import soot.jimple.CmplExpr;
import soot.jimple.ConcreteRef;
import soot.jimple.ConditionExpr;
import soot.jimple.ConstantSwitch;
import soot.jimple.ConvertToBaf;
import soot.jimple.DefinitionStmt;
import soot.jimple.DivExpr;
import soot.jimple.DynamicInvokeExpr;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.EqExpr;
import soot.jimple.EqualLocals;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.ExprSwitch;
import soot.jimple.FieldRef;
import soot.jimple.GeExpr;
import soot.jimple.GotoStmt;
import soot.jimple.GtExpr;
import soot.jimple.IdentityRef;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InstanceOfExpr;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.JimpleValueSwitch;
import soot.jimple.LeExpr;
import soot.jimple.LengthExpr;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.LtExpr;
import soot.jimple.MonitorStmt;
import soot.jimple.MulExpr;
import soot.jimple.NeExpr;
import soot.jimple.NegExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.NewMultiArrayExpr;
import soot.jimple.NopStmt;
import soot.jimple.NullConstant;
import soot.jimple.OrExpr;
import soot.jimple.ParameterRef;
import soot.jimple.Ref;
import soot.jimple.RefSwitch;
import soot.jimple.RemExpr;
import soot.jimple.RetStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.ShlExpr;
import soot.jimple.ShrExpr;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.StringConstant;
import soot.jimple.Stmt;
import soot.jimple.StmtSwitch;
import soot.jimple.SubExpr;
import soot.jimple.SwitchStmt;
import soot.jimple.TableSwitchStmt;
import soot.jimple.ThisRef;
import soot.jimple.ThrowStmt;
import soot.jimple.UnopExpr;
import soot.jimple.UshrExpr;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.XorExpr;

import soot.Local;
import soot.SootMethod;
import soot.Type;
import soot.ArrayType;
import soot.IntegerType;
//import soot.BooleanType;
import soot.Value;
import soot.ValueBox;
import soot.Unit;
import soot.SootField;
import soot.SootClass;
import soot.RefType;
import soot.RefLikeType;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ArrayDeque;

/* TODO: maybe have a way of making unique memory cells. Right now I am using hashCode on the argument */
public class Z3Converter {
    public static final String LEVEL_PRE = "_";
    public static final String THREAD_PRE = "^";
    public static final String PARAMETER_PRE = "Param";

    public static final InterpolationContext CONTEXT = new InterpolationContext();
    //for legacy purposes
    private static final Context iCtx = CONTEXT;

    public static final DatatypeSort refSort;
	    // = iCtx.mkUninterpretedSort("ReferenceSort");
    public static final DatatypeSort optRefSort;
    //public static final TupleSort fieldTupleSort;
    public static final FuncDecl isEnv;
    private static final Expr nullExpr;

    static {
	Constructor nil = iCtx.mkConstructor("null", "is_null", null, null, null);

	Constructor ac_c = iCtx.mkConstructor("Alloc", "isAlloc",
		new String[]{ "obj_id" }, new Sort[]{ iCtx.getIntSort() }, null);
	refSort = iCtx.mkDatatypeSort("Objs", new Constructor[]{ nil, ac_c });

	Constructor udef_c = iCtx.mkConstructor("Undef", "isUndef", null, null, null);
	Constructor actObj_c = iCtx.mkConstructor("DefObj", "isDefObj",
		new String[]{ "def_obj" }, new Sort[]{ refSort }, null);
	optRefSort = iCtx.mkDatatypeSort("MaybeObj",
		new Constructor[]{ udef_c, actObj_c });

	FuncDecl nullDecl = refSort.getConstructors()[0];
	nullExpr = iCtx.mkConst(nullDecl);

	isEnv = iCtx.mkFuncDecl("IsEnvObj",
		new Sort[]{ iCtx.getIntSort() }, iCtx.getBoolSort());

	ArraySort envFieldSort = iCtx.mkArraySort(refSort, optRefSort);
    }

    //public static final Sort floatSort = iCtx.mkRealSort();
    //public static final Sort arrayCellSort =
	//iCtx.mkUninterpretedSort("ArrayCellSort");
    public static final ArraySort primitiveArraySort =
	iCtx.mkArraySort(iCtx.getIntSort(), iCtx.getIntSort());
    public static final ArraySort objectArraySort =
	iCtx.mkArraySort(iCtx.getIntSort(), refSort);
    public static final Sort classSort = iCtx.mkUninterpretedSort("Class");//TODO

    public static boolean findExceptions = false;
    public static boolean findDeadlock = false;
    public static boolean findRaceCondition = false;
    //public static int inThread = -1;

    //private ArrayExpr arrayCells;// This actually only holds primitive arrays
    private static ArrayExpr arrayLengths = iCtx.mkArrayConst("ArrayLengths",
	    refSort, iCtx.getIntSort());//This actually holds both (and can)
    //Contrary to the note on modeling objects, I am going to leave this as
    //from all actual objects, including null, to classes.
    //This requires that I special case null
    private static FuncDecl CLASSES = iCtx.mkFuncDecl("?Class-of", refSort, classSort);


    //public static final BoolExpr excThrown =
	//iCtx.mkBoolConst("ExceptionThrown");

    private final UnitEdge edge;
    //This functions as a kind of second output from value expressions -- put changes to memory in it and conjoin it to the end of the enclosing Unit's BoolExpr.
    private BoolExpr sideEffects;
    private SubpathTuple lastVertex;
    private boolean inDefinition = false;

    //These really should have Post in their name, but I might do that later
    private HashMap<Local, Expr> newThreadMappings = new HashMap<Local, Expr>();

    private HashMap<Local, Expr> newThreadPreMappings = new HashMap<Local, Expr>();
    private Map<Integer, Expr> params = new HashMap<Integer, Expr>();//this also holds the ThisRef, at -1

    private ArrayExpr newPreArrayCells = null;
    private ArrayExpr newPostArrayCells = null;
    //private ArrayExpr newLocks = null;

    private Map<SootField, Expr> preStaticFields = new HashMap<SootField, Expr>();
    private Map<SootField, Expr> postStaticFields = new HashMap<SootField, Expr>();

    private Map<SootField, ArrayExpr> preFields =
	new HashMap<SootField, ArrayExpr>();
    private Map<SootField, ArrayExpr> postFields =
       new HashMap<SootField, ArrayExpr>();

    //It will be treated as an instance variable
    private SootField objectArrayCells = null;

    private Expr lastException = null;
    public static SootField lockField = null;
    private static List<Expr> threadLocalVars = null;

    //The following are properly initialized in initFakeFields
    //Set to true if an exception has been thrown
    public static SootField exceptionField = null;

    //Set to true if a deadlock has occured
    public static SootField deadlockField = null;
    //Element at i holds whether we are waiting for a lock
    public static List<SootField> waitingFields = null;
    //whether this step of the interleaving is enabled, depending on locks
    public static List<SootField> enabledFields = null;
    // holds the lock that this thread is waiting on
    private static List<SootField> lockHeldFields = null;

    //Each element is the current vector clock for thread t
    public static List<List<SootField>> threadClocks = null;
    //For the next three, a vector of variables (one for every thread),
    // not an array
    public static List<SootField> releaseClocks = null;
    public static List<SootField> writeClocks = null;
    public static List<SootField> readClocks = null;
    public static SootField raceFoundField = null;

    public Z3Converter(UnitEdge e) {
        edge = e;

        //arrayLengths = iCtx.mkArrayConst("ArrayLengths", arrayCellSort,
                //iCtx.getIntSort());
        sideEffects = iCtx.mkTrue();

	//All the null checks are necessary because we are assigning static
	//values in an (instance) constructor and only want to do it once.
	Type t = RefType.v();
	if(objectArrayCells == null) {
	    objectArrayCells = new FakeSootField("ObjectArrayCells", t);
	}

	initFakeFields();
    }

    //Initializes the static variables that represent modeling things:
    // The lock fields and thread control fields
    public static void initFakeFields() {

	Type number = soot.Scene.v().getType("int");
	Type bool = soot.Scene.v().getType("boolean");
	Type ref = RefType.v();
	if(lockField == null) {
	    lockField = new FakeSootField("Lock", number);
	}
	if(exceptionField == null) {
	    exceptionField = new FakeSootField("ExceptionThrown", bool);
	}
	if(deadlockField == null) {
	    deadlockField = new FakeSootField("DeadlockFound", bool);
	}
	if(waitingFields == null) {
	    waitingFields = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField wait = new FakeSootField("Waiting" + i, bool);
		waitingFields.add(wait);
	    }
	}

	if(lockHeldFields == null) {
	    lockHeldFields = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField lock = new FakeSootField("WaitForLock" + i, ref);
		lockHeldFields.add(lock);
	    }
	}

	if(enabledFields == null) {
	    enabledFields = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField cntnue = new FakeSootField("Enabled" + i, bool);
		enabledFields.add(cntnue);
	    }
	}

	if(threadLocalVars == null) {
	    threadLocalVars = new ArrayList<Expr>();
	    for(int i = 0; i < 2; i++) {
		Expr expr = iCtx.mkConst("?ThreadVar" + i, refSort);
		threadLocalVars.add(expr);
	    }
	}

	if(threadClocks == null) {
	    threadClocks = new ArrayList<List<SootField>>();
	    for(int i = 0; i < 2; i++) {
		ArrayList<SootField> ttt = new ArrayList<SootField>();
		for(int j = 0; j < 2; j++) {
		    SootField tC =
			new FakeSootField("ThreadClock" + i + "^" + j, number);
		    ttt.add(tC);
		}
		threadClocks.add(ttt);
	    }
	}

	if(releaseClocks == null) {
	    releaseClocks = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField rC = new FakeSootField("LReleaseClock" + i, number);
		releaseClocks.add(rC);
	    }
	}

	if(writeClocks == null) {
	    writeClocks = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField wC = new FakeSootField("WriteClock" + i, number);
		writeClocks.add(wC);
	    }
	}

	if(readClocks == null) {
	    readClocks = new ArrayList<SootField>();
	    for(int i = 0; i < 2; i++) {
		SootField rC = new FakeSootField("ReadClock" + i, number);
		readClocks.add(rC);
	    }
	}
	if(raceFoundField == null) {
	    raceFoundField = new FakeSootField("RaceFound", bool);
	}
    }

    public static String getRenameString(String name, String level) {
        String result = name + LEVEL_PRE + level;
        return result;
    }

    public static String getThreadString(String name, int thread) {
	return name + THREAD_PRE + thread;
    }

    public static String getFieldString(SootField f) {
	String newName;
	if(f.isDeclared()) {
	    newName = f.getDeclaringClass().getName();
	} else {
	    newName = "?FakeField";
	}
	newName = newName + "." + f.getName();
	return newName;
    }

    public static Sort getSort(Type t) {
	if(t instanceof IntegerType || t instanceof soot.LongType) {
	    return iCtx.getIntSort();
	}
	if (t instanceof ArrayType) {
	    return refSort;
	}
	return refSort;
    }


    public ArrayExpr getArrayCells() {
	ArrayExpr result = lastVertex.getArrayCells();
	if(result == null) {
	    String newName = getRenameString("ArrayCells",
		    String.valueOf(edge.ID));
            result = iCtx.mkArrayConst(newName,
		    refSort, primitiveArraySort);
	    newPreArrayCells = result;
	}
	return result;
    }

    //The argument is the condition on which the exception is NOT thrown
    public BoolExpr getExcThrownEq(BoolExpr safeCond) {
	BoolExpr cond = iCtx.mkNot(safeCond);
	BoolExpr curET = (BoolExpr) ofStaticField(exceptionField,
		iCtx.getBoolSort());
	inDefinition = true;
	Expr primeET = ofStaticField(exceptionField, iCtx.getBoolSort());
	inDefinition = false;

	BoolExpr result = iCtx.mkOr(curET, cond);
	result = iCtx.mkEq(primeET, result);
	return result;
    }

    public HashMap<Local, Expr> getNewThreadMappings() {
        return newThreadMappings;
    }

    public HashMap<Local, Expr> getNewThreadPreMappings() {
        return newThreadPreMappings;
    }

    public Map<Integer, Expr> getParams() {
	return params;
    }

    /* Returns null if there was no change */
    public ArrayExpr getNewPreArrayCells() {
        return newPreArrayCells;
    }

    /* Returns null if there was no change */
    public ArrayExpr getNewPostArrayCells() {
        return newPostArrayCells;
    }

    /* Returns null if there was no change */
    /* public ArrayExpr getNewLocks() {
        return newLocks;
    } */

    public Map<SootField, Expr> getPreStaticFields() {
	return preStaticFields;
    }

    public Map<SootField, Expr> getPostStaticFields() {
	return postStaticFields;
    }

    public Map<SootField, ArrayExpr> getPreFields() {
	return preFields;
    }

    public Map<SootField, ArrayExpr> getPostFields() {
	return postFields;
    }

    //Returns null if there was none
    public Expr getLastException() {
	return lastException;
    }

    public BoolExpr ofUnit(Unit u) {
	//This could throw a ClassCastException, but I want it to:
	//this method should only be called from a UnitSingleEdge
	lastVertex = ((UnitSingleEdge) edge).getTail();

	if(findDeadlock || findRaceCondition) {
	    BoolExpr enbld = (BoolExpr) ofStaticField(
		    enabledFields.get(edge.thread), iCtx.getBoolSort());
	    sideEffects = enbld;
	}

        BoolExpr result = null;
        if(u instanceof DefinitionStmt) {
            result = ofDefinitionStmt((DefinitionStmt) u);
        }
        if(u instanceof IfStmt) {
            result = ofIfStmt((IfStmt) u);
        }
        if(u instanceof GotoStmt) {
            result = iCtx.mkTrue();
        }
        if(u instanceof NopStmt) {
            result = iCtx.mkTrue();
        }
	//TODO: maybe throw an exception in the next two branches, because
	//they should never be called?
        if(u instanceof ReturnVoidStmt) {
            result = iCtx.mkTrue();
        }
        if(u instanceof ReturnStmt) {
            result = iCtx.mkTrue();
        }

        if (u instanceof EnterMonitorStmt) {
            result = ofEnterMonitorStmt((EnterMonitorStmt) u);
        }

        if (u instanceof ExitMonitorStmt) {
            result = ofExitMonitorStmt((ExitMonitorStmt) u);
        }

        if (result != null && !sideEffects.isTrue()) {
	    //System.out.println("Side effects: " + sideEffects);
            result = iCtx.mkAnd(sideEffects, result);
        }

        return result;
    }

    public BoolExpr makeParamEqs(InvokeExpr inv, Map<Integer, Expr> params) {
	List<Value> args = inv.getArgs();

	/* if(args == null) {
	    System.out.println("args null!");
	}
	if(params == null) {
	    System.out.println("params null!");
	} */

	BoolExpr[] eqs;
	if (inv instanceof InstanceInvokeExpr) {
	    eqs = new BoolExpr[args.size() + 1];
	    Value rcvr = ((InstanceInvokeExpr) inv).getBase();
	    //System.out.println(params.get(-1) + " = " + ofValue(rec));
	    Expr base = ofValue(rcvr);
            //try {
                eqs[eqs.length - 1] = iCtx.mkEq(params.get(-1), base);
            /* } catch (NullPointerException ex) {
                System.out.println("Param null? " + (params.get(-1) == null) + " base null? " + (base == null));
                System.out.println(inv);
                throw ex;
            } */

	    BoolExpr notNull = iCtx.mkNot(iCtx.mkEq(base, nullExpr));
	    notNull = getExcThrownEq(notNull);
	    sideEffects = iCtx.mkAnd(sideEffects, notNull);
	} else {
	    eqs = new BoolExpr[args.size()];
	}

	if(eqs.length != params.size()) {
	    //System.out.println("Args: " + args);
	    //System.out.println("Params: " + params);
	    //UnitHyperEdge he = (UnitHyperEdge) edge;
	    //System.out.println(he.getCallTail() + " " + he.getExitTail());
	    throw new IllegalArgumentException("The number of parameters " +
		    params.size() + " was not equal to the number of arguments " +
		    eqs.length + ". This includes the value for the keyword this.");
	}

	for(int i = 0; i < args.size(); i++) {
	    //System.out.println(params.get(i) + " = " + ofValue(args.get(i)));
	    eqs[i] = iCtx.mkEq(params.get(i), ofValue(args.get(i)));
	}
	return iCtx.mkAnd(eqs);
    }

    //TODO: need to add continuing constraint
    public BoolExpr ofInvokeStmt(InvokeStmt callStmt, Unit exitStmt,
	    SubpathTuple callPred, SubpathTuple exitPred) {
	InvokeExpr inv = callStmt.getInvokeExpr();

	lastVertex = callPred;
	//System.out.print("Converting params of " + exitPred.intervals());
	//System.out.println(": " + exitPred.getParams());
	BoolExpr result = makeParamEqs(inv, exitPred.getParams(edge.thread));
	lastVertex = null;

	if(exitStmt instanceof ReturnStmt) {
	    ReturnStmt r = (ReturnStmt) exitStmt;

	    lastVertex = exitPred;
	    ofValue(r.getOp());//run conversion to get any side effects -- there should never be any
	    lastVertex = null;
	}
	if(!sideEffects.isTrue()) {
	    result = iCtx.mkAnd(result, sideEffects);
	}
	return result;
    }

    //May throw ClassCastException if (1) Soot does not assign to local
    //(2) Soot nests the InvokeExpr in something before it is in the DefinitionStmt
    //TODO: need to add continuing constraint
    public BoolExpr ofCall(DefinitionStmt def, ReturnStmt ret,
	    SubpathTuple callPred, SubpathTuple exitPred) {
	lastVertex = exitPred;
	Expr retValue = ofValue(ret.getOp());

	lastVertex = callPred;
	inDefinition = true;
	Expr d = ofLocal((Local) def.getLeftOp());
	inDefinition = false;
	BoolExpr result = iCtx.mkEq(d, retValue);

	InvokeExpr inv = (InvokeExpr) def.getRightOp();

	BoolExpr paramEqs = makeParamEqs(inv, exitPred.getParams(edge.thread));
	/*
	try {
	    paramEqs = makeParamEqs(inv, exitPred.getParams(edge.thread));
	} catch (IllegalArgumentException ex) {
	    System.out.println("Exit pred: " + exitPred);
	    SubpathTuple current = exitPred;

	    System.out.println("Exit pred subpaths called: " + current.subpathsCalled);
	    System.out.println("Exit pred in-edges: " +current.getInEdges().size());

	    for(UnitEdge e : current.getInEdges()) {
		current = e.getVarTail();
		System.out.println("Current: " + current);
		Map<Integer, Expr> cParams = current.getParams(edge.thread);
		if(!cParams.isEmpty()) {
		    System.out.println(cParams);
		}
	    }
	    throw ex;
	} */
	lastVertex = null;

	return iCtx.mkAnd(result, paramEqs, sideEffects);
    }

    //TODO: this is not correct -- the statement throwing could be anything,
    //need to call exceptionCondition.
    public BoolExpr ofCallThrow(InvokeExpr inv, ThrowStmt thr,
	    SubpathTuple callPred, SubpathTuple throwPred) {
	lastVertex = throwPred;
	Expr thrown = ofValue(thr.getOp());

	lastVertex = callPred;
	BoolExpr paramEqs = makeParamEqs(inv, throwPred.getParams(edge.thread));
	String newName = getRenameString("Exception", edge.ID + THREAD_PRE + edge.thread);
	Expr exc = iCtx.mkConst(newName, refSort);
	lastException = exc;
	BoolExpr result = iCtx.mkEq(exc, thrown);
	return iCtx.mkAnd(result, paramEqs, sideEffects);
    }

    //TODO -- see below
    public BoolExpr exceptionCondition(Unit u, RefType eType) {
	return null;
    }

    //TODO: this code, which determines which exception was caught by the expression, will not work properly. In particular, an array store can throw an ArrayIndexOutOfBoundsException, a NullPointerException, or an ArrayStoreException
    public BoolExpr exceptionCondition(Unit u, boolean isNullPointer) {
	//I assume, based on testing, that no two expressions that could throw
	//exceptions could occur in the same Unit.
	if (u instanceof ThrowStmt) {
	    /* TODO -- could be similar to returning a value (in this case,
	     * the thrown exception), which is not handled yet. */
	    return iCtx.mkTrue();
	} else for (ValueBox box : u.getUseAndDefBoxes()) {
	    Value v = box.getValue();
	    if (v instanceof DivExpr) {
		Value denom = ((DivExpr) v).getOp2();
		ArithExpr e = (ArithExpr) ofValue(denom);
		return iCtx.mkEq(e, iCtx.mkInt(0));
	    } else if (v instanceof ArrayRef) {
		ArrayRef l = (ArrayRef) v;
		Expr arrExp = ofValue(l.getBase());//this will be an array cell
		ArithExpr index = (ArithExpr) ofValue(l.getIndex());//Hope this is true

		BoolExpr inBounds = iCtx.mkGe(index, iCtx.mkInt(0));
		ArithExpr length = (ArithExpr) iCtx.mkSelect(arrayLengths, arrExp);
		inBounds = iCtx.mkAnd(inBounds, iCtx.mkLt(index, length));
		return iCtx.mkNot(inBounds);
	    } else if (v instanceof NewArrayExpr) {
		NewArrayExpr n = (NewArrayExpr) v;
		ArithExpr length = (ArithExpr) ofValue(n.getSize());
		return iCtx.mkLt(length, iCtx.mkInt(0));
	    } else {
		//Null pointer
		if(v instanceof InstanceInvokeExpr) {
		    InstanceInvokeExpr inv = (InstanceInvokeExpr) v;
		    Expr base = ofValue(inv.getBase());
		    return iCtx.mkEq(base, nullExpr);
		} else if (v instanceof InstanceFieldRef) {
		    InstanceFieldRef f = (InstanceFieldRef) v;
		    Expr base = ofValue(f.getBase());
		    return iCtx.mkEq(base, nullExpr);
		} else if (v instanceof LengthExpr) {
		    Value op = ((LengthExpr) v).getOp();
		    Expr base = ofValue(op);
		    return iCtx.mkEq(base, nullExpr);
		} else if (v instanceof ArrayRef) {
		    ArrayRef l = (ArrayRef) v;
		    Expr arrExpr = ofValue(l.getBase());
		    return iCtx.mkEq(arrExpr, nullExpr);
		} else if (u instanceof ThrowStmt) {
		    Value op = ((ThrowStmt) u).getOp();
		    Expr base = ofValue(op);
		    return iCtx.mkEq(base, nullExpr);
		}
		//Can also get thrown if trying to throw null.
	    }


	    //TODO: later add for class cast and for null pointer (after objects)
	}
	return null;
    }

    public BoolExpr ofDefinitionStmt(DefinitionStmt s) {
        //System.out.println(s.getRightOp().getClass());
	//System.out.println(s);
        Expr rightR = ofValue(s.getRightOp());
        if (rightR == null) {
            throw new NullPointerException("rValue is null: " + s.getRightOp());
        }

        Expr leftR = null;
        Value left = s.getLeftOp();
        if (left instanceof ArrayRef) {
            rightR = makeArrayStore((ArrayRef) left, rightR);
	    inDefinition = true;
	    if(left.getType() instanceof IntegerType) {
		String newName = getRenameString("ArrayCells", "Def" + edge.ID);
		//TODO: reference arrays
		leftR = iCtx.mkArrayConst(newName, refSort,
			primitiveArraySort);
		newPostArrayCells = (ArrayExpr) leftR;
	    } else {
		String newName = getFieldString(objectArrayCells);
		newName = getRenameString(newName, "Def" + edge.ID);
		leftR = iCtx.mkConst(newName, rightR.getSort());
		postFields.put(objectArrayCells, (ArrayExpr) leftR);
	    }
	    //System.out.println("Left, right: " + leftR + ", " + rightR);
        } else if (left instanceof InstanceFieldRef) {
	    if(findRaceCondition) {
		raceFieldWrite((InstanceFieldRef) left);
	    }
	    rightR = storeInField((InstanceFieldRef) left, rightR);
	    FieldRef f = (FieldRef) left;
	    Sort fieldSort = rightR.getSort();
	    inDefinition = true;
	    String newName = getFieldString(f.getField());
	    newName = getRenameString(newName, "Def" + edge.ID);
	    leftR = iCtx.mkConst(newName, fieldSort);
	    postFields.put(f.getField(),  (ArrayExpr) leftR);
	} else {//This includes static fields
	    /* This is now handled in the code for ThisRef and ParameterRef
	     * if I ever actually process catches, I may need this since
	     * @caughtexception always has type Throwable, but the lvalue
	     * has the correct type.
	    if(s instanceof IdentityStmt) {
		Type t = left.getType();
		BoolExpr wellTyped = classConstraint(rightR, (RefType) t);

		sideEffects = iCtx.mkAnd(sideEffects, wellTyped);
	    } */
	    inDefinition = true;
            leftR = ofValue(left);
        }
	inDefinition = false;

	//System.out.println(s);
	//System.out.println("Left, right: " + leftR + ", " + rightR);
        if (leftR == null) {
            throw new NullPointerException("lValue " + s.getLeftOp() + " is null");
        }
	//System.out.println("Left, right sorts: " + leftR.getSort() + ", " + rightR.getSort());

	if(rightR instanceof BoolExpr) {//leftR should never be, since locals never
	    rightR = iCtx.mkITE((BoolExpr) rightR, iCtx.mkInt(1), iCtx.mkInt(0));
	}

	BoolExpr result = null;
	try {
	    result = iCtx.mkEq(leftR, rightR);
	} catch (com.microsoft.z3.Z3Exception ex) {
	    System.out.println("Soot left, right: " + left + " " + s.getRightOp());
            System.out.println("Types left, right: " + left.getType() + " " + (s.getRightOp().getType().getClass().getName()));
            System.out.println("Sorts left, right: " + leftR.getSort() + " " + rightR.getSort());
	    System.out.println("Z3 left, right: " + leftR + " " + rightR);
	    throw ex;
	}

        //System.out.println("Left: " + leftR + " Right: " + rightR);
        return result;
    }

    public Expr makeArrayStore(ArrayRef l, Expr val) {
	Type baseType = l.getBase().getType();
	if (baseType instanceof ArrayType) {
	    Type eT = ((ArrayType) baseType).getElementType();
	    // If I ever decide to "fake" string arrays, I might undo this.
	    Expr arrExp = ofValue(l.getBase());
	    Expr index = ofValue(l.getIndex());
	    ArrayExpr selectArray;
	    ArrayExpr arrayCells;
	    if(eT instanceof IntegerType) {
		//Could check the cast before doing it
		arrayCells = getArrayCells();
		selectArray = (ArrayExpr)
		    iCtx.mkSelect(arrayCells, arrExp);
	    } else {
		arrayCells = (ArrayExpr) lastVertex.getField(objectArrayCells);
		if (arrayCells == null) {
		    arrayCells =
			//TODO -- this will go away with multi-d arrays
			(ArrayExpr) makeNewInstanceFieldArray(objectArrayCells,
			    objectArraySort);
		}
		Expr tmp = ofInstanceField(arrExp, objectArrayCells,
			objectArraySort);
		//System.out.println("Tmp: " + tmp + " sort: " + tmp.getSort());
		selectArray = (ArrayExpr) tmp;
	    }
	    Expr storeEl = iCtx.mkStore(selectArray, index, val);
	    Expr storeArray = iCtx.mkStore(arrayCells, arrExp, storeEl);

	    return storeArray;
	} else {
            throw new IllegalArgumentException("The jimple for an array reference base did not have array type.");
        }
    }

    public BoolExpr ofIfStmt(IfStmt s) {
        Expr condition = ofValue(s.getCondition());
        if(condition instanceof BoolExpr) {
	    BoolExpr result = (BoolExpr) condition;
	    boolean takes = edge.getHead().takesBranch(edge.thread);
	    //System.out.println("thread: " + edge.thread);
	    //System.out.println(edge.getHead().subpathsString());
	    if (!takes) {
		result = iCtx.mkNot(result);
	    }

	    //You should only have one of these
	    BoolExpr reachProperty = null;
	    if(findExceptions) {
		BoolExpr excThrown = (BoolExpr) ofStaticField(exceptionField,
			iCtx.getBoolSort());
		reachProperty = excThrown;
	    }
	    if(findDeadlock) {
		BoolExpr deadlock = (BoolExpr) ofStaticField(deadlockField,
			iCtx.getBoolSort());
		reachProperty = deadlock;
	    }
	    if(findRaceCondition) {
		BoolExpr raceFound = (BoolExpr) ofStaticField(raceFoundField,
			iCtx.getBoolSort());
		reachProperty = raceFound;
	    }

	    if(reachProperty != null) {
		result = iCtx.mkOr(result, reachProperty);
	    }
	    return result;
        } else {
            throw new IllegalArgumentException("The Z3 expression for the condition of an if was not a BoolExpr.");
        }
    }

    public Expr ofValue(soot.Value v) {
        //System.out.println(v.getClass());
        if (v instanceof Constant) {
            return ofConstant((Constant) v);
        }
        if (v instanceof Local) {
            //System.out.println("Here in instanceof Local");
            return ofLocal((Local) v);
        }
        if (v instanceof ArrayRef) {
            return ofArrayRef((ArrayRef) v);
        }
        if (v instanceof BinopExpr) {
            return ofBinopExpr((BinopExpr) v);
        }
        if (v instanceof NegExpr) {
            return ofNegExpr((NegExpr) v);
        }
        if (v instanceof ParameterRef) {
            return ofParameterRef((ParameterRef) v);
        }
        if (v instanceof NewArrayExpr) {
            return ofNewArrayExpr((NewArrayExpr) v);
        }
        if (v instanceof LengthExpr) {
            return ofLengthExpr((LengthExpr) v);
        }
        if(v instanceof ThisRef) {
	    Expr result = lastVertex.getParams().get(edge.thread).
			    get(-1);
	    if (result != null) {
		return result;
	    }

	    String newName = getRenameString("This", "" + edge.ID);
	    result = iCtx.mkConst(newName, refSort);

	    RefType type = (RefType) v.getType();
	    BoolExpr wellTyped = classConstraint(result, type);
	    //the keyword this is never null
	    BoolExpr notNull = iCtx.mkNot(iCtx.mkEq(result, nullExpr));
	    sideEffects = iCtx.mkAnd(sideEffects, wellTyped, notNull);

	    params.put(-1, result);
            return result;
        }
	if(v instanceof CaughtExceptionRef) {
	    Expr exc = lastVertex.getLastException(edge.thread);
	    if (exc != null) {
		return exc;
	    } else {
		//this can happen if it was an implicit exception
		return iCtx.mkConst("ExceptionRef" + v.equivHashCode(), refSort);
	    }
	}
	if (v instanceof NewExpr) {
	    return ofNewExpr((NewExpr) v);
	}
	if(v instanceof InstanceFieldRef) {
	    return ofInstanceFieldRef((InstanceFieldRef) v);
	}
	if(v instanceof StaticFieldRef) {
	    return ofStaticFieldRef((StaticFieldRef) v);
	}
	if(v instanceof CastExpr) {
	    CastExpr c = (CastExpr) v;

	    Expr res = ofValue(c.getOp());
	    Type castT = c.getCastType();
	    //Since not handling floats, nothing clear to do for any other case
	    if(castT instanceof RefType) {
		BoolExpr exc = getExcThrownEq(classConstraint(res,
			    (RefType) castT));
		sideEffects = iCtx.mkAnd(sideEffects, exc);
	    }

	    return res;
	}
	if (v instanceof InstanceOfExpr) {
	    if(v.getType() instanceof RefType) {
		InstanceOfExpr io = (InstanceOfExpr) v;
		Value op = io.getOp();
		return classConstraint(ofValue(op), (RefType) io.getCheckType());
	    } else {
		return iCtx.mkTrue();
	    }
	}
	if(v instanceof InvokeExpr) {
	    InvokeExpr inv = (InvokeExpr) v;
	    soot.SootMethod tLocalGetIndic = null;
	    try {
		tLocalGetIndic = soot.Scene.v().getMethod(
			"<java.lang.ThreadLocal: java.lang.Object get()>");
	    } catch(RuntimeException ex) {
	    }
	    soot.SootMethod writeLock = null;
	    soot.SootMethod readLock = null;

	    try {
		writeLock = soot.Scene.v().getMethod("<java.util.concurrent.locks.ReadWriteLock: java.util.concurrent.locks.Lock writeLock()>");
		readLock = soot.Scene.v().getMethod("<java.util.concurrent.locks.ReadWriteLock: java.util.concurrent.locks.Lock readLock()>");
	    } catch(RuntimeException ex) {
	    }
	    if(inv.getMethod().equals(tLocalGetIndic)) {
		return ofTLocalGet(inv);
	    } else {
		if(inv.getMethod().equals(writeLock) ||
			inv.getMethod().equals(readLock)) {
		    InstanceInvokeExpr iInv = (InstanceInvokeExpr) inv;
		    return ofValue(iInv.getBase());
		} else {
		    Sort s = getSort(inv.getType());
		    return iCtx.mkConst("ReturnValue" + edge.ID, s);
		}
	    }
	}

        System.out.println("Could not translate " + v);
        return null;
    }

    public com.microsoft.z3.Expr ofConstant(Constant c) {
        if (c instanceof IntConstant) {
            IntConstant ic = (IntConstant) c;
            int value = ic.value;
            return this.iCtx.mkInt(value);
        }
        if (c instanceof LongConstant) {
            LongConstant lc = (LongConstant) c;
            long value = lc.value;
            return this.iCtx.mkInt(value);
        }
	/* As far as WH knows, Z3 cannot do interpolants with floating points,
	so we cannot process them.
	if(c instanceof DoubleConstant) {
	    DoubleConstant dc = (DoubleConstant) c;
	    return iCtx.mkConst("Double" + dc.value, floatSort);
	}
	if(c instanceof FloatConstant) {
	    FloatConstant fc = (FloatConstant) c;
	    return iCtx.mkConst("Float" + fc.value, floatSort);
	} */
        if (c instanceof StringConstant) {
            return iCtx.mkConst("StringConst" + c, refSort);
        }
        if( c instanceof NullConstant) {
            return nullExpr;
        }

        System.err.println("unsupported primitive constant type" + c);
        return null;
    }

    public com.microsoft.z3.Expr ofLocal(Local l) {
        if (!inDefinition) {
	    //if(lastVertex == null ) {
		//System.out.println(edge.getClass());
		//System.out.println("Tail: " + ((UnitSingleEdge) edge).getTail());
		//System.out.println("Null lastVertex");
	    //}
	    Expr res = lastVertex.getVariable(l, edge.thread);
	    if (res != null ) {
		return res;
	    }
	    //else continue to the following part of the method
        }

        Expr result = null;
        soot.Type t = l.getType();
        String newName = l.getName(); //
	newName = newName + THREAD_PRE + edge.thread;
	newName = getRenameString(newName, String.valueOf(edge.ID));

        if (t instanceof soot.IntegerType || t instanceof soot.LongType) {
            result = iCtx.mkIntConst(newName);
        } else if (t instanceof ArrayType) {
	    result = iCtx.mkConst(newName, refSort);
        } else {
            //Does not handle floating point
            result = iCtx.mkConst(newName, refSort);
        }

	Map<Local, Expr> newMappings;
	if(inDefinition) {
	    newMappings = newThreadMappings;
	    newMappings.put(l, result);
	} else {
	    newMappings = newThreadPreMappings;
	    newMappings.put(l, result);
	}

        return result;
        //return null;
    }

    //TODO default values
    public com.microsoft.z3.Expr ofStaticFieldRef(StaticFieldRef f) {
        soot.Type t = f.getType();
	Sort sort;
        if (t instanceof soot.IntegerType) {
            sort = iCtx.getIntSort();
        } else if (t instanceof ArrayType) {
	    sort = refSort;
        } else {
            //Does not handle floating point
            sort = refSort;
        }
	return ofStaticField(f.getField(), sort);
    }

    public Expr ofStaticField(SootField fld, Sort sort) {
        if (!inDefinition) {
	    Expr res = lastVertex.getStaticField(fld);

	    if (res != null ) {
		return res;
	    }
	    //else continue to the following part of the method
        }

        Expr result = null;
	String newName = getFieldString(fld);
	if(inDefinition) {
	    //System.out.println("Def name");
	    newName = newName + "Def";
	}
	newName = getRenameString(newName, String.valueOf(edge.ID));

	result = iCtx.mkConst(newName, sort);

	Map<SootField, Expr> newMappings;
	if(inDefinition) {
	    newMappings = postStaticFields;
	} else {
	    newMappings = preStaticFields;
	}
	newMappings.put(fld, result);

        return result;
        //return null;
    }

    public Expr ofArrayRef(ArrayRef l) {

        Expr arrExp = ofValue(l.getBase());//this will be an array cell
	BoolExpr nullBase = iCtx.mkNot(iCtx.mkEq(arrExp, nullExpr));
        if(!inDefinition) {
	    Type bt = l.getType();//TODO: maybe change this once multi-d arrays are fleshed out
	    //ArrayExpr arrayCells;
	    ArrayExpr selectArray;
	    if(bt instanceof IntegerType) {
		ArrayExpr arrayCells = getArrayCells();
		selectArray = (ArrayExpr) iCtx.mkSelect(arrayCells, arrExp);
	    } else {//Maybe later handle floats
		selectArray = (ArrayExpr) ofInstanceField(arrExp,
			       objectArrayCells, objectArraySort);
	    }

	    //System.out.println("Array base sort: " + arrExp.getSort());
	    //System.out.println("Array sort: " + arrayCells.getSort());
	    ArithExpr index = (ArithExpr) ofValue(l.getIndex());//Hope this cast works

	    BoolExpr inBounds = iCtx.mkGe(index, iCtx.mkInt(0));
	    ArithExpr length = (ArithExpr) iCtx.mkSelect(arrayLengths, arrExp);
	    inBounds = iCtx.mkAnd(inBounds, iCtx.mkLt(index, length));
	    inBounds = iCtx.mkAnd(inBounds, nullBase);
	    inBounds = getExcThrownEq(inBounds);
	    sideEffects = iCtx.mkAnd(sideEffects, inBounds);

	    return iCtx.mkSelect(selectArray, index);
        } else {
            throw new UnsupportedOperationException("Should not convert an array reference to a value when it is used as an LValue.");
        }
    }

    public Expr ofParameterRef(ParameterRef p) {
        soot.Type t = p.getType();
	List<HashMap<Integer, Expr>> localParams = lastVertex.getParams();
	HashMap<Integer, Expr> myParams = localParams.get(edge.thread);
	Expr result = myParams.get(p.getIndex());
	if(result != null) {
	    return result;
	}

        if (t instanceof soot.PrimType) {
            String newName = PARAMETER_PRE + p.getIndex();
	    newName = getRenameString(newName, String.valueOf(edge.ID));
            if (t instanceof soot.IntegerType) {
                result = iCtx.mkIntConst(newName);
            }
            //TODO: handle floating point
        /*  TODO: I believe that this is wrong -- if an array is passed in,
	 *  the parameter is just an array CELL, which has reference type.
	 * } else if (t instanceof soot.ArrayType) {
            Type eType = ((ArrayType) t).getElementType();

            //TODO: handle multi-dimensional arrays (recursive)
            String newName = PARAMETER_PRE + p.getIndex();
	    newName = getRenameString(newName, String.valueOf(edge.ID));
            if (eType instanceof soot.IntegerType) {
                result = iCtx.mkArrayConst(newName, iCtx.getIntSort(),
                        iCtx.getIntSort());
            } else {
		result = iCtx.mkArrayConst(newName, iCtx.getIntSort(),
			refSort);
	    }
	    */
	} else {
	    String newName = "Parameter" + p.getIndex();
	    newName = getRenameString(newName, String.valueOf(edge.ID));
	    result = iCtx.mkConst(newName, refSort);

	    RefType type = (RefType) p.getType();
	    BoolExpr wellTyped = classConstraint(result, type);
	    sideEffects = iCtx.mkAnd(sideEffects, wellTyped);
	}

	//System.out.println("At parameter " + p.getIndex() + ", Params: " + params);
	params.put(p.getIndex(), result);
	//System.out.println("Params now : " + params);
	return result;
    }

    public Expr ofNegExpr(NegExpr negE1) {
        Value op1 = negE1.getOp();
        Expr op1Expr = this.ofValue(op1);
        return this.iCtx.mkSub(this.iCtx.mkInt(0), (ArithExpr) op1Expr);
    }

    public com.microsoft.z3.Expr ofBinopExpr(BinopExpr v) {
        if (v instanceof AddExpr) {
            AddExpr addE1 = (AddExpr) v;
            Value op1 = addE1.getOp1();
            Value op2 = addE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkAdd((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof AndExpr) {
            AndExpr andE1 = (AndExpr) v;
            Value op1 = andE1.getOp1();
            Value op2 = andE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
	    if(op1Expr instanceof IntExpr) {
		op1Expr = iCtx.mkNot(iCtx.mkEq(op1Expr, iCtx.mkInt(0)));
	    }
            Expr op2Expr = this.ofValue(op2);
	    if(op2Expr instanceof IntExpr) {
		op2Expr = iCtx.mkNot(iCtx.mkEq(op2Expr, iCtx.mkInt(0)));
	    }
            return this.iCtx.mkAnd((BoolExpr) op1Expr, (BoolExpr) op2Expr);
        }
        if (v instanceof CmpExpr) {
            CmpExpr cmpE1 = (CmpExpr) v;
            Value op1 = cmpE1.getOp1();
            Value op2 = cmpE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
	    BoolExpr cond = iCtx.mkGt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    Expr result = iCtx.mkITE(cond, iCtx.mkInt(1), iCtx.mkInt(-1));
	    cond = iCtx.mkEq((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    result = iCtx.mkITE(cond, iCtx.mkInt(0), result);
            return result;
        }
        if (v instanceof CmpgExpr) {
	    return iCtx.mkIntConst("FloatGComparison" + edge.ID);
            /* CmpgExpr cmpE1 = (CmpgExpr) v;
            Value op1 = cmpE1.getOp1();
            Value op2 = cmpE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
	    BoolExpr cond = iCtx.mkGt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    Expr result = iCtx.mkITE(cond, iCtx.mkInt(1), iCtx.mkInt(-1));
	    cond = iCtx.mkEq((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    result = iCtx.mkITE(cond, iCtx.mkInt(0), result);
            return result; */
        }
        if (v instanceof CmplExpr) {
	    return iCtx.mkIntConst("FloatLComparison" + edge.ID);
	    /*
            CmplExpr cmpE1 = (CmplExpr) v;
            Value op1 = cmpE1.getOp1();
            Value op2 = cmpE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
	    BoolExpr cond = iCtx.mkGt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    Expr result = iCtx.mkITE(cond, iCtx.mkInt(1), iCtx.mkInt(-1));
	    cond = iCtx.mkEq((ArithExpr) op1Expr, (ArithExpr) op2Expr);
	    result = iCtx.mkITE(cond, iCtx.mkInt(0), result);
            return result;
	    */
        }
        if (v instanceof DivExpr) {
            // we only consider integer here, we may consider real later
            DivExpr divE1 = (DivExpr) v;
            Value op1 = divE1.getOp1();
            Value op2 = divE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
	    BoolExpr zeroCheck = iCtx.mkNot(iCtx.mkEq(op2Expr, iCtx.mkInt(0)));
	    zeroCheck = getExcThrownEq(zeroCheck);
	    sideEffects = iCtx.mkAnd(sideEffects, zeroCheck);
            return this.iCtx.mkDiv((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof EqExpr) {
            EqExpr eqE1 = (EqExpr) v;
            Value op1 = eqE1.getOp1();
            Value op2 = eqE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkEq(op1Expr, op2Expr);
        }
        if (v instanceof GeExpr) {
            GeExpr geE1 = (GeExpr) v;
            Value op1 = geE1.getOp1();
            Value op2 = geE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            //System.out.println(op1.getClass());
            //System.out.println("making " + op1Expr + " >= " + op2Expr);
            return this.iCtx.mkGe((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof GtExpr) {
            GtExpr gtE1 = (GtExpr) v;
            Value op1 = gtE1.getOp1();
            Value op2 = gtE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkGt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof LeExpr) {
            LeExpr leE1 = (LeExpr) v;
            Value op1 = leE1.getOp1();
            Value op2 = leE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkLe((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof LtExpr) {
            LtExpr ltE1 = (LtExpr) v;
            Value op1 = ltE1.getOp1();
            Value op2 = ltE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkLt((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof MulExpr) {
            MulExpr mulE1 = (MulExpr) v;
            Value op1 = mulE1.getOp1();
            Value op2 = mulE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkMul((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof NeExpr) {
            NeExpr neE1 = (NeExpr) v;
            Value op1 = neE1.getOp1();
            Value op2 = neE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
	    //System.out.println(op1Expr + " " + op2Expr);
            BoolExpr eqExpr = this.iCtx.mkEq( op1Expr,
                     op2Expr);
            return this.iCtx.mkNot(eqExpr);
        }
        if (v instanceof OrExpr) {
            OrExpr orE1 = (OrExpr) v;
            Value op1 = orE1.getOp1();
            Value op2 = orE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkOr((BoolExpr) op1Expr, (BoolExpr) op2Expr);
        }
        if (v instanceof RemExpr) {
            RemExpr remE1 = (RemExpr) v;
            Value op1 = remE1.getOp1();
            Value op2 = remE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkRem((IntExpr) op1Expr, (IntExpr) op2Expr);
        }
        if (v instanceof ShlExpr) {
            // do later
            System.err.println("ShlExpr is unsupported" + v);
	    return iCtx.mkIntConst("ShlExpr" + edge.ID);
        }
        if (v instanceof ShrExpr) {
            // do later
            System.err.println("ShrExpr is unsupported" + v);
	    return iCtx.mkIntConst("ShrExpr" + edge.ID);
        }
        if (v instanceof SubExpr) {
            SubExpr subE1 = (SubExpr) v;
            Value op1 = subE1.getOp1();
            Value op2 = subE1.getOp2();
            Expr op1Expr = this.ofValue(op1);
            Expr op2Expr = this.ofValue(op2);
            return this.iCtx.mkSub((ArithExpr) op1Expr, (ArithExpr) op2Expr);
        }
        if (v instanceof UshrExpr) {
            // do later
            System.err.println("UshrExpr is unsupported" + v);
	    return iCtx.mkIntConst("UshrExpr" + edge.ID);
        }
        if (v instanceof XorExpr) {
            // do later
            System.err.println("XorExpr is unsupported" + v);
	    return iCtx.mkIntConst("XorExpr" + edge.ID);
        }
        System.err.println("this BinopExpr is unsupported" + v);
        return null;
    }

    public Expr ofNewArrayExpr(NewArrayExpr n) {
        Type eType = n.getBaseType();
        ArithExpr length = (ArithExpr) ofValue(n.getSize());
        Expr cell = iCtx.mkConst("ArrayCell" + n.hashCode(), refSort);
	Expr cls = iCtx.mkConst(eType.toString(), classSort);
	Expr select = iCtx.mkApp(CLASSES, cell);
	sideEffects = iCtx.mkAnd(sideEffects, iCtx.mkEq(select, cls));

	Expr contents, inCell;
        if (eType instanceof IntegerType) {
            Expr defaultValue = iCtx.mkInt(0);
            contents = iCtx.mkConstArray(iCtx.getIntSort(), defaultValue);
	    ArrayExpr arrayCells = getArrayCells();
	    inCell = iCtx.mkSelect(arrayCells, cell);
	} else {
	    Expr defaultValue = nullExpr;
	    contents = iCtx.mkConstArray(iCtx.getIntSort(), defaultValue);
	    inCell = ofInstanceField(cell,
		    objectArrayCells, objectArraySort);
	}
	BoolExpr setCell = iCtx.mkEq(inCell, contents);
	sideEffects = iCtx.mkAnd(sideEffects, setCell);

        Expr selectLength = iCtx.mkSelect(arrayLengths, cell);
        BoolExpr setLength = iCtx.mkEq(selectLength, length);
	BoolExpr lengthIsNat = iCtx.mkGe(length, iCtx.mkInt(0));
	lengthIsNat = getExcThrownEq(lengthIsNat);
        sideEffects = iCtx.mkAnd(sideEffects, setLength, lengthIsNat);

        return cell;
    }

    public Expr ofLengthExpr(LengthExpr l) {
        Expr arr = ofValue(l.getOp());
        return iCtx.mkSelect(arrayLengths, arr);
    }

    public Expr ofNewExpr(NewExpr n) {
	FuncDecl mkAlloc = refSort.getConstructors()[1];
	IntExpr id = iCtx.mkInt(edge.ID);
	Expr res = mkAlloc.apply(id);

	BoolExpr fresh = iCtx.mkNot((BoolExpr) isEnv.apply(id));
	Expr c = iCtx.mkConst(n.getBaseType().getClassName(), classSort);
	Expr select = CLASSES.apply(res);
	sideEffects = iCtx.mkAnd(sideEffects, fresh, iCtx.mkEq(select, c));
	return res;
    }

    /* public static Expr defaultValue(Type t) {
	if(t instanceof IntegerType) {
	    return iCtx.mkInt(0);
	} else if (t instanceof RefLikeType) {
	    return nullExpr;
	}
	return null;
    } */

    public Expr ofInstanceFieldRef(InstanceFieldRef f) {
	Expr base = ofValue(f.getBase());
	Sort fieldSort = getSort(f.getType());
	if(findRaceCondition) {
	    conjoinFieldReadRaceConds(base, f);
	}
	return ofInstanceField(base, f.getField(), fieldSort);
    }

    public void conjoinFieldReadRaceConds(Expr base, InstanceFieldRef f) {
	boolean oldInDefinition = inDefinition;

	Sort intSort = iCtx.getIntSort();
	if( !oldInDefinition) {
	    BoolExpr[] raceConds = new BoolExpr[2];
	    inDefinition = false;

	    for(int i = 0; i < 2; i++) {
		IntExpr w = (IntExpr) ofInstanceField(base, writeClocks.get(i),
			intSort);
		IntExpr c=(IntExpr) ofStaticField(threadClocks.get(edge.thread).
			get(i), intSort);
		raceConds[i] = iCtx.mkLe(w, c);
	    }
	    inDefinition = false;
	    BoolExpr rf = (BoolExpr) ofStaticField(raceFoundField,
		    iCtx.getBoolSort());
	    inDefinition = true;
	    BoolExpr nRF = (BoolExpr) ofStaticField(raceFoundField,
		    iCtx.getBoolSort());
	    inDefinition = oldInDefinition;

	    BoolExpr raceCond = iCtx.mkNot(iCtx.mkAnd(raceConds));
	    raceCond = iCtx.mkOr(rf, raceCond);
	    raceCond = iCtx.mkEq(nRF, raceCond);
	    sideEffects = iCtx.mkAnd(sideEffects, raceCond);

	    int th = edge.thread;
	    inDefinition = false;
	    IntExpr curTime = (IntExpr) ofStaticField(
		    threadClocks.get(th).get(th), intSort);
	    Expr store = storeInField(f.getBase(),
		    readClocks.get(th), curTime);
	    inDefinition = oldInDefinition;
	    String newName = getFieldString(readClocks.get(th));
	    newName = newName + "Def" + edge.ID;
	    Expr nR = iCtx.mkConst(newName, store.getSort());
	    //postFields.put(readClocks.get(th), (ArrayExpr) nR);
	    //sideEffects = iCtx.mkAnd(sideEffects, iCtx.mkEq(nR, store));
	}

	inDefinition = oldInDefinition;
    }

    public Expr ofInstanceField(Expr base, SootField field, Sort fieldSort) {
	//System.out.println("Generating instance field for " + edge.ID);
	ArrayExpr array = lastVertex.getField(field);

	if(array == null) {
	    array = makeNewInstanceFieldArray(field, fieldSort);
	}

	BoolExpr notNull = iCtx.mkNot(iCtx.mkEq(base, nullExpr));
	notNull = getExcThrownEq(notNull);
	sideEffects = iCtx.mkAnd(sideEffects, notNull);

	if(field.getType() instanceof soot.RefLikeType) {
	    String newName = getFieldString(field);
	    newName = getRenameString(newName, "Def" + edge.ID);
	    ArrayExpr newField=(ArrayExpr) iCtx.mkConst(newName, array.getSort());

	    //This handles the actual result
	    Expr result = iCtx.mkSelect(newField, base);
	    try {
                result = optRefSort.getAccessors()[1][0].apply(result);
            } catch (com.microsoft.z3.Z3Exception ex) {
                System.out.println("Func: " + optRefSort.getAccessors()[1][0] + " argument: " + result);
                throw ex;
            }

	    //This handles the update for optional references
	    Expr curVal = iCtx.mkSelect(array, base);
	    BoolExpr isUndef = (BoolExpr)
		optRefSort.getRecognizers()[0].apply(curVal);
	    FuncDecl fInit = iCtx.mkFuncDecl(getFieldString(field) + "Init",
		    new Sort[]{ refSort }, refSort);
	    Expr newVal = fInit.apply(base);
	    Expr initObj = newVal;
	    newVal = optRefSort.getConstructors()[1].apply(newVal);
	    Expr newStore = iCtx.mkStore(array, base, newVal);
	    newStore = iCtx.mkITE(isUndef, newStore, array);
	    BoolExpr setStore = iCtx.mkEq(newField, newStore);

	    Expr newI = refSort.getAccessors()[1][0].apply(initObj);
	    BoolExpr prebuilt = (BoolExpr) isEnv.apply(newI);
	    BoolExpr nn = (BoolExpr) refSort.getRecognizers()[0].apply(initObj);
	    nn = iCtx.mkNot(nn);
	    prebuilt = iCtx.mkImplies(nn, prebuilt);

            sideEffects=iCtx.mkAnd(sideEffects, setStore, prebuilt);
            postFields.put(field, newField);

            //There could be a way to check this for arrays, but we do not really have it
            if(field.getType() instanceof RefType) {
                Type typeToCheck = field.getType();
                BoolExpr wellTyped=classConstraint(initObj, (RefType) field.getType());
                sideEffects=iCtx.mkAnd(sideEffects, wellTyped);
            }

	    //System.out.println("FPreSort: " + fPre.getSort() + " base sort: " + base.getSort());

	    return result;

	} else {
	    return iCtx.mkSelect(array, base);
	}

    }

    public void raceFieldWrite(InstanceFieldRef f) {
	Expr base = ofValue(f.getBase());
	Sort intSort = iCtx.getIntSort();
	BoolExpr[] raceConds = new BoolExpr[2];
	inDefinition = false;
	for(int i = 0; i < 2; i++) {
	    IntExpr w = (IntExpr) ofInstanceField(base, writeClocks.get(i),
		    intSort);
	    IntExpr c=(IntExpr) ofStaticField(threadClocks.get(edge.thread).
		    get(i), intSort);
	    raceConds[i] = iCtx.mkLe(w, c);
	    IntExpr r = (IntExpr) ofInstanceField(base, readClocks.get(i),
		    intSort);
	    raceConds[i] = iCtx.mkAnd(raceConds[i], iCtx.mkLe(r, c));
	}
	inDefinition = false;
	BoolExpr rf = (BoolExpr) ofStaticField(raceFoundField, iCtx.getBoolSort());
	inDefinition = true;
	BoolExpr nRF = (BoolExpr) ofStaticField(raceFoundField, iCtx.getBoolSort());
	inDefinition = false;

	BoolExpr raceCond = iCtx.mkNot(iCtx.mkAnd(raceConds));
	raceCond = iCtx.mkOr(rf, raceCond);
	raceCond = iCtx.mkEq(nRF, raceCond);
	sideEffects = iCtx.mkAnd(sideEffects, raceCond);

	BoolExpr[] clearRd = new BoolExpr[2];
	for(int i = 0; i < 2; i++) {
	    inDefinition = false;
	    ArrayExpr oR = (ArrayExpr) storeInField(f.getBase(),
		    readClocks.get(i), iCtx.mkInt(0));
	    inDefinition = true;
	    String newName = getFieldString(readClocks.get(i));
	    newName = newName + "Def_" + edge.ID;
	    ArrayExpr nR = (ArrayExpr) iCtx.mkConst(newName, oR.getSort());
	    inDefinition = false;
	    postFields.put(f.getField(), nR);
	    clearRd[i] = iCtx.mkEq(nR, oR);
	}
	BoolExpr clearR = iCtx.mkAnd(clearRd);

	int th = edge.thread;
	inDefinition = false;
	IntExpr ctt = (IntExpr) ofStaticField(threadClocks.get(th).get(th), intSort);
	ArrayExpr wrT = (ArrayExpr) storeInField(f.getBase(),
		writeClocks.get(th), ctt);
	inDefinition = true;
	String newName = getFieldString(writeClocks.get(th));
	newName = newName + "Def_" + edge.ID;
	ArrayExpr nR = (ArrayExpr) iCtx.mkConst(newName, wrT.getSort());
	BoolExpr wrAsgnTh = iCtx.mkEq(nR, wrT);
	postFields.put(writeClocks.get(th), nR);

	int oth = 1 - th;
	inDefinition = false;
	ArrayExpr wrU = (ArrayExpr) storeInField(f.getBase(),
		writeClocks.get(oth), iCtx.mkInt(0));
	inDefinition = true;
	newName = getFieldString(writeClocks.get(oth));
	newName = newName + "Def_" + edge.ID;
	nR = (ArrayExpr) iCtx.mkConst(newName, wrU.getSort());
	postFields.put(writeClocks.get(oth), nR);
	BoolExpr wrAsgnOth = iCtx.mkEq(nR, wrU);
	inDefinition = false;
	sideEffects = iCtx.mkAnd(sideEffects, clearR, wrAsgnTh, wrAsgnOth);
    }

    public Expr storeInField(InstanceFieldRef f, Expr val) {
	return storeInField(f.getBase(), f.getField(), val);
    }

    public Expr storeInField(Value base, SootField field, Expr val) {
	ArrayExpr fieldArr = lastVertex.getField(field);
	if (fieldArr == null) {
	    fieldArr = makeNewInstanceFieldArray(field, val.getSort());
	}
	Expr obj = ofValue(base);

	BoolExpr notNull = getExcThrownEq(iCtx.mkNot(iCtx.mkEq(obj, nullExpr)));
	sideEffects = iCtx.mkAnd(sideEffects, notNull);

	if(field.getType() instanceof RefType) {
	    FuncDecl mkMaybe = optRefSort.getConstructors()[1];
	    val = mkMaybe.apply(val);
	}

	Expr result = iCtx.mkStore(fieldArr, obj, val);
	return result;
    }

    public ArrayExpr makeNewInstanceFieldArray(InstanceFieldRef f) {
	Sort fieldSort = getSort(f.getType());
	return makeNewInstanceFieldArray(f.getField(), fieldSort);
    }

    public ArrayExpr makeNewInstanceFieldArray(SootField field, Sort fieldSort) {
	ArrayExpr pre = preFields.get(field);
	if(pre != null) {
	    return pre;
	}

	String newName = getFieldString(field);
	newName = getRenameString(newName, "" + edge.ID);
	if(fieldSort.equals(refSort)) {
	    fieldSort = optRefSort;
	}
	fieldSort = iCtx.mkArraySort(refSort, fieldSort);

	ArrayExpr result = (ArrayExpr) iCtx.mkConst(newName, fieldSort);
	preFields.put(field, result);

	return result;
    }

    public BoolExpr ofLock(InvokeExpr lockCallIn) {
	//This could throw a ClassCastException, but I want it to:
	//this method should only be called from a UnitSingleEdge
	lastVertex = ((UnitSingleEdge) edge).getTail();

	InstanceInvokeExpr lockCall;
	try{
	    lockCall = (InstanceInvokeExpr) lockCallIn;
	} catch (ClassCastException ex) {
	    //System.err.println("An invoke statement that was not a lock was found in an edge: " + lockCallIn);
	    return iCtx.mkTrue();

	}
	Value base = lockCall.getBase();
	Expr lock = ofValue(lockCall.getBase());


	Expr newVal = null;//value never used
	BoolExpr checkHeld = null;
	BoolExpr raceInfo = null;
	if(lockCall.getMethod().getName().equals("unlock")) {
	    newVal = iCtx.mkInt(-1);
	    //TODO: check this -- it is probably right
	    int t = edge.thread;
	    raceInfo = iCtx.mkTrue();
	    for(int i = 0; findRaceCondition && i < 2; i++) {
		SootField clock = releaseClocks.get(i);
		IntExpr tC = (IntExpr) ofStaticField(threadClocks.get(t).get(i),
			iCtx.getIntSort());
		ArrayExpr store = (ArrayExpr) storeInField(base, clock, tC);
		String newName = getRenameString(clock.getName() + "Def",
			String.valueOf(edge.ID));
		ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName,
			store.getSort());
		postFields.put(clock, nConst);
		BoolExpr relUp = iCtx.mkEq(nConst, store);
		inDefinition = true;
		Expr nTC = ofStaticField(threadClocks.get(edge.thread).get(i),
			iCtx.getIntSort());
		inDefinition = false;
		BoolExpr updTC = iCtx.mkEq(nTC, iCtx.mkAdd(tC, iCtx.mkInt(1)));
		raceInfo = iCtx.mkAnd(raceInfo, relUp, updTC);
	    }

	    ArrayExpr n = (ArrayExpr) storeInField(base, lockField, newVal);

	    String newName = getRenameString("?LockDef", String.valueOf(edge.ID));
	    ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName, n.getSort());
	    postFields.put(lockField, nConst);

	    BoolExpr enbld = (BoolExpr) ofStaticField(
		    enabledFields.get(edge.thread), iCtx.getBoolSort());

	    inDefinition = true;
	    BoolExpr enbldD = (BoolExpr) ofStaticField(
		    enabledFields.get(edge.thread), iCtx.getBoolSort());
	    inDefinition = false;
	    Expr lockWaiting = ofStaticField(lockHeldFields.get(1 - edge.thread),
		    refSort);
	    BoolExpr mkEnbldD = iCtx.mkEq(lock, lockWaiting);
	    mkEnbldD = iCtx.mkOr(enbld, mkEnbldD);
	    mkEnbldD = iCtx.mkEq(enbldD, mkEnbldD);
	    //postStaticFields.put(enabledFields.get(1 - edge.thread), enbldD);

	    BoolExpr result = iCtx.mkEq(nConst, n);
	    if(raceInfo != null) {
	       result = iCtx.mkAnd(result, raceInfo);
	    }
	    result = iCtx.mkAnd(enbld, result, mkEnbldD);
	    return  result;
	} else if(lockCall.getMethod().getName().equals("lock")) {
	    SootMethod readLock = null;
	    SootMethod writeLock = null;
	    try {
		readLock = soot.Scene.v().getMethod("<java.util.concurrent.locks.ReentrantReadWriteLock$ReadLock: void lock()>");
		writeLock = soot.Scene.v().getMethod("<java.util.concurrent.locks.Lock: void lock()>");
	    } catch (RuntimeException ex) {
	    }

	    Expr curVal = ofInstanceField(lock, lockField, iCtx.getIntSort());
	    int t = edge.thread;

	    if(lockCall.getMethod().equals(readLock)) {
		newVal = iCtx.mkInt(-15);
		checkHeld = ofReadLock(lockCall);
		//TODO
	    } else if(lockCall.getMethod().equals(writeLock)) {
		newVal = iCtx.mkInt(edge.thread);
		checkHeld = ofWriteLock(lockCall);
		//TODO
	    } else {

		newVal = iCtx.mkInt(edge.thread);

		//checkHeld = iCtx.mkAnd(checkHeld, otherWaiting);
		checkHeld = iCtx.mkEq(curVal, iCtx.mkInt(1 - t));


		//This can probably go down below, but it was never implemented
		//for the other kinds of lock, so I will wait.
		raceInfo = iCtx.mkTrue();
		if(findRaceCondition) {
		    BoolExpr[] raceII = new BoolExpr[2];
		    //t = edge.thread, in case you've forgotten
		    for(int k = 0; k < 2; k++) {
			IntExpr l = (IntExpr) ofInstanceField(lock,
				releaseClocks.get(k), iCtx.getIntSort());
			IntExpr c = (IntExpr) ofStaticField(threadClocks.get(t).get(k),
				iCtx.getIntSort());
			BoolExpr gt = iCtx.mkGt(l, c);
			IntExpr max = (IntExpr) iCtx.mkITE(gt, l, c);
			inDefinition = true;
			Expr nc = ofStaticField(threadClocks.get(t).get(k),
				iCtx.getIntSort());
			inDefinition = false;
			raceII[k] = iCtx.mkEq(nc, max);
		    }
		    raceInfo = iCtx.mkAnd(raceII);
		}
	    } //end of case on type of lock


	    final Sort bool = iCtx.getBoolSort();

	    inDefinition = true;
	    BoolExpr waiting = (BoolExpr)
		ofStaticField(waitingFields.get(t), bool);
	    inDefinition = false;
	    checkHeld = iCtx.mkEq(waiting, checkHeld);

	    BoolExpr causeDead = iCtx.mkTrue();
	    Expr ddLockD = iCtx.mkFalse();
	    if(findDeadlock) {

		BoolExpr otherWaiting = (BoolExpr)
		    ofStaticField(waitingFields.get(1 - t), bool);
		Expr lckPttve = ofStaticField(lockHeldFields.get(1 - t),
			refSort);
		Expr otherCurVal = ofInstanceField(lckPttve, lockField,
			refSort);
		BoolExpr actWaiting = iCtx.mkEq(otherCurVal, iCtx.mkInt(t));

		BoolExpr ddLock = (BoolExpr) ofStaticField(deadlockField, bool);
		inDefinition = true;
		ddLockD = ofStaticField(deadlockField, bool);
		inDefinition = false;
		causeDead = iCtx.mkAnd(waiting, otherWaiting, actWaiting);
		causeDead = iCtx.mkEq(ddLockD, iCtx.mkOr(ddLock, causeDead));
	    }

	    inDefinition = true;
	    Expr contD = ofStaticField(enabledFields.get(t), bool);
	    inDefinition = false;
	    BoolExpr setEnabled = iCtx.mkEq(contD,
		    iCtx.mkOr(iCtx.mkNot(waiting), (BoolExpr) ddLockD));
	    //postStaticFields.put(enabledFields.get(t), contD);

	    BoolExpr setLockHeld = iCtx.mkTrue();
	    if(findDeadlock) {
		inDefinition = true;
		Expr lockHeld = ofStaticField(lockHeldFields.get(t),
			refSort);
		inDefinition = false;
		setLockHeld = iCtx.mkEq(lockHeld, lock);
		setLockHeld = iCtx.mkOr(iCtx.mkNot(waiting), setLockHeld);
		postStaticFields.put(lockHeldFields.get(t), lockHeld);
	    }

	    checkHeld = iCtx.mkAnd(checkHeld, causeDead, setEnabled, setLockHeld);

	    ArrayExpr n = (ArrayExpr) storeInField(base, lockField, newVal);
	    ArrayExpr curLock = (ArrayExpr) lastVertex.getField(lockField);
	    if(curLock == null) {
		//this will always have something since storeInField just ran
		curLock = (ArrayExpr) preFields.get(lockField);
	    }

	    Expr newLock = iCtx.mkITE(waiting, curLock,  n);
	    String newName = getRenameString("?LockDef", String.valueOf(edge.ID));
	    ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName, n.getSort());
	    postFields.put(lockField, nConst);

	    BoolExpr result = iCtx.mkEq(nConst, newLock);
	    BoolExpr enbld;

	    if(findDeadlock) {
		enbld = (BoolExpr) ofStaticField(enabledFields.get(edge.thread),
					    iCtx.getBoolSort());
	    } else {
		enbld = (BoolExpr) contD;
	    }
	    result = iCtx.mkAnd(enbld, checkHeld, result);

	    if(raceInfo != null) {
	       result = iCtx.mkAnd(result, raceInfo);
	    }
	    return  result;
	} else {
	    //System.err.println("An invoke statement that was not a lock was found in an edge: " + lockCall);
	    return iCtx.mkTrue();
	}

    }

    public BoolExpr ofReadLock(InstanceInvokeExpr lockCall) {
	//System.out.println("In of Read lock");
	Expr lock = ofValue(lockCall.getBase());
	int t = edge.thread;

	Expr curVal = ofInstanceField(lock, lockField, iCtx.getIntSort());
	BoolExpr checkHeld0 = iCtx.mkEq(curVal, iCtx.mkInt(0));
	BoolExpr checkHeld1 = iCtx.mkEq(curVal, iCtx.mkInt(1));
	BoolExpr checkHeld = iCtx.mkOr(checkHeld0, checkHeld1);

	return checkHeld;
    }

    public BoolExpr ofWriteLock(InstanceInvokeExpr lockCall) {
	//System.out.println("In of Write lock");
	Expr lock = ofValue(lockCall.getBase());
	int t = edge.thread;

	Expr curVal = ofInstanceField(lock, lockField, iCtx.getIntSort());
	BoolExpr checkHeld0 = iCtx.mkEq(curVal, iCtx.mkInt(-15));
	BoolExpr checkHeld1 = iCtx.mkEq(curVal, iCtx.mkInt(1 - t));
	BoolExpr checkHeld = iCtx.mkOr(checkHeld0, checkHeld1);

	return checkHeld;
    }

    //This method is literally copy and pasted from ofLock. When possible,
    //refactor the two to call a common function
    public BoolExpr ofEnterMonitorStmt(EnterMonitorStmt e) {
        //This could throw a ClassCastException, but I want it to:
        //this method should only be called from a UnitSingleEdge
        lastVertex = ((UnitSingleEdge) edge).getTail();

        Value base = e.getOp();
        Expr lock = ofValue(base);

        Expr curVal = ofInstanceField(lock, lockField, iCtx.getIntSort());
        int t = edge.thread;

        Expr newVal = iCtx.mkInt(edge.thread);
        BoolExpr checkHeld = iCtx.mkEq(curVal, iCtx.mkInt(1 - t));
        BoolExpr raceInfo = iCtx.mkTrue();

        if(findRaceCondition) {
            BoolExpr[] raceII = new BoolExpr[2];
            //t = edge.thread, in case you've forgotten
            for(int k = 0; k < 2; k++) {
                IntExpr l = (IntExpr) ofInstanceField(lock,
                        releaseClocks.get(k), iCtx.getIntSort());
                IntExpr c = (IntExpr) ofStaticField(threadClocks.get(t).get(k),
                        iCtx.getIntSort());
                BoolExpr gt = iCtx.mkGt(l, c);
                IntExpr max = (IntExpr) iCtx.mkITE(gt, l, c);
                inDefinition = true;
                Expr nc = ofStaticField(threadClocks.get(t).get(k),
                        iCtx.getIntSort());
                inDefinition = false;
                raceII[k] = iCtx.mkEq(nc, max);
            }
            raceInfo = iCtx.mkAnd(raceII);
        }
        final Sort bool = iCtx.getBoolSort();

        inDefinition = true;
        BoolExpr waiting = (BoolExpr)
            ofStaticField(waitingFields.get(t), bool);
        inDefinition = false;
        checkHeld = iCtx.mkEq(waiting, checkHeld);

        BoolExpr causeDead = iCtx.mkTrue();
        Expr ddLockD = iCtx.mkFalse();
        if(findDeadlock) {

            BoolExpr otherWaiting = (BoolExpr)
                ofStaticField(waitingFields.get(1 - t), bool);
            Expr lckPttve = ofStaticField(lockHeldFields.get(1 - t),
                    refSort);
            Expr otherCurVal = ofInstanceField(lckPttve, lockField,
                    refSort);
            BoolExpr actWaiting = iCtx.mkEq(otherCurVal, iCtx.mkInt(t));

            BoolExpr ddLock = (BoolExpr) ofStaticField(deadlockField, bool);
            inDefinition = true;
            ddLockD = ofStaticField(deadlockField, bool);
            inDefinition = false;
            causeDead = iCtx.mkAnd(waiting, otherWaiting, actWaiting);
            causeDead = iCtx.mkEq(ddLockD, iCtx.mkOr(ddLock, causeDead));
        }

        inDefinition = true;
        Expr contD = ofStaticField(enabledFields.get(t), bool);
        inDefinition = false;
        BoolExpr setEnabled = iCtx.mkEq(contD,
                iCtx.mkOr(iCtx.mkNot(waiting), (BoolExpr) ddLockD));
        //postStaticFields.put(enabledFields.get(t), contD);

        BoolExpr setLockHeld = iCtx.mkTrue();
        if(findDeadlock) {
            inDefinition = true;
            Expr lockHeld = ofStaticField(lockHeldFields.get(t),
                    refSort);
            inDefinition = false;
            setLockHeld = iCtx.mkEq(lockHeld, lock);
            setLockHeld = iCtx.mkOr(iCtx.mkNot(waiting), setLockHeld);
            postStaticFields.put(lockHeldFields.get(t), lockHeld);
        }

        checkHeld = iCtx.mkAnd(checkHeld, causeDead, setEnabled, setLockHeld);

        ArrayExpr n = (ArrayExpr) storeInField(base, lockField, newVal);
        ArrayExpr curLock = (ArrayExpr) lastVertex.getField(lockField);
        if(curLock == null) {
            //this will always have something since storeInField just ran
            curLock = (ArrayExpr) preFields.get(lockField);
        }

        Expr newLock = iCtx.mkITE(waiting, curLock,  n);
        String newName = getRenameString("?LockDef", String.valueOf(edge.ID));
        ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName, n.getSort());
        postFields.put(lockField, nConst);

        BoolExpr result = iCtx.mkEq(nConst, newLock);
        BoolExpr enbld;

        if(findDeadlock) {
            enbld = (BoolExpr) ofStaticField(enabledFields.get(edge.thread),
                    iCtx.getBoolSort());
        } else {
            enbld = (BoolExpr) contD;
        }
        result = iCtx.mkAnd(enbld, checkHeld, result);

        if(raceInfo != null) {
            result = iCtx.mkAnd(result, raceInfo);
        }
        return  result;
    }

    public BoolExpr ofExitMonitorStmt(ExitMonitorStmt e) {
        lastVertex = ((UnitSingleEdge) edge).getTail();

        Value base = e.getOp();
        Expr lock = ofValue(base);

        Expr newVal = iCtx.mkInt(-1);
        //See note to check this in ofLock above
        int t = edge.thread;
        BoolExpr raceInfo = iCtx.mkTrue();
        for(int i = 0; findRaceCondition && i < 2; i++) {
            SootField clock = releaseClocks.get(i);
            IntExpr tC = (IntExpr) ofStaticField(threadClocks.get(t).get(i),
                    iCtx.getIntSort());
            ArrayExpr store = (ArrayExpr) storeInField(base, clock, tC);
            String newName = getRenameString(clock.getName() + "Def",
                    String.valueOf(edge.ID));
            ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName,
                    store.getSort());
            postFields.put(clock, nConst);
            BoolExpr relUp = iCtx.mkEq(nConst, store);
            inDefinition = true;
            Expr nTC = ofStaticField(threadClocks.get(edge.thread).get(i),
                    iCtx.getIntSort());
            inDefinition = false;
            BoolExpr updTC = iCtx.mkEq(nTC, iCtx.mkAdd(tC, iCtx.mkInt(1)));
            raceInfo = iCtx.mkAnd(raceInfo, relUp, updTC);
        }

        ArrayExpr n = (ArrayExpr) storeInField(base, lockField, newVal);

        String newName = getRenameString("?LockDef", String.valueOf(edge.ID));
        ArrayExpr nConst = (ArrayExpr) iCtx.mkConst(newName, n.getSort());
        postFields.put(lockField, nConst);

        BoolExpr enbld = (BoolExpr) ofStaticField(
                enabledFields.get(edge.thread), iCtx.getBoolSort());

        inDefinition = true;
        BoolExpr enbldD = (BoolExpr) ofStaticField(
                enabledFields.get(edge.thread), iCtx.getBoolSort());
        inDefinition = false;
        Expr lockWaiting = ofStaticField(lockHeldFields.get(1 - edge.thread),
                refSort);
        BoolExpr mkEnbldD = iCtx.mkEq(lock, lockWaiting);
        mkEnbldD = iCtx.mkOr(enbld, mkEnbldD);
        mkEnbldD = iCtx.mkEq(enbldD, mkEnbldD);
        //postStaticFields.put(enabledFields.get(1 - edge.thread), enbldD);

        BoolExpr result = iCtx.mkEq(nConst, n);
        if(raceInfo != null) {
            result = iCtx.mkAnd(result, raceInfo);
        }
        result = iCtx.mkAnd(enbld, result, mkEnbldD);
        return  result;
    }

    public Expr ofTLocalGet(InvokeExpr getCallIn) {
	int thread = edge.thread;
	return threadLocalVars.get(thread);
    }

    /*
    public ArrayExpr getLocks() {
	ArrayExpr result = lastVertex.getLocks();
	if(result == null) {
	    String newName = getRenameString("Locks", String.valueOf(edge.ID));
	    result = iCtx.mkArrayConst(newName, refSort, iCtx.getIntSort());
	    newLocks = result;
	}
	return result;
    } */

    public static String calledMethodName(Unit u) {
	for(ValueBox vb : u.getUseBoxes()) {
	    Value v = vb.getValue();
	    if (v instanceof InvokeExpr) {
		InvokeExpr inv = (InvokeExpr) v;
		return inv.getMethodRef().name();
	    }
	}
	return null;
    }

    public static List<SootField> fakeFieldsUsed(InvokeExpr inv, boolean inDef) {
	List<SootField> result = new ArrayList<SootField>();
	String name = inv.getMethodRef().name();
	if(name.contains("lock")) {
	    if(name.equals("lock")) {
		result.add(lockField);
		//result.add(deadlockField);
		/*  This already has a read and write, so no need to add more
		 * if(!inDef) {
		    result.add(waitingFields.get(1 - inThread));
		} else {
		    result.add(waitingFields.get(inThread));
		} */
	    } else if (name.equals("unlock")) {
		if(inDef) {
		    result.add(lockField);
		}
	    }
	}
	return result;
    }

    public static List<SootField> fakeFieldsTouched(InvokeExpr inv,
	    boolean inBoth) {
	List<SootField> result = new ArrayList<SootField>();
	String name = inv.getMethodRef().name();
	if(name.contains("lock")) {
	    if(name.equals("lock")) {
		result.add(lockField);
		/* result.add(deadlockField);
		result.add(waitingFields.get(inThread));
		if(inBoth) {
		    result.add(waitingFields.get(1 - inThread));
		} */
	    } else if (name.equals("unlock")) {
		result.add(lockField);
	    }
	}
	return result;
    }

    public static InvokeExpr hasInvoke(Unit u) {
	List<ValueBox> boxes = u.getUseBoxes();
	java.util.Set<soot.SootMethod> libM = UnwindingContext.getLibMethods();
	for(ValueBox box : boxes) {
	    if(box.getValue() instanceof InvokeExpr) {
		InvokeExpr inv = (InvokeExpr) box.getValue();
		if(libM.contains(inv.getMethod())) {
		    return inv;
		}
	    }
	}

	return null;
    }

    // Create the constraint for whether obj has type t (or a subtype)
    public static BoolExpr classConstraint(Expr obj, RefType t) {
	Expr classOf = CLASSES.apply(obj);

	soot.SootClass c = t.getSootClass();
	soot.FastHierarchy h = soot.Scene.v().getFastHierarchy();

	ArrayList<BoolExpr> disjuncts = new ArrayList<BoolExpr>();

	ArrayDeque<SootClass> stack = new ArrayDeque<SootClass>();
	stack.push(c);
	java.util.HashSet<SootClass> visited = new java.util.HashSet<SootClass>();
	visited.add(c);
	while(!stack.isEmpty()) {
	    c = stack.pop();

	    if(c.isConcrete() && c.resolvingLevel() >= SootClass.SIGNATURES) {
		Expr clss = iCtx.mkConst(c.getName(), classSort);
		disjuncts.add(iCtx.mkEq(classOf, clss));
	    }

	    if(c.isInterface()) {
		for(SootClass sub : h.getAllImplementersOfInterface(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
		for(SootClass sub : h.getAllSubinterfaces(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
	    } else {
		for(SootClass sub : h.getSubclassesOf(c)) {
		    if(visited.add(sub)) {
			stack.push(sub);
		    }
		}
	    }
	}
	BoolExpr result = iCtx.mkTrue();
	//this should only possibly happen at the entry function
	if( !disjuncts.isEmpty()) {
	    result = iCtx.mkOr(disjuncts.toArray(new BoolExpr[0]));
	}
	return result;
    }
}
