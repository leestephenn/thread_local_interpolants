/** Copyright (c) Stephen N. Lee*/

import soot.Body;
import soot.SootMethod;
import soot.SootClass;
import soot.Unit;
import soot.Scene;
import soot.Type;
import soot.jimple.Stmt;
import soot.Trap;
import soot.Local;
import soot.Value;
import soot.jimple.InvokeExpr;
import soot.RefType;
import soot.jimple.Jimple;

public class AddTrap {
    private AddTrap() {
    }


    /* TODO: Right now this only does one exception. It cannot be called
     * multiple times.
     * If I wanted to enable catching several exceptions, the way to do it
     * would be to have each handler have an IdentityStmt succeeded by a goto
     * to the one error function call, which would would be followed by the
     * return; */
    public static void addCatch(Body body, String exceptionName) {

	Jimple j = Jimple.v();

	int numLocals = body.getLocalCount();
	RefType errorType = RefType.v(exceptionName);
	Local arg = j.newLocal("ex" + (numLocals + 1), errorType);
	body.getLocals().add(arg);

	Unit id = j.newIdentityStmt(arg, j.newCaughtExceptionRef());

	//Maybe pass this in
	SootMethod err =
	    Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	InvokeExpr inv = j.newStaticInvokeExpr(err.makeRef());
	Unit errCall = j.newInvokeStmt(inv);

	Stmt ret = defaultReturn(body.getMethod().getReturnType());

	SootClass exc = errorType.getSootClass();
	Unit first = body.getUnits().getFirst();
	Unit last = body.getUnits().getLast();

	body.getUnits().add(id);
	body.getUnits().add(errCall);
	body.getUnits().add(ret);

	Trap t = j.newTrap(exc, first, last, id);
	body.getTraps().add(t);
    }

    public static Stmt defaultReturn(Type t) {
	if(t instanceof soot.VoidType) {
	    return Jimple.v().newReturnVoidStmt();
	}
	Value ret = null;
	if(t instanceof soot.RefLikeType) {
	    ret = soot.jimple.NullConstant.v();
	}
	if(t instanceof soot.IntegerType) {
	    ret = soot.jimple.IntConstant.v(0);
	}
	if(t instanceof soot.LongType) {
	    ret = soot.jimple.LongConstant.v(0);
	}
	if(t instanceof soot.DoubleType) {
	    ret = soot.jimple.DoubleConstant.v(0.0);
	}
	if(t instanceof soot.FloatType) {
	    ret = soot.jimple.FloatConstant.v(0.0f);
	}
	return Jimple.v().newReturnStmt(ret);
    }

}
