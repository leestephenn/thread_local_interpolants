/** Copyright (c) Stephen N. Lee*/

import com.microsoft.z3.Context;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import soot.Unit;
import soot.Local;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class DAGEdge extends RefinementEdge {
    private static int count = 0;

    private final BoolExpr rel;
    private final DAGVertex tail;
    private final DAGVertex head;
    //public final int thread;
    //public final int ID;

    private BoolExpr forwardInterpolantPair = null;
    private HashMap<soot.Local, Expr> activeSharedVars;
    private HashMap<Local, Expr> activeThreadVars;
    private ArrayExpr arrayCells = null;

    private Z3Converter cvtr;// = new Z3Converter(this);

    public DAGEdge(DAGVertex tail, Unit relation, DAGVertex head, int thread,
	    boolean isExceptional) {
	super(thread);
        this.tail = tail;
        this.head = head;
        //this.thread = thread;
        activeSharedVars = new HashMap<Local, Expr>(tail.getActiveSharedVars());
        Map<Local, Expr> tMap = tail.getActiveThreadVars().get(this.thread);
        activeThreadVars = new HashMap<Local, Expr>(tMap);

        //ID = count++;
	if(isExceptional) {
	    rel = exceptionGuard(relation);
	} else {
	    rel = sootToZ3(relation);
	}
    }


    public DAGVertex getTail() {
        return tail;
    }

    public DAGVertex getHead() {
        return head;
    }

    public BoolExpr transitionFormula() {
        return rel;
    }

    /* This should only be called once since there should only one sucessor
    public void conjoinForwardPath(BoolExpr b, InterpolationContext con) {
        forwardInterpolantPair = con.mkAnd(b, rel);
        //System.out.println("Looping in Disjoining interpolant to vertex?");
        tail.disjoinInterpolantPair(forwardInterpolantPair, con);
    } */

    public BoolExpr forwardInterpolantGen(Context con) {
        if (forwardInterpolantPair == null) {
            forwardInterpolantPair =
                con.mkAnd(rel, head.makeNewConstEqs(this),
                        head.forwardInterpolantGen(con));
        }
        return forwardInterpolantPair;
    }

    public BoolExpr getContext() {
	return forwardInterpolantPair;
    }

    public BoolExpr getForwardPath() {
        return forwardInterpolantPair;
    }

    public BoolExpr getBackwardPath() {
	final Context con = UnwindingHyperDAG.CONTEXT;
	return con.mkAnd(tail.backwardInterpolant(), rel);
    }


    public BoolExpr sootToZ3(Unit unit) {
        //System.out.println("Tail: " + tail.getInEdges().size() + " " +
                //tail.getActiveSharedVars());
        //System.out.println("Converting a unit to Z3 " + ID);
        try {
            BoolExpr result = cvtr.ofUnit(unit);
            for(Map.Entry<Local, Expr> ey : cvtr.getNewMappings().entrySet()) {
                if(UnwindingHyperDAG.isShared(ey.getKey())) {
                    activeSharedVars.put(ey.getKey(), ey.getValue());
                } else {
                    activeThreadVars.put(ey.getKey(), ey.getValue());
                }
            }

            ArrayExpr ac = cvtr.getNewArrayCells();
            if(cvtr.getNewArrayCells() == null) {
                arrayCells = tail.getActiveArrayCells();
            } else {
                arrayCells = ac;
            }
            //System.out.println("Edge " + ID + " active vars: " + activeSharedVars);
            return result;
        } catch (RuntimeException ex) {
            System.out.println("Unit: " + unit);
            throw ex;
        }
    }

    public BoolExpr exceptionGuard(Unit u) {
	return cvtr.exceptionCondition(u);
    }

    public Map<soot.Local, Expr> getActiveSharedVars() {
        return java.util.Collections.unmodifiableMap(activeSharedVars);
    }

    public Map<soot.Local, Expr> getActiveThreadVars() {
        return java.util.Collections.unmodifiableMap(activeThreadVars);
    }

    public ArrayExpr getActiveArrayCells() {
        return arrayCells;
    }

    /* TODO */
    private static com.microsoft.z3.IntExpr fake = null;

    public static BoolExpr sootToZ3_old(Unit unit) {
        Context con = UnwindingHyperDAG.CONTEXT;
        if( fake == null)  {
            fake = con.mkIntConst("xxx");
        }
        return con.mkEq(fake, con.mkInt(count++));
    }
}
