/** Copyright (c) Stephen N. Lee*/

import java.util.ArrayList;

public interface DAGTester {
    public abstract void processUnwinding(UnwindingVertex v);

    public abstract void processDAGqueue(ArrayList<DAGVertex> queue);
}
