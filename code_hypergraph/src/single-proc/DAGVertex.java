/** Copyright (c) Stephen N. Lee*/

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.enumerations.Z3_lbool;
import com.microsoft.z3.Expr;
import com.microsoft.z3.ArrayExpr;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Model;

import soot.Local;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.HashSet;

public class DAGVertex {

    private BoolExpr interpolant = null;
    private ArrayList<DAGEdge> inEdges = new ArrayList<DAGEdge>();
    private ArrayList<DAGEdge> outEdges = new ArrayList<DAGEdge>();
    private final UnwindingVertex uVertex;
    private BoolExpr forwardInterpolantPair = null;
    private HashMap<Local, Expr> activeSharedVars = new HashMap<Local, Expr>();
    private ArrayList<HashMap<Local, Expr>> activeThreadVars =
	new ArrayList<HashMap<Local, Expr>>();
    private ArrayExpr arrayCells = null;
    private boolean newArrayCells = false;
    private ArrayList<Local> newConsts = new ArrayList<Local>();
    private ArrayList<ArrayList<Local>> newThreadConsts =
	new ArrayList<ArrayList<Local>>();

    //private int count = 0;

    public DAGVertex(UnwindingVertex v) {
        uVertex = v;
	activeThreadVars = new ArrayList<HashMap<Local, Expr>>(v.getNumThreads());
	for (int i = 0; i < v.getNumThreads(); i++) {
	    activeThreadVars.add(new HashMap<Local, Expr>());
	    newThreadConsts.add(new ArrayList<Local>());
	}
    }

    public UnwindingVertex getVertex() {
        return uVertex;
    }

    //Returns null if not
    public BoolExpr getInterpolant() {
	return interpolant;
    }

    public void addInEdge(DAGEdge p) {
        inEdges.add(p);
	//System.out.println("Adding in-edge: " + p.transitionFormula());

	InterpolationContext con = UnwindingHyperDAG.CONTEXT;

	// This requires the filling with empty maps in the constructor
	for (int t = 0; t < activeThreadVars.size(); t++) {
	    HashMap<Local, Expr> lMap = activeThreadVars.get(t);
	    ArrayList<Local> nConsts = newThreadConsts.get(t);

	    Map<Local, Expr> etMap = p.getTail().getActiveThreadVars().get(t);
	    if (t == p.thread) {
		etMap = p.getActiveThreadVars();
	    }

	    for(Entry<Local, Expr> ey : etMap.entrySet()) {
		Expr curConst = lMap.get(ey.getKey());
		if(curConst == null ) {
		    lMap.put(ey.getKey(), ey.getValue());
		} else if(!ey.getValue().equals(curConst)) {
		    if (!nConsts.contains(ey.getKey())) {
			nConsts.add(ey.getKey());

			String newName = ey.getKey().getName() +
			    Z3Converter.THREAD_PRE + p.thread + toString();
			Expr newC = con.mkConst(newName, ey.getValue().getSort());
			lMap.put(ey.getKey(), newC);
		    }
		}
	    }
	}

	Map<Local, Expr> eMap = p.getActiveSharedVars();
	//System.out.println("Edge " + p.ID + " active vars: " + eMap);
	for(Entry<Local, Expr> ey : eMap.entrySet()) {
	    Expr curConst = activeSharedVars.get(ey.getKey());
	    if(curConst == null ) {
		activeSharedVars.put(ey.getKey(), ey.getValue());
	    } else if(!ey.getValue().equals(curConst)) {
		if (!newConsts.contains(ey.getKey())) {
		    newConsts.add(ey.getKey());

		    String newName = ey.getKey().getName() + toString();
			//uVertex.suffixLengths();
		    Expr newC = con.mkConst(newName, ey.getValue().getSort());
		    activeSharedVars.put(ey.getKey(), newC);
		}
	    }
	}

	ArrayExpr otherArray = p.getActiveArrayCells();
	if(arrayCells == null) {
	    arrayCells = otherArray;
	} else {//Technically the else is uneccesary
	    if(!(arrayCells.equals(otherArray))) {
		newArrayCells = true;
		String newName = "ArrayCells" + toString();
		ArrayExpr na = con.mkArrayConst(newName,
			Z3Converter.arrayCellSort, Z3Converter.primitiveArraySort);
		arrayCells = na;
	    }
	}

	//System.out.println("Active vars: " + activeSharedVars);
    }

    public void addOutEdge(DAGEdge e) {
	outEdges.add(e);
    }

    /* Should only be called after all predecessors have been added and after all
     * predecessors have an interpolant.*/
    public static int callCount = 0;
    public void findInterpolant(InterpolationContext con) {
	//System.out.println("Finding interpolant at " + this + " with " +
	       	//needsInterpolant() + " " + callCount++);
	if(inEdges.size() == 0) {
	    if (interpolant == null) {
		interpolant = con.mkTrue();
	    }
	} else if (interpolant == null) {
	    BoolExpr itpMinus = backwardInterpolant();
	    itpMinus = con.MkInterpolant(itpMinus);
	    BoolExpr itpPlus = forwardInterpolantGen(con);
	    BoolExpr unsat = con.mkAnd(itpMinus, itpPlus);
	    //System.out.println("Before first interpolant computation.");
            /* Maybe have the result of mkParams be a constant somewhere */
	    InterpolationContext.ComputeInterpolantResult o =
		con.ComputeInterpolant(unsat, con.mkParams());

            /* This if IS necessary: it is the point in McMillan's algorithm
             * (specifically, REFINE) where the program aborts if the program
             * is unsafe. */
	    if (o.status == Z3_lbool.Z3_L_FALSE) {
		/* The following if may not be necessary -- it is possible
		 * (but undocumented) that if the formula is FALSE, and with one
                 * interpolant in the formula, there will always be an interp
                 * array with 1 element. */
		if(o.interp != null && o.interp.length == 1) {
		    //System.out.println("Found interpolant for " + this);
		    interpolant = o.interp[0];
		} else {
                    throw new UnsupportedOperationException(
                            "No interpolant was formed from the refutation " +
                            "or too many: " + o.interp);
                }
	    } else {
		//System.out.println(unsat);
		//*
		MakeGraph graph = new MakeGraph(outEdges.get(0).getTail());
		graph.writeToFile("outputSatisfiable.dot",
			MakeGraph.SUFFIX_LENGTH_NAMES);
		//System.out.println(forwardInterpolantPair);
		Solver s = con.mkSolver();
		s.add(forwardInterpolantPair);
		System.out.println(s.check());
		Model model = s.getModel();
		System.out.println(s.getModel());

		DAGVertex current = this;
		while(!current.outEdges.isEmpty()) {
		    for (DAGEdge e : current.outEdges) {
			BoolExpr b = e.getForwardPath();
			Expr feas = model.evaluate(b, false);
			if(feas.isTrue()) {
			    System.out.println(e.transitionFormula() + " " +
				   //e.getHead().makeNewConstEqs(e) + " " +
				    e.getHead());
			    current = e.getHead();
			    break;
			}
		    }
		}

		// */
		throw new ErrorReachableException("Interpolant pair was "
			+ o.status + " at a DAG vertex for " + uVertex);

	    }
        }
    }

    public BoolExpr makeNewConstEqs(DAGEdge e) {
	final Context con = UnwindingHyperDAG.CONTEXT;

	BoolExpr[] sharedEq = new BoolExpr[newConsts.size()];

	Map<Local, Expr> vMap = activeSharedVars;
	Map<Local, Expr> eMap = e.getActiveSharedVars();
	for(int j = 0; j < sharedEq.length; j++) {
	    Local l = newConsts.get(j);
	    sharedEq[j] = con.mkEq(vMap.get(l), eMap.get(l));
	}

	BoolExpr result = con.mkAnd(sharedEq);

	for(int t = 0; t < newThreadConsts.size(); t++) {
	    ArrayList<Local> nConsts = newThreadConsts.get(t);
	    if(!nConsts.isEmpty()) {
		vMap = activeThreadVars.get(t);
		eMap = e.getTail().getActiveThreadVars().get(t);
		if (t == e.thread) {
		    eMap = e.getActiveThreadVars();
		}

		BoolExpr[] threadEq = new BoolExpr[nConsts.size()];
		for(int i = 0; i < threadEq.length; i++) {
		    Local l = nConsts.get(i);
		    threadEq[i] = con.mkEq(vMap.get(l), eMap.get(l));
		}
		BoolExpr junct = con.mkAnd(threadEq);
		result = con.mkAnd(result, junct);
	    }
	}

	ArrayExpr otherArray = e.getActiveArrayCells();
	if(arrayCells == null) {
	    arrayCells = otherArray;
	} else if(!otherArray.equals(arrayCells)) {
	    BoolExpr eq = con.mkEq(otherArray, arrayCells);
	    result = con.mkAnd(result, eq);
	}

	return result;
    }

    /* Node the two exit points */
    public BoolExpr backwardInterpolant() {
	if(interpolant != null) {
	    return interpolant;
	}

	final Context con = UnwindingHyperDAG.CONTEXT;
	BoolExpr[] eTransition = new BoolExpr[inEdges.size()];
	for(int i = 0; i < eTransition.length; i++) {
	    DAGEdge e = inEdges.get(i);
	    eTransition[i] = e.getBackwardPath();

	    BoolExpr[] mapEq = new BoolExpr[newConsts.size()];


	    if(!newConsts.isEmpty()) {
		BoolExpr tmp = makeNewConstEqs(e);
		eTransition[i] = con.mkAnd(tmp, eTransition[i]);
	    }
	}
	return con.mkOr(eTransition);
    }

    public BoolExpr forwardInterpolantGen(Context con) {

	if(forwardInterpolantPair == null) {
	    if(outEdges.isEmpty()) {
		/* This should be true iff every threadPath is empty, according
		 * to Nested Interpolants paper */
		//System.out.println("In empty forward of vertex " + this);
		forwardInterpolantPair = con.mkTrue();
	    } else {
		BoolExpr[] expr = new BoolExpr[outEdges.size()];
		for(int i = 0; i < expr.length; i++) {
		    DAGEdge e = outEdges.get(i);
		    expr[i] = e.forwardInterpolantGen(con);
		}
		forwardInterpolantPair = con.mkOr(expr);
	    }
	}
	return forwardInterpolantPair;
    }

    public java.util.List<DAGEdge> getInEdges() {
	return Collections.unmodifiableList(inEdges);
    }

    public java.util.List<DAGEdge> getOutEdges() {
	return Collections.unmodifiableList(outEdges);
    }

    /* TODO: two different DAGVertexes can have the same UnwindingVertex */
    public boolean equals(Object other) {
	if(!(other instanceof DAGVertex)) {
	    return false;
	} else {
	    return this.uVertex.equals(((DAGVertex) other).uVertex);
	}
    }

    public int hashCode() {
	return 29 * this.uVertex.hashCode();
    }

    // TODO this does not handle unused variables yet -- but they should always
    // be in lValues anyway
    public Expr getVariable(Local l, int thread) {
	if (UnwindingHyperDAG.isShared(l)) {
	    return activeSharedVars.get(l);
	} else {
	    HashMap<Local, Expr> map = getActiveThreadVars().get(thread);
	    return map.get(l);
	}
    }

    public Map<Local, Expr> getActiveSharedVars() {
	return Collections.unmodifiableMap(activeSharedVars);
    }

    //In other methods I make the return unmodifiable.
    //I would need to do two levels of that here, so I omit it.
    public List<HashMap<Local, Expr>> getActiveThreadVars() {
	int iEmpty = -1;
	if(inEdges.isEmpty()) {
	    return activeThreadVars;
	} else {
	    //The following loop finds the first index of an empty map
	    // (similar to indexOf)
	    for(int i = 0; i < activeThreadVars.size(); i++) {
		if (activeThreadVars.get(i).isEmpty()) {
		    iEmpty = i;
		    break;
		}
	    }
	    while(iEmpty >= 0) {
		// This assumes that all predecessor vertices have the same
		// thread mappings if there is no edge for that thread.
		HashMap<Local, Expr> away = inEdges.get(0).getTail().
		    getActiveThreadVars().get(iEmpty);
		activeThreadVars.set(iEmpty, away);

		iEmpty = activeThreadVars.indexOf(null);
	    }

	    return activeThreadVars;
	}
    }

    public List<Local> getNewVars() {
	return Collections.unmodifiableList(newConsts);
    }

    public boolean needsInterpolant() {
	//The following is simple but less efficient -- is empty is faster
	//Maybe change control flow to do that one first somehow?
	for(int i = 0; i < uVertex.getNumThreads(); i++) {
	    if(uVertex.getHeadUnit(i).branches()) {
		return true;
	    }
	}

	for (int i = 0; i < newThreadConsts.size(); i++) {
	    if(!newThreadConsts.get(i).isEmpty()) {
		return true;
	    }
	}
	return newArrayCells || !newConsts.isEmpty() || inEdges.isEmpty();
    }

    public ArrayExpr getActiveArrayCells() {
	return arrayCells;
    }

    public String toString() {
	return uVertex.suffixLengths().toString();
    }
}
