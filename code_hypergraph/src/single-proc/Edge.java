/** Copyright (c) Stephen N. Lee*/

import com.microsoft.z3.Context;
import com.microsoft.z3.BoolExpr;
import soot.Unit;

public class Edge {
    private final Unit rel;
    private final UnwindingVertex head;
    //private final UnwindingVertex tail;
    public final int thread;
    private final boolean isExceptional;

    public Edge(Unit u, UnwindingVertex head, int thread, boolean isE) {
	this.head = head;
	//this.tail = tail;
	rel = u;
	this.thread = thread;
	this.isExceptional = isE;

	//TODO for testing

	//Context c = TestForwardInterpolant.CONTEXT;
	//transitionRelation = c.mkEq(c.mkIntConst("x"), c.mkInt(u.hashCode() % 64));
    }

    /* public BoolExpr forwardInterpolantGen(Context con) {
	return con.mkAnd(transitionRelation, head.forwardInterpolantGen(con));
    }
    */


    public UnwindingVertex getHead() {
	return head;
    }

    /* public UnwindingVertex getTail() {
	return tail;
    } */

    /* This  really is not a transition formula. */
    public Unit transitionFormula() {
	return rel;
    }

    public boolean isExceptional() {
	return isExceptional;
    }
}
