/** Copyright (c) Stephen N. Lee*/

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Scene;
import soot.SootMethod;
import soot.Local;
//import soot.SootClass;
//import soot.jimple.toolkits.callgraph.CallGraph;
//import soot.jimple.toolkits.callgraph.ReachableMethods;
//import soot.jimple.toolkits.callgraph.Edge;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.jimple.InvokeStmt;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;

/* The following currently kind-of blurs the distinction between test and real class. */
public class Main extends BodyTransformer {
    private SootMethod errorIndic;// =

    private final String[] methodsToAnalyze;
    private final String analysis;

    /* The simple names of the methods to analyze ( */
    private Main(String analysis, String[] methods) {
	methodsToAnalyze = methods;
	if(analysis == null) {
	    this.analysis = null;
	} else {
	    switch (analysis) {
		case "-null":
		    this.analysis = "java.lang.NullPointerException";
		    break;
		case "-div":
		    this.analysis = "java.lang.ArithmeticException";
		    break;
		case "-index":
		    this.analysis = "java.lang.ArrayIndexOutOfBoundsException";
		    break;
		default:
		    this.analysis = null;
	    }
	}

    }


    /* Note the first exit, after determining whether the method should be
     * analyzed. */
    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	/* Check if the user wanted to analyze this method */
	boolean shouldAnalyze = false;
	for(String name : methodsToAnalyze) {
	    if(body.getMethod().getName().equals(name)) {
		shouldAnalyze = true;
	    }
	}
	if(!shouldAnalyze) {
	    return;
	}

	System.out.println("===========Transforming===========");

	errorIndic =
	    Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");

	/* if we check for multiple, this will be an isEmpty check on the list. */
	if(analysis != null) {
	    AddTrap.addCatch(body, analysis);
	}

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	try {
	    Unit error = findErrorNode(g);
	    UnwindingHyperDAG uhdag = new UnwindingHyperDAG(g, error, 2);
	    try {
		UnwindingVertex root = uhdag.unwind();
		//The following should have value false
		System.out.println("Final interpolant: " + root.getInterpolant());
	    } catch (ErrorReachableException ex) {
		ex.printStackTrace();
	    }

	    System.out.println("===========Done Transforming===========");
	} catch (IllegalArgumentException ex) {
	    System.out.println(ex.getMessage());
	}
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }


    public static void main(String[] args) {

	ArrayList<String> als = new ArrayList<String>();
	//This assumes that all options are before all method names
	for(int j = 0; j < args.length; j++) {
	    String e = args[j];
	    if(e.startsWith("-")) {
		als.add(e);
	    } else {
		break;
	    }
	}
	String analysis = als.isEmpty() ? null : als.get(0);
	int i = als.size();

	String[] methods = Arrays.copyOfRange(args, i, args.length);

	Options.v().set_output_format(Options.output_format_jimple);
	Options.v().set_omit_excepting_unit_edges(false);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new Main(analysis, methods)));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "NoDivideByZeroTest"};
	soot.Main.main(sootArgs);
    }
}
