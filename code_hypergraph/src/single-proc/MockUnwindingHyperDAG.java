/** Copyright (c) Stephen N. Lee*/

import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;

public class MockUnwindingHyperDAG extends UnwindingHyperDAG {
    private DAGTester tester;

    public MockUnwindingHyperDAG(ExceptionalUnitGraph g, Unit error, int numThreads, DAGTester d) {
	super(g, error, numThreads);
	tester = d;
    }

    /* v is an initial vertex in some path or procedure */
    public void interpolation(UnwindingVertex v) {
	if(v == null) {
	    throw new NullPointerException("Initial vertex in the unwinding was null");
	}
	tester.processUnwinding(v);

	/*
	ArrayList<DAGVertex> topo = buildDAGqueue(v);
	tester.processDAGqueue(topo); */
	ArrayList<DAGVertex> topo = buildTopoOrder(v);
	tester.processDAGqueue(topo);


	/* int size = 1;
	for (Integer i : v.suffixLengths()) {
	    size = size * (i.intValue() + 1);
	}
	ArrayList<DAGVertex> topo = new ArrayList<DAGVertex>(size);
	buildReverseTopoOrder(v, topo);
	java.util.Collections.reverse(topo);

	tester.processDAGqueue(topo); */
    }
}
