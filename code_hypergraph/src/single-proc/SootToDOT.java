/** Copyright (c) Stephen N. Lee*/

import java.util.Map;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.jimple.InvokeStmt;
import soot.SootMethod;
import soot.Scene;

import soot.Transform;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;

public class SootToDOT extends BodyTransformer implements DAGTester {
    private int count = 10;
    private String methodName;

    private SootToDOT() {
    }

    private ArrayList<DAGVertex> enteredD;
    private ArrayList<DAGVertex> exitedD;

    private boolean DFS(ArrayList<DAGVertex> queue) {
	enteredD = new ArrayList<DAGVertex>();
	exitedD = new ArrayList<DAGVertex>();

	boolean result = false;

	for(int i = queue.size() - 1; i >= 0; i--) {
	    DAGVertex dv = queue.get(i);
	    if (!enteredD.contains(dv)) {
		result = DFSVisit(dv) || result;
	    }
	}

	return result;
    }

    private boolean DFSVisit(DAGVertex dv) {
	enteredD.add(dv);
	boolean result = false;
	for(DAGEdge e : dv.getOutEdges()) {
	    DAGVertex next = e.getHead();
	    if(!enteredD.contains(next)) {
		DFSVisit(next);
	    } else {
		if(enteredD.contains(next) && !exitedD.contains(next)) {
		    System.out.println("Found back edge from " +
			    dv + " to " + next);
		    result = true;
		}
	    }
	}
	exitedD.add(dv);
	return result;
    }

    public void processUnwinding(UnwindingVertex headNode) {
	//System.out.println("Reached initial node.");
    }

    public void processDAGqueue(ArrayList<DAGVertex> queue) {
	System.out.println("Queue size: " + queue.size());
	if(DFS(queue)) {
	    throw new RuntimeException();
	}
	MakeGraph mg = new MakeGraph(queue.get(0));
	System.out.println("Writing file number " + count);
	mg.writeToFile("graphs/output" + methodName + count + ".dot",
		MakeGraph.SUFFIX_LENGTH_NAMES);
	count++;
    }

    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	soot.SootMethod method = body.getMethod();//.getName();
	if(method.isStatic()) {
	    count = 0;
	    System.out.println("\n=====Transform Output " + method.getName() + "=====");
	    methodName = method.getName();

	    Unit error = findErrorNode(g);
	    UnwindingHyperDAG uhdag = new MockUnwindingHyperDAG(g, error, 2, this);
	    if(!uhdag.errorLocation().equals(error)) {
		System.out.println("Error location was not set correctly");
	    }

	    UnwindingVertex startUW = new UnwindingVertex(uhdag);
	    uhdag.unwind(startUW);
	    System.out.println("\n==========================");
	}
    }

    public Unit findErrorNode(ExceptionalUnitGraph g) {
	SootMethod errorIndic = Scene.v().getMethod("<ThreadItpsAnalysisError: void analysisError()>");
	for(Unit u : g) {
	    if(u instanceof InvokeStmt) {
		//System.out.println(u);
		//System.out.println(u.getClass());
		InvokeStmt invoke = (InvokeStmt) u;
		SootMethod m = invoke.getInvokeExpr().getMethod();
		//System.out.println("Method: " + m);
		if (errorIndic.equals(m)) {
		    //System.out.println("Equals error: " + m);
		    return invoke;
		}
	    }
	}
	String n = g.getBody().getMethod().getName();
	throw new IllegalArgumentException("No error function indicated: " +
		"no call to analysisError() found in " + n);
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new SootToDOT()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "NoDivideByZeroTest",
	    /* "NoDivideByZeroTest"*/};
	soot.Main.main(sootArgs);
    }
}
