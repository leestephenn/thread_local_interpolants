/** Copyright (c) Stephen N. Lee*/

import java.util.Map;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.Transform;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;
import soot.Local;

import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;

/* TODO This does not test mutual exclusion sections where vertex does not have an incoming edge for every thread. */
public class TestBackwardInterpolant extends BodyTransformer implements DAGTester {
    //public static final InterpolationContext CONTEXT =

    private boolean contains(Expr expr, Expr subExpr) {
        for(Expr arg : expr.getArgs()) {
            if(arg.equals(subExpr)) {
                return true;
            } else {
                if(contains(arg, subExpr)) {
                    return true;
                } // else continue;
            }
        }
        return false;
    }

    private boolean hasConstEq(Expr expr, String name) {
        for(Expr arg : expr.getArgs()) {
            if(arg.isEq()) {
                boolean result = true;
                for(Expr subArg : arg.getArgs()) {
                    if (subArg.isConst()) {
                        //System.out.println("Subarg: " + subArg);
                        //System.out.println(subArg.toString().contains(name));
                        if(!subArg.toString().contains(name)) {
                            //System.out.println("returning true");
                            result = false;
                        }
                    } else {
                        result = false;
                    }
                }
                if(result) {
                    return true;
                }
            } else {
                if(hasConstEq(arg, name)) {
                    return true;
                } // else continue;
            }
        }
        return false;
    }

    public void processUnwinding(UnwindingVertex v) {
    }

    protected void internalTransform(Body body,
            String phaseName, Map<String, String> optionsMap) {

        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
        if(body.getMethod().isStatic()) {
            System.out.println("\n=====Transform Output " +
                    body.getMethod().getName() + "=====");

            Unit error = g.getTails().get(0);
            UnwindingHyperDAG uhdag = new MockUnwindingHyperDAG(g, error, 2, this);
            if(!uhdag.errorLocation().equals(error)) {
                System.out.println("Error location was not set correctly");
            }

            UnwindingVertex startUW = new UnwindingVertex(uhdag);
            uhdag.unwind(startUW);
        }
        System.out.println("\n==========================");
    }

    public void processDAGqueue(ArrayList<DAGVertex> queue) {

        final boolean verbose = false;

        System.out.println("Queue size: " + queue.size());
        System.out.println("Shared: " + UnwindingHyperDAG.shared);

        //System.out.println("After buildDAGqueue");

        InterpolationContext con = Z3Converter.CONTEXT;
        for (int oneTimeI = 0; oneTimeI < queue.size(); oneTimeI++) {
            DAGVertex dv = queue.get(oneTimeI);
            BoolExpr bI = dv.backwardInterpolant();

            if(verbose) {
                System.out.println(dv.getVertex().suffixLengths() +
                        " -: " + bI);
                System.out.println("Vars: " + dv.getActiveSharedVars());
            }

            ArrayList<BoolExpr> eFormulae = new ArrayList<BoolExpr>();
            for(int i = 0; i < dv.getInEdges().size(); i++) {
                DAGEdge e = dv.getInEdges().get(i);
                eFormulae.add(e.transitionFormula());

                if(verbose) {
                    System.out.print("E" + i + ": ");
                    System.out.println(e.transitionFormula());
                }
            }

            if(!bI.isOr()) {
                System.out.println("the minus interpolant was not a disjunction.");
            }

            Expr[] args = bI.getArgs();
            for(Expr arg : args) {
                int containsEdge = -1;
                int eI = 0;
                for (; eI < eFormulae.size(); eI++) {
                    if(contains(arg, eFormulae.get(eI))) {
                        containsEdge = eI;
                        break;
                    }
                }
                if(containsEdge < 0 ) {
                    System.out.println("Subformula " + arg +
                            " did not contain an edge formula.");
                    for (Expr subArg : arg.getArgs()) {
                        System.out.println("Subarg: " + subArg);
                    }
                } else {
                    eFormulae.remove(containsEdge);
                }
            }
            List<Local> newVars = dv.getNewVars();
            for (Local l : newVars) {
                if(!UnwindingHyperDAG.isShared(l)) {
                    System.out.println("A thread-local variable had a reassignment in a vertex.");
                }
            }
            for(Local l : newVars) {
                for(Expr disjunct : bI.getArgs()) {
                    //System.out.println("Disjunct: " + disjunct);
		    //System.out.println("Checking hasConstEq");
                    if (!hasConstEq(disjunct, l.getName())) {
                        System.out.println("Interpolant did not have a equation in edge for new const of local " + l);
                    }
                }
            }
            if(verbose) System.out.println();
        }
    }

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new TestBackwardInterpolant()));
        String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "Absolute" };
	    //"BoolVarIf", "NoDivideByZeroTest"}; These have method calls
        soot.Main.main(sootArgs);

    }

}
