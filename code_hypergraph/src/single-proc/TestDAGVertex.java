/** Copyright (c) Stephen N. Lee*/

import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;

public class TestDAGVertex {

    public static void main(String[] args) {
	//The line below was changed to take arguments in the inner constructor after it
	//ceased development. It may no longer work. TODO
	UnwindingVertex uw = new UnwindingVertex(new UnwindingHyperDAG(null, null, 2));
	DAGVertex dv = new DAGVertex(uw);


	InterpolationContext con = new InterpolationContext();
	dv.findInterpolant(con);
	if (!dv.getInterpolant().isTrue()) {
	    System.out.println("The interpolant for a vertex with no predessors was " + dv.getInterpolant() + ", not true");
	}

	DAGVertex dv2 = new DAGVertex(uw);
	BoolExpr rel = con.mkBoolConst("a");
	rel = con.mkAnd(rel, con.mkNot(rel));
	DAGEdge de = new DAGEdge(dv, rel, dv2);
	dv2.addInEdge(de); dv2.addOutEdge(de);
	try {
	    dv2.findInterpolant(con);
	    if (dv.getInterpolant() == null) {
		System.out.println("The findInterpolant method did not work for one predecessor.");
	    }
	} catch (ErrorReachableException ex) {
	    ex.printStackTrace();
	} catch (UnsupportedOperationException ex) {
	    ex.printStackTrace();
	}
    }
}
