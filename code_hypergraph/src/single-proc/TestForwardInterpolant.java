/** Copyright (c) Stephen N. Lee*/

import java.util.Map;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.Transform;
import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;

import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Expr;

public class TestForwardInterpolant extends BodyTransformer implements DAGTester {
    public static final InterpolationContext CONTEXT =
        Z3Converter.CONTEXT;

    private ArrayList<UnwindingVertex> enteredU;
    private ArrayList<UnwindingVertex> exitedU;

    private TestForwardInterpolant() {
    }

    private void DFS(UnwindingVertex queue) {
        enteredU = new ArrayList<UnwindingVertex>();
        exitedU = new ArrayList<UnwindingVertex>();

        //for(UnwindingVertex uv : queue) {
        //   if (!enteredU.contains(uv)) {
        DFSVisit(queue);
        //  }
        //}
    }

    private void DFSVisit(UnwindingVertex uv) {
        enteredU.add(uv);
        for(Edge e : uv.getSuccessors()) {
            UnwindingVertex next = e.getHead();
            if(!enteredU.contains(next)) {
                DFSVisit(next);
            } else {
                if(enteredU.contains(next) && !exitedU.contains(next)) {
                    System.out.println("Found back edge from " +
                            uv + " to " + next);
                }
            }
        }
        exitedU.add(uv);
    }

    private ArrayList<DAGVertex> enteredD;
    private ArrayList<DAGVertex> exitedD;

    private void DFS(ArrayList<DAGVertex> queue) {
        enteredD = new ArrayList<DAGVertex>();
        exitedD = new ArrayList<DAGVertex>();

        for(int i = queue.size() - 1; i >= 0; i--) {
            DAGVertex dv = queue.get(i);
            if (!enteredD.contains(dv)) {
                DFSVisit(dv);
            }
        }
    }

    private void DFSVisit(DAGVertex dv) {
        enteredD.add(dv);
        for(DAGEdge e : dv.getInEdges()) {
            DAGVertex next = e.getTail();
            if(!enteredD.contains(next)) {
                DFSVisit(next);
            } else {
                if(enteredD.contains(next) && !exitedD.contains(next)) {
                    System.out.println("Found back edge from " +
                            dv + " to " + next);
                }
            }
        }
        exitedD.add(dv);
    }


    protected void internalTransform(Body body,
            String phaseName, Map<String, String> optionsMap) {

        ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
        soot.SootMethod methodName = body.getMethod();//.getName();
        if(methodName.isStatic()) {
            System.out.println("\n=====Transform Output " + methodName + "=====");

            Unit error = g.getTails().get(0);
            UnwindingHyperDAG uhdag = new MockUnwindingHyperDAG(g, error, 2, this);
            if(!uhdag.errorLocation().equals(error)) {
                System.out.println("Error location was not set correctly");
            }

            UnwindingVertex startUW = new UnwindingVertex(uhdag);
            uhdag.unwind(startUW);
            System.out.println("\n==========================");
        }
    }

    public void processUnwinding(UnwindingVertex headNode) {
        DFS(headNode);

        System.out.println("Before buildDAGqueue " + headNode);
        //if(4 > -2) {
        //throw new RuntimeException("End trace");
        //}
    }

    public void processDAGqueue(ArrayList<DAGVertex> queue) {

        System.out.println("Queue size: " + queue.size());

        //System.out.println("After buildDAGqueue");
        for(DAGVertex dv : queue) {
	    //System.out.println(dv);
	    //System.out.println(dv.getInEdges());
            if(dv.getInEdges().size() > 2) {
                System.out.println("DAGVertex " + dv.getVertex() +
                        " had " + dv.getInEdges().size() +
                        " predecessors.");
            }
        }

        DAGVertex sink = null;
        for(DAGVertex dv : queue) {
            if(dv.getOutEdges().isEmpty()) {
                //System.out.println("Empty outedges here");
                if(sink == null) {
                    sink = dv;
                } else {
                    System.out.println("Two vertices " + dv + " " + sink +
                            " had no outedges.");
                }
            }
        }

        DFS(queue);
        //System.out.println("No cycles");
        //DAGVertex start = queue.get(queue.size() - 1);

        InterpolationContext con = CONTEXT;
        //if(4 > -1) {
        //throw new RuntimeException("Abort");
        //}

        for(int i = queue.size() - 1; i >= 0; i-- ) {
            //System.out.println("Looping in TestForwardInterpolant " + i);
            //if(i == 1) {
            //throw new RuntimeException("Abort");
            //}

            DAGVertex init = queue.get(i);
            BoolExpr iPlus = init.forwardInterpolantGen(con);

            if (!iPlus.isOr()) {
                if (i == (queue.size() - 1)) {
                    if (!iPlus.isTrue()) {
                        System.out.println("Sink had forward " +
                                init.forwardInterpolantGen(con) +
                                ", not true");
                    }
                } else {
                    System.out.print( "Forward formula was not a disjunction");
                    System.out.println(" for vertex " + init.getVertex());
                }
            } else {
                Expr[] args = iPlus.getArgs();

                for(int j = 0; j < args.length; j++) {
                    if(!args[j].isAnd()) {
                        System.out.println("Operand " + j
                                + " was not a conjuction.");
                    }
                }
            }

            if(iPlus.getNumArgs() != init.getOutEdges().size()) {
                System.out.print("Formula had ");
                System.out.print(iPlus.getNumArgs());
                System.out.print(" operands, not ");
                System.out.println(init.getOutEdges().size());
            }

            for (DAGEdge e : init.getInEdges()) {
                if(!e.getHead().equals(init)) {
                    System.out.println("In-edge " + e.transitionFormula() +
                            " did not have head " + init);
                }

                BoolExpr ePlus = e.forwardInterpolantGen(con);
                if(!ePlus.isAnd()) {
                    System.out.println("Forward formula for edge " +
                            e.transitionFormula() + " was not a conjunction.");
                } else {
                    Expr[] args = ePlus.getArgs();
                    boolean is_in = false;
                    for (Expr expr : args) {
                        if(expr.equals(iPlus)) {
                            is_in = true;
                        }
                    }

                    if (! is_in) {
                        System.out.println(
                                "The head's interpolant partner " +
                                iPlus + " was not in the edge's interpolant partner " +
                                ePlus + " for edge " +
                                e.transitionFormula());
                        throw new RuntimeException("Abort2");
                    }
                }
            }

            //System.out.println("end " + i);
        }
    }

    public static void main(String[] args) {
        Options.v().set_output_format(Options.output_format_jimple);

        Pack jtp = PackManager.v().getPack("jtp");
        jtp.add(new Transform("jtp.test", new TestForwardInterpolant()));
        String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "Absolute",
	     /* "NoDivideByZeroTest"*/};
        soot.Main.main(sootArgs);
    }

}
