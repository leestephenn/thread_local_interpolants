/** Copyright (c) Stephen N. Lee*/

import soot.Transform;
import java.util.Map;
import java.util.LinkedList;
import soot.toolkits.graph.ExceptionalUnitGraph;

import soot.PackManager;
import soot.Pack;
import soot.options.Options;
import soot.BodyTransformer;
import soot.Body;
import soot.Transform;
import soot.Unit;

public class TestGraphExpand extends BodyTransformer {
    protected void internalTransform(Body body,
	    String phaseName, Map<String, String> optionsMap) {

	ExceptionalUnitGraph g = new ExceptionalUnitGraph(body);
	if(body.getMethod().getName().equals("main")) {
	    System.out.println("\n=====Transform Output=====");

	    Unit error = g.getTails().get(0);
	    UnwindingHyperDAG uhdag = new UnwindingHyperDAG(g, error, 2);
	    if(!uhdag.errorLocation().equals(error)) {
		System.out.println("Error location was not set correctly");
	    }

	    UnwindingVertex start = new UnwindingVertex(uhdag);
	    LinkedList<UnwindingVertex> ll = new LinkedList<UnwindingVertex>();
	    start.expand(g, ll);
	    for(UnwindingVertex v : ll) {
		boolean noSuccessor = true;
		for(Edge e : v.getSuccessors()) {
		    if (e.getHead().equals(start)) {
			noSuccessor = false;
		    }
		}
		if(noSuccessor) {
		    System.out.println("Successor was not set correctly");
		}
	    }

	    int numPreds = g.getUnexceptionalPredsOf(error).size();
	    if(ll.size() != uhdag.numThreads * numPreds) {
		//System.out.println("Queue size: " );
		System.out.println("Had " + uhdag.numThreads + " threads " +
			" but only " + ll.size() + " predecessors");
	    }

	    LinkedList<UnwindingVertex> l2 =
		new LinkedList<UnwindingVertex>();
	    int totMade = 0;

	    for(UnwindingVertex v : ll) {
		v.expand(g, l2);

		LinkedList<UnwindingVertex> pec =
		    new LinkedList<UnwindingVertex>();
		v.expand(g, pec);
		totMade = totMade + pec.size();
	    }

	    if(totMade == 0 || l2.size() == 0) {
		//Unlike usual, this is not necessarily an error --
		// it's just not what I intend to be testing
		System.out.println("Zero predecessors");
	    }

	    if(totMade == l2.size()) {
		System.out.println("Either duplication did not occur " +
			"or it was not handled properly.");
	    }

	    int totSuccessors = 0;
	    for(UnwindingVertex v : l2) {
		totSuccessors = totSuccessors + v.getSuccessors().size();
	    }

	    if(totMade != totSuccessors) {
		System.out.println("The successors " + totSuccessors +
			" were not equal to the total made " + totMade);
	    }


	    UnwindingVertex next = l2.get(0);
	    LinkedList<UnwindingVertex> fast =
		new LinkedList<UnwindingVertex>();
	    while (!next.expand(g, fast)) {
		next = fast.getLast();
	    }

	    java.util.List<Unit> heads = g.getHeads();
	    //System.out.println("heads size: " + heads.size());
	    for(Edge e : next.getSuccessors()) {
		if (! heads.contains(e.transitionFormula())) {
		    System.out.println("\"Initial\" nodes " + next +
			    " was not a head of the CFG.");
		}
	    }

	    System.out.println("\n==========================");
	}
    }

    public static void main(String[] args) {
	Options.v().set_output_format(Options.output_format_jimple);

	Pack jtp = PackManager.v().getPack("jtp");
	jtp.add(new Transform("jtp.test", new TestGraphExpand()));
	String[] sootArgs = {"-pp", "-cp", "../playWithSoot/", "HelloWorld"};
	soot.Main.main(sootArgs);

    }

}
