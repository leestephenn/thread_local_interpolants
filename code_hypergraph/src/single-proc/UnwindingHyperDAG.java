/** Copyright (c) Stephen N. Lee*/

import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.Unit;
import soot.jimple.ThrowStmt;
import soot.toolkits.graph.ExceptionalUnitGraph.ExceptionDest;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;

public class UnwindingHyperDAG {
    public static final InterpolationContext CONTEXT = new InterpolationContext();
    //public static boolean varsShared = true;//TODO

    public static HashMap<String, Boolean> shared = new HashMap<String, Boolean>();
    public static boolean isShared(soot.Local l) {
	/* Boolean result = shared.get(l.getName());
	if(result == null) {
	    shared.put(l.getName(), varsShared);
	    result = varsShared;
	    varsShared = !varsShared;
	    System.out.println(l + " is shared? " + result);
	}
	return result; */
	return false;
	//return true;
    }
}
    //private ExceptionalUnitGraph cfg;//CFG that this hyper DAG is generated from
    //private Unit errorUnit;
    //public final int numThreads;
    ///* ctx could (if we wanted to) be re-initialized every time we find a DAG */
    //private InterpolationContext ctx = CONTEXT;
    //public static boolean varsShared = true;//TODO
    //private int DOTcount = 0;

    //public UnwindingHyperDAG(ExceptionalUnitGraph g, Unit error, int numThreads) {
	//cfg = g;
	//errorUnit = error;
	//this.numThreads = numThreads;
    //}

    //public UnwindingVertex unwind() {
	//UnwindingVertex errorVertex = new UnwindingVertex(this);
	//unwind(errorVertex);
	//return errorVertex;//the unwind method modifies v
    //}
    //
    ///* vertex is the error location (where we are tracing backwards from */
    //public void unwind(UnwindingVertex vertex) {
	////Using the (doubly) linked list as a queue
	//LinkedList<UnwindingVertex> preds = new LinkedList<UnwindingVertex>();
	//preds.offer(vertex);
	//
	//while(!preds.isEmpty()) {
	    //UnwindingVertex v = preds.removeFirst();
	    //boolean initial = v.expand(cfg, preds);
	    //if(initial) {
		////DAGVertex.callCount = 0;
		//interpolation(v);
	    //}
	//}
    //}

    ///* The v here will typically be the same passed to interpolation,
     //* an initial node in some path or procedure.
     //* The return will be a list of nodes in breadth-first order, starting from
     //* the v node. */
    //public ArrayList<DAGVertex> buildDAGqueue(UnwindingVertex v) {
	////System.out.println("Beginning of buildDAGqueue");
	//ArrayList<DAGVertex> queue = new ArrayList<DAGVertex>();
	//queue.add(new DAGVertex(v));

	//for(int i = 0; i < queue.size(); i++) {
	    ////System.out.println("Looping in buildDAGqueue 1? " + i + " " + queue.size());
	    //DAGVertex t = queue.get(i);//Do not actually remove the elements

            //for(Edge e : t.getVertex().getSuccessors()) {
                //DAGVertex nV = new DAGVertex(e.getHead());

		//int last_index = queue.lastIndexOf(nV);
		//if(last_index > 0) {
		    //nV = queue.get(last_index);
		//} else {
		    //queue.add(nV);
		//}

		////DAGEdge nE = new DAGEdge(t, e.transitionFormula(), nV, e.thread,
			//e.isExceptional());
		//t.addOutEdge(nE);
		//nV.addInEdge(nE);
            //}
	    ////System.out.println();
        //}

	//return queue;
    //}

    ///* To get topological order, reverse the out array (as the "Reverse" in the
     //* method name suggests. */
    ////This will be called multiple times in an interprocedural graph
    //private void buildReverseTopoOrder(UnwindingVertex v,
	    //ArrayList<UnwindingVertex> out) {

	//for(Edge e : v.getSuccessors()) {

	    //final UnwindingVertex head = e.getHead();
	    //if (! out.contains(head)) {
		//buildReverseTopoOrder(head, out);
	    //}
	//}

	//out.add(v);
    //}

    ///* This will eventually take multiple initial nodes */
    //public ArrayList<DAGVertex> buildTopoOrder(UnwindingVertex uv) {

	//int size = 1;
	//for (Integer i : uv.suffixLengths()) {
	    //size = size * (i.intValue() + 1);
	//}
	//ArrayList<UnwindingVertex> vvv = new ArrayList<UnwindingVertex>(size);
	//buildReverseTopoOrder(uv, vvv);
	//java.util.Collections.reverse(vvv);

	//DAGVertex[] result = new DAGVertex[vvv.size()];

	//for (int i = 0; i < vvv.size(); i++) {
	    //UnwindingVertex t = vvv.get(i);

	    //DAGVertex v = result[i];
	    //if(v == null) {
		//v = new DAGVertex(t);
		//result[i] = v;
	    //}
	    //
	    //for(Edge e : t.getSuccessors()) {
		//final UnwindingVertex head = e.getHead();
		////Could use subList to possibly make this more efficient
		//int index = vvv.indexOf(head);
		//if(result[index] == null) {
		    //result[index] = new DAGVertex(head);
		//}

		////DAGEdge nE = new DAGEdge(v, e.transitionFormula(), result[index],
		       	//e.thread, e.isExceptional());
		//v.addOutEdge(nE);
		//result[index].addInEdge(nE);
	    //}
	//}

	////if it matters, can just use Arrays.asList
	//return new ArrayList<DAGVertex>(Arrays.asList(result));
    //}


    ///* v is an initial vertex in some path or procedure */
    ///* For interprocedural, the argument will actually be multiple v's */
    //public void interpolation(UnwindingVertex v) {
	////This is not actually a queue -- we never actually remove elements
	////System.out.println("Call to interpolation on vertex " + v);
	//ArrayList<DAGVertex> queue = buildDAGqueue(v);
	//System.out.println("Queue size: " + queue.size());

	//DAGVertex first = queue.get(0);
	//if(first.getInEdges().isEmpty()) {
	    ///* This should be true for the error node, according to the
	     //* Nested Interpolants paper */
	    //first.forwardInterpolantGen(ctx);
	//} else {
	    //throw new IllegalStateException("The first DAG vertex was not at" +
		    //" a source node.");
	//}

        //for (DAGVertex f : queue) {
	    //if(f.needsInterpolant()) {
		//f.findInterpolant(ctx);
	    //}
        //}

	//MakeGraph graph = new MakeGraph(queue.get(0));
	////TODO: maybe add the method name to the file name (get from the error node)
	////Or add the path lengths
	//graph.writeToFile("outputInterpolant" + (DOTcount++) + ".dot",
		//MakeGraph.INTERPOLANT_NAMES);

        //for (DAGVertex f : queue) {
	    //if(f.needsInterpolant()) {
		//f.getVertex().disjoinInterpolant(ctx, f.getInterpolant());
	    //}
        //}
	//System.out.println("Falsified one interleaving of paths, starting at " +
		//v);
    //}

    ///* TODO? */
    //public Unit errorLocation() {
	//return errorUnit;
    //}

    //public static HashMap<String, Boolean> shared = new HashMap<String, Boolean>();
    //public static boolean isShared(soot.Local l) {
	///* Boolean result = shared.get(l.getName());
	//if(result == null) {
	    //shared.put(l.getName(), varsShared);
	    //result = varsShared;
	    //varsShared = !varsShared;
	    //System.out.println(l + " is shared? " + result);
	//}
	//return result; */
	//return varsShared;
	////return true;
    //}
//}
