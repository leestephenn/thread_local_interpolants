/** Copyright (c) Stephen N. Lee*/

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.microsoft.z3.Context;
import com.microsoft.z3.InterpolationContext;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.enumerations.Z3_lbool;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.ExceptionalUnitGraph.ExceptionDest;
//import java.util.Set;
import soot.Unit;
import soot.jimple.ThrowStmt;

public class UnwindingVertex {

    //list of successors in the graph. Currently, these are unordered.
    //Also, currently, these have no good way to represent interprocedural
    private ArrayList<Edge> successors = new ArrayList<Edge>();
    /* dag is only really needed for few vertices (those with no path on one
     * thread) but must be passed around all the time.
     * For the first vertex, it is also used to get the number of threads */
    private UnwindingHyperDAG dag;

    private ArrayList<LinkedList<Unit>> threadPaths;
    private UnwindingVertex cover = null;
    private BoolExpr interpolant = null;
    //perhaps to WH's dislike, the above could have value true fairly reasonably
    //(the variable must have a default value in Java)

    //private ArrayList<Edge> interpolantPreds = new ArrayList<Edge>();

    public UnwindingVertex(UnwindingHyperDAG d) {
	if (d == null) {
	    throw new NullPointerException("initial DAG cannot be null");
	}
	dag = d;
	threadPaths = new ArrayList<LinkedList<Unit>>(dag.numThreads);
	for(int i = 0; i < dag.numThreads; i++) {
	    threadPaths.add(new LinkedList<Unit>());
	}
    }

    /* Add a constructor that does nothing because everything is (and can be) done
     * in onePredForThread */
    private UnwindingVertex() {
    }

    /* Overide the equals method in Object */
    /* This assumes that threads are always in the same order. */
    public boolean equals(Object o) {
	if(!(o instanceof UnwindingVertex)) {
	    return false;
	} else {
	    UnwindingVertex other = (UnwindingVertex) o;
	    return this.threadPaths.equals(other.threadPaths);
	}
    }

    /* override the hashCode method in Object */
    public int hashCode() {
	return 43 * threadPaths.hashCode();
    }

    /* The result parameter is really only used for output, of the nodes and
     * of the parent structure. */
    public boolean expand(ExceptionalUnitGraph cfg,
	    LinkedList<UnwindingVertex> result) {

	boolean atInitial = true;

	////System.out.println("threadPaths size: " + threadPaths.size());
	for(int i = 0; i < threadPaths.size(); i++) {
	    Unit head = threadPaths.get(i).peek();
	    if (head == null) {// if the path was empty
		head = dag.errorLocation();
	    }
	    List<Unit> preds = cfg.getUnexceptionalPredsOf(head);

	    for(Unit unit : preds) {
		// This assumes that initial nodes have no predecessors
		atInitial = false;

		////System.out.println("Making next for thread " + i);
		UnwindingVertex next = onePredForThread(unit, i);

		int is_in = result.indexOf(next);//This is the reason we use LinkedList as our queue

		//System.out.println("Contains: " + (is_in >= 0));
		//System.out.print("Self thread path lengths: [");
		//for(LinkedList<Unit> path : threadPaths) {
		    //System.out.print(path.size() + ",");
		//}
		//System.out.println("]");
		//System.out.print("next thread path lengths: [");
		//for(LinkedList<Unit> path : next.threadPaths) {
		    //System.out.print(path.size() + ", ");
		//}
		//System.out.println("]");

		Edge newEdge = new Edge(unit, this, i, false);
		if(is_in < 0) {
		    next.successors.add(newEdge);
		    result.add(next);
		} else {
		    result.get(is_in).successors.add(newEdge);
		}
	    }

	    preds = cfg.getExceptionalPredsOf(head);
	    for(Unit unit : preds) {
		// This assumes that initial nodes have no predecessors
		atInitial = false;

		//For this to definitely work, Soot must be run with
		// Options.v().set_omit_excepting_unit_edges(false);
		boolean add = unit instanceof ThrowStmt;
		if (!add) {
		    for(ExceptionDest d : cfg.getExceptionDests(unit)) {
			if(d.getTrap() != null) {
			    add = true;
			    break;
			}
		    }
		}

		if(add) {
		    //Same as above, except with edges marked as exceptional
		    UnwindingVertex next = onePredForThread(unit, i);

		    int is_in = result.indexOf(next);
		    Edge newEdge = new Edge(unit, this, i, true);
		    if(is_in < 0) {
			next.successors.add(newEdge);
			result.add(next);
		    } else {
			result.get(is_in).successors.add(newEdge);
		    }
		}
	    }
	}

	return atInitial;
    }

    /* Take a unit that precedes this vertex and the thread on which it precedes
     * and return an new vertex for that path. */
    private UnwindingVertex onePredForThread(Unit unit, int thread) {
	UnwindingVertex next = new UnwindingVertex();
	next.dag = this.dag;
	next.threadPaths = new ArrayList<LinkedList<Unit>>(threadPaths.size());

	for(int j = 0; j < threadPaths.size(); j++) {
	    // Might be inefficient -- could just set to same object
	    next.threadPaths.add(new LinkedList<Unit>(threadPaths.get(j)));
	}
	////System.out.println("next threadPath size: " + next.threadPaths.size());
	next.threadPaths.get(thread).push(unit);
	return next;
    }

    public List<Edge> getSuccessors() {
       return successors;
    }

    /* public boolean hasInterpolant() {
	return (interpolant == null);
    } */

    public BoolExpr getInterpolant() {
	return interpolant;
    }

    /* This should only be true if this is the error node */
    public boolean hasSuccesors() {
	return !successors.isEmpty();
    }

    public void disjoinInterpolant(Context con, BoolExpr other) {
	if (interpolant == null) {
	    interpolant = other;
	} else {
	    interpolant = con.mkOr(interpolant, other);
	}
    }

    public ArrayList<Integer> suffixLengths() {
	ArrayList<Integer> sizes = new ArrayList<Integer>(threadPaths.size());
	for(int i = 0; i < threadPaths.size(); i++) {
	    sizes.add(threadPaths.get(i).size());
	}
	return sizes;
    }

    public String toString() {
	return super.toString() + suffixLengths().toString();
    }

    public int getNumThreads() {
	return dag.numThreads;//could instead use threadPaths.size()
    }

    public Unit getHeadUnit(int thread) {
	Unit result = threadPaths.get(thread).peek();
	if (result == null) {
	    result = dag.errorLocation();
	}
	return result;
    }
}
